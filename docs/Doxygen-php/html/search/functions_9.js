var searchData=
[
  ['send_5femail_256',['send_email',['../class_user_1_1_user_mailer.html#a4266abaf363649c8938e1149f7ff2d3d',1,'User::UserMailer']]],
  ['send_5femail_5fchange_5fstep1_257',['send_email_change_step1',['../class_user_1_1_user_email_change.html#a49d20a2e753cb19cd851fd39dd4cbca0',1,'User::UserEmailChange']]],
  ['send_5femail_5fchange_5fstep2_258',['send_email_change_step2',['../class_user_1_1_user_email_change.html#a20ec796852e31936bab035360739a334',1,'User::UserEmailChange']]],
  ['send_5femail_5fverification_259',['send_email_verification',['../class_user_1_1_user_access.html#ad8c262881afe6ff59baf7c98368dea39',1,'User::UserAccess']]],
  ['send_5fpassword_5freset_260',['send_password_reset',['../class_user_1_1_user_access.html#a6aaa8aa0ccde931eba751a737e32b72e',1,'User::UserAccess']]],
  ['source_5fcreate_5fnew_261',['source_create_new',['../class_feed_1_1_feed_manager.html#a53e925964cb29299aa5d697c236dac73',1,'Feed::FeedManager']]],
  ['source_5fget_5fall_262',['source_get_all',['../class_feed_1_1_feed_manager.html#acd7b7a3931ba4cb7943a5dc5d5ec7e9d',1,'Feed::FeedManager']]],
  ['source_5fget_5fdetails_263',['source_get_details',['../class_feed_1_1_feed_manager.html#a9601d5092fcae2ee9b5d17d5ba49b658',1,'Feed::FeedManager']]],
  ['source_5fremove_5fand_5fpurge_5fposts_264',['source_remove_and_purge_posts',['../class_feed_1_1_feed_manager.html#a6d5368b0006ae35af1adb52cb47d717c',1,'Feed::FeedManager']]],
  ['source_5ftoggle_5ffavourite_265',['source_toggle_favourite',['../class_user_1_1_source_fav.html#a78884bd7a9d5db02160b8bcf9d31e25e',1,'User::SourceFav']]],
  ['source_5fupdate_5fname_266',['source_update_name',['../class_feed_1_1_feed_manager.html#abcd11efb85496384c631db6e23c625d5',1,'Feed::FeedManager']]],
  ['submit_5fcomment_267',['submit_comment',['../class_post_1_1_post_details.html#a7465e5778d401ca3ca8c38bb22153a25',1,'Post::PostDetails']]]
];
