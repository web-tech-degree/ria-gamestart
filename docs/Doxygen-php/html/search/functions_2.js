var searchData=
[
  ['db_5fget_5fuser_5fdetails_5fby_5femail_209',['db_get_user_details_by_email',['../class_user_1_1_user_access.html#a21b9bc6e9fccfc1aee2d12c020bb139b',1,'User::UserAccess']]],
  ['db_5fget_5fuser_5fpassword_5fby_5fusername_210',['db_get_user_password_by_username',['../class_user_1_1_user_access.html#abe678a0ace63c2a0e3f63ac93c6ce06e',1,'User::UserAccess']]],
  ['db_5fhash_5fand_5finsert_5fnew_5f2fa_5ftoken_211',['db_hash_and_insert_new_2fa_token',['../class_user_1_1_user_access.html#a3da6cdff298a973bee978b8d7136a190',1,'User::UserAccess']]],
  ['db_5fhash_5fand_5finsert_5fnew_5femailverify_5ftoken_212',['db_hash_and_insert_new_emailverify_token',['../class_user_1_1_user_access.html#a6a4aa2ee59061cece595d41fe23a9f6f',1,'User::UserAccess']]],
  ['db_5fremove_5fexisting_5femailverify_5ftokens_213',['db_remove_existing_emailverify_tokens',['../class_user_1_1_user_access.html#a04fc4a5c0d29311d84d51233e3e3066c',1,'User::UserAccess']]],
  ['delete_5fcomment_214',['delete_comment',['../class_post_1_1_post_details.html#ad572e9765cbcf4b132b98abb80a2cb86',1,'Post::PostDetails']]]
];
