# Table of Contents
- 1: Repository Information
- 2: Normal Configuration
- 3: Development
- 4: Guidelines
- 5: Building
- 6: Testing
- ZZ: Project Creation

# 1 Repository Info
`master` branch is pulled to from `development` for stable versions; direct pushing to `master` is unavailable.



## 1.1 Members
- Adam: @addzyy
- Adrian: Airi - @adi73434
- Luke: @Astolfis
- Sam: @SamB889
- Michael: @Mfisk91 and @Mfisk1991



## 1.2 TypeDoc Documentation
The emitted documentation from TypeDoc for the TypeScript/JavaScript files is available here:
- `./ria-gamestart/docs/typedoc/`

and it is hosted here:
- https://gamestart.aterlux.com/typedoc/index.html



## 1.3 Doxygen Documentation
The emitted documentation from Doxygen for the PHP files is available here:
- `./ria-gamestart/docs/Doxygen-php/`

and it is hosted here, but the second link is where the useful stuff is:
- https://gamestart.aterlux.com/Doxygen-php/html/index.html
- https://gamestart.aterlux.com/Doxygen-php/html/namespaces.html



## 1.4 Lines of Code
This is just for fun, but if you want to see the lines of code you can do that here:
- `./ria-gamestart/gameinc-g/.VSCodeCounter/`

just make sure you have a Markdown viewer installed. Also, if you are viewing this after cloning the repository, the links to the files should work.

On the other hand, to see the hosted versions (after outputting as HTML), you can do that here:
- https://gamestart.aterlux.com/loc/index.html

but note that the links to the code files will not work.



# 2 Prerequisite/Configuration
These steps are essentially what you need to do to set up the development environment for this reposotiry.

**Note:** These are not the project creation steps; those are, essentially, already done, so you just need to download the tools and the repository.



## 2.1 VSCode
Get here: https://code.visualstudio.com



### 2.1.1 VSCode Extensions
- `shd101wyy.markdown-preview-enhanced`
- `stylelint.vscode-stylelint`
- `dbaeumer.vscode-eslint`
- `dinhani.divider`
- `exodiusstudios.comment-anchors`
- `eamodio.gitlens`
- `alefragnani.bookmarks`
- `oouo-diogo-perdigao.docthis`
- `zignd.html-css-class-completion`



Also look for `Live Share` and install the stuff for that



### 2.1.2 Adrian's VSCode config
If you download this extension `shan.code-settings-sync`

Then you can upload/download settings using GitHub's Gists.

Here's the Gist for the settings I uploaded - please don't overwrite it:
- `95e5cebe0d60994e590a25a8b20efed9`


To configure:

- Hit `F1` then go to `Sync: Advanced Options` > `Sync: Download Settings from Public GIST`.
- Then **either**:
- - **Hit** `F1` then go to `Sync: Advanced Options` > `Sync: Open Settings`, you should be greeted with a GUI and be able to enter the gist in there <br> ![](./readme-assets/settings-sync.png)
- - **Or** hit `F1` then go to `Preferences: Open Settings (JSON)`, and find a setting called `sync.gist` _or_, if you can't find it, make one. The settings in JSON format are also pasted below. <br> ![](./readme-assets/user-settings-json.png)


Here's the settings I think would be most appropriate, alongside the actual gist id

```json
"sync.gist": "95e5cebe0d60994e590a25a8b20efed9",
"sync.forceUpload": false,
"sync.removeExtensions": false,
```

**Note** that this will overwrite your settings and will install, as of time of writing, 73 extensions, and I think it also installs themes so there'll be a bunch of those too. I think it also removes your installed extensions by default if they aren't in the Gist, so I think you can stop that by disabling `Remove Extensions`.

Your menu bar will also no longer display all the options, so you'll have to toggle that by pressing `Alt`. You can also just comment out this line in the settings if you don't like that:
```json
"window.menuBarVisibility": "toggle",
```



## 2.2 npm
Find here https://www.npmjs.com/get-npm and install



### 2.2.1 node_modules installation
Yes, the node_modules is huge.

GitLab doesn't like that.

Just go into folder `./ria-gamestart/gameinc-g/` and run ~~`> npm install`~~ `> npm ci`.

**Note:** the `> npm install` command was incorrect and would, as far as I'm aware, check what's in package.json but install newer versions than those specified in package-lock.json. This would make changes to package-lock.json; meanwhile, if you run `> npm ci`, it'll install everything ***exactly as specified***, which is the correct way.

This will generate a `node_modules` folder, which is basically a duplicate of what the `Project Creation` steps create.



## 2.3 PHP Composer
Find here https://getcomposer.org/download/ and install

You can look at the basic usage [here](https://getcomposer.org/doc/01-basic-usage.md)

Basically, this is like npm but for PHP packages and libraries.

This technically may not be required for everyone as it's for PHP stuff, but I'm putting this here anyway. However, unlike npm, this ***is*** required on the server, and that is discussed in the deployment section.



### 2.3.1 PHP Composer dependency installation
Again, same thing as 2.2.1 where we're not redistributing the packages on the GitLab repository, and instead using a file that tells composer which versions we use.

Just go into folder `./ria-gamestart/gameinc-g/` and run `> composer install`.



## 2.4 Local Backend Server
This is really probably a disaster to do this, but basically because when you run `> npm start`, it only runs a server for React, and not PHP or SQL -- and we need a PHP and SQL database to connect to.


Now, you *could* run `> npm run build` instead and copy the `build` folder into the htdocs directory in XAMPP (or use a Symbolic Link), but the compilation takes far too long.

Therefore, as the React app -- while running as a development server with `> npm start` -- runs on http://localhost:3000/, as in on **port 3000**, you can just run XAMPP and keep it on **port 80**, and host only the PHP content in there.

Then, in `./ria-gamestart/gameinc-g/src/.env` you can see a variable named `REACT_APP_API_URL` which basically points to `http://localhost:80/`. This is just so that we can use it in `fetch()` when we call the server, and it's done like this so we can change the port any time for everything. Essentially, this allows us to fetch resources from PHP/SQL running under XAMPP while in `npm start`'s development server.

Also, as the PHP is located in `./ria-gamestart/gameinc-g/public/php/`, you need to copy that into the XAMPP folder. However, since copying over the files manually or editing them outside of the repository would be annoying, you can **just create a Symbolic Link** from the `./ria-gaemstart/gameinc-g/public/php` folder to `xampp/htdocs/php`, or whatever your XAMPP install is. See images below.

This is done with the [Link Shell Extension](https://schinagl.priv.at/nt/hardlinkshellext/linkshellextension.html) program.

Here, we pick link source.
![](./readme-assets/php-symlink-1.png)

Then we drop it as a symbolic link
![](./readme-assets/php-symlink-2.png)

The result is a "mirror" of the `./ria-gamestart/gameinc-g/public/php/` folder in XAMPP's htdocs, and these are basically the same thing. It's like a shortcut but more elaborate -- you can edit the contents in the right, and it'll update on the left, and vice versa, but it's _only stored where the actual source directory is_.
![](./readme-assets/php-symlink-3.png)



## 2.5 Important configuration for SMTP and other stuff
### This is only applicable if you are developing locally instead of with Live Share.

Because I'm not going to share my gmail password on the Git code, you'll have to create/configure the following file in the directory:
- `./ria-gamestart/gameinc-g/public/php/conf_general.php`

You should consult the following file for the template, or the codeblock below:
- `./ria-gamestart/gameinc-g/public/php/conf_general_template.php`


```php
<?php

namespace Conf;

/**
 * @brief This is used to set parameters for SMTP connections, and some other stuff.
 * Note that this file is blank in the repository, so you will have to create it yourself.
 */
class General
{
	public $smtp_username = "email@domain.com";
	public $smtp_password = "password";
	public $server_url = "https://gamestart.aterlux.com";
	public $server_api_url = "https://gamestart.aterlux.com/php";
}
```

**NOTE:** If you are using Gmail, you will have to create an "App Password" as shown [here](https://support.google.com/accounts/answer/185833?hl=en-GB)


<br><br>
<br><br>
<br><br>



# 3 Developing
**Either**:
- **Open** terminal in the `./ria-gamestart/gameinc-g/` folder and run `> npm start`.
- **Or** open terminal in VSCode, cd into `gameinc-g`, and run `> npm start`.

Note that if your VSCode workspace root is `./ria-gamestart/gameinc-g/` instead of `./ria-gamestart/` then you can just run `> npm start` as the terminal will already be located in the subfolder with the dev stuff.

ESLint and Stylelint should work automatically and appear in the `Problems` tab next to `Terminal`.

Host your backend with XAMPP, as shown in 2.4. Once you've done all the stuff in 2.4, all you need to do is run the XAMPP server. You will **not** need to run `> npm run build` and will be able to use `> npm start` with database connectivity.

**[Click here to see kinda how that works](https://www.youtube.com/watch?v=Vd5cT7HZWGs)**

We can also develop on the same code at the same time using VSCode's `Live Share` stuff. It's kinda like collaborating on the same file in Google Drive, but it's for the whole project. Also. with that, I can run `> npm start` and everyone connected to the session can also go to `http://localhost:3000` to view the website. It will also recompile automatically when anyone else saves, so it could make the development experience with multiple people quite pleasant.



## 3.1 Creating TypeDoc documentation
This is essentially JSDoc, but I found it worked better.

**The output of the documentation is in `./ria-gamestart/docs/typedoc/`.**

Assuming you have the VSCode extensions synced, or installed `oouo-diogo-perdigao.docthis` manually, you can auto-create a TypeDoc comment for a class/function etc. You can also manually create a TypeDoc comment by typing `/**`, from where you _also_ get the option to use the Document This thing.

![](./readme-assets/jsdoc.png)

<br>

You can then create the documentation files using the following command (and as pictured), which uses the `typedoc.json` config.
- `> npx typedoc`


![](./readme-assets/npx-typedoc.png)



## 3.2 Creating DoxyGen PHP documentation
Download [Doxygen](https://www.doxygen.nl/download.html)

**The output of the documentation is in `./ria-gamestart/docs/Doxygen-php/html/`.** 

_Just Open the index.html file in there_



Also, here's a Doxygen output for a C++ project

![](./readme-assets/doxygen-ncode-output.png)

You'll be able to tell that the `Main Page` tab in _our_ `index.html` file is rather empty, which I assume is because the PHP itself isn't a program, just some scripts. Basically, just open the `Namespaces` or `Files` tabs.

- Left: `Namespaces` tab containing classes/namespaces
- Right: `Namespaces` > `AuthUser` documented class

![](./readme-assets/doxygen-namespaces-output.png)

- Left: `Files` > `File List` tab
- Right: `Files` > `Globals` tab

![](./readme-assets/doxygen-files-and-globals-output.png)



### 3.2.1 Generating docs with CLI

If you [add Doxygen to your PATH variable](https://YOUTUBEVIDEOHERE), as shown below

![](./readme-assets/doxygen-path.png)

you can use the following command when you are in `./ria-gamestart/gameinc-g/` folder, as shown, which uses the doxyfile-php config:
- `doxygen doxyfile-php` 

![](./readme-assets/doxygen-command.png)



### 3.2.2 Generating docs with GUI
Or you can use the GUI by loading the config file into `doxywizard.exe`.

![](./readme-assets/doxygen-1.png)
![](./readme-assets/doxygen-2.png)



## 3.3 Extras
You may lint PHP files with the following command:
- `> phpcbf public/php/__api.php`

where `public/php/__api.php` is a relative path to a file (or directory), such as that one.

However, since probably only I will be doing PHP, and I try to write code that passes the linter from the get-go, this shouldn't be necessary.



<br><br>
<br><br>
<br><br>



# 4 Guidelines
Considering the ECMAScript compatibility:
- https://kangax.github.io/compat-table/es6/
- https://kangax.github.io/compat-table/es2016plus/
- https://kangax.github.io/compat-table/esnext/

I think we'll just use ES6.

Workspace VSCode config is in
- `./ria-gamestart/gameinc-g/.vscode/`

ESLint config is in:
- `./ria-gamestart/gameinc-g/.eslintrc.js`

ESLint global config, which the .eslintrc.js file references, is in:
- `./ria-gamestart/gameinc-g/.eslint-global-config.js`

TypeScript config is in:
- `./ria-gamestart/gameinc-g/tsconfig.json`

Stylelint config is in:
- `./ria-gamestart/gameinc-g/.stylelintrc.json`



<br><br>
<br><br>
<br><br>



# 5 Building and Deployment
You can build the app with:
- `> npm run build`

Files in `./ria-gamestart/gameinc-g/public` also get copied to the build output

Then the `./ria-gamestart/gameinc-g/build` folder is what we put on a webserver to host.



## 5.1 Live Server Details
These are available in MS Teams; for security reasons, as this repository is public, I'd rather not add them here.

URL: https://gamestart.aterlux.com/



## 5.2 Environment File (.env)
The location of this file is:
- `./ria-gamestart/gameinc-g/.env`

Essentially, this file specifies some "stuff", where "stuff" is [anything we want, whatever we imagine](https://create-react-app.dev/docs/adding-custom-environment-variables/)

The variables there can be called practically whatever we like, but custom ones [need to be prefixed with `REACT_APP`](https://create-react-app.dev/docs/adding-custom-environment-variables/); I think the `PUBLIC_URL` is an "internal" thing and therefore doesn't need it.


> Note: You must create custom environment variables beginning with REACT_APP_. Any other variables except NODE_ENV will be ignored to avoid accidentally exposing a private key on the machine that could have the same name. Changing any environment variables will require you to restart the development server if it is running.


One such thing is, as an example, `PUBLIC_URL=https://localhost/build`, which sets all `%PUBLIC_URL%` fields in `./ria-gamestart/gameinc-g/public/index.html` to that. This allows the root of the project to be whatever you want when you run `npm run build`, but it does not impact the dev server (`npm start`).

The example output is as shown:
![](./readme-assets/build-env-public_url.png)



## 5.3 Composer PHP
You need to install composer on the server, then on the server run:
- `> composer.phar install --no-dev`

Note that the composer is installed _within_ the php folder on the production server.

Hence, the `vender/autoload.php` directory is different on production vs the local server, as shown below:

![](./readme-assets/prod-vs-local-vendor-autoload.png)

Also, as you can see, you should not allow Cross-Origin requests; that is only there for local server stuff to allow different ports.



## 5.4 PHP Differences
While the TypeScript and front-end stuff can relatively easily be switched to the production, the PHP bits are a little bit more annoying.

Basically, you'll have to change the top of the `__api.php` and `__api_ext.php` files according to the comments, which is also shown above.



## 5.5 SQL
Because the PHP files self-define their timezone to "Europe/London", you should set the MariaDB timezone to that. Alternatively, you could just comment out `date_default_timezone_set` within `_dbconnect.php`, which would use your default PHP timezone... which hopefully should already match your MariaDB/MySQL timezome.

Also, you need to change the database info in `_dbconnect.php`.



## 5.6 Fetch Requests
Because the development server was using CORS, the credentials were omitted in every fetch() request in JavaScript. When deploying, you should find all occurences of `credentials: "omit",` and replace them with `credentials: "include",` in the TypeScript files.

You can do this via the `Search` tab in VSCode (You can also access this by hitting `F1` then typing `> Search: Find in Files`), which has a nifty `Replace All` feature.



<br><br>
<br><br>
<br><br>



# 6 Testing
This section includes some testing bits, which are mostly just links to files and pages.

Ideally, these will be numbered and dated, and all versions will be kept to show a log of progress, though this is mostly just a note for "future maintainers".

For HAR files, I think you can open them with [this](https://toolbox.googleapps.com/apps/har_analyzer/)



## 6.1 [OWASP ZAP](https://www.zaproxy.org/) Security Test
The output of these is available here:
- `./ria-gamestart/docs/owasp-zap/`


based on `2021-04-25-test1.html`, it doesn't seem to show anything too alarming.



## 6.2 [PageSpeed Insights](https://developers.google.com/speed/pagespeed/insights/)
The output of these is available here:
- `./ria-gamestart/docs/pagespeed-insights/`


These do not provide an export option, hence just use links or screenshots



## 6.3 [Pingdom](https://www.pingdom.com/)
The output of these is available here (HAR format + link):
- `./ria-gamestart/docs/pingdom/`



## 6.4 [WebPageTest](https://www.webpagetest.org/)
The output of these is available here (HAR format + link):
- `./ria-gamestart/docs/webpagetest/`



<br><br>
<br><br>
<br><br>



# ZZ Project Creation
This is something that's already done; you don't need to care about this for simply working on this project as everything will be installed locally in section 2.2.1.

The `--save-dev` in npm commands saves the package in package-lock.json so `> npm install` will re-install them, though I think since some version of npm, the `--save-dev` parameter is default, so whatever.

***However, don't use `> npm install` after pulling from the repository; use `> npm ci` instead so that the `package-lock.json` file isn't modified. Unless you're adding packages, in which case use install.***



<br><br>



## ZZ.1 React
https://reactjs.org/docs/getting-started.html
https://reactjs.org/docs/create-a-new-react-app.html



### ZZ.1.1 React Setup
Make sure you're in directory
`./ria-gamestart/gameinc-g/`


Install with:
- `> npm install --save react react-dom typescript @types/react @types/react-dom`

Create a project with:
- `> npx create-react-app gameinc-g --template typescript`



<br><br>



## ZZ.2 TypeScript
https://www.typescriptlang.org/
https://webpack.js.org/guides/typescript/



### ZZ.2.1 TypeScript Setup
Make sure you're in directory
- `./ria-gamestart/gameinc-g`


Install with:
- `> npm install --save-dev typescript ts-loader`



<br><br>



## ZZ.3 ESLint with TypeScript and React
This'll be used for linting ~~JavaScript~~ TypeScript and fixing issues.

The way it is configured, .js/.jsx files will be linted as if they are normal JavaScript files - ignoring all of the TypeScript stuff - whereas .ts/.tsx files will use all the normal rules alongside the TypeScript stuff.


The ESLint config that ESLint _uses_ is in:
- `./ria-gamestart/gameinc-g/.eslintrc.js`

but the ESLint config itself also references this file for the rules:
- `./ria-gamestart/gameinc-g/.eslint-global-config.js`


TypeScript config is in:
- `./ria-gamestart/gameinc-g/tsconfig.json`



### ZZ.3.1 ESLint setup for React/TypeScript project

Make sure you're in directory:
- `./ria-gamestart/gameinc-g/`

Initially, I installed this:
- `> npm install --save-dev eslint eslint-config-google typescript @typescript-eslint/eslint-plugin eslint-plugin-react @typescript-eslint/parser`


But I also ran the configuration as shown here

![](./readme-assets/eslint-configuration.png)



<br><br>



## ZZ.4 Stylelint
This'll be used for re-ordering CSS into a specific order just so everything is a bit cleaner.


Stylelint config is in:
- `./ria-gamestart/gameinc-g/.stylelintrc.json`



### ZZ.5.1 Stylelint Setup
Make sure you're in directory:
- `./ria-gamestart/gameinc-g/`

Install with:
- `> npm install --save-dev stylelint stylelint-config-standard stylelint-config-idiomatic-order`



## ZZ.6 JSDoc and TypeDoc
This'll be used in VSCode for showing richer documentation with IntelliSense, and for creating a website with documentation.

I'm not really sure if JSDoc itself is needed as TypeDoc will be used to actually generate the documentation, but I installed it anyway.



### ZZ.6.1 JSDoc and TypeDoc Setup
Install with:
- `> npm install --save-dev jsdoc`
- `> npm install --save-dev typedoc`

Theme install:
- `> npm install typedoc-neo-theme`

See the TypeDoc config in `./ria-gamestart/gameinc-g/typedoc.json`



## ZZ.7 PHP Composer
This'll be used for downloading PHP libraries such as [JWT](https://github.com/firebase/php-jwt).

Install with:
- `> php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"`
- `> php composer-setup.php`



### ZZ.7.1 PHP Composer Setup
Download composer from here: https://getcomposer.org/download/



## ZZ8 Doxygen for PHP
This will be used for generating documentation for PHP. Rather than comparing _this_ to JSDoc/TypDoc, rather, JSDoc and TypeDoc should be compared to Doxygen. It's what I use for C++ and it works for "C-style" languages, and you can [document JavaScript with Doxygen](https://coherent-labs.com/posts/documenting-javascript-with-doxygen/), but [we'll be using TypeDoc for TypeScript](https://blog.cloudflare.com/generating-documentation-for-typescript-projects/)

The Doxygen config file in `./ria-gamestart/gameinc-g/doxyfile-php` uses the following folders, which are relative from the `doxyfile-php` file:
- `public/php/` for source file input
- `../docs/Doxygen-php/` for documentation output

I'm also using a [custom dark theme](https://github.com/MaJerle/doxygen-dark-theme) for this, even though it's imperfect; I'm just shrewd like that.



## ZZ9 Additional Libraries



### ZZ9.1 Frontend
<!-- - `> npm install bootstrap@next` -->
- `> npm install bootstrap@4`
- `> npm install react-bootstrap bootstrap`
- `> npm install jwt-decode`
- `> npm install @shopify/draggable@1.0.0-beta.11`
- `> npm install react-notifications`
- `> npm install three`
- `> npm install @types/three`
- `<script src="%PUBLIC_URL%/include/three.min.js"></script>` [Download here](https://threejs.org/)



### ZZ9.3 Backend
- `> composer require firebase/php-jwt`
- `> composer require phpmailer/phpmailer`
- `> composer require p3k/picofeed`
