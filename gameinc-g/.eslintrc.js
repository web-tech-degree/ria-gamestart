module.exports = {

	// Environment
	// https://kangax.github.io/compat-table/es6/
	// https://kangax.github.io/compat-table/es2016plus/
	// https://kangax.github.io/compat-table/esnext/
	"env": {
		"es6": true,
		// "es2021": true,
		"browser": true,
	},

	// Default standards
	"extends": [
		"plugin:react/recommended",
		"google",
		// This basically imports the global config, but
		// we need to import it here for JavaScript stuff
		// and then the "overrides" has to re-import it for
		// itself
		"./.eslint-global-config.js",
	],

	"parserOptions": {
		"ecmaFeatures": {
			"jsx": true,
		},
		// 'ecmaVersion': 12,
		"ecmaVersion": 2018,
		"sourceType": "module",
		// "tsconfigRootDir": __dirname,
		// "project": "tsconfig.json",
	},

	"plugins": [
		"react",
	],

	// Global variables so that we don't get screamed at by ESLint
	"globals": {
		// I forgot what Atomics are for, but I'm leaving this here
		// "Atomics": "readonly",
		// Define any custom variables
	},


	// -------------------------------------------------------------------------
	// SECTION TypeScript specific
	// TypeScript specific stuff, done like this so that we can have JavaScript
	// files without all the TypeScript ESLint stuff in case anyone doesn't want
	// to use TypeScript.
	// -------------------------------------------------------------------------
	"overrides": [
		{
			// Target only TypeScript files
			"files": [
				"**/*.ts",
				"**/*.tsx",
			],
			"extends": [
				"plugin:react/recommended",
				"plugin:@typescript-eslint/eslint-recommended",
				"plugin:@typescript-eslint/recommended",
				"google",
				// This basically imports the global config, but
				// we use a different parser for TypeScript files
				"./.eslint-global-config.js",
			],
			"parser": "@typescript-eslint/parser",
			"parserOptions": {
				// "project": "./tsconfig.json",
			},
			"plugins": [
				"react", "@typescript-eslint",
			],
			"rules": {
				// Allow the use of ts-ignore for one liners
				// "@typescript-eslint/ban-ts-ignore": "off",
			},
		},
	],
	// -------------------------------------------------------------------------
	// !SECTION TypeScript specific
	// -------------------------------------------------------------------------
};
