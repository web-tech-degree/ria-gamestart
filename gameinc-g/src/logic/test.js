
/**
 * This function takes a string and checks if its length is longer than 5.
 *
 * @param {string} varName
 * @return {boolean}
 */
const isStringLongerThan5 = (varName) => {
	console.log("exampleBehaviour" + varName);

	if (varName.length > 5) {
		return true;
	}
	else {
		return false;
	}
};

export {
	isStringLongerThan5,
};
