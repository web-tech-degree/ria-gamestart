import {postUserComment} from "../components/FeedHub/types";

// -----------------------------------------------------------------------------
// SECTION Types
// -----------------------------------------------------------------------------
type authTokenDataFormat = {
	id: string,
	username: string,
	email: string,
	isEmailVerified: string,
	userLevel: string,
}

// This is how the JWT authentication token shall be formatted
// when it is returned from the API. If it's not formatted like this,
// something went horribly wrong
// NOTE: I'm pretty sure order doesn't matter, but I just wrote this how it
// appeared in console.log
type authTokenFormat = {
	alg: string,
	aud: string,
	data: authTokenDataFormat,
	exp: number,
	iat: number,
	iss: string,
	nbf: number,
}

type verifyAuthTokenRet = {
	message: string,
}
// -----------------------------------------------------------------------------
// !SECTION
// -----------------------------------------------------------------------------





const globals = {
	// This specifies, in milliseconds, how often the verification token shall be
	// rechecked
	// NOTE: I'd rather have used this with process.env (.env), but TypeScript complains that
	// it may be undefined and that it's a string or whatever
	TOKEN_REFRESH_RATE: 6000,
	// In ms, how often we'll check against the server for new bookmarks
	FETCH_BOOKMARK_AVAILABILITY_RATE: 1000,
	// In ms, how often we'll try to fetch new posts if any are made available
	FETCH_BOOKMARKED_POSTS_RATE: 1000,
	// In ms, how often the bookmark feed rendere actually checks the localStorage and if new
	// posts were fetched
	BOOKMARK_RENDERER_REFRESH_RATE: 1000,
	// Past this many bookmarks, we'll warn the user
	BOOKMARK_HIGH_COUNT_WARNING: 10,
	DEBUGGER_DBG_LOG: "true",
	DEBUGGER_DBG_App: "false",
	DEBUGGER_DBG_AdminHub: "false",
	DEBUGGER_DBG_FeedEditor: "false",
	DEBUGGER_DBG_SourceEditor: "false",
	DEBUGGER_DBG_FeedHub: "false",
	DEBUGGER_DBG_Feed: "false",
	DEBUGGER_DBG_FeedFavourites: "false",
	DEBUGGER_DBG_Item: "false",
	DEBUGGER_DBG_ItemLightbox: "false",
	DEBUGGER_DBG_LeftNav: "false",
	DEBUGGER_DBG_BotNav: "false",
	DEBUGGER_DBG_LoginLanding: "false",
	DEBUGGER_DBG_Register: "false",
	DEBUGGER_DBG_Login: "false",
	DEBUGGER_DBG_ForgotPassword: "false",
	DEBUGGER_DBG_ReconfirmEmail: "false",
	DEBUGGER_DBG_Profile: "false",
};





// -----------------------------------------------------------------------------
// SECTION Global dbgLog
// -----------------------------------------------------------------------------
// NOTE: This is not actually in normal use as dbgLog is implemented per-component
// so that we can toggle each component's debugger using the .env file.

// These types are copied from the types from console.log.bind(window.close)
let globalDebugLog: (
	// eslint-disable-next-line
	message?:any,
	// eslint-disable-next-line
	...optionalParams: any[]
) => void;

// If debugging mode is set to true, set globalDebugLog to actually log stuff to console
if (globals.DEBUGGER_DBG_LOG === "true") {
	globalDebugLog = console.log.bind(window.close);
}
else {
	// Otherwise, we want to keep the console clean
	globalDebugLog = (): void => {
		return;
	};
}
// -----------------------------------------------------------------------------
// !SECTION
// -----------------------------------------------------------------------------





/**
 * @see https://stackoverflow.com/a/5306832
 *
 * @param {any[]} arr
 * @param {number} moveFrom
 * @param {number} moveTo
 * @return {*}  {any[]}
 */
const moveArray = (
	// eslint-disable-next-line
	arr: any[],
	moveFrom: number,
	// eslint-disable-next-line
	moveTo: number): any[] => {
	while (moveFrom < 0) {
		moveFrom += arr.length;
	}
	while (moveTo < 0) {
		moveTo += arr.length;
	}
	if (moveTo >= arr.length) {
		let k = moveTo - arr.length + 1;
		while (k--) {
			arr.push(undefined);
		}
	}
	arr.splice(moveTo, 0, arr.splice(moveFrom, 1)[0]);
	return arr;
};





/**
 * This copies the provided text to clipboard.
 *
 * See: https://stackoverflow.com/a/30810322
 *
 * @param {string} text
 * @return {*}  {boolean}
 */
const copyTextToClipboard = (text: string): boolean => {
	if (!navigator.clipboard) {
		return false;
	}
	navigator.clipboard.writeText(text).then(function() {
		console.log("Async: Copying to clipboard was successful!");
	}, function(err) {
		console.error("Async: Could not copy text: ", err);
		return false;
	});
	return true;
};





/**
 * This checks if the input name contains any of the input file strings.
 *
 * See: https://stackoverflow.com/a/17355937
 *
 * @param {string} fileName
 * @param {string[]} exts
 * @return {*}  {boolean}
 */
const checkFileHasExtension = (fileName: string, exts: string[]): boolean => {
	return (new RegExp("(" + exts.join("|").replace(/\./g, "\\.") + ")$", "i")).test(fileName);
};


type getImageResolutionCallback = (width: number, height: number) => void;

/**
 * This calls the provided callback function and gives it the width and height properties.
 * If, by this point in the code, you have not yet yelled expletives into the abyss, you probably
 * will start. And it'll be a consistent pattern, so beware for the abyss that is this codebase does stare back.
 *
 * Oh, yeah, also: you need to check that the file exists before calling this function, and then you should probably
 * verify that the image resolution was actually fetched properly.
 *
 * @param {File} fi
 * @param {getImageResolutionCallback} cb
 */
const getImageResolution = (fi: File, cb: getImageResolutionCallback): void => {
	if (fi !== null) {
		const img = new Image();
		img.src = window.URL.createObjectURL(fi);
		img.onload = () => {
			cb(img.naturalWidth, img.naturalHeight);
		};
	}
};





/**
 * See: https://stackoverflow.com/a/1199420
 *
 * @param {string} str
 * @param {number} n
 * @return {*}  {string}
 */
const stringEllipsisTruncate = (str: string, n: number): string =>{
	return (str.length > n) ? str.substr(0, n-1) + "..." : str;
};





/**
 * Because when we are JSON encoding the output from PHP, we use JSON_NUMERIC_CHECK, replies with ONLY number
 * content will also be converted to a number because JSON is stupid like that. So to "fix" that, all replies
 * simply have a prefix "ihatejson_" so they absolutely *will* be a string, and here we just need to remove that
 * prefix.
 *
 * @usage Pass a newArray for the array you want this function to push to.
 *
 * @param {postUserComment[]} newArray
 * @param {postUserComment[]} arr
 */
const sliceReplyPrefixFromPostUserCommentThread = (newArray: postUserComment[], arr: postUserComment[]): void => {
	for (const k in arr) {
	// Don't loop over prototypes
		if (arr.hasOwnProperty(k) ) {
			// If the array element is not null, aka it's a thing
			if (arr[k] !== null) {
				// We add the comment (with sliced text, cause it starts with "ihatejson_")
				if (arr[k].text !== null) {
					if (arr[k].text !== undefined) {
						arr[k].text = arr[k].text.slice(10);
						newArray.push(arr[k]);
					}
				}
				// We recurse over the loop again if we have replies on the object
				if (typeof arr[k] === "object" && arr[k].replies !== null) {
					sliceReplyPrefixFromPostUserCommentThread(newArray, arr[k] as unknown as postUserComment[]);
				}
			}
		}
	}
};



export {
	globals,
	globalDebugLog,
	moveArray,
	copyTextToClipboard,
	checkFileHasExtension,
	getImageResolution,
	stringEllipsisTruncate,
	sliceReplyPrefixFromPostUserCommentThread,
};

export type {
	verifyAuthTokenRet,
	authTokenFormat,
};
