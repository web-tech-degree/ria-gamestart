import React, {Component, Fragment} from "react";
import "./AdminHub.css";
import {globals} from "../../logic/globals";





// -----------------------------------------------------------------------------
// Types
import type {sourceItem, sourceItemWithDetails, selectedSourceDetailsData} from "./types";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Modes
import {modesAdminHub, modesAdminHubSources, modesSourceEditorGrid2} from "./modes";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Components
import PopUp from "../PopUp/PopUp";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Libraries
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Assets
// -----------------------------------------------------------------------------





interface SourceEditorState {
	mode: modesAdminHubSources,
	modeGrid2: modesSourceEditorGrid2,
	// This'll store all of the sources in grid1
	sourcesAll: Array<sourceItem>,
	// This'll store which source is selected
	selectedSourcePosInSourcesAll: number,
	// This'll store the selected source's details for grid2
	sourceSelectedDetails: sourceItemWithDetails,
	sourceUrlToAdd: string,
	sourceNameToAdd: string,
	showDeletePopup: boolean,
	updatedSourceName: string,
}

type SourceEditorProps = {
	setMode: (mode: modesAdminHub) => void,
	authToken: string,
}





/**
 * This is the grid that is displayed when the Admin is in the mode of editing "sources",
 * whether it be adding new RSS sources, adding feed-source links, or removing feed-source links.
 *
 * @class SourceEditor
 * @extends {Component}
 */
class SourceEditor extends Component<SourceEditorProps, SourceEditorState> {
	/**
	 * Creates an instance of SourceEditor.
	 * @param {SourceEditorProps} props
	 * @memberof SourceEditor
	 */
	constructor(props: SourceEditorProps) {
		super(props);
		this.state = {
			mode: modesAdminHubSources.placeholder,
			modeGrid2: modesSourceEditorGrid2.DoNotRender,
			sourcesAll: [],
			selectedSourcePosInSourcesAll: 0,
			sourceSelectedDetails: {source_id: 0, name: "", url: "", num_of_posts: 0},
			sourceUrlToAdd: "",
			sourceNameToAdd: "",
			showDeletePopup: false,
			updatedSourceName: "",
		};

		this.dbgLog();

		this.renderGrid1 = this.renderGrid1.bind(this);
		this.renderGrid2 = this.renderGrid2.bind(this);
		this.addNewSource = this.addNewSource.bind(this);
		this.updateSourceName = this.updateSourceName.bind(this);
	}



	/**
	 * This checks if this component should be "debugged" via the process.env variables. If it's true,
	 * we bind console.log so that logging works normally, otherwise we make it not do anything.
	 *
	 * @param {*} [mode]
	 * @param {...any[]} optionalParams
	 * @memberof SourceEditor
	 */
	dbgLog(
		// eslint-disable-next-line
		mode?: any,...optionalParams: any[]
	): void {
		if (globals.DEBUGGER_DBG_LOG === "true" && globals.DEBUGGER_DBG_SourceEditor === "true") {
			this.dbgLog = console.log.bind(window.close);
		}
		else {
			this.dbgLog = (): void => {
				return;
			};
		}
	}



	/**
	 * We want to get all sources when mounted
	 *
	 * @memberof SourceEditor
	 */
	componentDidMount(): void {
		// -------------------------------------------------------------------------
		// Check user is admin
		// -------------------------------------------------------------------------
		this.getAllSources();
	}



	/**
	 * This gets all available feeds' names
	 *
	 * @memberof SourceEditor
	 */
	getAllSources(): void {
		fetch(process.env.REACT_APP_API_URL + "__api.php", {
			method: "POST",
			credentials: "omit",
			body: JSON.stringify({
				request_type: "source_get_all",
				user_auth_token: this.props.authToken,
			}),
		})
			.then((response) => response.json())
			.then((data) => {
				this.dbgLog("SourceEditor: getAllSources response");
				if (data.message === "fetch_success") {
					// We store the ids and URLs of all the sources
					this.setState({
						sourcesAll: data.sources,
					});
					this.dbgLog("SourceEditor: getAllSources success");
				}
				// NOTE: Manage error states
				// This only really needs to be one else;
				// I don't see a reason to care about specifics here
			});
	}



	/**
	 * This will grab the details we want for the selected source and set it to the state
	 *
	 * @memberof SourceEditor
	 */
	getSelectedSourceDetails(): void {
		fetch(process.env.REACT_APP_API_URL + "__api.php", {
			method: "POST",
			credentials: "omit",
			body: JSON.stringify({
				request_type: "source_get_details",
				user_auth_token: this.props.authToken,
				source: this.state.sourcesAll[this.state.selectedSourcePosInSourcesAll].source_id,
			}),
		})
			.then((response) => response.json())
			.then((data: selectedSourceDetailsData) => {
				this.dbgLog("SourceEditor: getAllSources response");
				if (data.message === "fetch_success") {
					// We store the ids and URLs of all the sources
					this.setState({
						sourceSelectedDetails: {
							source_id: data.details.source_id,
							url: data.details.url,
							name: data.details.name,
							num_of_posts: data.num_of_posts,
						},
					});
					this.dbgLog("SourceEditor: getAllSources success");
				}
				// NOTE: Manage error states
				// This only really needs to be one else;
				// I don't see a reason to care about specifics here
			});
	}



	/**
	 * This attempts to add the typed URL as a source into the DB, which will be parsed
	 * come time that the PHP that parses the RSS into our DB actually executes.
	 *
	 * @param {React.FormEvent<HTMLFormElement>} event
	 * @memberof SourceEditor
	 */
	addNewSource(event: React.FormEvent<HTMLFormElement>): void {
		event.preventDefault();
		fetch(process.env.REACT_APP_API_URL + "__api.php", {
			method: "POST",
			credentials: "omit",
			body: JSON.stringify({
				request_type: "source_create_new",
				user_auth_token: this.props.authToken,
				source_url: this.state.sourceUrlToAdd,
				source_name: this.state.sourceNameToAdd,
			}),
		})
			.then((response) => response.json())
			.then((data) => {
				this.dbgLog("SourceEditor: addNewSource response");
				if (data.message === "create_success") {
					this.dbgLog("SourceEditor: addNewSource success");
					// Because we updated the DB, we wanna fetch the sources again
					// so that they're actually updated in the UI
					this.getAllSources();
				}
				// TODO: Manage error states
				// This will actually have at least one custom error state
				// we care about, that being "source_already_exists"
				else if (data.message === "source_already_exists") {
				}
			});
	}



	/**
	 * This updates the name of the selected source
	 *
	 * @param {React.FormEvent<HTMLFormElement>} event
	 * @memberof SourceEditor
	 */
	updateSourceName(event: React.FormEvent<HTMLFormElement>): void {
		event.preventDefault();
		fetch(process.env.REACT_APP_API_URL + "__api.php", {
			method: "POST",
			credentials: "omit",
			body: JSON.stringify({
				request_type: "source_update_name",
				user_auth_token: this.props.authToken,
				selected_source_id: this.state.sourcesAll[this.state.selectedSourcePosInSourcesAll].source_id,
				new_name: this.state.updatedSourceName,
			}),
		})
			.then((response) => response.json())
			.then((data) => {
				this.dbgLog("SourceEditor: updateSourceName response");
				if (data.message === "update_success") {
					this.dbgLog("SourceEditor: updateSourceName success");
					this.getAllSources();
					this.getSelectedSourceDetails();
				}
			});
	}



	/**
	 * This handles removing the source and purging all of its post.
	 *
	 * Depending on the features, this'll also purge a bunch of other things like
	 * the user fav sources.
	 *
	 * @param {number} sourceIdToDelete
	 * @memberof SourceEditor
	 */
	sourceRemoveAndPurgePosts(sourceIdToDelete: number): void {
		fetch(process.env.REACT_APP_API_URL + "__api.php", {
			method: "POST",
			credentials: "omit",
			body: JSON.stringify({
				request_type: "source_remove_and_purge_posts",
				user_auth_token: this.props.authToken,
				source: sourceIdToDelete,
			}),
		})
			.then((response) => response.json())
			.then((data) => {
				this.dbgLog("SourceEditor: sourceRemoveAndPurgePosts response");
				if (data.message === "remove_success") {
					this.dbgLog("SourceEditor: sourceRemoveAndPurgePosts success");
					// Because we updated the DB, we wanna fetch the sources again
					// so that they're actually updated in the UI
					this.getAllSources();
					// We also hide the second grid because that has no more reason to stay open
					// and we wipe the state of the selected source and the details pertaining to that source
					this.setState({
						modeGrid2: modesSourceEditorGrid2.DoNotRender,
						selectedSourcePosInSourcesAll: 0,
						sourceSelectedDetails: {source_id: 0, name: "", url: "", num_of_posts: 0},
					}, () => {
						this.getAllSources();
					});
				}
				// TODO: Manage error states
				// This will actually have the following error states that should inform the user
				else if (data.message === "removing_postitems_error") {
				}
				else if (data.message === "removing_postlinks_error") {
				}
				else if (data.message === "removing_source_error") {
				}
			});
	}



	/**
	 * This renders the first grid, which is used either to select a source or to add a new source.
	 *
	 * @return {*}  {JSX.Element}
	 * @memberof SourceEditor
	 */
	renderGrid1(): JSX.Element {
		const sourceArray: Array<JSX.Element> = [];

		for (let i = 0; i < this.state.sourcesAll.length; i++) {
			// -----------------------------------------------------------------
			// SECTION HTML: This is for each source rendered in grid 1
			// This generated sourceArray which is inserted in the return statement
			// -----------------------------------------------------------------
			const sourceItem =
				<button className="admin-grid-item" onClick={() => {
					this.setState({
						selectedSourcePosInSourcesAll: i,
						modeGrid2: modesSourceEditorGrid2.ListSourceDetails,
					}, () => {
						// Trigger this on callback as setState is asynchronous
						this.getSelectedSourceDetails();
					});
				}}>
					<p>Name: {this.state.sourcesAll[i].name}</p>
					<p>URL: {this.state.sourcesAll[i].url}</p>
				</button>;
			// -----------------------------------------------------------------
			// !SECTION
			// -----------------------------------------------------------------

			sourceArray.push(sourceItem);
		}
		return (
			// -------------------------------------------------------------
			// SECTION HTML: Linked sources grid
			// -------------------------------------------------------------
			<div className="admin-grid-inner">
				<div className="top-bar">
					<p className="font-code">Admin: Source Editor</p>
					<div><button className="admin-button" onClick={() => this.props.setMode(modesAdminHub.Feeds)}>Feed Editor</button></div>
				</div>

				{/* {sourceArray} will contain all of the existing sources */}
				{sourceArray}

				<button className="admin-button" onClick={() => {
					this.setState({
						modeGrid2: modesSourceEditorGrid2.AddNewSource,
					});
				}}>Add New Source</button>
			</div>
			// -------------------------------------------------------------
			// !SECTION
			// -------------------------------------------------------------
		);
	}



	/**
	 * This will either render the details of the selected source, or the option to add
	 * a new source.
	 *
	 * @return {*}  {JSX.Element}
	 * @memberof SourceEditor
	 */
	renderGrid2(): JSX.Element {
		switch (this.state.modeGrid2) {
		case modesSourceEditorGrid2.ListSourceDetails:
			// -----------------------------------------------------------------
			// SECTION HTML: Source details display in grid 2
			// -----------------------------------------------------------------
			return <div className="admin-grid-inner">
				<div className="top-bar">
					<p className="font-code">Details for: {this.state.sourcesAll[this.state.selectedSourcePosInSourcesAll].name}</p>
					<form onSubmit={this.updateSourceName}>
						<label htmlFor="source-name">New name</label>
						<input type="text" name="source-name" placeholder="Source name" value={this.state.updatedSourceName} onChange={(event) => this.setState({updatedSourceName: event.target.value})}/>
						<button type="submit" className="admin-button">Update</button>
					</form>
				</div>

				<p className="text-light">ID: {this.state.sourceSelectedDetails.source_id}</p>
				<p className="text-light">Name: {this.state.sourceSelectedDetails.name}</p>
				<p className="text-light">URL: {this.state.sourceSelectedDetails.url}</p>
				<p className="text-light">Number of posts: {this.state.sourceSelectedDetails.num_of_posts}</p>

				{/*
					The selected source will also have a button to delete the source.
					The way this works is, on click, a PopUp will be triggered and its callback
					will be set to this.sourceRemoveAndPurgePosts. The argument given will be the
					source id based on the state
				*/}

				<button className="admin-button extra-margin-top" onClick={() => {
					this.setState({
						showDeletePopup: true,
					});
				}}>Delete Source</button>
			</div>;
			// -----------------------------------------------------------------
			// !SECTION
			// -----------------------------------------------------------------
		case modesSourceEditorGrid2.AddNewSource:
			// -----------------------------------------------------------------
			// SECTION HTML: Add new source input in grid 2
			// -----------------------------------------------------------------
			return <div className="admin-grid-inner">
				<div className="top-bar">
					<p className="font-code">Add a Source</p>
				</div>
				<div className="admin-grid-item">
					<form className="simple-vertical-form" onSubmit={this.addNewSource}>
						<label htmlFor="sourceName"><b>Source Name</b></label>
						<input type="text" placeholder="Source Name" name="sourceName" required onChange={(event) => this.setState({sourceNameToAdd: event.target.value})}/>
						<label htmlFor="sourceUrl"><b>Source URL</b></label>
						<input type="text" placeholder="Source URL" name="sourceUrl" required onChange={(event) => this.setState({sourceUrlToAdd: event.target.value})}/>
						<button className="admin-button" type="submit">Add</button>
					</form>
				</div>
			</div>;
			// -----------------------------------------------------------------
			// !SECTION
			// -----------------------------------------------------------------
		case modesSourceEditorGrid2.DoNotRender:
			return <Fragment/>;
		default:
			return <Fragment/>;
		}
	}



	/**
	 * This is the main render method, which is mostly delegated to the renderGrid1 and renderGrid2 methods.
	 *
	 * This also has a popup which will display based on the state, asking if the admin wants to confirm deletion
	 * of the source.
	 *
	 * @return {*}
	 * @memberof SourceEditor
	 */
	render(): JSX.Element {
		// ---------------------------------------------------------------------
		// SECTION HTML: This is the HTML for the source editor which will expand
		// depending on what the user selects
		// ---------------------------------------------------------------------
		return (
			<Fragment>
				{this.renderGrid1()}
				{this.renderGrid2()}
				{this.state.showDeletePopup ?
				// NOTE: we pass in a HTML element as the message so that we can render it a bit more legibly
					<PopUp
						// -------------------------------------------------------------
						// SECTION HTML: We can style this popup via the message
						// -------------------------------------------------------------
						message=
							{
								<div>
									<p>Are you sure you want to delete source: </p>
									<p style={{color: "red"}}>{this.state.sourceSelectedDetails.url}</p>
									<p>AND <b>all of the posts</b> from this source which are not linked to other sources?</p>
								</div>
							}
						// -----------------------------------------------------
						// !SECTION
						// -----------------------------------------------------:
						// The options are basically accepted as an Array of {text/callback} objects
						// See PopUp.tsx, where propsOptions is basically what this takes
						options={
							[
							// First option is "Yes", which on click triggers the deletion of the source
								{
									text: "Yes",
									callback: () => {
										this.sourceRemoveAndPurgePosts(this.state.sourceSelectedDetails.source_id);
										this.setState({
											showDeletePopup: false,
										});
									},
								},
								// Second option is "Cancel", which on click just cancels
								{
									text: "Cancel",
									callback: () => {
										this.setState({
											showDeletePopup: false,
										});
										this.dbgLog("cancelled");
									},
								},
							]
						}/> :
					""
				}
			</Fragment>
		);
		// ---------------------------------------------------------------------
		// !SECTION
		// ---------------------------------------------------------------------
	}
}

export default SourceEditor;
