type feedItem = {
	// eslint-disable-next-line
	feed_id: number,
	name: string,
}

type sourceItem = {
	// eslint-disable-next-line
	source_id: number,
	// eslint-disable-next-line
	name: string,
	url: string,
}

// Ths essentially builds upon sourceItem, adding the extra property
type sourceItemWithDetails = sourceItem & {
	// eslint-disable-next-line
	num_of_posts: number,
}

// This is the format in which the data is returned...
// it doesn't exactly match sourceItemWithDetails
type selectedSourceDetailsData = {
	message: string,
	details: sourceItem,
	// eslint-disable-next-line
	num_of_posts: number,
}

export type {
	feedItem,
	sourceItem,
	sourceItemWithDetails,
	selectedSourceDetailsData,
};
