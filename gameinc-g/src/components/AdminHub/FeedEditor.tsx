import React, {Component, Fragment} from "react";
import "./AdminHub.css";
import {globals} from "../../logic/globals";




// -----------------------------------------------------------------------------
// Types
import type {feedItem, sourceItem} from "./types";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Modes
import {modesAdminHub, modesAdminHubFeeds, modesFeedEditorGrid2, modesFeedEditorGrid3} from "./modes";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Components
import PopUp from "../PopUp/PopUp";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Libraries
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Assets
// -----------------------------------------------------------------------------





type FeedEditorState = {
	modeGrid1: modesAdminHubFeeds,
	modeGrid2: modesFeedEditorGrid2,
	modeGrid3: modesFeedEditorGrid3,
	showDeletePopup: boolean,
	// -------------------------------------------------------------------------
	// Grid1
	// This stores all the feed name/ids
	feedsAll: Array<feedItem>,
	// This stores the name of the selected feed
	selectedFeedPosInFeeedsAll: number,
	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// Grid2
	// This stores all the sources that are linked to the selected feed
	sourcesLinked: Array<sourceItem>,
	// This stores the name of the feed the user wants to add
	feedNameToAdd: string,
	updatedFeedName: string,
	// -------------------------------------------------------------------------
	// -------------------------------------------------------------------------
	// Grid3
	// grid3Display: Array<JSX.Element>,
	// This stores all the sources that are not yet linked, hence linkable, to the selected feed
	sourcesLinkable: Array<sourceItem>,
	// -------------------------------------------------------------------------
}

type FeedEditorProps = {
	setMode: (mode: modesAdminHub) => void,
	authToken: string,
}





/**
 * This is the grid that is displayed when the Admin is in the mode of editing "feeds",
 * whether it be adding new feeds, adding feed-source links, or removing feed-source links.
 *
 * @class FeedEditor
 * @extends {Component}
 */
class FeedEditor extends Component<FeedEditorProps, FeedEditorState> {
	/**
	 * Creates an instance of FeedEditor.
	 * @param {FeedEditorProps} props
	 * @memberof FeedEditor
	 */
	constructor(props: FeedEditorProps) {
		super(props);
		this.state = {
			modeGrid1: modesAdminHubFeeds.placeholder,
			modeGrid2: modesFeedEditorGrid2.DoNotRender,
			modeGrid3: modesFeedEditorGrid3.DoNotRender,
			showDeletePopup: false,
			// grid1Display: [],
			feedsAll: [],
			selectedFeedPosInFeeedsAll: 0,
			// grid2Display: [],
			sourcesLinked: [],
			feedNameToAdd: "",
			updatedFeedName: "",
			// grid3Display: [],
			sourcesLinkable: [],
		};

		this.dbgLog();

		this.renderGrid1 = this.renderGrid1.bind(this);
		this.renderGrid2 = this.renderGrid2.bind(this);
		this.renderGrid3 = this.renderGrid3.bind(this);
		this.addNewFeedCategory = this.addNewFeedCategory.bind(this);
		this.updateFeedName = this.updateFeedName.bind(this);
		this.removeFeedAndCleanLinkedSources = this.removeFeedAndCleanLinkedSources.bind(this);
		this.requestRemoveFeedAndCleanLinkedSources = this.requestRemoveFeedAndCleanLinkedSources.bind(this);
	}



	/**
	 * This checks if this component should be "debugged" via the process.env variables. If it's true,
	 * we bind console.log so that logging works normally, otherwise we make it not do anything.
	 *
	 * @param {*} [mode]
	 * @param {...any[]} optionalParams
	 * @memberof FeedEditor
	 */
	dbgLog(
		// eslint-disable-next-line
		mode?: any,...optionalParams: any[]
	): void {
		if (globals.DEBUGGER_DBG_LOG === "true" && globals.DEBUGGER_DBG_FeedEditor === "true") {
			this.dbgLog = console.log.bind(window.close);
		}
		else {
			this.dbgLog = (): void => {
				return;
			};
		}
	}



	/**
	 * On mount, we get all the feeds that are currently available.
	 *
	 * @memberof FeedEditor
	 */
	componentDidMount(): void {
		// -------------------------------------------------------------------------
		// Check user is admin
		// -------------------------------------------------------------------------
		this.getAllFeeds();
	}



	/**
	 * Simply switch which mode to display via a change of the state; another method
	 * looks at this mode state and renders the respective login/register etc. fields.
	 *
	 * @param {string} modeToSetTo
	 * @memberof FeedEditor
	 */
	setMode(modeToSetTo: modesAdminHubFeeds): void {
		this.dbgLog("LoginLanding: setMode");
		this.setState(() => ( {
			modeGrid1: modeToSetTo,
		}));
	}



	/**
	 * This is used to set what is rendered in grid 2. Essentially, this'll either be
	 * "attached sources to selected feed" or "add feed"
	 *
	 * @param {modesFeedEditorGrid2} modeToSetTo
	 * @memberof FeedEditor
	 */
	setModeGrid2(modeToSetTo: modesFeedEditorGrid2): void {
		this.dbgLog("LoginLanding: setMode");
		this.setState(() => ( {
			modeGrid2: modeToSetTo,
		}));
	}



	/**
	 * This is used to set what is rendered in grid 3. Essentially, this'll either be
	 * "liste available sources" or nothing
	 *
	 * @param {modesFeedEditorGrid2} modeToSetTo
	 * @memberof FeedEditor
	 */
	setModeGrid3(modeToSetTo: modesFeedEditorGrid3): void {
		this.dbgLog("LoginLanding: setMode");
		this.setState(() => ( {
			modeGrid3: modeToSetTo,
		}));
	}



	// -------------------------------------------------------------------------
	// SECTION Grid1 stuff
	// -------------------------------------------------------------------------
	/**
	 * This gets all available feeds' names
	 *
	 * @memberof FeedEditor
	 */
	getAllFeeds(): void {
		fetch(process.env.REACT_APP_API_URL + "__api.php", {
			method: "POST",
			credentials: "omit",
			body: JSON.stringify({
				request_type: "feed_get_all",
				user_auth_token: this.props.authToken,
			}),
		})
			.then((response) => response.json())
			.then((data) => {
				this.dbgLog("FeedEditor: getAllFeeds response");
				if (data.message === "fetch_success") {
					// We store the ids and names of all the feeds
					this.setState({
						feedsAll: data.feeds,
					});
					this.dbgLog("FeedEditor: getAllFeeds success");
				}
				// NOTE: Manage error states
				// This only really needs to be one else;
				// I don't see a reason to care about specifics here
			});
	}
	// -------------------------------------------------------------------------
	// !SECTION
	// -------------------------------------------------------------------------



	// -------------------------------------------------------------------------
	// SECTION Grid2 stuff
	// -------------------------------------------------------------------------
	/**
	 * This gets all sources that are linked to the currently selected feed.
	 *
	 * @param {number} feedId
	 * @memberof FeedEditor
	 */
	getSourcesLinkedToFeed(feedId: number): void {
		fetch(process.env.REACT_APP_API_URL + "__api.php", {
			method: "POST",
			credentials: "omit",
			body: JSON.stringify({
				request_type: "feed_get_linked_sources",
				user_auth_token: this.props.authToken,
				selected_feed_id: feedId,
			}),
		})
			.then((response) => response.json())
			.then((data) => {
				this.dbgLog("FeedEditor: getSourcesLinkedToFeed response");

				if (data.message === "fetch_success") {
					this.setState({
						sourcesLinked: data.sources,
					});
					this.dbgLog("FeedEditor: getSourcesLinkedToFeed success");
				}
				// NOTE: Manage error states
				// This only really needs to be one else;
				// I don't see a reason to care about specifics here
			});
	}



	/**
	 * This is passed the source which shall be unlinked from the selected feed.
	 *
	 * Here we can prompt the user for a confirmation before making the server
	 * fetch request to the to unlink it.
	 *
	 * @param {number} sourceIdToUnlink
	 * @memberof FeedEditor
	 */
	unlinkSourceFromFeed(sourceIdToUnlink: number): void {
		fetch(process.env.REACT_APP_API_URL + "__api.php", {
			method: "POST",
			credentials: "omit",
			body: JSON.stringify({
				request_type: "feed_unlink_source",
				user_auth_token: this.props.authToken,
				feed: this.state.feedsAll[this.state.selectedFeedPosInFeeedsAll].feed_id,
				source_to_unlink: sourceIdToUnlink,
			}),
		})
			.then((response) => response.json())
			.then((data) => {
				this.dbgLog("FeedEditor: unlinkSourceFromFeed response");
				if (data.message === "unlink_success") {
					this.dbgLog("FeedEditor: unlinkSourceFromFeed success");
					// As the unlink was successfuol, we expect the feed to be gone
					// so we perform this to just re-fetch the feeds
					this.getSourcesLinkedToFeed(this.state.feedsAll[this.state.selectedFeedPosInFeeedsAll].feed_id);
					this.getLinkableSources(this.state.feedsAll[this.state.selectedFeedPosInFeeedsAll].feed_id);
				}
				// NOTE: Manage error states
				// This only really needs to be one else;
				// I don't see a reason to care about specifics here
			});
	}



	/**
	 * This will fetch sources which have not been linked to this feed,
	 * hence being linkable.
	 *
	 * @param {number} feedId
	 * @memberof FeedEditor
	 */
	getLinkableSources(feedId: number): void {
		fetch(process.env.REACT_APP_API_URL + "__api.php", {
			method: "POST",
			credentials: "omit",
			body: JSON.stringify({
				request_type: "feed_get_linkable_sources",
				user_auth_token: this.props.authToken,
				feed: feedId,
			}),
		})
			.then((response) => response.json())
			.then((data) => {
				this.dbgLog("FeedEditor: getLinkableSources response");
				if (data.message === "fetch_success") {
					this.setState({
						sourcesLinkable: data.sources,
					});
					this.dbgLog("FeedEditor: getLinkableSources success");
				}
				// NOTE: Manage error states
				// This only really needs to be one else;
				// I don't see a reason to care about specifics here
			});
	}



	/**
	 * This handles linking the clicked source to the currently selected feed.
	 *
	 * @param {number} sourceIdToLink
	 * @memberof FeedEditor
	 */
	linkSourceToFeed(sourceIdToLink: number): void {
		// -------------------------------------------------------------------------
		// onClick of linkSourceToSelectedFeed button
		// - Display all available feeds in grid3 that are not yet linked to the selected source
		// - onClick of any feed, link the feed and source
		// -------------------------------------------------------------------------
		fetch(process.env.REACT_APP_API_URL + "__api.php", {
			method: "POST",
			credentials: "omit",
			body: JSON.stringify({
				request_type: "feed_link_source",
				user_auth_token: this.props.authToken,
				feed: this.state.feedsAll[this.state.selectedFeedPosInFeeedsAll].feed_id,
				source_to_link: sourceIdToLink,
			}),
		})
			.then((response) => response.json())
			.then((data) => {
				this.dbgLog("FeedEditor: linkSourceToFeed response");
				if (data.message === "link_success") {
					this.dbgLog("FeedEditor: linkSourceToFeed success");
					// Same as with unlinkSourceFromFeed, there was a change
					// so we want to make the UI match what the database holds
					this.getLinkableSources(this.state.feedsAll[this.state.selectedFeedPosInFeeedsAll].feed_id);
					this.getSourcesLinkedToFeed(this.state.feedsAll[this.state.selectedFeedPosInFeeedsAll].feed_id);
				}
				// TODO: Manage error states
				// This only really needs to be one else;
				// I don't see a reason to care about specifics here
			});
	}



	/**
	 * This makes the delete popup appear, which then triggers removeFeedAndCleanLinkedSources.
	 *
	 * This function doesn't actually do any of the work for that
	 *
	 * @memberof FeedEditor
	 */
	requestRemoveFeedAndCleanLinkedSources(): void {
		this.setState({
			showDeletePopup: true,
		});
	}



	/**
	 * This will remove the current feed and any feed-source links associated to it
	 *
	 * First, however, the Admin will be prompted if they want to actually delete
	 *
	 * @note Do not call this directly without going through a PopUp
	 *
	 * @note We specifically take a parameter here instead of using the state so that,
	 * no matter what, the deleted feed is the same as the feed the user confirmed. Though
	 * I actually don't think this makes any difference, even hypothetically, I do feel better
	 * about myself doing it this way.
	 *
	 * @param {number} feedToPurgeId
	 * @memberof FeedEditor
	 */
	removeFeedAndCleanLinkedSources(feedToPurgeId: number): void {
		fetch(process.env.REACT_APP_API_URL + "__api.php", {
			method: "POST",
			credentials: "omit",
			body: JSON.stringify({
				request_type: "feed_unlink_all_and_remove_feed",
				user_auth_token: this.props.authToken,
				feed: feedToPurgeId,
			}),
		})
			.then((response) => response.json())
			.then((data) => {
				this.dbgLog("FeedEditor: removeFeedAndCleanLinkedSources response");
				if (data.message === "unlink_and_remove_success") {
					this.dbgLog("FeedEditor: removeFeedAndCleanLinkedSources success");
					// Because we updated the DB, we wanna fetch the feeds again
					// so that they're actually updated in the UI
					this.getAllFeeds();
					// We also hide grid 2 because that'll no longer have the correct info
					this.setState({
						modeGrid2: modesFeedEditorGrid2.DoNotRender,
					});
				}
				// TODO: Manage error states
				else if (data.message === "sources_unlink_error") {
				}
				else if (data.message === "sources_unlinked_but_remove_did_error") {
				}
			});
	}



	/**
	 * This updates the name of the selected feed
	 *
	 * @param {React.FormEvent<HTMLFormElement>} event
	 * @memberof SourceEditor
	 */
	updateFeedName(event: React.FormEvent<HTMLFormElement>): void {
		event.preventDefault();
		fetch(process.env.REACT_APP_API_URL + "__api.php", {
			method: "POST",
			credentials: "omit",
			body: JSON.stringify({
				request_type: "feed_update_name",
				user_auth_token: this.props.authToken,
				selected_feed_id: this.state.feedsAll[this.state.selectedFeedPosInFeeedsAll].feed_id,
				new_name: this.state.updatedFeedName,
			}),
		})
			.then((response) => response.json())
			.then((data) => {
				this.dbgLog("FeedEditor: updateFeedName response");
				if (data.message === "update_success") {
					this.dbgLog("FeedEditor: updateFeedName success");
					this.getAllFeeds();
				}
			});
	}
	// -------------------------------------------------------------------------
	// !SECTION
	// -------------------------------------------------------------------------



	// -------------------------------------------------------------------------
	// SECTION Grid3 stuff
	// -------------------------------------------------------------------------
	/**
	 * This makes the request to add the feed category to the db.
	 *
	 * @param {React.FormEvent<HTMLFormElement>} event
	 * @memberof FeedEditor
	 */
	addNewFeedCategory(event: React.FormEvent<HTMLFormElement>): void {
		event.preventDefault();
		// -------------------------------------------------------------------------
		// onClick of addfeed button
		// - Display option to add feed by name in grid2
		// - If admin clicks yes, try to add the feed name to the DB
		// - Also Need to check feed name does not already exist (PHP)
		// -------------------------------------------------------------------------
		fetch(process.env.REACT_APP_API_URL + "__api.php", {
			method: "POST",
			credentials: "omit",
			body: JSON.stringify({
				request_type: "feed_create_new",
				user_auth_token: this.props.authToken,
				feed: this.state.feedNameToAdd,
			}),
		})
			.then((response) => response.json())
			.then((data) => {
				this.dbgLog("FeedEditor: addNewFeedCategory response");
				if (data.message === "create_success") {
					this.dbgLog("FeedEditor: addNewFeedCategory success");
					// Because we updated the DB, we wanna fetch the feeds again
					// so that they're actually updated in the UI
					this.getAllFeeds();
				}
				// TODO: Manage error states
				// This will actually have at least one custom error state
				// we care about, that being "feed_already_exists"
				else if (data.message === "feed_already_exists") {
				}
			});
	}
	// -------------------------------------------------------------------------
	// !SECTION
	// -------------------------------------------------------------------------



	/**
	 * Here, a list of existing feeds will render. Clicking on a feed name here will make that
	 * feed the "selected" feed, which will be used to display the sources attached to said feed
	 * in grid 2.
	 *
	 * @return {*}  {JSX.Element}
	 * @memberof FeedEditor
	 */
	renderGrid1(): JSX.Element {
		// -------------------------------------------------------------------------
		// onClick of any feed in grid1
		// - Get all sources linked to that feed
		// - Display all linked sources in grid2
		// - Display an "X" button next to each source, which will send a request to unlink the
		// selected source from the selected feed
		// - Display a button to linkSourceToSelectedFeed
		// -------------------------------------------------------------------------

		const feedArray: Array<JSX.Element> = [];

		// Basically, we render a button for every feed
		// This button, when clicked, sets the state
		// - storing the selected feed
		// - telling grid2 to switch to listing linked sources
		// - telling grid3 to hide
		// Each button "knows" the value via this.state.feedsAll[i] which is based on the loop
		// It also triggers to fetch request to get the linked sources based on the name,
		// and wipes the sourcesLinkable so that the old linkable sources are not displayed
		// when grid3 switches to ListLinkableSources
		for (let i = 0; i < this.state.feedsAll.length; i++) {
			// -----------------------------------------------------------------
			// SECTION HTML: This is for each feed rendered in grid 1
			// This generated feedArray which is inserted in the return statement
			// -----------------------------------------------------------------
			const feedItem =
				<button className="admin-button admin-grid-item" onClick={() => {
					this.setState({
						selectedFeedPosInFeeedsAll: i,
						modeGrid2: modesFeedEditorGrid2.ListLinkedSources,
						modeGrid3: modesFeedEditorGrid3.DoNotRender,
						sourcesLinkable: [],
					});
					this.getSourcesLinkedToFeed(this.state.feedsAll[i].feed_id);
				}}>
					{this.state.feedsAll[i].name}
				</button>;
			// -----------------------------------------------------------------
			// !SECTION
			// -----------------------------------------------------------------

			feedArray.push(feedItem);
		}
		return (
			// -------------------------------------------------------------
			// SECTION HTML: Linked sources grid
			// -------------------------------------------------------------
			<div className="admin-grid-inner">
				<div className="top-bar">
					<p className="font-code">Admin: Feed Editor</p>
					<div><button className="admin-button" onClick={() => this.props.setMode(modesAdminHub.Sources)}>Source Editor</button></div>
				</div>

				{/* {feedArray} will contain all of the available feeds */}
				{feedArray}
				<button className="admin-button" onClick={() => {
					this.setState({
						modeGrid2: modesFeedEditorGrid2.AddNewFeed,
						modeGrid3: modesFeedEditorGrid3.DoNotRender,
					});
				}}>Add New Feed</button>
			</div>
			// -------------------------------------------------------------
			// !SECTION
			// -------------------------------------------------------------
		);
	}



	/**
	 * This either renders a grid with a list of linked sources, or
	 * a grid with the add-feed functionality
	 *
	 * @return {*}  {JSX.Element}
	 * @memberof FeedEditor
	 */
	renderGrid2(): JSX.Element | undefined {
		switch (this.state.modeGrid2) {
		case modesFeedEditorGrid2.ListLinkedSources:
			const sourcesArray: Array<JSX.Element> = [];

			// For every item in the linked sources, push it to the array to display
			for (let i = 0; i < this.state.sourcesLinked.length; i++) {
				// -------------------------------------------------------------
				// SECTION HTML: This is for each source rendered in grid 2
				// when the user is displaying in that mode
				// This generates the sourcesArray displayed in the return
				// Each source will have a button to remove it from the feed
				// -------------------------------------------------------------
				const sourceItem = <div className="admin-grid-item">
					<p>Name: {this.state.sourcesLinked[i].name}</p>
					<p>URL: {this.state.sourcesLinked[i].url}</p>
					<button className="admin-button" onClick={() => this.unlinkSourceFromFeed(this.state.sourcesLinked[i].source_id)}>Unlink</button>
				</div>;
				// -------------------------------------------------------------
				// !SECTION
				// -------------------------------------------------------------
				sourcesArray.push(sourceItem);
			}
			return (
				// -------------------------------------------------------------
				// SECTION HTML: Linked sources grid
				// -------------------------------------------------------------
				<div className="admin-grid-inner">
					<div className="top-bar">
						<p className="font-code">Sources for: {this.state.feedsAll[this.state.selectedFeedPosInFeeedsAll].name}</p>
						<form onSubmit={this.updateFeedName}>
							<label htmlFor="feedName">New name</label>
							<input type="text" placeholder="Feed name" name="feedName" required onChange={(event) => this.setState({updatedFeedName: event.target.value})}/>
							<button className="admin-button" type="submit">Update</button>
						</form>
					</div>

					{/* {sourcesArray} will contain all of the linked sources */}
					{sourcesArray}


					{/* This button will allow the user to add a new source to the selected feed
					by displaying grid 3. It also triggers the fetch request to get the data needed for
					grid 3 */}
					<button className="admin-button" onClick={() => {
						this.setState({
							modeGrid3: modesFeedEditorGrid3.ListLinkableSources,
						});
						this.getLinkableSources(this.state.feedsAll[this.state.selectedFeedPosInFeeedsAll].feed_id);
					}}>Link New Source</button>

					<div><button className="admin-button extra-margin-top" onClick={this.requestRemoveFeedAndCleanLinkedSources}>Delete Feed</button></div>

				</div>
				// -------------------------------------------------------------
				// !SECTION
				// -------------------------------------------------------------
			);

		case modesFeedEditorGrid2.AddNewFeed:
			return (
				// -------------------------------------------------------------
				// SECTION HTML: Add new feed grid
				// -------------------------------------------------------------
				<div className="admin-grid-inner">
					<div className="top-bar">
						<p className="font-code">New Feed Creation</p>
					</div>
					<div className="admin-grid-item">
						<form className="simple-vertical-form" onSubmit={this.addNewFeedCategory}>
							<label htmlFor="feedName"><b>Feed Name</b></label>
							<input type="text" placeholder="Feed Name" name="feedName" required onChange={(event) => this.setState({feedNameToAdd: event.target.value})}/>
							<button className="admin-button" type="submit">Add</button>
						</form>
					</div>
				</div>
				// -------------------------------------------------------------
				// !SECTION
				// -------------------------------------------------------------
			);
		default:
			return;
		}
	}



	/**
	 * This grid will display the linkable sources to the selected feed
	 *
	 * @return {*}  {JSX.Element}
	 * @memberof FeedEditor
	 */
	renderGrid3(): JSX.Element {
		switch (this.state.modeGrid3) {
		case modesFeedEditorGrid3.ListLinkableSources:
			const linkableSourcesArray: Array<JSX.Element> = [];

			// For every item in the linked sources, push it to the array to display
			for (let i = 0; i < this.state.sourcesLinkable.length; i++) {
				// -------------------------------------------------------------
				// SECTION HTML: This is for each linkable source
				// Each source will have a button to link it to the feed
				// based on this.state.sourcesLinkable[i]
				// -------------------------------------------------------------
				const sourceItem = <div className="admin-grid-item">
					<p>Name: {this.state.sourcesLinkable[i].name}</p>
					<p>URL: {this.state.sourcesLinkable[i].url}</p>
					<button className="admin-button" onClick={() => this.linkSourceToFeed(this.state.sourcesLinkable[i].source_id)}>Link</button>
				</div>;
				// -------------------------------------------------------------
				// !SECTION
				// -------------------------------------------------------------
				linkableSourcesArray.push(sourceItem);
			}
			return (
				// -------------------------------------------------------------
				// SECTION HTML: Linked sources grid
				// -------------------------------------------------------------
				<div className="admin-grid-inner">
					<div className="top-bar">
						<p className="font-code">Linkable Sources</p>
						<div><button className="admin-button" onClick={() => this.props.setMode(modesAdminHub.Sources)}>Add New Source</button></div>
					</div>
					{/* {linkableSourcesArray} will contain all of the sources which can be linked */}
					{linkableSourcesArray}
				</div>
				// -------------------------------------------------------------
				// !SECTION
				// -------------------------------------------------------------
			);
		default:
			return <Fragment/>;
		}
	}



	/**
	 * This renders the FeedEditor grids based on whether the user is viewing,
	 * linking, adding categories, etc.
	 *
	 * See https://flaviocopes.com/jsx-return-multiple-elements/
	 *
	 * Because this component is rendered inside of a grid container, and
	 * the grids need to be besides each other, we need to return **multiple**
	 * elements. This Fragment becomes nothing, so these grids still end up
	 * as direct children inside of the <div className="grid-inner-container"
	 *
	 * @note The way the renderGrids are set up, they will only return something if they
	 * need to; aside from renderGrid1, these may return nothing depending on
	 * the state
	 *
	 * @return {*}
	 * @memberof FeedEditor
	 */
	render(): JSX.Element {
		return (
			<Fragment>
				{this.renderGrid1()}
				{this.renderGrid2()}
				{this.renderGrid3()}
				{/* If this is set to true, aka we should show popup, show the popup.
				Concatenating the message to have the selected feed,
				and passing in the appropriate options*/}
				{this.state.showDeletePopup ?
					<PopUp message={"Are you sure you want to delete feed: " + this.state.feedsAll[this.state.selectedFeedPosInFeeedsAll].name}
						// The options are basically accepted as an Array of {text/callback} objects
						// See PopUp.tsx, where propsOptions is basically what this takes
						options={
							[
								// First option is "Yes", which on click triggers the delete
								{
									text: "Yes",
									callback: () => {
										this.removeFeedAndCleanLinkedSources(this.state.feedsAll[this.state.selectedFeedPosInFeeedsAll].feed_id);
										this.setState({
											showDeletePopup: false,
										});
									},
								},
								// Second option is "Cancel", which on click just cancels
								{
									text: "Cancel",
									callback: () => {
										this.setState({
											showDeletePopup: false,
										});
										this.dbgLog("cancelled");
									},
								},
							]
						}/> :
					""
				}
			</Fragment>
		);
	}
}

export default FeedEditor;
