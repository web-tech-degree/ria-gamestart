import React, {Component} from "react";
import "./AdminHub.css";
import {globals} from "../../logic/globals";





// -----------------------------------------------------------------------------
// Types
import type {createNotificationType} from "../types";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Modes
import {modesAdminHub} from "./modes";
import {modesAppExternal, modesAppInternal} from "../modes";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Components
import PopUp from "../PopUp/PopUp";
import FeedEditor from "./FeedEditor";
import SourceEditor from "./SourceEditor";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Libraries
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Assets
// -----------------------------------------------------------------------------





interface FeedAdminState {
	mode: modesAdminHub,
}

type FeedAdminProps = {
	createNotification: createNotificationType,
	// This function shall be passed into this component from the parent, returning true/false
	// based on whether the user is an admin according to the jwt token
	checkIsAdmin: () => boolean,
	setMainMode: (val: modesAppInternal | modesAppExternal) => void,
	authToken: string,
}





/**
 * This is the class for the component that renders the admin pages for controlling the feeds.
 *
 * This component should be rendered besides a LeftNav component, with the parent component having
 * the class "grid-container".
 *
 * @class AdminHub
 * @extends {Component}
 */
class AdminHub extends Component<FeedAdminProps, FeedAdminState> {
	/**
	 * Creates an instance of AdminHub.
	 * @param {FeedAdminProps} props
	 * @memberof AdminHub
	 */
	constructor(props: FeedAdminProps) {
		super(props);
		this.state = {
			mode: modesAdminHub.Feeds,
		};

		this.dbgLog();

		this.setMode = this.setMode.bind(this);
		this.renderIfAdmin = this.renderIfAdmin.bind(this);
	}



	/**
	 * This checks if this component should be "debugged" via the process.env variables. If it's true,
	 * we bind console.log so that logging works normally, otherwise we make it not do anything.
	 *
	 * @param {*} [mode]
	 * @param {...any[]} optionalParams
	 * @memberof AdminHub
	 */
	dbgLog(
		// eslint-disable-next-line
		mode?: any,...optionalParams: any[]
	): void {
		if (globals.DEBUGGER_DBG_LOG === "true" && globals.DEBUGGER_DBG_AdminHub === "true") {
			this.dbgLog = console.log.bind(window.close);
		}
		else {
			this.dbgLog = (): void => {
				return;
			};
		}
	}



	/**
	 * Set the mode of this component
	 *
	 * @param {modesAdminHub} modeToSetTo
	 * @memberof AdminHub
	 */
	setMode(modeToSetTo: modesAdminHub): void {
		this.setState({
			mode: modeToSetTo,
		});
	}



	/**
	 * This checks if the user is an admin. If they are not, it gives a PopUp, and if they are,
	 * it renders the editors.
	 *
	 * @return {*}  {JSX.Element}
	 * @memberof AdminHub
	 */
	renderIfAdmin(): JSX.Element {
		let component: JSX.Element;

		// If user isn't an admin, handle that here and return early before the actual rendering
		if (!this.props.checkIsAdmin()) {
			component = <PopUp message={"You are not an admin, would you like to go to the login page or feeds?"}
				options={
					[
						{
							text: "Login",
							callback: () => {
								this.props.setMainMode(modesAppInternal.Login);
							},
						},
						{
							text: "Feeds",
							callback: () => {
								this.props.setMainMode(modesAppInternal.FeedHub);
							},
						},
					]
				}/>;
			return component;
		}

		// User is admin, so we render the admin grids based on the state
		switch (this.state.mode) {
		case modesAdminHub.Feeds:
			component = <FeedEditor setMode={this.setMode} authToken={this.props.authToken}/>;
			break;
		case modesAdminHub.Sources:
			component = <SourceEditor setMode={this.setMode} authToken={this.props.authToken}/>;
			break;
		default:
			component = <div>default... something went wrong</div>;
			break;
		}

		return component;
	}



	/**
	 * Render the admin pages for the feeds, which themselves show as grids.
	 *
	 * @return {*}
	 * @memberof AdminHub
	 */
	render(): JSX.Element {
		// ---------------------------------------------------------------------
		// SECTION HTML:
		// ---------------------------------------------------------------------
		return (
			<div className="admin-grid-inner-container">
				{this.renderIfAdmin()}
			</div>
		);
		// ---------------------------------------------------------------------
		// !SECTION
		// ---------------------------------------------------------------------
	}
}

export default AdminHub;
