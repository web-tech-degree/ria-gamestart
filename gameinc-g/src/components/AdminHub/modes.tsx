// -----------------------------------------------------------------------------
// Only allow the following modes for the FeedAdmin component
// -----------------------------------------------------------------------------
enum modesAdminHub {
	Feeds = "Feeds",
	Sources = "Sources",
}

// -----------------------------------------------------------------------------
// Only allow the following modes when the modesFeedAdmin is set to Feeds
// -----------------------------------------------------------------------------
enum modesAdminHubFeeds {
	placeholder = "placeholder",
}

enum modesFeedEditorGrid2 {
	ListLinkedSources = "ListLinkedSources",
	AddNewFeed = "AddNewFeed",
	DoNotRender = "DoNotRender",
}

enum modesFeedEditorGrid3 {
	ListLinkableSources = "ListLinkableSources",
	DoNotRender = "DoNotRender",
}

// -----------------------------------------------------------------------------
// Only allow the following modes when the modesFeedAdmin is set to Sources
// -----------------------------------------------------------------------------
enum modesAdminHubSources {
	placeholder = "placeholder",
}

enum modesSourceEditorGrid2 {
	ListSourceDetails = "ListSourceDetails",
	AddNewSource = "AddNewSource",
	DoNotRender = "DoNotRender",
}



export {
	modesAdminHub,
	modesAdminHubFeeds,
	modesFeedEditorGrid2,
	modesFeedEditorGrid3,
	modesAdminHubSources,
	modesSourceEditorGrid2,
};
