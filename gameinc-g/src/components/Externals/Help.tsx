import React, {Fragment} from "react";
import "./Help.css";





// -----------------------------------------------------------------------------
// Types
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Modes
import {modesAppInternal, modesAppExternal, modesAppBackground} from "../modes";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Components
import Nav from "./Nav";
import Footer from "./Footer";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Libraries
import {Accordion, Button, Card} from "react-bootstrap";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Assets
// -----------------------------------------------------------------------------





type HelpState = {
	something: string,
}

type HelpProps = {
	setMainMode: (modeToSetTo: modesAppInternal | modesAppExternal) => void,
	setBackgroundMode: (modeToSetTo: modesAppBackground) => void,
}





/**
 * This class will handle the rendering of the Help page.
 *
 * @class Help
 * @extends {React.Component<HelpProps, HelpState>}
 */
class Help extends React.Component<HelpProps, HelpState> {
	/**
	 * Creates an instance of Help. See the documentation of App.tsx for how this works; this is the same thing
	 * but different names etc.
	 *
	 * @param {HelpProps} props
	 * @memberof Help
	 */
	constructor(props: HelpProps) {
		super(props);
		this.state = {
			something: "asdf",
		};
	}



	/**
	 * Automatically set the background to the gray colour when the help page is active
	 *
	 * @memberof Help
	 */
	componentDidMount(): void {
		this.props.setBackgroundMode(modesAppBackground.GrayColor);
	}



	/**
	 * This renders the help page
	 *
	 * @return {*}  {JSX.Element}
	 * @memberof Help
	 */
	render(): JSX.Element {
		return (
		// =============================================================================
		// SECTION HTML: Help page start
		// =============================================================================
			<Fragment>
				<Nav setMainMode={this.props.setMainMode}></Nav>
				{/* here */}
				<div className="d-md-flex h-md-100 align-items-center">

					{/* <!-- First Half --> */}

					<div className="col-md-6 p-0 bg-white h-md-100">
						<div className="text-white d-md-flex align-items-center h-100 p-5 justify-content-center">
							<div className="pt-5 pb-5">
								<form>
									<h5>Contact Us</h5>
									<div className="form-group">
										<label htmlFor="formGroupExampleInput">First Name</label>
										<input type="text" className="form-control" id="formGroupExampleInput" placeholder="Type Here..."/>
									</div>
									<div className="form-group">
										<label htmlFor="formGroupExampleInput">Second Name</label>
										<input type="text" className="form-control" id="formGroupExampleInput" placeholder="Type Here..."/>
									</div>
									<div className="form-group">
										<label htmlFor="exampleInputEmail1">Email</label>
										<input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="test@mail.com"/>
									</div>
									<div className="form-group">
										<label htmlFor="exampleInputPassword1">Topic</label>
										<input type="text" className="form-control" id="formGroupExampleInput" placeholder="Type Here..."/>
									</div>
									<div className="form-group">
										<label htmlFor="exampleFormControlTextarea1">Message</label>
										<textarea className="form-control textarea-no-resize" id="exampleFormControlTextarea1" rows={3}></textarea>
									</div>
									<div className="form-button just-content-center">
										<button type="submit" className="btn">Submit</button>
									</div>
								</form>
							</div>
						</div>
					</div>

					{/* <!-- Second Half --> */}
					<div className=" second col-md-6 p-0 h-md-100">
						<div className="d-md-flex align-items-center h-md-100 p-5 justify-content-center">
							<div className="container">
								<h2>How Can We Help?</h2>
								<Accordion defaultActiveKey="0">
									<Card>
										<Card.Header>
											<Accordion.Toggle as={Button} variant="link" eventKey="0">
											Take a Tour
											</Accordion.Toggle>
										</Card.Header>
										<Accordion.Collapse eventKey="0">
											<Card.Body>	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
												Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</Card.Body>
										</Accordion.Collapse>
									</Card>
									<Card>
										<Card.Header>
											<Accordion.Toggle as={Button} variant="link" eventKey="1">
											Create an Account
											</Accordion.Toggle>
										</Card.Header>
										<Accordion.Collapse eventKey="1">
											<Card.Body>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
												Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</Card.Body>
										</Accordion.Collapse>
									</Card>
									<Card>
										<Card.Header>
											<Accordion.Toggle as={Button} variant="link" eventKey="2">
											Delete Acount
											</Accordion.Toggle>
										</Card.Header>
										<Accordion.Collapse eventKey="2">
											<Card.Body>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
												Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</Card.Body>
										</Accordion.Collapse>
									</Card>
									<Card>
										<Card.Header>
											<Accordion.Toggle as={Button} variant="link" eventKey="3">
											Ways You Can Contact Us
											</Accordion.Toggle>
										</Card.Header>
										<Accordion.Collapse eventKey="3">
											<Card.Body>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
												Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</Card.Body>
										</Accordion.Collapse>
									</Card>
									<Card>
										<Card.Header>
											<Accordion.Toggle as={Button} variant="link" eventKey="4">
											About Us
											</Accordion.Toggle>
										</Card.Header>
										<Accordion.Collapse eventKey="4">
											<Card.Body>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
												Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</Card.Body>
										</Accordion.Collapse>
									</Card>
								</Accordion>
								{/* <div id="accordion">
									<div className="card">
										<div className="card-header">
											<a className="card-link" data-toggle="collapse" href="#collapseOne">
												Take a Tour
											</a>
										</div>
										<div id="collapseOne" className="collapse show" data-parent="#accordion">
											<div className="card-body">
												Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
												Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
											</div>
										</div>
									</div>
									<div className="card">
										<div className="card-header">
											<a className="collapsed card-link" data-toggle="collapse" href="#collapseTwo">
												Create an Account
											</a>
										</div>
										<div id="collapseTwo" className="collapse" data-parent="#accordion">
											<div className="card-body">
												Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
												Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
											</div>
										</div>
									</div>
									<div className="card">
										<div className="card-header">
											<a className="collapsed card-link" data-toggle="collapse" href="#collapseThree">
												Delete Acount
											</a>
										</div>
										<div id="collapseThree" className="collapse" data-parent="#accordion">
											<div className="card-body">
												Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
												Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
											</div>
										</div>
									</div>
									<div className="card">
										<div className="card-header">
											<a className="collapsed card-link" data-toggle="collapse" href="#collapseFour">
												Ways You Can Contact Us
											</a>
										</div>
										<div id="collapseFour" className="collapse" data-parent="#accordion">
											<div className="card-body">
												Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
												Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
											</div>
										</div>
									</div>
									<div className="card">
										<div className="card-header">
											<a className="collapsed card-link" data-toggle="collapse" href="#collapseFive">
												About Us
											</a>
										</div>
										<div id="collapseFive" className="collapse" data-parent="#accordion">
											<div className="card-body">
												Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
												Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
											</div>
										</div>
									</div>
								</div> */}
							</div>
						</div>
					</div>
				</div>

				<Footer setMainMode={this.props.setMainMode}></Footer>
			</Fragment>
		// -----------------------------------------------------------------
		// !SECTION
		// -----------------------------------------------------------------
		);
	}
}

export default Help;
