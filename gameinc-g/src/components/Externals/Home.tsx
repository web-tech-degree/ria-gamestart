import React, {Fragment} from "react";
import "./Home.css";





// -----------------------------------------------------------------------------
// Types
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Modes
import {modesAppInternal, modesAppExternal, modesAppBackground} from "../modes";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Components
import Nav from "./Nav";
import Footer from "./Footer";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Libraries
import Carousel from "react-bootstrap/Carousel";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Assets
import imgBanner1 from "../../assets/img/banner.svg";
import imgBanner2 from "../../assets/img/banner.svg";
import imgBanner3 from "../../assets/img/banner.svg";
import imgBestSelling from "../../assets/img/star-number-five.svg";
import imgMostPopular from "../../assets/img/most-popular.svg";
import imgNews from "../../assets/img/news.svg";
// -----------------------------------------------------------------------------





type HomeState = {
	something: string,
}

type HomeProps = {
	setMainMode: (modeToSetTo: modesAppInternal | modesAppExternal) => void,
	setBackgroundMode: (modeToSetTo: modesAppBackground) => void,
}





/**
 * This class will handle the rendering of the Home page.
 *
 * @class Home
 * @extends {React.Component<HomeProps, HomeState>}
 */
class Home extends React.Component<HomeProps, HomeState> {
	/**
	 * Creates an instance of Home. See the documentation of App.tsx for how this works; this is the same thing
	 * but different names etc.
	 *
	 * @param {HomeProps} props
	 * @memberof Home
	 */
	constructor(props: HomeProps) {
		super(props);
		this.state = {
			something: "asdf",
		};
	}



	/**
	 * Automatically set the background to the gray colour when the home page is active
	 *
	 * @memberof Home
	 */
	componentDidMount(): void {
		this.props.setBackgroundMode(modesAppBackground.GrayColor);
	}



	/**
	 * This renders the home page
	 *
	 * @return {*}  {JSX.Element}
	 * @memberof Home
	 */
	render(): JSX.Element {
		return (
		// =============================================================================
		// SECTION HTML: Home page start
		// =============================================================================
			<Fragment>
				<Nav setMainMode={this.props.setMainMode}></Nav>

				{/* Carousel */}
				<Carousel>
					<Carousel.Item>
						<img
							className="d-block w-100"
							src={imgBanner1}
							alt="First slide"
						/>
						<Carousel.Caption>
							<h3>First slide label</h3>
							<p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
						</Carousel.Caption>
					</Carousel.Item>
					<Carousel.Item>
						<img
							className="d-block w-100"
							src={imgBanner2}
							alt="Second slide"
						/>

						<Carousel.Caption>
							<h3>Second slide label</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
						</Carousel.Caption>
					</Carousel.Item>
					<Carousel.Item>
						<img
							className="d-block w-100"
							src={imgBanner3}
							alt="Third slide"
						/>

						<Carousel.Caption>
							<h3>Third slide label</h3>
							<p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
						</Carousel.Caption>
					</Carousel.Item>
				</Carousel>
				{/* End of Carousel */}

				{/* Heading */}
				<div className="heading">
					<p>RSS News Feeds Categories</p>
				</div>
				{/* End of Heading */}

				{/* Start of Cards */}

				<div className="cards">
					<div className="row row-cols-1 row-cols-md-3 g-4">
						<div className="col">
							<div className="card">
								<div className="card-body">
									<img src={imgBestSelling} className="card-img-top" alt="Best sellers"/>
									<h5 className="card-title">Best Selling</h5>
									<p className="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
								</div>
							</div>
						</div>
						<div className="col">
							<div className="card">
								<div className="card-body">
									<img src={imgMostPopular} className="card-img-top" alt="Most Popular"/>
									<h5 className="card-title">Most Popular</h5>
									<p className="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
								</div>
							</div>
						</div>
						<div className="col">
							<div className="card">
								<div className="card-body">
									<img src={imgNews} className="card-img-top" alt="News"/>
									<h5 className="card-title">Game News</h5>
									<p className="card-text">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				{/* End of Cards */}

				{/* Start of About Us Section */}
				<div className="about-us">
					<div className="card mb-3">
						<div className="card-body">
							<h5 className="card-title">About Us</h5>
							<p className="card-text">
								This is a wider card with supporting text below as a natural lead-in to additional content
								This content is a little bit longer. This is a wider card with supporting text below as a natural lead-in to additional content.
								This content is a little bit longer. This is a wider card with supporting text below as a natural lead-in to additional content.
								This content is a little bit longer. This is a wider card with supporting text below as a natural lead-in to additional content.
								This content is a little bit longer.
							</p>
							<br></br>
						</div>
					</div>
				</div>
				{/* End of About Us Section */}

				<Footer setMainMode={this.props.setMainMode}></Footer>
			</Fragment>
		// -----------------------------------------------------------------
		// !SECTION
		// -----------------------------------------------------------------
		);
	}
}

export default Home;
