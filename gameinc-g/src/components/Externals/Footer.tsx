import React, {Fragment} from "react";
import "./Footer.css";





// -----------------------------------------------------------------------------
// Types
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Modes
import {modesAppInternal, modesAppExternal} from "../modes";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Components
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Libraries
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Assets
// -----------------------------------------------------------------------------





type FooterState = {
	something: string,
}

type FooterProps = {
	setMainMode: (modeToSetTo: modesAppInternal | modesAppExternal) => void,
}





/**
 * This class will handle the rendering of the Footer element.
 *
 * @class Footer
 * @extends {React.Component<FooterProps, FooterState>}
 */
class Footer extends React.Component<FooterProps, FooterState> {
	/**
	 * Creates an instance of Footer. See the documentation of App.tsx for how this works; this is the same thing
	 * but different names etc.
	 *
	 * @param {FooterProps} props
	 * @memberof Footer
	 */
	constructor(props: FooterProps) {
		super(props);
		this.state = {
			something: "asdf",
		};
	}



	/**
	 * This renders the footer, but because of some implementation issues here and there as of the
	 * time of writing, it's not used everywhere where a footer actually is rendered.
	 *
	 * @return {*}  {JSX.Element}
	 * @memberof Footer
	 */
	render(): JSX.Element {
		return (
		// =============================================================================
		// SECTION HTML: Externals footer section
		// =============================================================================
			<Fragment>
				<div className="footer-container">
					<div className="text-center p-3">
					© 2021 Copyright:
						<a className="text" href="#">Game.inc.com</a>
					</div>
				</div>
			</Fragment>
		// -----------------------------------------------------------------
		// !SECTION
		// -----------------------------------------------------------------
		);
	}
}

export default Footer;
