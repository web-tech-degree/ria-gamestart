import React, {Fragment} from "react";
import "./Nav.css";





// -----------------------------------------------------------------------------
// Types
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Modes
import {modesAppInternal, modesAppExternal} from "../modes";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Components
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Libraries
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Assets
import imgGameIng from "../../assets/img/game-ing.svg";
// -----------------------------------------------------------------------------





type NavState = {
	something: string,
}

type NavProps = {
	setMainMode: (modeToSetTo: modesAppInternal | modesAppExternal) => void,
}





/**
 * This class will handle the rendering of the (top) Nav for the external pages.
 *
 * @class Nav
 * @extends {React.Component<NavProps, NavState>}
 */
class Nav extends React.Component<NavProps, NavState> {
	/**
	 * Creates an instance of Nav. See the documentation of App.tsx for how this works; this is the same thing
	 * but different names etc.
	 *
	 * @param {NavProps} props
	 * @memberof Nav
	 */
	constructor(props: NavProps) {
		super(props);
		this.state = {
			something: "asdf",
		};
	}



	/**
	 * This renders the nav for the external pages
	 *
	 * @return {*}  {JSX.Element}
	 * @memberof Nav
	 */
	render(): JSX.Element {
		return (
		// =============================================================================
		// SECTION HTML: Nav bar in the external pages
		// =============================================================================
			<Fragment>
				<nav className="navbar">
					<div className="container-fluid">
						<a className="navbar-brand" onClick={() => this.props.setMainMode(modesAppExternal.ExtHome)}>
							<img className="d-inline-block align-text-top cursor-pointer" src={imgGameIng} alt=""/>
						</a>
						<ul className="nav justify-content-right">
							<li className="nav-item">
								<a className="nav-link link-cursor" aria-current="page" onClick={() => this.props.setMainMode(modesAppExternal.ExtHome)}>Home</a>
							</li>
							<li className="nav-item">
								<a className="nav-link link-cursor" onClick={() => this.props.setMainMode(modesAppInternal.FeedHub)}>Feeds</a>
							</li>
							<li className="nav-item">
								<a className="nav-link link-cursor" onClick={() => this.props.setMainMode(modesAppExternal.ExtHelp)}>Help</a>
							</li>
							<li className="button nav-item">
								<button type="button" className="btn btn-primary" onClick={() => this.props.setMainMode(modesAppInternal.Login)}>SIGN IN</button>
							</li>
						</ul>
					</div>
				</nav>
			</Fragment>
		// -----------------------------------------------------------------
		// !SECTION
		// -----------------------------------------------------------------
		);
	}
}

export default Nav;
