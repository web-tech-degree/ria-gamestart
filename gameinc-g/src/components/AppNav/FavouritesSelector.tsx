import React from "react";
import "./FavouritesSelector.css";





// -----------------------------------------------------------------------------
// Types
import type {createNotificationType} from "../types";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Modes
import {modesDisplayOrientation, modesNotifTypes} from "../modes";
import {feedTypes} from "../FeedHub/modes";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Components
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Libraries
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Assets
// -----------------------------------------------------------------------------





type FavouritesSelectorState = {
	// eslint-disable-next-line
	favouritesAvailable: {source_id: number, name: string, url: string}[],
}

type FavouritesSelectorProps = {
	createNotification: createNotificationType,
	navIconRef: React.RefObject<HTMLSpanElement>
	spawnFeed: (type: feedTypes, extraId?: number) => void,
	closeMenu: () => void,
	authToken: string,
	displayMode?: modesDisplayOrientation;
}





/**
 * This is used to select a favourite source to display as a "feed", based on the
 * user's favourited sources.
 *
 * @class FavouritesSelector
 * @extends {Component}
 */
class FavouritesSelector extends React.Component<FavouritesSelectorProps, FavouritesSelectorState> {
	/**
	 * Creates an instance of FavouritesSelector.
	 * @param {FavouritesSelectorProps} props
	 * @memberof FavouritesSelector
	 */
	constructor(props: FavouritesSelectorProps) {
		super(props);
		this.state = {
			favouritesAvailable: [],
		};

		this.refInnerEl = React.createRef();

		this.handleClickOutside = this.handleClickOutside.bind(this);
	}
	refInnerEl: React.RefObject<HTMLDivElement>;

	/**
	 * When the component mounts, get the favourited posts
	 *
	 *  See https://stackoverflow.com/a/42234988
	 *
	 * @memberof FavouritesSelector
	 */
	componentDidMount(): void {
		this.fetchFavourited();
		document.addEventListener("mousedown", this.handleClickOutside);
	}



	/**
	 * See https://stackoverflow.com/a/42234988
	 *
	 * @memberof FavouritesSelector
	 */
	componentWillUnmount(): void {
		document.addEventListener("mousedown", this.handleClickOutside);
	}



	/**
	 * See https://stackoverflow.com/a/42234988
	 *
	 * This handles the clicks and discerns whether or not the elements we defined with ref
	 * as "inner" elements contains the element which was clicked.
	 *
	 * This way, if the user clicks on something inside of a <div ref={this.refInnerEl}>,
	 * we can tell that the user clicked inside, hence we will not close.
	 *
	 * This also is neat because it allows a user to click and drag text on an "inner" element,
	 * and then drag their mouse "outside" and release, and it still will not close; a click has to
	 * be explicitly on the outside for us to close.
	 *
	 * @param {MouseEvent} event
	 * @memberof FavouritesSelector
	 */
	handleClickOutside(event: MouseEvent): void {
		/**
		 * This is just a function which checks if the provided ref contains the clicked element.
		 * This is done just to clean up the check with a function instead of copy-pasting the line 5000 times
		 *
		 * @param {React.RefObject<HTMLDivElement>} refTarget
		 * @return {*}  {string}
		 */
		const checkClickEventNotInRefElement = (refTarget: React.RefObject<HTMLDivElement> | React.RefObject<HTMLSpanElement>): boolean => {
			// First check to make sure the target is not null
			if (refTarget.current) {
				return refTarget.current.contains(event.target as Node) ? false : true;
			}
			// NOTE: I don't really know if this will ever fire, because the refTarget should be set before the user
			// clicks anything, but whatever - TypeScript was complaining
			else {
				return false;
			}
		};

		// Here we check that the user did NOT click on the popup or on the favourites icon
		if (
			checkClickEventNotInRefElement(this.refInnerEl) &&
			checkClickEventNotInRefElement(this.props.navIconRef)
		) {
			this.props.closeMenu();
		}
	}



	/**
	 * This fetches the user's favourite sources
	 *
	 * @memberof FavouritesSelector
	 */
	fetchFavourited(): void {
		fetch(process.env.REACT_APP_API_URL + "__api.php", {
			method: "POST",
			credentials: "omit",
			body: JSON.stringify({
				request_type: "fetch_user_favourite_sources",
				user_auth_token: this.props.authToken,
			}),
		})
			.then((response) => response.json())
			.then((data) => {
				if (data.message === "fetch_success") {
					this.setState({
						favouritesAvailable: data.sources,
					});
				}
				else if (data.message === "token_validation_error") {
					this.props.createNotification(modesNotifTypes.Info, null, "You must be logged in to view favourites", null, null);
				}
			});
	}



	/**
	 * This is the favourites selector menu which should be rendered within the LeftNav.
	 *
	 * Basically, this needs to be rendered under a relative element, currently as of time of writing it
	 * is a random div next to the favourites icon, and it will render besides that.
	 *
	 * @return {*}  {JSX.Element}
	 * @memberof FavouritesSelector
	 */
	render(): JSX.Element {
		return (
			<div ref={this.refInnerEl}
				// NOTE this is a hacky way to get the favourites selector to display differently on mobile/desktop
				className={this.props.displayMode === modesDisplayOrientation.Mobile ? "favourites-selector-mobile" : "favourites-selector"}
			>

				<div className="favourites-top-bar">
					{/* <span/> */}
					<span className="center-text">Favourites</span>
					<span className="right-close" onClick={this.props.closeMenu}></span>
				</div>

				<div className="favourites-list">
					{this.state.favouritesAvailable.map((fav) => {
						//
						return <button className="default-button" key={fav.source_id} onClick={() => {
							this.props.spawnFeed(feedTypes.Favourites, fav.source_id);
						}}>{fav.name}</button>;
					})}
				</div>
			</div>
		);
	}
}

export default FavouritesSelector;
