import React, {Fragment} from "react";
import "./BotNav.css";
import {globals} from "../../logic/globals";





// -----------------------------------------------------------------------------
// Types
import type {createNotificationType} from "../types";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Modes
import {modesAppExternal, modesAppInternal, modesDisplayOrientation, modesNotifTypes} from "../modes";
import {feedTypes} from "../FeedHub/modes";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Components
import FavouritesSelector from "./FavouritesSelector";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Libraries
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Assets
// -----------------------------------------------------------------------------





type BotNavState = {
	pfpLocation: string,
	isFavouritesSelectorOpen: boolean,
}

type BotNavProps = {
	createNotification: createNotificationType,
	setMainMode: (mode: modesAppInternal | modesAppExternal) => void,
	mainMode: modesAppInternal | modesAppExternal,
	authToken: string,
	checkIsAdmin: () => boolean,
	spawnFeed: (type: feedTypes, extraId?: number) => void,
	debugParseSources: () => void,
	updateAuthToken: (val: string) => void,
}





/**
 * This is the bot nav component for the main app, not the website-y stuff.
 *
 * @note This is functionally a copy-paste of LeftNav, but it has some styling
 * changes
 *
 * @class BotNav
 * @extends {React.Component}
 */
class BotNav extends React.Component<BotNavProps, BotNavState> {
	/**
	 * Creates an instance of BotNav.
	 * @param {BotNavProps} props
	 * @memberof BotNav
	 */
	constructor(props: BotNavProps) {
		super(props);
		this.state = {
			pfpLocation: "",
			isFavouritesSelectorOpen: false,
		};
		this.refInnerElFavs = React.createRef();

		this.dbgLog();

		this.logUserOut = this.logUserOut.bind(this);
		this.closeFavouritesSelector = this.closeFavouritesSelector.bind(this);
	}
	refInnerElFavs: React.RefObject<HTMLSpanElement>;



	/**
	 * This checks if this component should be "debugged" via the process.env variables. If it's true,
	 * we bind console.log so that logging works normally, otherwise we make it not do anything.
	 *
	 * @param {*} [mode]
	 * @param {...any[]} optionalParams
	 * @memberof BotNav
	 */
	dbgLog(
		// eslint-disable-next-line
		mode?: any,...optionalParams: any[]
	): void {
		if (globals.DEBUGGER_DBG_LOG === "true" && globals.DEBUGGER_DBG_BotNav === "true") {
			this.dbgLog = console.log.bind(window.close);
		}
		else {
			this.dbgLog = (): void => {
				return;
			};
		}
	}



	/**
	 * Here we just grab the user's PFP
	 *
	 * @memberof BotNav
	 */
	componentDidMount(): void {
		this.fetchUserPfp();
	}



	/**
	 * Basically, here we just check if the auth token changed so that we can re-fetch
	 * the user's pfp. We also only need to worry about the pfp here because the check for
	 * admin level is done in a different way.
	 *
	 * @param {BotNavProps} prevProps
	 * @memberof BotNav
	 */
	componentDidUpdate(prevProps: BotNavProps): void {
		if (prevProps.authToken !== this.props.authToken) {
			this.fetchUserPfp();
		}
	}



	/**
	 * All we're doing here is telling the parent, App, to re-verify the token
	 * We do this so the validity of the token is actually accurate so we only display stuff
	 * that is accessible to the user based on their authentication
	 *
	 * @memberof BotNav
	 */
	// componentDidMount(): void {
	// 	this.props.verifyAuthToken();
	// }



	/**
	 * This fetches the URL of the user's profile picture
	 *
	 * @memberof BotNav
	 */
	fetchUserPfp(): void {
		fetch(process.env.REACT_APP_API_URL + "__api.php", {
			method: "POST",
			credentials: "omit",
			body: JSON.stringify({
				request_type: "fetch_user_pfp_only",
				user_auth_token: this.props.authToken,
			}),
		})
			.then((response) => response.json())
			.then((data) => {
				this.setState({
					pfpLocation: data.pfp_location,
				});
			});
	}



	/**
	 * This shows the additional admin options if the user is an admin
	 *
	 * @return {*}  {JSX.Element}
	 * @memberof BotNav
	 */
	renderAdminOptions(): JSX.Element[] {
		const elementArray: Array<JSX.Element> = [];

		if (this.props.checkIsAdmin()) {
			if (this.props.mainMode === modesAppInternal.AdminHub) {
				elementArray.push(<button className="default-button" onClick={() => this.props.setMainMode(modesAppInternal.FeedHub)}>Feeds</button>);
			}
			else if (this.props.mainMode === modesAppInternal.FeedHub) {
				elementArray.push(<button className="admin-button" onClick={() => this.props.setMainMode(modesAppInternal.AdminHub)}>Admin</button>);
			}
			elementArray.push(<button className="default-button" onClick={() => this.props.debugParseSources()}>Scrape RSS</button>);
		}

		return elementArray;
	}



	/**
	 * This toggles the state of whether or not the favourites selector is open
	 *
	 * @memberof BotNav
	 */
	toggleFavouritesSelector(): void {
		this.setState({
			isFavouritesSelectorOpen: !this.state.isFavouritesSelectorOpen,
		});
	}



	/**
	 * This closes the state of whether or not the favourites selector is open
	 *
	 * @memberof BotNav
	 */
	closeFavouritesSelector(): void {
		this.setState({
			isFavouritesSelectorOpen: false,
		});
	}



	/**
	 * This handles the functionality needed when the user clicked log out
	 *
	 * @memberof BotNav
	 */
	logUserOut(): void {
		if (this.props.authToken === "logged_out") {
			this.props.createNotification(modesNotifTypes.Info, "Logged Out", "You are already logged out", null, null);
		}
		else {
			this.props.updateAuthToken("logged_out");
			this.props.createNotification(modesNotifTypes.Success, "Logged Out", "You have been successfully logged out", null, null);
		}
	}



	/**
	 * This renders the BotNav which will be displayed in the main app-y pages
	 *
	 * @return {*}
	 * @memberof BotNav
	 */
	render(): JSX.Element {
		// ---------------------------------------------------------------------
		// SECTION HTML: This is the navigation with all of the options
		// with some additional things where applicable. I.e., if the user is an admin,
		// display the button to switch to admin hub
		// ---------------------------------------------------------------------
		return (
			<Fragment>
				<div className="nav-icon-popup-container">
					{this.state.isFavouritesSelectorOpen ? <FavouritesSelector
						navIconRef={this.refInnerElFavs}
						createNotification={this.props.createNotification}
						spawnFeed={this.props.spawnFeed}
						closeMenu={this.closeFavouritesSelector}
						authToken={this.props.authToken}
						displayMode={modesDisplayOrientation.Mobile}
					/> : ""}
				</div>
				<div className="mobile-nav-container">
					<img
						className="profile-picture"
						src={process.env.REACT_APP_API_URL + this.state.pfpLocation}
						onClick={() => this.props.setMainMode(modesAppInternal.Profile)}
					/>


					<span className="nav-icon add-feed" onClick={() => this.props.spawnFeed(feedTypes.Normal)}/>
					<span className="nav-icon notifications"/>

					{/* This is done as a div because it needs a relatively-positioned parent */}

					{/* This ref is used so that FavouritesSelector knows if the favourites icon was clicked. This is done to
					stop the menu re-opening if the user clicks on the favourites icon while it's open, as clicking off the menu
					will close based on the refs. */}
					<span ref={this.refInnerElFavs} className="nav-icon favourites" onClick={() => this.toggleFavouritesSelector()}/>

					<span className="nav-icon bookmarks" onClick={() => this.props.spawnFeed(feedTypes.Bookmarks)}/>
					<span className="nav-icon help" onClick={() => this.props.setMainMode(modesAppExternal.ExtHelp)}/>
					{/* <span className="nav-icon logout" onClick={this.logUserOut}/> */}
				</div>
			</Fragment>
		);
		// ---------------------------------------------------------------------
		// !SECTION
		// ---------------------------------------------------------------------
	}
}


export default BotNav;
