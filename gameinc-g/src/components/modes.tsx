// -----------------------------------------------------------------------------
// Only allow the following modes within the App
// These are essentially any modes that are rendered in the context of the main app
// -----------------------------------------------------------------------------
enum modesAppInternal {
	Login = "Login",
	Profile = "Profile",
	FeedHub = "FeedHub",
	AdminHub = "AdminHub",
	DebuggerMenu = "DebuggerMenu",
}


// -----------------------------------------------------------------------------
// Only allow the following modes within the App
// These are essentially any modes that are rendered outside of the context of the main app
// -----------------------------------------------------------------------------
enum modesAppExternal {
	ExtHome = "ExtHome",
	ExtHelp = "ExtHelp",
}

enum modesAppBackground {
	GrayColor = "Gray Color",
	GameIngImg = "GameIngImg",
	VantaBirds = "VantaBirds",
}

enum modesNotifTypes {
	Info = "Info",
	Success = "Success",
	Warning = "Warning",
	Error = "Error",
}

enum modesDisplayOrientation {
	Desktop = "Desktop",
	Mobile = "Mobile",
}

export {
	modesAppInternal,
	modesAppExternal,
	modesAppBackground,
	modesNotifTypes,
	modesDisplayOrientation,
};
