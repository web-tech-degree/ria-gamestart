enum modesProfile {
	Main = "Main",
	EditDetails = "EditDetails",
	ChangePassword = "ChangePassword",
}

export {
	modesProfile,
};
