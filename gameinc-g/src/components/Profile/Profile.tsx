import React, {Fragment} from "react";
import "./Profile.css";
import {globals, checkFileHasExtension, getImageResolution} from "../../logic/globals";





// -----------------------------------------------------------------------------
// Types
import type {createNotificationType} from "../types";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Modes
import {modesProfile} from "./modes";
import {modesAppExternal, modesAppInternal, modesNotifTypes} from "../modes";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Components
import PopUp from "../PopUp/PopUp";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Libraries
import {Dropdown} from "react-bootstrap";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Assets
import pfpEditOverlay from "../../assets/material-white/image.svg";
// -----------------------------------------------------------------------------





type ProfileState = {
	mode: modesProfile,
	username: string,
	email: string,
	pfpLocation: string,
	// This is used to store the updated image
	newPfpImageData: File | null,
	// This is either true/false/null. This is done so that we don't instantly kick the
	// user off the page by defaulting to false (which is what the popup message reacts to)
	fetchDetailsSuccess: boolean | null,
	resetPasswordCurrent: string,
	resetPasswordDesired: string,
	resetPasswordDesiredConf: string,
}

type ProfileProps = {
	createNotification: createNotificationType,
	setMainMode: (val: modesAppInternal | modesAppExternal) => void,
	authToken: string,
	updateAuthToken: (val: string) => void,
}





/**
 * This class handles the profile section, with the ability to edit details, change
 * the password,
 *
 * @class Profile
 * @extends {Component}
 */
class Profile extends React.Component<ProfileProps, ProfileState> {
	/**
	 * Creates an instance of Profile.
	 * @param {ProfileProps} props
	 * @memberof Profile
	 */
	constructor(props: ProfileProps) {
		super(props);
		this.state = {
			mode: modesProfile.Main,
			username: "",
			email: "",
			pfpLocation: "",
			newPfpImageData: null,
			fetchDetailsSuccess: null,
			resetPasswordCurrent: "",
			resetPasswordDesired: "",
			resetPasswordDesiredConf: "",
		};

		this.pfpEditRef = React.createRef();

		this.handlePfpEdit = this.handlePfpEdit.bind(this);
		this.changePasswordSubmit = this.changePasswordSubmit.bind(this);
		this.logUserOut = this.logUserOut.bind(this);
	}
	pfpEditRef: React.RefObject<HTMLInputElement>;



	/**
	 * This checks if this component should be "debugged" via the process.env variables. If it's true,
	 * we bind console.log so that logging works normally, otherwise we make it not do anything.
	 *
	 * @param {*} [mode]
	 * @param {...any[]} optionalParams
	 * @memberof class Profile extends React.Component<ProfileProps, ProfileState> {
	 */
	dbgLog(
		// eslint-disable-next-line
		mode?: any,...optionalParams: any[]
	): void {
		if (globals.DEBUGGER_DBG_LOG === "true" && globals.DEBUGGER_DBG_Profile === "true") {
			this.dbgLog = console.log.bind(window.close);
		}
		else {
			this.dbgLog = (): void => {
				return;
			};
		}
	}



	/**
	 * This fetches the logged in user's details when they come on this page
	 *
	 * @memberof Profile
	 */
	componentDidMount(): void {
		this.fetchUserDetails();
	}



	/**
	 * This set's the profile mode based on whatever was passed
	 *
	 * @param {modesProfile} modeToSetTo
	 * @memberof Profile
	 */
	setMode(modeToSetTo: modesProfile): void {
		this.setState({
			mode: modeToSetTo,
		});
	}



	/**
	 * This fethces the user's details from the server and stores them in the state.
	 *
	 * Note that this has nothing to do with the password.
	 *
	 * @memberof Profile
	 */
	fetchUserDetails(): void {
		fetch(process.env.REACT_APP_API_URL + "__api.php", {
			method: "POST",
			credentials: "omit",
			body: JSON.stringify({
				request_type: "profile_fetch_user_details",
				user_auth_token: this.props.authToken,
			}),
		})
			.then((response) => response.json())
			// We expect to receive data matching postData type
			.then((data) => {
				if (data.message === "fetch_success") {
					this.setState({
						username: data.details.username,
						email: data.details.email,
						pfpLocation: data.details.pfp_location,
						fetchDetailsSuccess: true,
					});
				}
				else {
					// Fetching failed, so we'll cause the popup to display
					this.setState({
						fetchDetailsSuccess: false,
					});
				}
			});
	}



	/**
	 * This sets the mode to the default main mode and resets the details so that they are the
	 * ones on the server, rather than whatever they were edited to.
	 *
	 * Note that this has nothing to do with the password.
	 *
	 * @memberof Profile
	 */
	cancelEditUserDetails(): void {
		this.fetchUserDetails();
		this.setMode(modesProfile.Main);
	}



	/**
	 * We update the user details, optionally also updating the image if it was changed. This
	 * is done to not eat unnecessary bandwidth.
	 *
	 * @memberof Profile
	 */
	updateUserDetails(): void {
		/**
		 * This is just an internal function to handle the responses because they'll overlap,
		 * except for the image related stuff
		 *
		 * @param {{message: string}} data
		 */
		const handleRequestResponse = (data: {message: string}) => {
			switch (data.message) {
			case "email_change_failed":
				this.props.createNotification(modesNotifTypes.Error, null, "Attempting to make a request to change your email failed", null, null);
				break;
			case "username_taken":
				this.props.createNotification(modesNotifTypes.Warning, null, "That username is already taken", null, null);
				break;
			case "image_failed":
				this.props.createNotification(modesNotifTypes.Error, null, "Updating your profile picture failed, but we " +
				"proceeded to try to update any other details you updated", null, null);
				break;
			case "missing_fields":
				this.props.createNotification(modesNotifTypes.Warning, null, "It seems that some fields were missing, " +
				"assuming this isn't an error on our end, please re-check your details", null, null);
				break;
			case "missing_image_field":
				this.props.createNotification(modesNotifTypes.Error, null, "It seems you tried to update your profile picture, but no picture was uploaded", null, null);
				break;
			case "update_success":
				// Update was successful, so we re-fetch their details and inform them of success. Also exit edit mode
				// and go back to main mode
				this.fetchUserDetails();
				this.setMode(modesProfile.Main);
				this.props.createNotification(modesNotifTypes.Success, null, "Details updated!", null, null);
				break;
			}
		};

		// Here we make the request to update the user details alongside with the profile picture
		if (this.state.newPfpImageData !== null) {
			const formData = new FormData();

			// Because we want to send an image and some JSON data, we append the JSON data
			// to a field called "json", which the PHP will check against
			formData.append("json", JSON.stringify({
				request_type: "profile_update_user_details_and_pfp",
				user_auth_token: this.props.authToken,
				username: this.state.username,
				email: this.state.email,
			}));
			// Here we append the actual image, which we'll access with $_FILES["pfp_image"]
			formData.append("pfp_image", this.state.newPfpImageData);

			fetch(process.env.REACT_APP_API_URL + "__api.php", {
				method: "POST",
				credentials: "omit",
				body: formData,
			})
				.then((response) => response.json())
				.then((data) => {
					handleRequestResponse(data);
				});
		}
		// Here we make the request to update user details without changing the profile picture
		else {
			fetch(process.env.REACT_APP_API_URL + "__api.php", {
				method: "POST",
				credentials: "omit",
				body: JSON.stringify({
					request_type: "profile_update_user_details",
					user_auth_token: this.props.authToken,
					username: this.state.username,
					email: this.state.email,
				}),
			})
				.then((response) => response.json())
				.then((data) => {
					handleRequestResponse(data);
				});
		}
	}



	/**
	 *
	 *
	 * @param {React.ChangeEvent<HTMLInputElement>} event
	 * @memberof Profile
	 */
	handlePfpEdit(event: React.ChangeEvent<HTMLInputElement>): void {
		const acceptableFileTypes = [".jpg", ".jpeg", ".png"];
		let imgW: number;
		let imgH: number;

		let imgFile: File;
		// Stop if null
		// This is needed for getImageResolution to work
		if (event.target.files === null || event.target.files[0] === null) {
			return;
		}
		else {
			imgFile = event.target.files[0];
		}

		// This function tries to get the image resolution, and on callback we expect
		// to receive the width and height, which we then assign
		getImageResolution(event.target.files[0], (width: number, height: number) => {
			imgW = width;
			imgH = height;

			// If the image res is larger than a 3840x2160 image (arbitrary random number I chose)
			// then we don't continue
			if ((imgW * imgH) > 8294400) {
				this.props.createNotification(modesNotifTypes.Warning, "Image Too Large",
					"Sorry, the resolution of the image you chose is too large", 10000, null);
				return;
			}

			// Make sure image is of the specified formats
			if (!checkFileHasExtension(imgFile.name, acceptableFileTypes)) {
				this.props.createNotification(modesNotifTypes.Warning, "Incorrect Image File Format", "We currently do not support the file format you selected", 10000, null);
				return;
			}

			// NOTE: Temporary check because there's some arbitrary bug on the server which makes the page refresh after a PNG upload
			// We do not return from here because this is not a "fatal" error or anything, just something we need to inform the user about
			if (checkFileHasExtension(imgFile.name, ["png"])) {
				this.props.createNotification(modesNotifTypes.Info, "Image File Format",
					"We currently support PNGs, but after uploading, the page will refresh." +
					"This arbitrarily happens with most, but not all, PNGs. We're sorry about this bug.", 20000, null);
			}

			// Make sure the file is smaller then 4MiB
			if (imgFile.size/1024/1024 > 4) {
				this.props.createNotification(modesNotifTypes.Info, "Image Size", "Please upload an image under 4MiB", null, null);
				return;
			}
			// If we get here, all the checks were fine
			this.setState(
				{
					newPfpImageData: imgFile,
				});
			return;
		});
	}



	/**
	 * This sends a request to change the password, using the auth token and requiring
	 * the user to confirm their current password.
	 *
	 * @param {React.FormEvent<HTMLFormElement>} event
	 * @memberof Profile
	 */
	changePasswordSubmit(event: React.FormEvent<HTMLFormElement>): void {
		event.preventDefault();

		this.dbgLog("Profile: changePasswordSubmit");

		// First check that the passwords are the same
		if (this.state.resetPasswordDesired !== this.state.resetPasswordDesiredConf) {
			this.props.createNotification(modesNotifTypes.Warning, "Mismatched Passwords", "Please make sure the passwords you entered are the same", null, null);
			return;
		}

		fetch(process.env.REACT_APP_API_URL + "__api.php", {
			method: "POST",
			credentials: "omit",
			body: JSON.stringify({
				request_type: "user_normal_change_password",
				user_auth_token: this.props.authToken,
				current_password: this.state.resetPasswordCurrent,
				desired_password: this.state.resetPasswordDesired,
				desired_password_conf: this.state.resetPasswordDesiredConf,
			}),
		})
			.then((response) => response.json())
			.then((data) => {
				this.dbgLog("Profile: changePasswordSubmit: response");
				if (data.message === "password_change_success") {
					this.dbgLog("Profile: changePasswordSubmit: success");
					this.props.createNotification(modesNotifTypes.Success, "Password Reset Success", null, null, null);
					// Update the auth token
					this.props.updateAuthToken(data.jwt);

					// Reset state of the password change fields
					this.setState({
						resetPasswordCurrent: "",
						resetPasswordDesired: "",
						resetPasswordDesiredConf: "",
					});
				}
				else if (data.message === "incorrect_password") {
					this.props.createNotification(modesNotifTypes.Warning, "Incorrect Password", "Please check your current password is accurate", null, null);
				}
				else if (data.message === "mismatch_new_passwords") {
					this.props.createNotification(modesNotifTypes.Warning, "Mismatched Passwords", "Please make sure the passwords you entered are the same", null, null);
				}
			});
	}


	/**
	 * This handles the functionality needed when the user clicked log out
	 *
	 * @memberof Profile
	 */
	logUserOut(): void {
		this.props.updateAuthToken("logged_out");
		this.props.setMainMode(modesAppInternal.FeedHub);
		this.props.createNotification(modesNotifTypes.Success, "Logged Out", "You have been successfully logged out", null, null);
	}



	/**
	 *
	 *
	 * @memberof Profile
	 */
	deleteUserAccount(): void {
		//
	}



	/**
	 *
	 *
	 * @return {*}  {JSX.Element}
	 * @memberof Profile
	 */
	renderRightSectionTopBar(): JSX.Element {
		return <div className="top-bar">
			<Dropdown>
				<Dropdown.Toggle className="profile-dropdown">
					Settings
				</Dropdown.Toggle>

				<Dropdown.Menu>
					<Dropdown.Item onClick={() => this.setMode(modesProfile.Main)} >View Profile</Dropdown.Item>
					<Dropdown.Item onClick={() => this.setMode(modesProfile.EditDetails)} >Edit Details</Dropdown.Item>
					<Dropdown.Item onClick={() => this.setMode(modesProfile.ChangePassword)} >Change Password</Dropdown.Item>
					<Dropdown.Divider/>
					<Dropdown.Item onClick={() => this.logUserOut()} >Logout</Dropdown.Item>
					<Dropdown.Divider/>
					<Dropdown.Divider/>
					<Dropdown.Item onClick={() => this.deleteUserAccount()} className="profile-dropdown-warning" >Delete Account</Dropdown.Item>
				</Dropdown.Menu>
			</Dropdown>
		</div>;
	}



	/**
	 * This is what is rendered when in the default profile page
	 *
	 * @return {*}  {JSX.Element}
	 * @memberof Profile
	 */
	renderRightMain(): JSX.Element {
		// ---------------------------------------------------------------------
		// SECTION HTML: Right Section Default profile
		// ---------------------------------------------------------------------
		return <div className="profile-right-container">

			{this.renderRightSectionTopBar()}

			<div className="mid-content">
				<label htmlFor="username"></label>
				<p className="profile-details"> <b>Username: </b> {this.state.username}</p>
				<label htmlFor="email"></label>
				<p className="profile-details"> <b>Email: </b> {this.state.email} </p>
			</div>

			<div className="bot-bar">

			</div>
		</div>;
		// ---------------------------------------------------------------------
		// !SECTION
		// ---------------------------------------------------------------------
	}



	/**
	 * This is what is rendered when the user is editing their details
	 *
	 * @return {*}  {JSX.Element}
	 * @memberof Profile
	 */
	renderRightEditDetails(): JSX.Element {
		// ---------------------------------------------------------------------
		// SECTION HTML: Right Section Editing Details
		// ---------------------------------------------------------------------
		return <div className="profile-right-container">

			{this.renderRightSectionTopBar()}

			<div className="mid-content">

				<div className="main-details">
					<label htmlFor="username"></label>
					<input type="text" name="username" value={this.state.username} onChange={(event) => this.setState({username: event.target.value})}/>
					<label htmlFor="email"></label>
					<input type="text" name="email" value={this.state.email} onChange={(event) => this.setState({email: event.target.value})}/>
				</div>

				<div className="pfp-edit">
					{/* The profile picture uploading is done such that the label has to be clicked, and the input tag is hidden.
					Then when the label is clicked, it tells the input element that it was clicked, thus triggering the upload prompt. */}
					<label htmlFor="pfp-upload" onClick={() => this.pfpEditRef.current?.click()}>Upload Profile Picture</label>
					<input type="file" name="pfp-upload" ref={this.pfpEditRef} onChange={this.handlePfpEdit}/>
				</div>

				<div className="edit-actions">
					<button className="default-button" onClick={() => this.cancelEditUserDetails()}>Cancel</button>
					<button className="default-button" onClick={() => this.updateUserDetails()}>Save Details</button>
				</div>

			</div>

			<div className="bot-bar">

			</div>
		</div>;
		// ---------------------------------------------------------------------
		// !SECTION
		// ---------------------------------------------------------------------
	}



	/**
	 * This is what is rendered when the user is changing their password
	 *
	 * @return {*}  {JSX.Element}
	 * @memberof Profile
	 */
	renderRightChangePassword(): JSX.Element {
		// ---------------------------------------------------------------------
		// SECTION HTML: Right Section Changing Password
		// ---------------------------------------------------------------------
		return <div className="profile-right-container">

			{this.renderRightSectionTopBar()}

			<div className="mid-content">

				<div className="main-details">
					{/* <div className="align-items-center p-5 justify-content-center"> */}
					<form className="change-password-form" onSubmit={this.changePasswordSubmit}>
						<h5>Reset Password</h5>
						<div className="change-password-form-el">
							<label htmlFor="current-pw">Current Password</label>
							<input type="password" id="current-pw" placeholder="Type Here..." required
								onChange={(event) => this.setState({resetPasswordCurrent: event.target.value})}/>
						</div>
						<div className="change-password-form-el">
							<label htmlFor="password">New Password</label>
							<input type="password" id="password" placeholder="Type Here..." required
								onChange={(event) => this.setState({resetPasswordDesired: event.target.value})}/>
						</div>
						<div className="change-password-form-el">
							<label htmlFor="password-conf">Confirm New Password</label>
							<input type="password" id="password-conf" placeholder="Type Here..." required
								onChange={(event) => this.setState({resetPasswordDesiredConf: event.target.value})}/>
						</div>
						<div className="change-password-form-el buttons">
							<button className="default-button" onClick={() => this.cancelEditUserDetails()}>Cancel</button>
							<button className="default-button" type="submit">Submit</button>
						</div>
					</form>
					{/* </div> */}
				</div>


				<div className="edit-actions">
				</div>

			</div>

			<div className="bot-bar">

			</div>
		</div>;


		// ---------------------------------------------------------------------
		// !SECTION
		// ---------------------------------------------------------------------
	}



	/**
	 * This renders the "left" section, which can be moved to the top, based on whether it's
	 * just displaying, or if the user is in EditDetails mode which allows clicking on the image
	 * to upload a pfp.
	 *
	 * @return {*}  {JSX.Element}
	 * @memberof Profile
	 */
	renderLeftSectionBasedOnMode(): JSX.Element {
		// Don't render anything if we couldn't fetch details
		if (this.state.fetchDetailsSuccess !== true) {
			return <Fragment></Fragment>;
		}


		// Here we render based on what mode the profile page is in
		if (this.state.mode === modesProfile.EditDetails) {
			// -----------------------------------------------------------------
			// SECTION HTML: Left Section Editing
			// -----------------------------------------------------------------
			return <div className="profile-left-container">
				<div className="top-bar-overlay">
					<span className="back-button" onClick={() => this.props.setMainMode(modesAppInternal.FeedHub)}></span>

				</div>

				<div className="mid-content">
					<div className="pfp-wrapper">
						{/* If a new pfp image was not set, we set the src to be the old image. Whereas if the user did upload a new image,
					we'll render it here by using URL.createObjectURL */}
						<div className="pfp-container">
							<img className="pfp-image-editing-overlay"
								src={pfpEditOverlay}
							>

							</img>
							<img className="pfp-image editing"
								src={this.state.newPfpImageData !== null ? URL.createObjectURL(this.state.newPfpImageData) : process.env.REACT_APP_API_URL + this.state.pfpLocation}
								// This hooks into the pfpEditRef input element, which is set to display: none
								onClick={() => this.pfpEditRef.current?.click()}
							>
							</img>
						</div>
					</div>
				</div>
			</div>;
			// -----------------------------------------------------------------
			// !SECTION
			// -----------------------------------------------------------------
		}
		else {
			// -----------------------------------------------------------------
			// SECTION HTML: Left Section All Others
			// -----------------------------------------------------------------
			return <div className="profile-left-container">

				<div className="top-bar-overlay">
					<span className="back-button" onClick={() => this.props.setMainMode(modesAppInternal.FeedHub)}></span>

				</div>

				<div className="mid-content">
					<div className="pfp-wrapper">
						<div className="pfp-container">
							<img className="pfp-image" src={process.env.REACT_APP_API_URL + this.state.pfpLocation}></img>
						</div>
					</div>
				</div>
			</div>;
			// -----------------------------------------------------------------
			// !SECTION
			// -----------------------------------------------------------------
		}
	}



	/**
	 * This handles the rendering of the main profile bits based on what the mode is set to
	 *
	 * @return {*}  {JSX.Element}
	 * @memberof Profile
	 */
	renderRightSectionBasedOnMode(): JSX.Element {
		// Don't render anything if we couldn't fetch details
		if (this.state.fetchDetailsSuccess !== true) {
			return <Fragment></Fragment>;
		}

		// Here we render based on what mode the profile page is in
		switch (this.state.mode) {
		case modesProfile.Main:
			return this.renderRightMain();
		case modesProfile.EditDetails:
			return this.renderRightEditDetails();
		case modesProfile.ChangePassword:
			return this.renderRightChangePassword();
		default:
			return <Fragment></Fragment>;
		}
	}



	/**
	 *
	 *
	 * @return {*}  {JSX.Element}
	 * @memberof Profile
	 */
	render(): JSX.Element {
		return (
			<Fragment>
				<div className="profile-wrapper">
					{this.renderLeftSectionBasedOnMode()}
					{this.renderRightSectionBasedOnMode()}
				</div>

				{/* This renders a popup message is fetching details failed, prompting the user to either go to login
				page or to feedhub page */}
				{this.state.fetchDetailsSuccess === false ?
					<PopUp message={"You are not logged in, would you like to login or return to feeds page?"}
						options={
							[
								{
									text: "Login",
									callback: () => {
										this.props.setMainMode(modesAppInternal.Login);
									},
								},
								{
									text: "Feeds",
									callback: () => {
										this.props.setMainMode(modesAppInternal.FeedHub);
									},
								},
							]
						}/> :
					""
				}
			</Fragment>
		);
	}
}

export default Profile;
