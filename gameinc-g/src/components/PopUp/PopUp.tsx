import React from "react";
import "./PopUp.css";





// -----------------------------------------------------------------------------
// Types
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Modes
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Components
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Libraries
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Assets
// -----------------------------------------------------------------------------





type propsOptions = {
	text: string,
	callback: () => void,
}

type PopUpProps = {
	// This is the message, which we also allow to be a JSX.Element so that the caller
	// can create a HTML message
	message: string | JSX.Element,
	// The way this is set up, we can allow any arbitrary number of options
	// and each option will have a callback function on click
	options: Array<propsOptions>
	innerRef?: React.RefObject<HTMLDivElement>,
}





/**
 * This is a PopUp that will be drawn above the screen, it can be provided
 * with callbacks and custom option text. HTML can also be passed into the main message.
 *
 * @class PopUp
 * @extends {Component}
 */
class PopUp extends React.Component<PopUpProps> {
	/**
	 * Creates an instance of PopUp.
	 * @param {PopUpProps} props
	 * @memberof PopUp
	 */
	constructor(props: PopUpProps) {
		super(props);
	}



	/**
	 * Render the PopUp, giving it a ref if one was actually passed.
	 *
	 * @return {*}  {JSX.Element}
	 * @memberof PopUp
	 */
	render(): JSX.Element {
		return (
			<div ref={this.props.innerRef} className="modal">
				<div className="modal-content">
					{this.props.message}

					{/* Render every option, showing the text for the option and
					assigning the correct callback function */}
					{this.props.options.map((option) => {
						return <button className="default-button" key={option.text} onClick={option.callback}>{option.text}</button>;
					})}
				</div>
			</div>
		);
	}
}

export default PopUp;
