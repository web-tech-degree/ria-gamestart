import React, {Fragment} from "react";
import "./DebuggerMenu.css";





// -----------------------------------------------------------------------------
// Types
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Modes
import {modesAppInternal, modesAppExternal, modesAppBackground} from "../modes";
import {globals} from "../../logic/globals";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Components
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Libraries
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Assets
// -----------------------------------------------------------------------------





type DebuggerMenuProps = {
	setMainMode: (modeToSetTo: modesAppInternal | modesAppExternal) => void,
}





/**
 * This class will handle the rendering of the DebuggerMenu page.
 *
 * @class DebuggerMenu
 * @extends {React.Component<DebuggerMenuProps, DebuggerMenuState>}
 */
class DebuggerMenu extends React.Component<DebuggerMenuProps> {
	/**
	 * Creates an instance of DebuggerMenu. See the documentation of App.tsx for how this works; this is the same thing
	 * but different names etc.
	 *
	 * @param {DebuggerMenuProps} props
	 * @memberof DebuggerMenu
	 */
	constructor(props: DebuggerMenuProps) {
		super(props);
	}



	/**
	 * Render debugger options
	 *
	 * @return {*}  {JSX.Element}
	 * @memberof DebuggerMenu
	 */
	render(): JSX.Element {
		return (
			<Fragment>
				<div className="ugly-debugger-menu">
					<p className="text-light">The top option, Activate/Disable Debugging, toggles whether or not ANY debug loggers will run, but it does not explicitly enable any</p>

					{/* I'm aware this is extremely ugly, but I couldn't be bothered moving the debugger options into their own object/array, which would allow mapping the buttons
					or using a for loop*/}

					{globals.DEBUGGER_DBG_LOG === "true" ? <button className="admin-button" onClick={() => globals.DEBUGGER_DBG_LOG = "false"}>Disable Debugging</button> :
						<button className="admin-button" onClick={() => globals.DEBUGGER_DBG_LOG = "true"}>Enable Debugging</button>}


					{globals.DEBUGGER_DBG_App === "true" ? <button className="admin-button" onClick={() => globals.DEBUGGER_DBG_App = "false"}>Deactivate Debugging App</button> :
						<button className="admin-button" onClick={() => globals.DEBUGGER_DBG_App = "true"}>Activate Debugging App</button>}


					{globals.DEBUGGER_DBG_AdminHub === "true" ? <button className="admin-button" onClick={() => globals.DEBUGGER_DBG_AdminHub = "false"}>Deactivate Debugging AdminHub</button> :
						<button className="admin-button" onClick={() => globals.DEBUGGER_DBG_AdminHub = "true"}>Activate Debugging AdminHub</button>}


					{globals.DEBUGGER_DBG_FeedEditor === "true" ? <button className="admin-button" onClick={() => globals.DEBUGGER_DBG_FeedEditor = "false"}>Deactivate Debugging FeedEditor</button> :
						<button className="admin-button" onClick={() => globals.DEBUGGER_DBG_FeedEditor = "true"}>Activate Debugging FeedEditor</button>}


					{globals.DEBUGGER_DBG_SourceEditor === "true" ? <button className="admin-button" onClick={() => globals.DEBUGGER_DBG_SourceEditor = "false"}>Deactivate Debugging SourceEditor</button> :
						<button className="admin-button" onClick={() => globals.DEBUGGER_DBG_SourceEditor = "true"}>Activate Debugging SourceEditor</button>}


					{globals.DEBUGGER_DBG_FeedHub === "true" ? <button className="admin-button" onClick={() => globals.DEBUGGER_DBG_FeedHub = "false"}>Deactivate Debugging FeedHub</button> :
						<button className="admin-button" onClick={() => globals.DEBUGGER_DBG_FeedHub = "true"}>Activate Debugging FeedHub</button>}


					{globals.DEBUGGER_DBG_Feed === "true" ? <button className="admin-button" onClick={() => globals.DEBUGGER_DBG_Feed = "false"}>Deactivate Debugging Feed</button> :
						<button className="admin-button" onClick={() => globals.DEBUGGER_DBG_Feed = "true"}>Activate Debugging Feed</button>}


					{globals.DEBUGGER_DBG_FeedFavourites === "true" ? <button className="admin-button" onClick={() => globals.DEBUGGER_DBG_FeedFavourites = "false"}>Deactivate Debugging FeedFavourites</button> :
						<button className="admin-button" onClick={() => globals.DEBUGGER_DBG_FeedFavourites = "true"}>Activate Debugging FeedFavourites</button>}


					{globals.DEBUGGER_DBG_Item === "true" ? <button className="admin-button" onClick={() => globals.DEBUGGER_DBG_Item = "false"}>Deactivate Debugging Item</button> :
						<button className="admin-button" onClick={() => globals.DEBUGGER_DBG_Item = "true"}>Activate Debugging Item</button>}


					{globals.DEBUGGER_DBG_ItemLightbox === "true" ? <button className="admin-button" onClick={() => globals.DEBUGGER_DBG_ItemLightbox = "false"}>Deactivate Debugging ItemLightbox</button> :
						<button className="admin-button" onClick={() => globals.DEBUGGER_DBG_ItemLightbox = "true"}>Activate Debugging ItemLightbox</button>}


					{globals.DEBUGGER_DBG_LeftNav === "true" ? <button className="admin-button" onClick={() => globals.DEBUGGER_DBG_LeftNav = "false"}>Deactivate Debugging LeftNav</button> :
						<button className="admin-button" onClick={() => globals.DEBUGGER_DBG_LeftNav = "true"}>Activate Debugging LeftNav</button>}


					{globals.DEBUGGER_DBG_BotNav === "true" ? <button className="admin-button" onClick={() => globals.DEBUGGER_DBG_BotNav = "false"}>Deactivate Debugging BotNav</button> :
						<button className="admin-button" onClick={() => globals.DEBUGGER_DBG_BotNav = "true"}>Activate Debugging BotNav</button>}


					{globals.DEBUGGER_DBG_LoginLanding === "true" ? <button className="admin-button" onClick={() => globals.DEBUGGER_DBG_LoginLanding = "false"}>Deactivate Debugging LoginLanding</button> :
						<button className="admin-button" onClick={() => globals.DEBUGGER_DBG_LoginLanding = "true"}>Activate Debugging LoginLanding</button>}


					{globals.DEBUGGER_DBG_Register === "true" ? <button className="admin-button" onClick={() => globals.DEBUGGER_DBG_Register = "false"}>Deactivate Debugging Register</button> :
						<button className="admin-button" onClick={() => globals.DEBUGGER_DBG_Register = "true"}>Activate Debugging Register</button>}


					{globals.DEBUGGER_DBG_Login === "true" ? <button className="admin-button" onClick={() => globals.DEBUGGER_DBG_Login = "false"}>Deactivate Debugging Login</button> :
						<button className="admin-button" onClick={() => globals.DEBUGGER_DBG_Login = "true"}>Activate Debugging Login</button>}


					{globals.DEBUGGER_DBG_ForgotPassword === "true" ? <button className="admin-button" onClick={() => globals.DEBUGGER_DBG_ForgotPassword = "false"}>Deactivate Debugging ForgotPassword</button> :
						<button className="admin-button" onClick={() => globals.DEBUGGER_DBG_ForgotPassword = "true"}>Activate Debugging ForgotPassword</button>}


					{globals.DEBUGGER_DBG_ReconfirmEmail === "true" ? <button className="admin-button" onClick={() => globals.DEBUGGER_DBG_ReconfirmEmail = "false"}>Deactivate Debugging ReconfirmEmail</button> :
						<button className="admin-button" onClick={() => globals.DEBUGGER_DBG_ReconfirmEmail = "true"}>Activate Debugging ReconfirmEmail</button>}


					{globals.DEBUGGER_DBG_Profile === "true" ? <button className="admin-button" onClick={() => globals.DEBUGGER_DBG_Profile = "false"}>Deactivate Debugging Profile</button> :
						<button className="admin-button" onClick={() => globals.DEBUGGER_DBG_Profile = "true"}>Activate Debugging Profile</button>}


				</div>
			</Fragment>
		);
	}
}

export default DebuggerMenu;
