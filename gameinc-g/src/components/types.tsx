import {feedTypes} from "./FeedHub/modes";
import {modesNotifTypes} from "./modes";

type createNotificationType = (type: modesNotifTypes, title: string | null, body: string | null, time: number | null, callback: (() => void) | null) => void;

type appFeedAdderType = {
	shouldAdd: boolean,
	type: feedTypes,
	extraId?: number
}

export type {
	createNotificationType,
	appFeedAdderType,
};
