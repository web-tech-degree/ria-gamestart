import React, {Fragment} from "react";
import {copyTextToClipboard, globals} from "../../logic/globals";
import "./Item.css";





// -----------------------------------------------------------------------------
// Types
import type {postItemDetails} from "./types";
import type {createNotificationType} from "../types";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Modes
import {modesNotifTypes} from "../modes";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Components
import PopUp from "../PopUp/PopUp";
import Bookmarker from "./Bookmarker";
import VoteButtonDown from "./VoteButtonDown";
import VoteButtonUp from "./VoteButtonUp";
import Favouritiser from "./Favouritiser";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Libraries
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Assets
// -----------------------------------------------------------------------------





type ItemState = {
	showCopyPopup: boolean,
	postLink: string,
	postItemDetails: postItemDetails,
}

type ItemProps = {
	createNotification: createNotificationType,
	authToken: string,
	postItemDetails: postItemDetails,
	openItemInLightbox: (postDataToOpen: postItemDetails, isFromBookmarks: boolean) => void,
	isItemFromBookmarks: boolean,
	itemPostIdsToRefresh: number[],
	setLightboxToForceRefreshOnNextOpen: () => void,
}

type detailUpdateRet = {
	message: string,
	postDetails: postItemDetails,
}





/**
 *
 *
 * @class Item
 * @extends {React.Component}
 */
class Item extends React.Component<ItemProps, ItemState> {
	/**
	 * Creates an instance of Item.
	 * @param {ItemProps} props
	 * @memberof Item
	 */
	constructor(props: ItemProps) {
		super(props);
		this.state = {
			showCopyPopup: false,
			postLink: "",
			// This looks quite horrific, but we need to initialise the object... and it has a bunch of
			// properties
			postItemDetails: {
				postitem_id: 0,
				hash: "",
				title: "",
				url: "",
				content: "",
				date: "",
				author: "",
				total_votes: 0,
				user_vote_status: 0,
				user_bookmark_status: 0,
				user_fav_source_status: 0,
				source_id: 0,
				source_name: "",
				source_url: "",
				source_avatar: "",
				parentFeed: {feed_id: 0, name: ""},
			},
		};

		this.dbgLog();

		this.generateAndCopyLink = this.generateAndCopyLink.bind(this);
		this.updateSelfDetails = this.updateSelfDetails.bind(this);
	}



	/**
	 * This checks if this component should be "debugged" via the process.env variables. If it's true,
	 * we bind console.log so that logging works normally, otherwise we make it not do anything.
	 *
	 * @param {*} [mode]
	 * @param {...any[]} optionalParams
	 * @memberof Login
	 */
	dbgLog(
		// eslint-disable-next-line
		mode?: any,...optionalParams: any[]
	): void {
		if (globals.DEBUGGER_DBG_LOG === "true" && globals.DEBUGGER_DBG_Item === "true") {
			this.dbgLog = console.log.bind(window.close);
		}
		else {
			this.dbgLog = (): void => {
				return;
			};
		}
	}



	/**
	 * When an item is mounted, we copy the props into the state because we'll be potentially "self-managing"
	 * the item's details, for which we'll need a state.
	 *
	 * NOTE that this implementation may be VERY finnicky and not be maintainable in the case that the Feed
	 * ALSO needs to modify the Item, but if that's the case, we'll need to also modify the implementation
	 * in Feed so that instead of spawning Items and "disgarding" their representative state (basically, currently,
	 * it spawns the Items and then purges the postItemDetails array and only leaves the JSX.Element array), will
	 * need to remain or whatever. I don't know.
	 *
	 * @memberof Item
	 */
	componentDidMount(): void {
		this.setState({
			postItemDetails: this.props.postItemDetails,
		});
	}



	/**
	 * Whenever the component updates, and the array keeping track of which Items to update changed,
	 * then loop over the array and if any of the postitem_ids that should update match the
	 * postitem_id of this specific Item, then trigger the update for the Item.
	 *
	 * @param {ItemProps} prevProps
	 * @memberof Item
	 */
	componentDidUpdate(prevProps: ItemProps): void {
		if (prevProps.itemPostIdsToRefresh !== this.props.itemPostIdsToRefresh) {
			this.props.itemPostIdsToRefresh.forEach((arrItem) => {
				if (arrItem === this.props.postItemDetails.postitem_id) {
					this.updateSelfDetails();
				}
			});
		}
	}



	/**
	 * This tries to generate a link to link to the selected post item, falling back to using a popup.
	 *
	 * @memberof Item
	 */
	generateAndCopyLink(): void {
		console.log(this.props.itemPostIdsToRefresh);

		// We generate the link
		this.setState({
			postLink: window.location.protocol +
				"//" +
				window.location.hostname +
				// If there's a port, we add the port and the ":", otherwise we don't add anything
				(location.port ?
					":" + location.port :
					""
				) +
				"?post_item_id=" +
				this.props.postItemDetails.postitem_id +
				"&feed_name=" +
				this.props.postItemDetails.parentFeed.name,
		}, () => {
			// On callback, we try to copy to clipboard
			if (copyTextToClipboard(this.state.postLink)) {
				// On success, we inform the user
				this.props.createNotification(modesNotifTypes.Success, "Link Copied", "We copied the link to this post to your clipboard", null, null);
			}
			else {
				// If the copy fails, set state to show the popup
				this.setState({showCopyPopup: true});
			}
		});
	}



	/**
	 * This method is used to update *ONLY* this specific post's details. This is used so that
	 * we can re-fetch the details properly from the server (making sure they're in sync)
	 * after actions such as upvote and bookmark toggling
	 *
	 * @memberof Item
	 */
	updateSelfDetails(): void {
		this.dbgLog("Item: updateSelfDetails");

		// NOTE: This whole fetch's data could be skimmed down to only fetch the bookmarks
		// and vote info, but I'm lazy.
		fetch(process.env.REACT_APP_API_URL + "__api.php", {
			method: "POST",
			credentials: "omit",
			body: JSON.stringify({
				request_type: "refetch_post_within_feed",
				user_auth_token: this.props.authToken,
				post_id: this.state.postItemDetails.postitem_id,
			}),
		})
			.then((response) => response.json())
			.then((data: detailUpdateRet) => {
				if (data.message === "fetch_success") {
					this.setState({
						postItemDetails: {
							postitem_id: this.state.postItemDetails.postitem_id,
							hash: data.postDetails.hash,
							title: data.postDetails.title,
							url: data.postDetails.url,
							content: data.postDetails.content,
							date: data.postDetails.date,
							author: data.postDetails.author,
							total_votes: data.postDetails.total_votes,
							user_vote_status: data.postDetails.user_vote_status,
							user_bookmark_status: data.postDetails.user_bookmark_status,
							user_fav_source_status: data.postDetails.user_fav_source_status,
							source_id: data.postDetails.source_id,
							source_name: data.postDetails.source_name,
							source_url: data.postDetails.source_url,
							source_avatar: data.postDetails.source_avatar,
							// NOTE We explicitly do NOT change the parentFeed details because we still need them
							parentFeed: {feed_id: this.state.postItemDetails.parentFeed.feed_id, name: this.state.postItemDetails.parentFeed.name},
						},
					});
				}
				else {
					this.props.createNotification(modesNotifTypes.Error, null, "Re-fetching post details error", null, null);
				}
			});
	}

	// -------------------------------------------------------------------------
	// TODO: Add upvote/downvote fetches here
	// -------------------------------------------------------------------------



	/**
	 *
	 *
	 * @return {*}
	 * @memberof Item
	 */
	render(): JSX.Element {
		// This generates an Item based on the props passed to it from Grid
		return (
			// -----------------------------------------------------------------
			// SECTION HTML: Style this to however each post item should look
			// -----------------------------------------------------------------
			<div className="feed-post-item">

				<div className="inner-margin">

					<div className="source-bar-container">
						<div className="source-bar-left">
							{/* Because the bookmarks don't store their source, their source-name will be undefined */}
							{this.state.postItemDetails.source_name !== undefined ? <h3>{this.state.postItemDetails.source_name}</h3> : <h3>unknown</h3>}
						</div>
						<div className="source-bar-right">
							<span className="copy-link" onClick={this.generateAndCopyLink}/>
							<Bookmarker
								bookmarkStatus={this.state.postItemDetails.user_bookmark_status}
								authToken={this.props.authToken}
								postId={this.state.postItemDetails.postitem_id}
								updateParent={this.updateSelfDetails}
								createNotification={this.props.createNotification}
								isItemFromBookmarks={this.props.isItemFromBookmarks}
								setLightboxToForceRefreshOnNextOpen={this.props.setLightboxToForceRefreshOnNextOpen}
							/>
						</div>
					</div>



					{/* Only render the author if there actually is an author provided */}
					{this.state.postItemDetails.author.length > 0 ? <h3>{this.state.postItemDetails.author}</h3> : ""}

					{/* <b>id is {this.props.postItemDetails.elementId}</b> */}
					<p>{this.state.postItemDetails.title}</p>
					{/* The content will be clickable, and onClick it will display the lightbox */}
					<div className="feed-post-item-content" onClick={() => this.props.openItemInLightbox(this.state.postItemDetails, this.props.isItemFromBookmarks)}>
						<p>{this.state.postItemDetails.content}</p>
					</div>



					{/*
				This is so unbelievably hacky, I know. But basically, if showCopyPopup is set to true, we render a popup with the message
				containing the concatenated string of https + domain + params, which will cause the app to open the lightbox when entered.
				This is also done as a popup in case copying fails, which may or may not actually happen.
				*/}
					{this.state.showCopyPopup ?
						<PopUp
							message=
								{
									<Fragment>
										<p className="text-dark">Here&apos;s the link to share this post</p>
										<code>{this.state.postLink}</code>
										<i className="text-dark">We opened this popup because we couldn&apos;t copy the link to your clipbaord</i>
									</Fragment>
								}
							options={
								[
									{
										text: "Ok",
										callback: () => {
											this.setState({
												showCopyPopup: false,
											});
										},
									},
								]
							}
						/> : ""
					}



					<div className="bottom-bar">
						{/* We only render the vote options if the item is not from bookmarks */}
						{this.props.isItemFromBookmarks ? "" :

							<Fragment>

								<span className="total-votes">{this.state.postItemDetails.total_votes}</span>

								<VoteButtonUp
									voteStatus={this.state.postItemDetails.user_vote_status}
									authToken={this.props.authToken}
									postId={this.state.postItemDetails.postitem_id}
									updateParent={this.updateSelfDetails}
									createNotification={this.props.createNotification}
									setLightboxToForceRefreshOnNextOpen={this.props.setLightboxToForceRefreshOnNextOpen}
								/>
								<VoteButtonDown
									voteStatus={this.state.postItemDetails.user_vote_status}
									authToken={this.props.authToken}
									postId={this.state.postItemDetails.postitem_id}
									updateParent={this.updateSelfDetails}
									createNotification={this.props.createNotification}
									setLightboxToForceRefreshOnNextOpen={this.props.setLightboxToForceRefreshOnNextOpen}
								/>
							</Fragment>

						}
					</div>

				</div>
			</div>
		);
		// ---------------------------------------------------------------------
		// !SECTION
		// ---------------------------------------------------------------------
	}
}

export default Item;
