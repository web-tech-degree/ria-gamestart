import React, {Component, Fragment} from "react";
import {authTokenFormat} from "../../logic/globals";
import "./Comment.css";





// -----------------------------------------------------------------------------
// Types
import type {postUserComment} from "./types";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Modes
import {modeCommentBox} from "./modes";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Components
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Libraries
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Assets
// -----------------------------------------------------------------------------





type CommeentState = {
	commentBox: boolean,
	commentBoxMode: modeCommentBox,
	commentText: string,
	commentTextEdit: string,
	commentData: postUserComment | null,
}
type CommentProps = {
	authToken: string,
	authTokenDecoded: authTokenFormat | null,
	rootCommentIsOpen: boolean,
	addCommentIsOpen: boolean,
	addCommentClose: () => void,
	addCommentOpen: () => void,
	submitComment: (clickedCommentId: number | string, commentText: string) => Promise<boolean>,
	editCommentSubmit: (editCommentId: number | string, commentText: string) => Promise<boolean>,
	deleteCommentHandler: (deleteCommentId: number) => void,
	commentsArray?: postUserComment[],
	comment?: postUserComment,
	isInitial: boolean,
}





/**
 * This is the recursive comment rendering, which renders the specified comments details - if they
 * have been specified - and spawns another <Comment/> underneath itself if it has replies. This also
 * has some actions tied to it, such as adding a sub-comment, and editing or deleting the comment if it
 * belongs to the logged in user; however, note that the logic for these is contained in ItemLightbox.
 *
 * @class Comment
 * @extends {Component}
 */
class Comment extends Component<CommentProps, CommeentState> {
	/**
	 * Creates an instance of Comment.
	 * @param {CommentProps} props
	 * @memberof Comment
	 */
	constructor(props: CommentProps) {
		super(props);
		this.state = {
			commentBox: false,
			commentBoxMode: modeCommentBox.AddComment,
			commentText: "",
			commentTextEdit: "",
			commentData: null,
		};

		this.renderCommentsLoop = this.renderCommentsLoop.bind(this);
		this.renderRealComment = this.renderRealComment.bind(this);
		this.renderCommentSubmit = this.renderCommentSubmit.bind(this);
		this.openSubmitComment = this.openSubmitComment.bind(this);
		this.openEditComment = this.openEditComment.bind(this);
		this.deleteComment = this.deleteComment.bind(this);
		this.renderCommentDating = this.renderCommentDating.bind(this);
	}



	/**
	 * We set the state to be the prop comment
	 *
	 * @memberof Comment
	 */
	componentDidMount(): void {
		if (this.props.comment !== undefined) {
			const a = this.props.comment as postUserComment;
			if (this.props.comment?.text.length > 0) {
				// Set the state here so we can access the data
				this.setState({
					commentData: a,
				});
			}
		}
	}



	/**
	 * This makes sure that comments can be opened again on the first click
	 *
	 * @memberof Comment
	 */
	componentWillUnmount(): void {
		this.props.addCommentClose();
	}



	/**
	 * This will close any previously opened comment boxes if the props changed. This way,
	 * the user can click to a different "add comment" button and it will open without having
	 * to click twice (first to open, then to close).
	 *
	 * @note Because these <Comment/> elements are recursively added and nested, this could become
	 * a very expensive state change, though I don't know how React implements anything so I don't know
	 * the implications.
	 *
	 * @param {CommentProps} prevProps
	 * @param {CommeentState} prevState
	 * @memberof Comment
	 */
	componentDidUpdate(prevProps: CommentProps, prevState: CommeentState): void {
		// Only bother updating when the props differ between update
		if (this.props.addCommentIsOpen !== prevProps.addCommentIsOpen) {
			// Because this runs after the component update, we only want to hide components that
			// were PREVIOUSLY open
			// This way, if a user *just* opened a comment (hence this.state.commentBox === true)
			// we will not close it
			if (prevState.commentBox === true) {
				this.setState({
					commentBox: false,
				});
			}
		}
		// Here we check if the root comment has changed, and if it has been set to open.
		if (this.props.rootCommentIsOpen !== prevProps.rootCommentIsOpen) {
			if (this.props.rootCommentIsOpen === true) {
				this.setState({
					commentBox: false,
				});
			}
		}

		if (this.props.comment !== prevProps.comment) {
			if (this.props.comment !== undefined) {
				const a = this.props.comment as postUserComment;
				if (this.props.comment?.text.length > 0) {
				// Set the state here so we can access the data
					this.setState({
						commentData: a,
					});
				}
			}
		}
	}



	/**
	 * This will open this comment's add comment box and toggle close
	 * of all of the other boxes via a prop change
	 *
	 * @memberof Comment
	 */
	openSubmitComment(): void {
		this.setState({
			commentBox: true,
			commentBoxMode: modeCommentBox.AddComment,
		});
		if (!this.props.addCommentIsOpen) {
			this.props.addCommentOpen();
		}
		else {
			this.props.addCommentClose();
		}
	}



	/**
	 * This renders the submission box to add a comment as a child of the clicked comment.
	 *
	 * @return {*}  {JSX.Element}
	 * @memberof Comment
	 */
	renderCommentSubmit(): JSX.Element {
		if (this.state.commentBox && this.state.commentBoxMode === modeCommentBox.AddComment && this.state.commentData !== null) {
			// shorthand cause lazy, also because TypeScript was complaining about nulls
			const a = this.state.commentData;
			return <form className="bare-login-form" onSubmit={(event) => {
				// Prevent default submitting
				event.preventDefault();

				// Here we try to submit a comment, and if it was successful we close the comment box
				// This is the same implementation as in ItemLightbox.tsx
				this.props.submitComment(a.postcomment_id, this.state.commentText).then((addWasSuccessful) => {
					if (addWasSuccessful) {
						this.props.addCommentOpen();
						this.setState({
							commentText: "",
						});
					}
				});
			}}>
				<label htmlFor="comment-insert"><b>Add Comment</b></label>
				<input type="text" placeholder="Enter comment" name="comment-insert" value={this.state.commentText}
					required onChange={(event) => this.setState({commentText: event.target.value})}/>

				<button className="default-button" type="submit">Post comment</button>
			</form>;
		}
		return <Fragment></Fragment>;
	}


	/**
	 * This toggles the state to allow the edit comment box to open
	 *
	 * @memberof Comment
	 */
	openEditComment(): void {
		this.setState({
			commentBox: true,
			commentBoxMode: modeCommentBox.EditComment,
		});
		if (!this.props.addCommentIsOpen) {
			this.props.addCommentOpen();
		}
		else {
			this.props.addCommentClose();
		}
	}



	/**
	 * This renders the comment edit box of the clicked comment
	 *
	 * @return {*}  {JSX.Element}
	 * @memberof Comment
	 */
	renderCommentEdit(): JSX.Element {
		if (this.state.commentBox && this.state.commentBoxMode === modeCommentBox.EditComment && this.state.commentData !== null) {
			// shorthand cause lazy, also because TypeScript was complaining about nulls
			const a = this.state.commentData;
			return <form className="bare-login-form" onSubmit={(event) => {
				// Prevent default submitting
				event.preventDefault();
				// Here we try to submit a comment, and if it was successful we close the comment box
				// This is the same implementation as in ItemLightbox.tsx
				this.props.editCommentSubmit(a.postcomment_id, this.state.commentText).then((addWasSuccessful) => {
					if (addWasSuccessful) {
						this.props.addCommentOpen();
						this.setState({
							commentText: "",
							commentBoxMode: modeCommentBox.AddComment,
						});
					}
				});
			}}>
				<label htmlFor="comment-insert"><b>Edit Comment</b></label>
				<input type="text" placeholder="Enter comment" name="comment-insert" value={this.state.commentText}
					required onChange={(event) => this.setState({commentText: event.target.value})}/>

				<button className="default-button" type="submit">Post comment</button>
			</form>;
		}
		return <Fragment></Fragment>;
	}



	/**
	 * This triggers the delete comment method passed in as a prop ItemLightbox, giving it the ID
	 * of the comment to delete.
	 *
	 * @memberof Comment
	 */
	deleteComment(): void {
		// Because we "permit" commentData to be null, we first check it actually exists
		if (this.state.commentData !== null) {
			this.props.deleteCommentHandler(this.state.commentData.postcomment_id);
		}
	}



	/**
	 * This renders the additional buttons for every comment depending on whether or not the user is logged
	 * in and if this specific comment appears to belong to them.
	 *
	 * @return {*}  {JSX.Element}
	 * @memberof Comment
	 */
	renderCommentAuthorButtons(): JSX.Element {
		// We only bother with checking if the authToken is not set to logged_out
		if (this.props.authToken !== "logged_out" && this.props.authTokenDecoded?.data.id !== undefined) {
			// If the user's ID in the auth token matches the comment's user id, render edit comment option
			if (parseInt(this.props.authTokenDecoded.data.id) === this.state.commentData?.user_id) {
				return <Fragment>
					<button className="default-button" onClick={this.openEditComment}>EDIT COMMENT</button>
					<button className="default-button" onClick={this.deleteComment}>DELETE COMMENT</button>
				</Fragment>;
			}
			else {
				return <Fragment></Fragment>;
			}
		}
		else {
			return <Fragment></Fragment>;
		}
	}



	/**
	 * This checks if the comment has been deleted, or if it has been edited, and it renders the timestamps
	 * accordingly.
	 *
	 * @param {postUserComment} data
	 * @return {*}  {JSX.Element}
	 * @memberof Comment
	 */
	renderCommentDating(data: postUserComment): JSX.Element {
		let element: JSX.Element;


		// Check whether to show the post/edit dates, or just the post date
		if (data.comment_posted_at === data.comment_updated_at) {
			// -----------------------------------------------------------------
			// Use single date
			// -----------------------------------------------------------------
			if (data.user_id === undefined || data.user_id === null) {
				element = <p className="comment-date">Deleted: {data.comment_posted_at.toString()}</p>;
			}
			else {
				element = <p className="comment-date">Posted: {data.comment_posted_at.toString()}</p>;
			}
		}
		else {
			// -----------------------------------------------------------------
			// Use both dates
			// -----------------------------------------------------------------
			if (data.user_id === undefined || data.user_id === null) {
				element = <Fragment>
					<p className="comment-date">Posted: {data.comment_posted_at.toString()}</p>
					<p className="comment-date">Deleted: {data.comment_updated_at.toString()}</p>
				</Fragment>;
			}
			else {
				element = <Fragment>
					<p className="comment-date">Posted: {data.comment_posted_at.toString()}</p>
					<p className="comment-date">Updated: {data.comment_updated_at.toString()}</p>
				</Fragment>;
			}
		}

		return element;
	}


	/**
	 * This renders the "real" comment, aka the actual body of the comment.
	 *
	 * @return {*}  {JSX.Element}
	 * @memberof Comment
	 */
	renderRealComment(): JSX.Element | undefined {
		// First we check if the comment is not undefined
		// aka, if it's not undefined, this element is a *comment* comment and we need to render a comment

		if (this.state.commentData !== null) {
			const a = this.state.commentData;
			return (
			// ---------------------------------------------------------
			// SECTION HTML: This is the HTML for every real comment
			// ---------------------------------------------------------
				<Fragment>
					<li className="comment-container">

						<div className="comment-container-inner">
							<div className="comment-top-bar">
								{/* We render a pfp regardless, but the source is based on whether or not the user has an avatar.
							If they do, we use their avatar, if they don't we use the default png */}
								<img className="comment-pfp-image" src={
									a.avatar !== null && a.avatar !== undefined ?
										process.env.REACT_APP_API_URL + "pfp_images/" + a.avatar :
										process.env.REACT_APP_API_URL + "pfp_images/" + "default.png"
								}>
								</img>

								{/* If we got a username, we render the username, otherwise we render [DeletedUser].
							This is done because users may be deleted*/}
								{a.username ? <p className="comment-username">{a.username}</p> : <p className="comment-username deleted">[DeletedUser]</p>}



								{this.renderCommentDating(a)}
							</div>

							<div className="comment-buttons">
								<div className="comment-content">

									<p className="comment-text">{a.text.toString()}</p>
								</div>

								<button className="default-button comment-add-child" onClick={this.openSubmitComment}>add comment</button>

								{this.renderCommentAuthorButtons()}
								{this.renderCommentSubmit()}
								{this.renderCommentEdit()}
							</div>
						</div>

					</li>
				</Fragment>
			// ---------------------------------------------------------
			// !SECTION
			// ---------------------------------------------------------
			);
		}
	}


	/**
	 * This is the main loop used to render the <Comment/> components.
	 *
	 * It renders a "real" <Comment/> by passing it the comment prop only if the current position
	 * of the commentsArray has an actual comment.
	 *
	 * It renders a "sub-comment" <Comment/>, which itself also repeats this iteration, by passing
	 * it a commentsArray of the next layer within the array.
	 *
	 * @return {*}  {JSX.Element}
	 * @memberof Comment
	 */
	renderCommentsLoop(): JSX.Element[] {
		const retEl: JSX.Element[] = [];

		const arr = this.props.commentsArray;
		// Loop over every array we got given by the parent, which was either ItemLightbox or Comment
		if (arr !== undefined) {
			for (const k in arr as postUserComment[]) {
			// Don't loop over prototypes
				if (arr.hasOwnProperty(k) ) {
				// If the array element is not null, aka it's a thing
					if (arr[k] !== null) {
					// Check if it has a parent. If not, it means it's a top-level comment
						if (arr[k].parent_postcomment_id === null) {
						// retEl.push(this.renderCommentParent(arr[k].postcomment_id, arr[k].text));
							retEl.push(<Comment
								authToken={this.props.authToken}
								authTokenDecoded={this.props.authTokenDecoded}
								rootCommentIsOpen={this.props.rootCommentIsOpen}
								addCommentIsOpen={this.props.addCommentIsOpen}
								addCommentClose={this.props.addCommentClose}
								addCommentOpen={this.props.addCommentOpen}
								submitComment={this.props.submitComment}
								editCommentSubmit={this.props.editCommentSubmit}
								deleteCommentHandler={this.props.deleteCommentHandler}
								comment={arr[k]}
								isInitial={false}
							/>);
						}
						// If it's not a top level comment, we render the post as a sub-comment
						// We also make sure the post content is not undefined
						else if (arr[k].text !== undefined) {
							// retEl.push(this.renderRealComment(arr[k].postcomment_id, arr[k].text));
							retEl.push(<Comment
								authToken={this.props.authToken}
								authTokenDecoded={this.props.authTokenDecoded}
								rootCommentIsOpen={this.props.rootCommentIsOpen}
								addCommentIsOpen={this.props.addCommentIsOpen}
								addCommentClose={this.props.addCommentClose}
								addCommentOpen={this.props.addCommentOpen}
								submitComment={this.props.submitComment}
								editCommentSubmit={this.props.editCommentSubmit}
								deleteCommentHandler={this.props.deleteCommentHandler}
								comment={arr[k]}
								isInitial={false}
							/>);
							// retEl.push(<Comment postId)
						// ---------------------------------------------------------
						// NOTE: I know this is really ugly
						// ---------------------------------------------------------
						// retEl.push(this.renderCommentSubmit());
						}


						// Regardless of whether we were handling a top-level or sub-comment,
						// we'll check if it's an object and if there are replies on it. If there are replies
						// on the object, then that means we have to loop over it again with this <Comment/> element
						// and we spawn it inside of a ul tag.
						// NOTE: This is recursive until there's no more nested replies
						if (typeof arr[k] === "object" && typeof arr[k].replies === "object") {
							retEl.push(
								<Fragment>
									<ul className="comment-wrapper">
										<Comment
											authToken={this.props.authToken}
											authTokenDecoded={this.props.authTokenDecoded}
											rootCommentIsOpen={this.props.rootCommentIsOpen}
											addCommentIsOpen={this.props.addCommentIsOpen}
											addCommentClose={this.props.addCommentClose}
											addCommentOpen={this.props.addCommentOpen}
											submitComment={this.props.submitComment}
											editCommentSubmit={this.props.editCommentSubmit}
											deleteCommentHandler={this.props.deleteCommentHandler}
											commentsArray={arr[k] as unknown as postUserComment[]}
											isInitial={false}
										/>
									</ul>
								</Fragment>);
						}
						// Otherwise, if there were no replies on the object, we do not spawn the comment in a ul tag,
						else if (typeof arr[k] === "object") {
							retEl.push(<Comment
								authToken={this.props.authToken}
								authTokenDecoded={this.props.authTokenDecoded}
								rootCommentIsOpen={this.props.rootCommentIsOpen}
								addCommentIsOpen={this.props.addCommentIsOpen}
								addCommentClose={this.props.addCommentClose}
								addCommentOpen={this.props.addCommentOpen}
								submitComment={this.props.submitComment}
								editCommentSubmit={this.props.editCommentSubmit}
								deleteCommentHandler={this.props.deleteCommentHandler}
								commentsArray={arr[k] as unknown as postUserComment[]}
								isInitial={false}
							/>);
						}
					}
				}
			}
		}
		return retEl;
	}



	/**
	 * This renders the comment, initially wrapping the first commeing in a <ul> el so that
	 * every comment thereafter is within this. This is mainly just a stylistic choice.
	 *
	 * And for every comment thereafter, it runs the renderCommentsLoop which handles rendering
	 * the nested comments and so on.
	 *
	 * @return {*}
	 * @memberof Comment
	 */
	render(): JSX.Element {
		// ---------------------------------------------------------------------
		// This is used to render the first comment, as spawned from ItemLightbox
		// ---------------------------------------------------------------------
		if (this.props.isInitial) {
			return (
				<ul className="comment-wrapper">
					{/* This renders the actual comment if we passed in a "comment" prop.*/}
					{this.renderRealComment()}
					{/*
					Render comments loop essentially renders additional "real" comments if they exist
					This is done to recursively render comments under comments by using the "commentsArray" prop
				*/}
					{this.renderCommentsLoop()}
				</ul>
			);
		}
		else {
			return (
				<Fragment>
					{this.renderRealComment()}
					{this.renderCommentsLoop()}
				</Fragment>
			);
		}
	}
}
export default Comment;
