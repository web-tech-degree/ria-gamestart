import React, {Component} from "react";
import "./Bookmarker.css";





// -----------------------------------------------------------------------------
// Types
import type {createNotificationType} from "../types";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Modes
import {modesNotifTypes} from "../modes";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Components
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Libraries
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Assets
// -----------------------------------------------------------------------------





type BookmarkerProps = {
	bookmarkStatus: number,
	authToken: string,
	// eslint-disable-next-line
	postId: number,
	updateParent: () => void,
	createNotification: createNotificationType,
	isItemFromBookmarks: boolean,
	// This can be undefined because this is only passed/used here from ItemLightbox
	addItemInFeedToRefreshQueue?: (postitemId: number) => void,
	// This can be undefiend because this is only passed/used here from Item
	setLightboxToForceRefreshOnNextOpen?: () => void,
}





/**
 * This is a button that'll toggle the bookmark status of the specified
 * post via the props, if the user is logged in.
 *
 * @class Bookmarker
 * @extends {Component}
 */
class Bookmarker extends Component<BookmarkerProps> {
	/**
	 * Creates an instance of Bookmarker.
	 * @param {BookmarkerProps} props
	 * @memberof Bookmarker
	 */
	constructor(props: BookmarkerProps) {
		super(props);

		this.toggleBookmark = this.toggleBookmark.bind(this);
	}



	/**
	 * Here we attempt to toggle the bookmark
	 *
	 * @memberof Bookmarker
	 */
	toggleBookmark(): void {
		fetch(process.env.REACT_APP_API_URL + "__api.php", {
			method: "POST",
			credentials: "omit",
			body: JSON.stringify({
				request_type: "post_toggle_bookmark",
				user_auth_token: this.props.authToken,
				post_id: this.props.postId,
				// Here we make our desired bookmark status be 0 if current status is 1
				// and vice versa
				bookmark_status_desired: (this.props.bookmarkStatus === 1 ? 0 : 1),
			}),
		})
			.then((response) => response.json())
			.then((data) => {
				if (data.message === "token_validation_error") {
					this.props.createNotification(modesNotifTypes.Warning, null, "Please log in to vote on posts", null, null);
				}
				else if (data.message !== "bookmark_success") {
					this.props.createNotification(modesNotifTypes.Error, null, "Voting failed", null, null);
				}
				this.props.updateParent();

				// If this function isn't undefined, then that means it was passed in from ItemLightbox.
				if (this.props.addItemInFeedToRefreshQueue !== undefined) {
					this.props.addItemInFeedToRefreshQueue(this.props.postId);
				}
				// If this function isn't undefined, then that means it was passed in from Item.
				else if (this.props.setLightboxToForceRefreshOnNextOpen !== undefined) {
					this.props.setLightboxToForceRefreshOnNextOpen();
				}
			});
	}



	/**
	 * This renders the bookmarker, showing based on whether or not the user has bookmarked the specified post.
	 *
	 * @return {*}  {JSX.Element}
	 * @memberof Bookmarker
	 */
	render(): JSX.Element {
		if (this.props.bookmarkStatus === 1 || this.props.isItemFromBookmarks) {
			return <span className="bookmarker bookmarked" onClick={this.toggleBookmark}></span>;
		}
		// Otherwise we render the default span
		else {
			return <span className="bookmarker default" onClick={this.toggleBookmark}></span>;
		}
	}
}

export default Bookmarker;
