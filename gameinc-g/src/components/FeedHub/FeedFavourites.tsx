import React, {Fragment} from "react";
import "./Feed.css";
import {globals} from "../../logic/globals";





// -----------------------------------------------------------------------------
// Types
import type {sourceDetails, postItemDetails} from "./types";
import type {createNotificationType} from "../types";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Modes
import {modesFeed} from "./modes";
import {modesNotifTypes} from "../modes";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Components
import Item from "./Item";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Libraries
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Assets
// -----------------------------------------------------------------------------





type FeedFavouritesState = {
	feedMode: modesFeed,
	searchQuery: string,
	activeSource: sourceDetails,
	feedToChangeTo: string,
	highestPostId: number,
	lowestPostId: number,
	newItems: Array<postItemDetails>,
	displayedItemsData: postItemDetails[],
	feedDropdownIsOpen: boolean,
	// This is the time at which the last grabPreviousFetch request was made
	// This is used to limit how fast you can grab by scrolling
	grabPreviousLastTime: number,
	// This is used to store the interval that checks if any new posts are available
	checkNewPostsAvailableInterval: ReturnType<typeof setInterval> | null,
	newPostAvailability: {isAvailable: boolean, availableCount: number},
}

type FeedFavouritesProps = {
	createNotification: createNotificationType,
	authToken: string,
	feedNumber: number,
	// This is a function given by FeedHub which removes this feed using
	// this feed's feedNumber
	removeThisFeed: (feedNumber: number) => void,
	// This is the method which triggers the <ItemLightbox/> in FeedHub to display
	// We don't actually do anything with it here; instead, give this to each item
	// to open.
	openItemInLightbox: (postDataToOpen: postItemDetails, isFromBookmarks: boolean) => void,
	moveFeedLeft: (feedNumber: number) => void,
	moveFeedRight: (feedNumber: number) => void,
	initSourceId: number,
	itemPostIdsToRefresh: number[],
	setLightboxToForceRefreshOnNextOpen: () => void,
}

type postData = {
	message: string,
	posts: postItemDetails[],
	arrangement: string,
}

// This allows a selection of where to add posts
// They will either be added to the top (latest) or to the bottom (previous)
enum addItemsWhere {
	latest,
	previous,
}





/**
 * This class is the component which is spawned for every favourites feed. It manages
 * fetching periodically and on scroll, and it spawns items based on the fetched post info.
 *
 * This really, probably, should be implemented in the Feed.tsx file with some different
 * functionality based on the mode (modesFeed), but this was a quicker way to implement.
 *
 * @class FeedFavourites
 * @extends {React.Component}
 */
class FeedFavourites extends React.Component<FeedFavouritesProps, FeedFavouritesState> {
	/**
	 * Creates an instance of FeedFavourites.
	 * @param {FeedFavouritesProps} props
	 * @memberof FeedFavourites
	 */
	constructor(props: FeedFavouritesProps) {
		super(props);
		this.state = {
			feedMode: modesFeed.SourceFavourites,
			searchQuery: "",
			activeSource: {
				source_id: this.props.initSourceId,
				name: "",
				url: "",
				avatar: "",
			},
			feedToChangeTo: "",
			highestPostId: 0,
			lowestPostId: 0,
			newItems: [],
			displayedItemsData: [],
			feedDropdownIsOpen: false,
			grabPreviousLastTime: Date.now(),
			checkNewPostsAvailableInterval: null,
			newPostAvailability: {isAvailable: true, availableCount: 1},
		};

		this.dbgLog();

		this.refFeedContentContainer = React.createRef();

		// We use this to target the dropdown container
		this.refFeedOptionDropdown = React.createRef();
		this.refFeedOptionSearch = React.createRef();
		// this.addLatestItems = this.addLatestItems.bind(this);
		this.grabLatestItems = this.grabLatestItems.bind(this);
		this.onFeedScroll = this.onFeedScroll.bind(this);
		this.renderRefreshPrompt = this.renderRefreshPrompt.bind(this);
	}
	refFeedContentContainer: React.RefObject<HTMLDivElement>;
	refFeedOptionDropdown: React.RefObject<HTMLDivElement>;
	refFeedOptionSearch: React.RefObject<HTMLInputElement>;



	/**
	 * This checks if this component should be "debugged" via the process.env variables. If it's true,
	 * we bind console.log so that logging works normally, otherwise we make it not do anything.
	 *
	 * @param {*} [mode]
	 * @param {...any[]} optionalParams
	 * @memberof FeedFavourites
	 */
	dbgLog(
		// eslint-disable-next-line
		mode?: any,...optionalParams: any[]
	): void {
		if (globals.DEBUGGER_DBG_LOG === "true" && globals.DEBUGGER_DBG_FeedFavourites === "true") {
			this.dbgLog = console.log.bind(window.close);
		}
		else {
			this.dbgLog = (): void => {
				return;
			};
		}
	}


	/**
	 * See https://stackoverflow.com/a/42234988 and ItemLightbox.tsx
	 *
	 * @memberof FeedEditor
	 */
	componentDidMount(): void {
		this.dbgLog("FeedFavourites: componentDidMount");

		const sti = setInterval(() => {
			this.checkNewPostsAvailable();
		}, 1000);


		// Add an interval to the state. This'll trigger checkNewPostsAvailable every second
		this.setState({
			checkNewPostsAvailableInterval: sti,
		});

		this.grabSourceInfo();
		this.grabLatestItems(true);
	}



	/**
	 * See https://stackoverflow.com/a/42234988 and ItemLightbox.tsx
	 *
	 * @memberof Feed
	 */
	componentWillUnmount(): void {
		clearInterval(this.state.checkNewPostsAvailableInterval as ReturnType<typeof setInterval>);
		this.dbgLog("Feed: componentWillUnmount");
	}



	/**
	 * This adds <Item/>s to the state of rendered items.
	 *
	 * This current implementation basically just "lets go" of the <Item/>s' states in the sense
	 * that they are rendered but we do not hold onto them.
	 *
	 * This should be changed so that addItems adds "stuff" to an array in the state, and then the render
	 * method uses that array of "stuff" to render the actual <Item/>s, which would allow us to still have
	 * access to the things we give to each <Item/>.
	 *
	 * Alternatively, I think something could be done with refs to target the <Item/>s after they have been spawned.
	 *
	 * But basically, because this is implemented imperfectly, one <Item/> cannot really "tell another <Item/> to update",
	 * and I think there were some other things that couldn't really be implemented because of this.
	 *
	 * @param {postData} data
	 * @param {addItemsWhere} location
	 * @memberof FeedFavourites
	 */
	addItems(data: postData, location: addItemsWhere): void {
		this.dbgLog("FeedFavourites: aFeedFavouritesdItems");
		const newItemArray: postItemDetails[] = [];

		// Each thing in data.posts shall be a postItemDetails
		// which contains all of the data for one post item thingy
		// We then create a newItem based off of the data of this, and add
		// that to the array of new items
		for (let i = 0; i < data.posts.length; i++) {
			// We add the feed details to the post's data so that we can identify which
			// feed the posts were from. This is useful for passing the feed name from
			// Item into ItemLightbox
			// NOTE: parentFeed is not used for any functionality, just display
			data.posts[i].parentFeed = {feed_id: this.state.activeSource.source_id, name: this.state.activeSource.name};
			// Generate the element, passing down the necessary info to create it

			newItemArray.push(data.posts[i]);
		}

		// The location can ONLY ever be set to latest or previous
		// this selects which was set
		if (location === addItemsWhere.latest) {
			this.setState((prevState) => ({
				// [0] is used to give us the first data.posts item which should
				// have the highest postitem_id which we will then use to keep track of which posts to get
				highestPostId: data.posts[0].postitem_id,
				// This adds to the bottom of the array, aka top of page
				displayedItemsData: newItemArray.concat(prevState.displayedItemsData),
			}));
		}
		else if (location === addItemsWhere.previous) {
			this.setState((prevState) => ({
				// Same as the above, but instead updates the lowest post IDs and adds the post to
				// the end of the page
				lowestPostId: data.posts[data.posts.length -1].postitem_id,
				displayedItemsData: prevState.displayedItemsData.concat(newItemArray),
				// We also update this to allow new fetches
				grabPreviousLastTime: Date.now(),
			}));
		}
	}



	/**
	 * This checks if any new posts are available
	 * in the selected source.
	 *
	 * @memberof FeedFavourites
	 */
	checkNewPostsAvailable(): void {
		this.dbgLog("FeedFavourites: checkNewPostsAvailable");
		fetch(process.env.REACT_APP_API_URL + "__api.php", {

			method: "POST",
			credentials: "omit",
			body: JSON.stringify({
				request_type: "check_new_posts_available_by_source",
				active_source_id: this.state.activeSource.source_id,
				highest_current: this.state.highestPostId,
			}),
		})
			.then((response) => response.json())
			.then((data) => {
				if (data.message === "new_posts_available") {
					this.dbgLog("FeedFavourites: checkNewPostsAvailable: New posts available");
					this.setState({
						newPostAvailability: {isAvailable: true, availableCount: data.count},
					});
				}
				else if (data.message === "no_new_posts") {
					// We don't set the state here because we expect grabLatestPosts
					// to simply have ran and set newPostAvailability to false
					this.dbgLog("FeedFavourites: checkNewPostsAvailable: No new posts available");
				}
			});
	}



	/**
	 * This is used to get the info of this source.
	 *
	 * @memberof FeedFavourites
	 */
	grabSourceInfo(): void {
		this.dbgLog("FeedFavourites: grabSourceInfo");
		fetch(process.env.REACT_APP_API_URL + "__api.php", {
			method: "POST",
			credentials: "omit",
			body: JSON.stringify({
				request_type: "feed_get_source_info",
				active_source_id: this.state.activeSource.source_id,
			}),
		})
			.then((response) => response.json())
			.then((data) => {
				if (data.message === "fetch_success") {
					this.dbgLog("FeedFavourites: grabSourceInfo: Success");
					this.setState({
						activeSource: {source_id: data.info.source_id, url: data.info.url, name: data.info.name, avatar: data.info.avatar},
					});
				}
			});
	}



	/**
	 * This is the fetch statement which shall return the postData. This is supposed
	 * to be somewhat "abstracted" and give postData regardless of the mode.
	 *
	 * @return {*}  {Promise<postData>}
	 * @memberof FeedFavourites
	 */
	async grabLatestItemsFetch(): Promise<postData> {
		this.dbgLog("FeedFavourites: grabLatestItemsFetch");

		return fetch(process.env.REACT_APP_API_URL + "__api.php", {
			method: "POST",
			credentials: "omit",
			body: JSON.stringify({
				request_type: "get_latest_posts_by_source",
				user_auth_token: this.props.authToken,
				active_source_id: this.state.activeSource.source_id,
				highest_current: this.state.highestPostId,
			}),
		})
			.then((response) => response.json());
	}



	/**
	 * Try to fetch the latest posts based on the category of this feed, adding any new posts to the top
	 * of the feed. Also, if this is being fetched as the "initial" fetch, this method will also set the
	 * lowest post ID.
	 *
	 * @usage Trigger this either on a timer or on some sort of refresh button
	 *
	 * @param {boolean} isInitialRequest
	 * @memberof FeedFavourites
	 */
	grabLatestItems(isInitialRequest: boolean): void {
		// Only make a request if posts are available
		if (this.state.newPostAvailability.isAvailable) {
			this.dbgLog("FeedFavourites: grabLatestItems");
			this.grabLatestItemsFetch()
				.then((data) => {
					this.setState({
						newPostAvailability: {isAvailable: false, availableCount: 0},
					});
					if (data.message === "fetch_success") {
						// If we are too far behind, the API still sent us the latest x feeds, but we first need to
						// wipe the current feeds because this will act like a "refresh"
						if (data.arrangement === "too_far_behind") {
							this.setState({
								displayedItemsData: [],
							});
						}
						// If there's at least 1 item, we add the items to the feed (top)
						// though this should never return less than 1 because we're already checking
						// for available posts
						if (data.posts.length > 0) {
						// If this is the first request for this specific feed, we also want to
						// set the lowest post ID
							if (isInitialRequest) {
								this.setState({
									lowestPostId: data.posts[data.posts.length-1].postitem_id,
								});
							}
							this.addItems(data, addItemsWhere.latest);
						}
					}
				});
		}
	}



	/**
	 * Similar to grabLatestItemsFetch, this tries to abstract the fetch logic
	 * away from the handling of the data.
	 *
	 * @return {*}  {Promise<postData>}
	 * @memberof FeedFavourites
	 */
	async grabPreviousItemsFetch(): Promise<postData> {
		this.dbgLog("FeedFavourites: grabPreviousItemsFetch: Category");

		return fetch(process.env.REACT_APP_API_URL + "__api.php", {
			method: "POST",
			credentials: "omit",
			body: JSON.stringify({
				request_type: "get_previous_posts_by_source",
				user_auth_token: this.props.authToken,
				active_source_id: this.state.activeSource.source_id,
				lowest_current: this.state.lowestPostId,
			}),
		})
			.then((response) => response.json());
	}



	/**
	 * This gets the previous items in the feed
	 *
	 * @usage Trigger this once the user hits, or is close to hitting, the bottom of the feed
	 *
	 * @memberof FeedFavourites
	 */
	grabPreviousItems(): void {
		this.dbgLog("Feed: grabPreviousItems");
		const elapsed = Date.now() - this.state.grabPreviousLastTime;

		// If not enough time has passed, try again
		if (elapsed < parseInt(process.env.REACT_APP_POST_FETCH_DELAY as string)) {
			this.dbgLog("FeedFavourites: grabPreviousItems: waiting");
			setTimeout(() => {
				this.grabPreviousItems();
			}, 1);
			return;
		}

		this.dbgLog("FeedFavourites: grabPreviousItems: executing");
		this.grabPreviousItemsFetch()
			.then((data: postData) => {
				this.dbgLog(data);
				// We expect to receive data matching postData type
				if (data.message === "fetch_success") {
					// If there's at least 1 item, we add the items to the feed (bottom)
					if (data.posts.length > 0) {
						this.addItems(data, addItemsWhere.previous);
					}
				}
			});
	}



	/**
	 * This is triggered every time a user scrolls, and checks if they are at the bottom of the feed.
	 *
	 * @memberof FeedFavourites
	 */
	onFeedScroll(): void {
		// Don't run if undefined/null etc.
		if (this.refFeedContentContainer === null || this.refFeedContentContainer.current === null || this.refFeedContentContainer.current === undefined) {
			return;
		}


		const fTop = this.refFeedContentContainer.current.scrollTop;
		const cHeight = this.refFeedContentContainer.current.clientHeight;
		const sHeight = this.refFeedContentContainer.current.scrollHeight;

		// If the user is at the bottom, we try fetching
		if (Math.ceil(fTop) + cHeight >= sHeight) {
			console.log("GRAB PREVIOUS");

			this.dbgLog("FeedFavourites: onFeedScroll: User at bottom");
			this.grabPreviousItems();
		}
	}



	/**
	 * This renders a prompt to the user if new posts are available
	 *
	 * @return {*}  {JSX.Element}
	 * @memberof FeedFavourites
	 */
	renderRefreshPrompt(): JSX.Element {
		if (this.state.newPostAvailability.isAvailable && this.state.feedMode === modesFeed.SourceFavourites) {
			if (this.state.newPostAvailability.availableCount <= parseInt(process.env.REACT_APP_MAX_NEW_POST_FETCH_BEFORE_REFRESH as string)) {
				// -------------------------------------------------------------
				// SECTION HTML: This is the button that displays to get new posts
				// -------------------------------------------------------------
				return <button className="default-button" onClick={() => this.grabLatestItems(false)}>fetch new</button>;
				// -------------------------------------------------------------
				// !SECTION
				// -------------------------------------------------------------
			}
			else if (this.state.newPostAvailability.availableCount > parseInt(process.env.REACT_APP_MAX_NEW_POST_FETCH_BEFORE_REFRESH as string)) {
				// -------------------------------------------------------------
				// SECTION HTML: This is the button that displays to refresh the feed
				// because the amount of posts on the server that area head is too high
				// -------------------------------------------------------------
				return <button className="default-button" onClick={() => this.grabLatestItems(false)}>refresh</button>;
				// -------------------------------------------------------------
				// !SECTION
				// -------------------------------------------------------------
			}
		}
		return <Fragment></Fragment>;
	}



	/**
	 * This renders the a feed in the style of favourites. This really should just be combined into
	 * Feed.tsx, but due to time constraints it's written like this. The whole point of feedMode: modesFeed
	 * state was for this, but a couple of things would need to be changed to make that work fluently.
	 *
	 * @return {*}
	 * @memberof FeedFavourites
	 */
	render(): JSX.Element {
		// ---------------------------------------------------------------------
		// SECTION HTML: Individual Feed
		// ---------------------------------------------------------------------
		return (
			// Make the id that of the one passed down through props
			// We do this so that we can target this within the draggable event as we'll be overriding
			// the event
			// We also get a prop to this feed and give it a scroll handler
			<div
				id={this.props.feedNumber.toString() as string}
				className="feed-container feed-make-draggable"
			>
				<div className="grid-top-bar-container">
					<div className="grid-top-bar feed-draggable-part">
						<span className="icon-button grid-move-left" onClick={() =>this.props.moveFeedLeft(this.props.feedNumber)}></span>
						<p className="combosearch-replacement-text">{this.state.activeSource.name}</p>
						<span className="icon-button grid-move-right" onClick={() =>this.props.moveFeedRight(this.props.feedNumber)}></span>
						<span className="icon-button grid-close" onClick={() => this.props.removeThisFeed(this.props.feedNumber)}></span>
					</div>
				</div>
				{/* This'll render a refresh prompt if new post items are available */}
				{this.renderRefreshPrompt()}
				{/* This renders all the displayed items */}
				<div className="feed-inner-content" ref={this.refFeedContentContainer} onScroll={this.onFeedScroll}>
					{/* This renders the Item elements based on the data we've stored in the state
					it's done like this instead of generating the Items and then storing them in the state so that we can actually
					receive prop updates in the Items */}
					{
						this.state.displayedItemsData.map((itemData) => {
							return <Item
								createNotification={this.props.createNotification}
								key={"feednum" + this.props.feedNumber + "_favouritesSourceId" + this.state.activeSource.source_id + "_postId" + itemData.postitem_id}
								authToken={this.props.authToken}
								postItemDetails={itemData}
								openItemInLightbox={this.props.openItemInLightbox}
								isItemFromBookmarks={false}
								itemPostIdsToRefresh={this.props.itemPostIdsToRefresh}
								setLightboxToForceRefreshOnNextOpen={this.props.setLightboxToForceRefreshOnNextOpen}
							/>;
						})
					}
				</div>
			</div>
		);
		// ---------------------------------------------------------------------
		// !SECTION
		// ---------------------------------------------------------------------
	}
}


export default FeedFavourites;
