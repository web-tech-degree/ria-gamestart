import React from "react";
import {globals, stringEllipsisTruncate} from "../../logic/globals";





// -----------------------------------------------------------------------------
// Types
import type {postItemDetails} from "./types";
import type {createNotificationType} from "../types";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Modes
import {modesNotifTypes} from "../modes";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Components
import Item from "./Item";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Libraries
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Assets
// -----------------------------------------------------------------------------





type FeedBookmarksState = {
	placeholder: string,
	bookmarkGenerator: ReturnType<typeof setInterval> | null,
	postDataFromStorage: postItemDetails[],
	displayedItemsData: postItemDetails[],
}

type FeedBookmarksProps = {
	createNotification: createNotificationType,
	authToken: string,
	feedNumber: number,
	// This is a function given by FeedHub which removes this feed using
	// this feed's feedNumber
	removeThisFeed: (feedNumber: number) => void,
	// This is the method which triggers the <ItemLightbox/> in FeedHub to display
	// We don't actually do anything with it here; instead, give this to each item
	// to open.
	openItemInLightbox: (postDataToOpen: postItemDetails, isFromBookmarks: boolean) => void,
	moveFeedLeft: (feedNumber: number) => void,
	moveFeedRight: (feedNumber: number) => void,
	newBookmarksWereFetched: boolean,
	newBookmarksWereRendered: () => void,
	itemPostIdsToRefresh: number[],
	setLightboxToForceRefreshOnNextOpen: () => void,
}





/**
 * This class handles the bookmarks feed, which reads from the localStorage and displays
 * the posts using that. This potentially should be implemented in the Feed.tsx file, instead
 * of being in a separate file, but that may convolute a bunch of the rendering and functions.
 *
 * @class FeedBookmarks
 * @extends {React.Component}
 */
class FeedBookmarks extends React.Component<FeedBookmarksProps, FeedBookmarksState> {
	/**
	 * Creates an instance of Feed.
	 * @param {FeedBookmarksProps} props
	 * @memberof FeedBookmarks
	 */
	constructor(props: FeedBookmarksProps) {
		super(props);
		this.state = {
			placeholder: "",
			bookmarkGenerator: null,
			postDataFromStorage: [],
			displayedItemsData: [],
		};

		this.dbgLog();

		this.refFeedContentContainer = React.createRef();
	}
	refFeedContentContainer: React.RefObject<HTMLDivElement>;



	/**
	 * This checks if this component should be "debugged" via the process.env variables. If it's true,
	 * we bind console.log so that logging works normally, otherwise we make it not do anything.
	 *
	 * @param {*} [mode]
	 * @param {...any[]} optionalParams
	 * @memberof FeedBookmarks
	 */
	dbgLog(
		// eslint-disable-next-line
		mode?: any,...optionalParams: any[]
	): void {
		if (globals.DEBUGGER_DBG_LOG === "true" && globals.DEBUGGER_DBG_Feed === "true") {
			this.dbgLog = console.log.bind(window.close);
		}
		else {
			this.dbgLog = (): void => {
				return;
			};
		}
	}



	/**
	 * We spawn an interval here which will check if new posts should be rendered
	 *
	 * @memberof FeedBookmarks
	 */
	componentDidMount(): void {
		const stiBookmarkGenerator = setInterval(() => {
			this.dbgLog("App: componentDidMount: rechekTokenInterval");
			this.readFromLocalStorage();
		}, globals.BOOKMARK_RENDERER_REFRESH_RATE);

		this.setState({
			bookmarkGenerator: stiBookmarkGenerator,
		});
	}



	/**
	 * Here we just remove the interval
	 *
	 * @memberof FeedBookmarks
	 */
	componentWillUnmount(): void {
		clearInterval(this.state.bookmarkGenerator as ReturnType<typeof setInterval>);
	}



	/**
	 * Here we check if the post data from localStorage changed, and if it did, we re-render the page
	 *
	 * @param {FeedBookmarksProps} prevProps
	 * @param {FeedBookmarksState} prevState
	 * @memberof FeedBookmarks
	 */
	componentDidUpdate(prevProps: FeedBookmarksProps, prevState: FeedBookmarksState): void {
		this.dbgLog("Feed: addItems");

		if (prevState.postDataFromStorage !== this.state.postDataFromStorage) {
			this.regenerateItemsWithTruncate();
		}
	}



	/**
	 * Here we read the data from localStorage, which is probably a horrible implementation because
	 * reading all the time from disk is dumb, but I wanted this to be a localStorage-first implementation
	 * where we rely on that for all of the data, except for when rendering.
	 *
	 * @memberof FeedBookmarks
	 */
	readFromLocalStorage(): void {
		const storagePosts = localStorage.getItem("bookmarkedPosts");

		if (storagePosts !== null && storagePosts !== undefined) {
			if (storagePosts.length > 0) {
				this.setState({
					postDataFromStorage: JSON.parse(storagePosts) as postItemDetails[],
				});
			}
		}
	}



	/**
	 * This regenerates the items from the localStorage, basically mimicking the truncating
	 * that the PHP normally does, but because we store the full data it has to be truncated
	 * in the client side.
	 *
	 * @memberof FeedBookmarks
	 */
	regenerateItemsWithTruncate(): void {
		this.dbgLog("FeedBookmarks: regenerateItemsWithTruncate");
		const newItemArray:postItemDetails[] = [];

		const postData = this.state.postDataFromStorage;

		// Each thing in postData shall be a postItemDetails
		// which contains all of the data for one post item thingy
		// We then create a newItem based off of the data of this, and add
		// that to the array of new items
		for (let i = 0; i < postData.length; i++) {
			// We don't have the data for the parent feed and don't really care
			postData[i].parentFeed = {feed_id: 0, name: "Bookmarked"};
			// Here we truncate the title and content because the data we have in localStorage
			// is the FULL data

			postData[i].title = stringEllipsisTruncate(postData[i].title, 50);
			postData[i].content = stringEllipsisTruncate(postData[i].content, 200);
			// Generate the element, passing down the necessary info to create it

			newItemArray.push(postData[i]);
		}

		// Contrary to the Feed.tsx implementation, here we just change the displayed items instead
		// of adding
		this.setState({
			displayedItemsData: newItemArray,
		});
	}


	/**
	 * This renders the bookmarks feed, which differs to the standard Feed as this does not have
	 * the selection menu at the top.
	 *
	 * @return {*}
	 * @memberof FeedBookmarks
	 */
	render(): JSX.Element {
		// ---------------------------------------------------------------------
		// SECTION HTML: Individual Feed
		// ---------------------------------------------------------------------
		return (
			// Make the id that of the one passed down through props
			// We do this so that we can target this within the draggable event as we'll be overriding
			// the event
			// We also get a prop to this feed and give it a scroll handler
			<div
				id={this.props.feedNumber.toString() as string}
				className="feed-container feed-make-draggable"
			>
				<div className="grid-top-bar-container">
					<div className="grid-top-bar feed-draggable-part">
						<span className="icon-button grid-move-left" onClick={() =>this.props.moveFeedLeft(this.props.feedNumber)}></span>
						<p className="combosearch-replacement-text">Bookmarks</p>
						<span className="icon-button grid-move-right" onClick={() =>this.props.moveFeedRight(this.props.feedNumber)}></span>
						<span className="icon-button grid-close" onClick={() => this.props.removeThisFeed(this.props.feedNumber)}></span>
					</div>
					{/* <input type="submit" className="combosearch-submit" value="Search"/> */}
				</div>
				{/* This'll render a refresh prompt if new post items are available */}
				{/* {this.renderRefreshPrompt()} */}
				{/* This renders all the displayed items */}
				<div className="feed-inner-content" ref={this.refFeedContentContainer}>
					{/* This renders the Item elements based on the data we've stored in the state
					it's done like this instead of generating the Items and then storing them in the state so that we can actually
					receive prop updates in the Items */}
					{
						this.state.displayedItemsData.map((itemData) => {
							return <Item
								createNotification={this.props.createNotification}
								key={"feednum" + this.props.feedNumber + "_bookmark_postId" + itemData.postitem_id}
								authToken={this.props.authToken}
								postItemDetails={itemData}
								openItemInLightbox={this.props.openItemInLightbox}
								isItemFromBookmarks={true}
								itemPostIdsToRefresh={this.props.itemPostIdsToRefresh}
								setLightboxToForceRefreshOnNextOpen={this.props.setLightboxToForceRefreshOnNextOpen}
							/>;
						})
					}
				</div>
			</div>
		);
		// ---------------------------------------------------------------------
		// !SECTION
		// ---------------------------------------------------------------------
	}
}


export default FeedBookmarks;
