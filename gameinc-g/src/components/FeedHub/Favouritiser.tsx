import React, {Component} from "react";
import "./Favouritiser.css";





// -----------------------------------------------------------------------------
// Types
import type {createNotificationType} from "../types";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Modes
import {modesNotifTypes} from "../modes";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Components
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Libraries
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Assets
import starBlack from "../../assets/material-black/star.svg";
import starBlackOutline from "../../assets/material-white/star-outline.svg";
// -----------------------------------------------------------------------------





type FavouritiserProps = {
	favouriteStatus: number,
	authToken: string,
	sourceId: number,
	updateParent: () => void,
	createNotification: createNotificationType,
	isItemFromBookmarks: boolean,
}





/**
 * This is a button that'll toggle the favourite status of the specified
 * post via the props, if the user is logged in.
 *
 * @class Favouritiser
 * @extends {Component}
 */
class Favouritiser extends Component<FavouritiserProps> {
	/**
	 * Creates an instance of Favouritiser.
	 * @param {FavouritiserProps} props
	 * @memberof Favouritiser
	 */
	constructor(props: FavouritiserProps) {
		super(props);

		this.toggleFavourite = this.toggleFavourite.bind(this);
	}



	/**
	 * Here we attempt to toggle the favourite status of the source
	 *
	 * @memberof Favouritiser
	 */
	toggleFavourite(): void {
		fetch(process.env.REACT_APP_API_URL + "__api.php", {
			method: "POST",
			credentials: "omit",
			body: JSON.stringify({
				request_type: "source_toggle_favourite",
				user_auth_token: this.props.authToken,
				source_id: this.props.sourceId,
				// Here we make our desired favourite status be 0 if current status is 1
				// and vice versa
				favourite_status_desired: (this.props.favouriteStatus === 1 ? 0 : 1),
			}),
		})
			.then((response) => response.json())
			.then((data) => {
				if (data.message === "token_validation_error") {
					this.props.createNotification(modesNotifTypes.Warning, null, "Please log in to favourite sources", null, null);
				}
				else if (data.message !== "favourite_success") {
					this.props.createNotification(modesNotifTypes.Error, null, "Favouriting failed", null, null);
				}
				this.props.updateParent();
			});
	}



	/**
	 * This renders the favourite (star) based on the user's current "favourite" status of the
	 * specified source.
	 *
	 * @return {*}  {JSX.Element}
	 * @memberof Favouritiser
	 */
	render(): JSX.Element {
		if (this.props.favouriteStatus === 1 || this.props.isItemFromBookmarks) {
			return <span className="favouritiser-icon favourited" onClick={this.toggleFavourite}/>;
		}
		// Otherwise we render the default button
		else {
			return <span className="favouritiser-icon" onClick={this.toggleFavourite}/>;
		}
	}
}

export default Favouritiser;
