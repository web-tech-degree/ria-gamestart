import React from "react";
import {authTokenFormat, globals, moveArray} from "../../logic/globals";
import "./FeedHub.css";





// -----------------------------------------------------------------------------
// Types
import type {activeFeedData, postItemDetails} from "./types";
import type {appFeedAdderType, createNotificationType} from "../types";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Modes
import {feedTypes, modesFeed} from "./modes";
import {modesAppBackground, modesNotifTypes} from "../modes";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Components
import Feed from "./Feed";
import ItemLightbox from "./ItemLightbox";
import FeedBookmarks from "./FeedBookmarks";
import FeedFavourites from "./FeedFavourites";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Libraries
import {Swappable, SwappableEventNames} from "@shopify/draggable";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Assets
// -----------------------------------------------------------------------------





type FeedHubState = {
	// This array will have our feeds, and through the use of props.feedNumber, we'll be able
	// to identify which feed to delete
	activeFeedsData: activeFeedData[],
	feedWillBeAddded: boolean,
	shouldRemoveFeed: boolean,
	// This is used to keep track of the "feedNumber" which will be added to activeFeeds.
	// This is iterated ONLY and never becomes smaller just so that every feedNumber is unique.
	// Even if the feedNumbers in the activeFeeds array are something like 1, 3, 4, 5, 8, 20, 21,
	// nothing will break.
	feedIterator: number,
	// This is used to toggle whether or not the lightbox should display
	itemLightboxShouldDisplay: boolean,
	itemLightboxPostId: number,
	itemLightboxFeedName: string,
	isItemFromBookmarks: boolean,
	swappables: Swappable<SwappableEventNames> | null,
	swapFeeds: boolean,
	swapFeedFrom: number,
	swapFeedTo: number,
	// This will store the postitem_ids which were update either in an ItemLightbox element,
	// which will make the specific Item update if the user interacted with the bookmarks or votes.
	itemPostIdsToRefresh: number[],
	// This is used to tell the ItemLightbox to re-fetch the details in case the user voted/bookmarked
	// a post within an Item
	forceRefreshLightboxOnNextOpen: boolean,
}

type FeedHubProps = {
	createNotification: createNotificationType,
	setBackgroundMode: (modeToSetTo: modesAppBackground) => void,
	// This is essentially used as a "notification"
	// It is given to FeedHub from App, and this will
	// allow us to check if we should add a feed.
	// When it changes to true, we'll spawn a feed and then change it back to false
	feedAdder: appFeedAdderType,
	// This is a handler passed from App which we run whenever the feed was actually added,
	// which updates the state of App to make feedAdder be false
	feedWasAdded: () => void,
	authToken: string,
	authTokenDecoded: authTokenFormat | null,
	newBookmarksWereFetched: boolean,
	newBookmarksWereRendered: () => void,
}





/**
 * This class is the component which manages the grids of feeds, rendering whichever
 * feeds the user requested and having whatever inter-feed functionality, such as swapping
 * feed locations with each other, that we want.
 *
 * This component should be rendered besides a LeftNav component, with the parent component having
 * the class "grid-container".
 *
 * @class FeedHub
 * @extends {React.Component<TestAppPropsType, TestAppStateType>}
 */
class FeedHub extends React.Component<FeedHubProps, FeedHubState> {
	/**
	 * Creates an instance of About. See the documentation of App.tsx for how this works; this is the same thing
	 * but different names etc.
	 * As `rssFeedEl` is an array, we can make it blank on load with `[]`, but we must define it here.
	 *
	 * @param {FeedHubProps} props
	 * @memberof FeedHub
	 */
	constructor(props: FeedHubProps) {
		super(props);
		this.state = {
			activeFeedsData: [],
			shouldRemoveFeed: false,
			feedIterator: 1,
			feedWillBeAddded: false,
			itemLightboxShouldDisplay: false,
			itemLightboxPostId: 0,
			itemLightboxFeedName: "",
			isItemFromBookmarks: false,
			swappables: null,
			swapFeeds: false,
			swapFeedFrom: 0,
			swapFeedTo: 0,
			itemPostIdsToRefresh: [],
			forceRefreshLightboxOnNextOpen: false,
		};

		this.dbgLog();

		// this.refFeeds = React.createRef<(Feed | null)[]>();

		this.refFeeds = [];
		// this.generateActiveFeedElements = this.generateActiveFeedElements.bind(this);
		this.setLightboxToForceRefreshOnNextOpen = this.setLightboxToForceRefreshOnNextOpen.bind(this);
		this.removeFeed = this.removeFeed.bind(this);
		this.openItemInLightbox = this.openItemInLightbox.bind(this);
		this.closeLightbox = this.closeLightbox.bind(this);
		this.moveFeedLeft = this.moveFeedLeft.bind(this);
		this.moveFeedRight = this.moveFeedRight.bind(this);
		this.addItemInFeedToRefreshQueue = this.addItemInFeedToRefreshQueue.bind(this);
	}
	// Create an array of ref feeds, which will be used to target any feeds we created
	// -------------------------------------------------------------------------
	// DO NOT ASK ME: https://dev.to/elramus/comment/eaa7
	// -------------------------------------------------------------------------
	// refFeeds: React.RefObject<(Feed | null)[]>;
	refFeeds: (Feed | FeedBookmarks | null)[];



	/**
	 * This checks if this component should be "debugged" via the process.env variables. If it's true,
	 * we bind console.log so that logging works normally, otherwise we make it not do anything.
	 *
	 * @param {*} [mode]
	 * @param {...any[]} optionalParams
	 * @memberof FeedHub
	 */
	dbgLog(
		// eslint-disable-next-line
		mode?: any,...optionalParams: any[]
	): void {
		if (globals.DEBUGGER_DBG_LOG === "true" && globals.DEBUGGER_DBG_FeedHub === "true") {
			this.dbgLog = console.log.bind(window.close);
		}
		else {
			this.dbgLog = (): void => {
				return;
			};
		}
	}



	// <(Feed | null)[]>([])
	/**
	 * When this mounts, we set the background and spawn a feed
	 *
	 *
	 * @memberof FeedHub
	 */
	componentDidMount(): void {
		this.dbgLog("FeedHub: componentDidMount");
		// -------------------------------------------------------------------------
		// TODO: keep a log of all active feeds in the localStorage
		// Then, whenever this component gets mounted, check if that localStorage is set
		// and load the feeds according to those in the localStorage. This will allow us to
		// "persist" the state of feeds between states (but not the items in the feeds - those will be re-fetched).
		// -------------------------------------------------------------------------
		this.addNewFeed();
		// this.refFeeds = React.useRef<(Feed | null)[]>([]);

		// Set the background mode to the image
		this.props.setBackgroundMode(modesAppBackground.GameIngImg);


		const urlParams = new URLSearchParams(window.location.search);
		const postItemIdParam = urlParams.get("post_item_id");
		const feedNameParam = urlParams.get("feed_name");

		if (postItemIdParam !== null && feedNameParam !== null) {
			// We create a nigh useless object for the post item details which will be used
			// to trigger the spawning of ItemLightbox. All the non-filled data isn't actually
			// necessary.
			// NOTE: The implementation of this basically makes the parentFeed name useless,
			// and it's not actually used in anything programmatically; it's simply passed
			// for display purposes.
			const fakePostItemDetails: postItemDetails = {
				postitem_id: parseInt(postItemIdParam),
				hash: "",
				title: "",
				url: "",
				content: "",
				date: "",
				author: "",
				total_votes: 0,
				user_vote_status: 0,
				user_bookmark_status: 0,
				user_fav_source_status: 0,
				source_id: 0,
				source_name: "",
				source_url: "",
				source_avatar: "",
				parentFeed: {
					feed_id: 0,
					name: feedNameParam,
				},
			};
			// We add a new history item and clear the parameters
			window.history.pushState({}, document.title, "/");
			this.openItemInLightbox(fakePostItemDetails, false);
		}
	}



	/**
	 * This runs when the component updates, refreshing the swappable grids if they changed in length
	 * to ensure the correct elements are targetted. It also handles adding Feed, FeedBookmarks, and
	 * FeedFavourites feeds if the user requested to open one, which set the props accordingly.
	 *
	 * @param {FeedHubProps} prevProps
	 * @param {FeedHubState} prevState
	 * @memberof FeedHub
	 */
	componentDidUpdate(prevProps: FeedHubProps, prevState: FeedHubState): void {
		this.dbgLog("FeedHub: componentDidUpdate");
		// If the number of grids has changed, we'll need to re-assign the swappables
		if (this.state.activeFeedsData.length !== prevState.activeFeedsData.length) {
			// this.swapFeedsDrag();
			this.refreshSwappableGrids();
		}

		// If the parent is telling us to add a new feed, we should be a good child
		// and listen. We also tell the parent we added a feed so we don't recursively try to
		// add feeds.
		if (
			this.props.feedAdder.shouldAdd &&
			this.props.feedAdder.type === feedTypes.Normal
		) {
			this.props.feedWasAdded();
			this.addNewFeed();
		}
		else if (
			this.props.feedAdder.shouldAdd &&
			this.props.feedAdder.type === feedTypes.Bookmarks
		) {
			this.props.feedWasAdded();
			this.addBookmarkFeed();
		}
		else if (
			this.props.feedAdder.shouldAdd &&
			this.props.feedAdder.type === feedTypes.Favourites &&
			this.props.feedAdder.extraId !== undefined
		) {
			this.props.feedWasAdded();
			this.addFavouritesFeed(this.props.feedAdder.extraId);
		}
	}


	/**
	 * This is a method passed down to Item elements to be able to tell ItemLightbox
	 * whether or not it should re-fetch its details even if the opened lightbox post is the
	 * same as the last opened lightbox post. This is so that if a user bookmarks or votes on
	 * a post within an Item, the ItemLightbox will know to update itself.
	 *
	 * @memberof FeedHub
	 */
	setLightboxToForceRefreshOnNextOpen(): void {
		this.setState({
			forceRefreshLightboxOnNextOpen: true,
		});
	}



	/**
	 * This creates the Swappable object for all of the feeds we want to make swappable.
	 * Additionally, we "hook" into the :swap and :stop events to track which elements
	 * the user is dragging to and from. This is done so that we can swap those elements
	 * arround in the state array because the way Swappable works just swaps the content,
	 * which results in the moveFeedLeft/moveFeedRight arrows not working.
	 *
	 * @note This does not need to run when a feed's source is changed,
	 * but it does need to run when a new feed is added or one is removed.
	 *
	 * @memberof FeedHub
	 */
	refreshSwappableGrids(): void {
		this.dbgLog("FeedHub: refreshSwappableGrids");
		const containerSelector = ".feedhub-container .feed-container";
		const containers = document.querySelectorAll(containerSelector);
		// NOTE: This destorying stage is important
		this.state.swappables?.destroy();

		// We create the swappables on the state so we can destroy them
		this.setState({
			// -----------------------------------------------------------------
			// See: https://github.com/Shopify/draggable/tree/master/src/Draggable
			// -----------------------------------------------------------------
			swappables: new Swappable(containers, {
				// This sets what to target and make draggable
				draggable: ".feedhub-container .feed-container",
				// This allows us to target only one specific part of the element
				// to make draggable
				handle: ".feed-draggable-part",
				mirror: {
					appendTo: containerSelector,
					constrainDimensions: true,
				},
				delay: 200,
				distance: 50,
			}),
			// This is the callback after setState
			// we do this because we want to be working with the actual swappables
			// and not the swappables before the state change (self reminder: setState is async)
		}, () => {
			// We hold the IDs here for the elements which will be moved from and to
			// based on which element was selected and which was "dropped" on
			// These IDs will correspond to the feedNumber, so we can use
			// findFeedIndexByFeedNumber the same way as we do on moveFeedLeft/moveFeedRight
			let mvFromId: number;
			let mvToId: number;
			// We want to cancel the default swapping behaviour
			this.state.swappables?.on("swappable:swap", (e) => {
				e.cancel();
			});
			// We want to grab the element the user is dragging over
			this.state.swappables?.on("drag:over", (e) => {
				mvFromId = parseInt(e.source.id);
				mvToId = parseInt(e.over.id);
				e.source.style.opacity = "50%";
			});
			// When the user drags out, we reset the move to target
			this.state.swappables?.on("drag:out", () => {
				mvToId = mvFromId;
			});
			// This is basically once the user "drops"
			this.state.swappables?.on("drag:stop", () => {
				// We copy the array of feeds so we can modify it
				const activeFeedsCopy = [...this.state.activeFeedsData];

				// We get the array index of the elements which we want to swap
				const mvFromIndex = this.findFeedIndexByFeedNumber(activeFeedsCopy, mvFromId);
				const mvToIndex = this.findFeedIndexByFeedNumber(activeFeedsCopy, mvToId);

				// We swap the elements
				this.swapFeedsViaDrag(mvFromIndex, mvToIndex);
			});
		});
	}



	/**
	 * This performs the swapping of the grids when they are dragged
	 *
	 * @param {number} from
	 * @param {number} to
	 * @memberof FeedHub
	 */
	swapFeedsViaDrag(from: number, to: number): void {
		// FIXME: For some reason this needs to be on a timeout, otherwise the state won't change???
		setTimeout(() => {
			const activeFeedsCopy = [...this.state.activeFeedsData];

			// moveArray(activeFeedsCopy, from, to);

			const tempEl = activeFeedsCopy[from];
			activeFeedsCopy[from] = activeFeedsCopy[to];
			activeFeedsCopy[to] = tempEl;

			// We update the state
			this.setState({
				activeFeedsData: activeFeedsCopy,
			});
		// FIXME: For some reason this needs to be on a timeout, otherwise the state won't change???
		}, 10);
	}



	/**
	 *
	 *
	 * @param {activeFeedData[]} arr
	 * @param {number} feedNumberToFind
	 * @return {*}  {number}
	 * @memberof FeedHub
	 */
	findFeedIndexByFeedNumber(arr: activeFeedData[], feedNumberToFind: number): number {
		this.dbgLog("FeedHub: findFeedIndexByFeedNumber");
		// We basically "loop" over the array and we "grab" each feed's feedNumber,
		// We then try to find the feed whose feedNumber matches the feedNumber passed
		return arr.map((feed) => {
			return feed.feedNumber;
		}).indexOf(feedNumberToFind);
	}



	/**
	 * Move the provided feed, via it's feedNumber, to the left (Backwards one in the array).
	 *
	 * This implementation makes the grid loop back over to the other side if it's at the end.
	 *
	 * @param {number} feedNumberToMove
	 * @memberof FeedHub
	 */
	moveFeedLeft(feedNumberToMove: number): void {
		this.dbgLog("FeedHub: moveFeedLeft");
		const activeFeedsCopy = [...this.state.activeFeedsData];

		const mvFrom = this.findFeedIndexByFeedNumber(activeFeedsCopy, feedNumberToMove);
		moveArray(activeFeedsCopy, mvFrom, mvFrom-1);
		this.setState({
			activeFeedsData: activeFeedsCopy,
		});
	}



	/**
	 * Move the provided feed, via it's feedNumber, to the right (Forwards one in the array).
	 *
	 * This implementation makes the grid loop back over to the other side if it's at the end.
	 *
	 * @param {number} feedNumberToMove
	 * @memberof FeedHub
	 */
	moveFeedRight(feedNumberToMove: number): void {
		this.dbgLog("FeedHub: moveFeedRight");
		const activeFeedsCopy = [...this.state.activeFeedsData];

		const mvFrom = this.findFeedIndexByFeedNumber(activeFeedsCopy, feedNumberToMove);

		// If the next element in the array is not undefined, aka there IS an element
		// to the "right" of the selected grid, we move it
		if (activeFeedsCopy[mvFrom+1] !== undefined) {
			moveArray(activeFeedsCopy, mvFrom, mvFrom+1);
		}
		else {
			// > Argument of type 'activeFeedData | undefined' is not assignable to parameter of type 'activeFeedData'.
			// > Type 'undefined' is not assignable to type 'activeFeedData' ts(2345)
			// See above comment from TypeScript^  Here we "assert" that the array activeFeedData is a activeFeedData
			// and what we basically do is move the "popped" (last) activeFeedData to the beginning
			activeFeedsCopy.splice(0, 0, activeFeedsCopy.pop() as activeFeedData);
		}
		this.setState({
			activeFeedsData: activeFeedsCopy,
		});
	}



	/**
	 * To add a new feed, we add a new item to the array of activeFeeds.
	 * The "item" added is a Feed element which also has the feedIterator as its
	 * feedNumber, so that when it triggers props.removeThisFeed, we'll be able to identify it.
	 *
	 * @memberof FeedHub
	 */
	addNewFeed(): void {
		this.dbgLog("FeedHub: addNewFeed");
		this.setState((prevState) => ({
			activeFeedsData: [...prevState.activeFeedsData, {feedNumber: this.state.feedIterator, feedType: feedTypes.Normal}],
			// We iterate over the feed numbers, ensuring that every newly added feel is of
			// a +1 higher feedNumber. This also doesn't reset when the array is spliced so it's
			// always unique.
			feedIterator: this.state.feedIterator + 1,
		}));
	}



	/**
	 * To add a new bookmark feed, we add a new item to the array of activeFeeds.
	 * The "item" added is a FeedBookmark element which also has the feedIterator as its
	 * feedNumber, so that when it triggers props.removeThisFeed, we'll be able to identify it.
	 *
	 * @memberof FeedHub
	 */
	addBookmarkFeed(): void {
		this.dbgLog("FeedHub: addBookmarkFeed");
		this.setState((prevState) => ({
			activeFeedsData: [...prevState.activeFeedsData, {feedNumber: prevState.feedIterator, feedType: feedTypes.Bookmarks}],
			// We iterate over the feed numbers, ensuring that every newly added feel is of
			// a +1 higher feedNumber. This also doesn't reset when the array is spliced so it's
			// always unique.
			feedIterator: this.state.feedIterator + 1,
		}));
	}



	/**
	 * This adds a favourites feed the same was as the normal Feed, but it also adds an extra id
	 * which is what will be used to initialise the source ID.
	 *
	 * @param {number} clickedSourceId
	 * @memberof FeedHub
	 */
	addFavouritesFeed(clickedSourceId: number): void {
		this.dbgLog("FeedHub: addFavouritesFeed");
		if (this.props.feedAdder.extraId !== undefined) {
			const extraId = this.props.feedAdder.extraId;
			this.setState((prevState: FeedHubState) => ({
				activeFeedsData: [...prevState.activeFeedsData, {feedNumber: prevState.feedIterator, feedType: feedTypes.Favourites, extraId: extraId}],
				// We iterate over the feed numbers, ensuring that every newly added feel is of
				// a +1 higher feedNumber. This also doesn't reset when the array is spliced so it's
				// always unique.
				feedIterator: this.state.feedIterator + 1,
			}));
		}
	}



	/**
	 * This removes the feed from the state based on its key.
	 *
	 * When each <Feed/> is created in generateActiveFeedElements, they are given a feedNumber which corresponds
	 * to a number based on the activeFeeds array.
	 *
	 * @param {number} feedNumberToDelete
	 * @memberof FeedHub
	 */
	removeFeed(feedNumberToDelete: number): void {
		this.dbgLog("FeedHub: removeFeed");
		// We copy the activeFeeds array
		const activeFeedsCopy = [...this.state.activeFeedsData];

		const indexWhereRemove = this.findFeedIndexByFeedNumber(activeFeedsCopy, feedNumberToDelete);

		// We then splice the array, aka removing the item in the array of numbers
		// and from the array of actual elements
		activeFeedsCopy.splice(indexWhereRemove, 1);

		// Update the state with the new array.
		this.setState({
			activeFeedsData: activeFeedsCopy,
		});
	}



	/**
	 * This sets the state to reflect that the lightbox should display, which is what <ItemLightbox/> looks
	 * at. When itemLightboxShouldDisplay becomes true, the lightbox will display the post ID according to
	 * itemLightboxPostId.
	 *
	 * Additionally, here we figure out if the lightbox should display "normally", or with the data from bookmarks.
	 *
	 * @param {postItemDetails} postDataToOpen
	 * @param {boolean} isFromBookmarks
	 * @memberof FeedHub
	 */
	openItemInLightbox(postDataToOpen: postItemDetails, isFromBookmarks: boolean): void {
		this.dbgLog("FeedHub: openItemInLightbox");
		this.setState({
			itemLightboxShouldDisplay: true,
			itemLightboxPostId: postDataToOpen.postitem_id,
			itemLightboxFeedName: postDataToOpen.parentFeed.name,
			isItemFromBookmarks: isFromBookmarks,
		});
	}



	/**
	 * This allows the ItemLightbox to make a specified Item, based on the postitem_id, to
	 * refresh itself. This is used in cases where the user votes or bookmarks a post within
	 * the ItemLightbox so that the Item will also re-fetch its details.
	 *
	 * @param {number} postitemId
	 * @memberof FeedHub
	 */
	addItemInFeedToRefreshQueue(postitemId: number): void {
		// prevState is used here instead of this.state
		// For why, see: https://reactjs.org/docs/state-and-lifecycle.html#state-updates-may-be-asynchronous
		// even though I didn't run into any issues, I just saw this.
		this.setState((prevState: FeedHubState) => ({
			itemPostIdsToRefresh: [...prevState.itemPostIdsToRefresh, postitemId],
		}));
	}



	/**
	 * This simply closes the lightbox by setting itemLightboxShouldDisplay to false. This works because
	 * <ItemLightbox/> only renders when it's set to true.
	 *
	 * @memberof FeedHub
	 */
	closeLightbox(): void {
		this.dbgLog("FeedHub: closeLightbox");
		this.setState({
			itemLightboxShouldDisplay: false,
		});
	}



	/**
	 * Renders a button to toggle the elements using [[toggleSomething]] and renders the specified component.
	 * `this.state.rssFeedEl` displays all of the "feeds" in that variable in the state, and as the state
	 * is changed with this.setState, this gets updated, rendering the new elements
	 *
	 * Note that the state here gets wiped when the component is unmounted
	 * see [[https://stackoverflow.com/questions/45470100/how-do-i-persist-state-data-on-page-navigation]]
	 * and [[https://www.joshwcomeau.com/react/persisting-react-state-in-localstorage/]]
	 * for how posts within a feed could perhaps be saved to localStorage, which might play nicely into the
	 * feature of keeping bookmarks locally - once we get there
	 * @return {JSX.Element}
	 * @memberof FeedHub
	 */
	render(): JSX.Element {
		return (
			<div className="feedhub-container">
				{/* This renders the Item elements based on the data we've stored in the state
					it's done like this instead of generating the Items and then storing them in the state so that we can actually
					receive prop updates in the Items */}
				{
					this.state.activeFeedsData.map((feedData) => {
						// -----------------------------------------------------
						// Render normal Feed
						// -----------------------------------------------------
						if (feedData.feedType === feedTypes.Normal) {
							return <Feed
								createNotification={this.props.createNotification}
								ref={(reff) => this.refFeeds.push(reff)}
								key={feedData.feedNumber}
								authToken={this.props.authToken}
								feedNumber={feedData.feedNumber}
								removeThisFeed={this.removeFeed}
								openItemInLightbox={this.openItemInLightbox}
								moveFeedLeft={this.moveFeedLeft}
								moveFeedRight={this.moveFeedRight}
								itemPostIdsToRefresh={this.state.itemPostIdsToRefresh}
								setLightboxToForceRefreshOnNextOpen={this.setLightboxToForceRefreshOnNextOpen}
							/>;
						}
						// -----------------------------------------------------
						// Render bookmarks Feed
						// -----------------------------------------------------
						else if (feedData.feedType === feedTypes.Bookmarks) {
							return <FeedBookmarks
								createNotification={this.props.createNotification}
								ref={(reff) => this.refFeeds.push(reff)}
								key={feedData.feedNumber}
								authToken={this.props.authToken}
								feedNumber={feedData.feedNumber}
								removeThisFeed={this.removeFeed}
								openItemInLightbox={this.openItemInLightbox}
								moveFeedLeft={this.moveFeedLeft}
								moveFeedRight={this.moveFeedRight}
								newBookmarksWereFetched={this.props.newBookmarksWereFetched}
								newBookmarksWereRendered={this.props.newBookmarksWereRendered}
								itemPostIdsToRefresh={this.state.itemPostIdsToRefresh}
								setLightboxToForceRefreshOnNextOpen={this.setLightboxToForceRefreshOnNextOpen}
							/>;
						}
						// -----------------------------------------------------
						// Render favourites Feed
						// -----------------------------------------------------
						else if (feedData.feedType === feedTypes.Favourites) {
							// Need to check for undefined
							if (feedData.extraId !== undefined) {
								return <FeedFavourites
									createNotification={this.props.createNotification}
									key={this.state.feedIterator}
									authToken={this.props.authToken}
									feedNumber={this.state.feedIterator}
									removeThisFeed={this.removeFeed}
									openItemInLightbox={this.openItemInLightbox}
									moveFeedLeft={this.moveFeedLeft}
									moveFeedRight={this.moveFeedRight}
									initSourceId={feedData.extraId}
									itemPostIdsToRefresh={this.state.itemPostIdsToRefresh}
									setLightboxToForceRefreshOnNextOpen={this.setLightboxToForceRefreshOnNextOpen}
								/>;
							}
						}
					})
				}

				{/* See openItemInLightbox(), closeLightbox(), and the actual implementation of ItemLightbox for comments*/}
				<ItemLightbox
					authToken={this.props.authToken}
					authTokenDecoded={this.props.authTokenDecoded}
					createNotification={this.props.createNotification}
					parentFeedName={this.state.itemLightboxFeedName}
					itemLightboxShouldDisplay={this.state.itemLightboxShouldDisplay}
					itemLightboxPostId={this.state.itemLightboxPostId}
					forceRefreshPostDetails={this.state.forceRefreshLightboxOnNextOpen}
					isItemFromBookmarks={this.state.isItemFromBookmarks}
					addItemInFeedToRefreshQueue={this.addItemInFeedToRefreshQueue}
					closeLightbox={this.closeLightbox}
				/>
			</div>
		);
	}
}

export default FeedHub;
