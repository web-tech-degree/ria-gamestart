import React, {Fragment} from "react";
import "./Feed.css";
import {globals} from "../../logic/globals";





// -----------------------------------------------------------------------------
// Types
import type {feedDetails, postItemDetails} from "./types";
import type {createNotificationType} from "../types";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Modes
import {modesFeed} from "./modes";
import {modesNotifTypes} from "../modes";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Components
import Item from "./Item";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Libraries
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Assets
// -----------------------------------------------------------------------------





type FeedState = {
	feedMode: modesFeed,
	searchQuery: string,
	activeFeed: feedDetails,
	feedToChangeTo: string,
	// This will be an array of objects of id/name
	availableFeeds: Array<feedDetails>
	highestPostId: number,
	lowestPostId: number,
	newItems: Array<postItemDetails>,
	displayedItemsData: postItemDetails[],
	feedDropdownIsOpen: boolean,
	// This is the time at which the last grabPreviousFetch request was made
	// This is used to limit how fast you can grab by scrolling
	grabPreviousLastTime: number,
	// This is used to store the interval that checks if any new posts are available
	checkNewPostsAvailableInterval: ReturnType<typeof setInterval> | null,
	fetchAvailablePostInterval: ReturnType<typeof setInterval> | null,
	newPostAvailability: {isAvailable: boolean, availableCount: number},
}

type FeedProps = {
	createNotification: createNotificationType,
	authToken: string,
	feedNumber: number,
	// This is a function given by FeedHub which removes this feed using
	// this feed's feedNumber
	removeThisFeed: (feedNumber: number) => void,
	// This is the method which triggers the <ItemLightbox/> in FeedHub to display
	// We don't actually do anything with it here; instead, give this to each item
	// to open.
	openItemInLightbox: (postDataToOpen: postItemDetails, isFromBookmarks: boolean) => void,
	moveFeedLeft: (feedNumber: number) => void,
	moveFeedRight: (feedNumber: number) => void,
	itemPostIdsToRefresh: number[],
	setLightboxToForceRefreshOnNextOpen: () => void,
}

type postData = {
	message: string,
	posts: postItemDetails[],
	arrangement: string,
}

// This allows a selection of where to add posts
// They will either be added to the top (latest) or to the bottom (previous)
// NOTE: for some reason ESLint thinks this was being unused, even though it was being used
// so I just ignored them, lol.
// eslint-disable-next-line
enum addItemsWhere {
	// eslint-disable-next-line
	latest,
	// eslint-disable-next-line
	previous,
}





/**
 * This class is the component which is spawned for every feed. It manages fetching periodically
 * and on scroll, and it spawns items based on the fetched post info.
 *
 * For the bookmark feed,see FeedBookmarks.tsx
 *
 * @class Feed
 * @extends {React.Component}
 */
class Feed extends React.Component<FeedProps, FeedState> {
	/**
	 * Creates an instance of Feed.
	 * @param {FeedProps} props
	 * @memberof Feed
	 */
	constructor(props: FeedProps) {
		super(props);
		this.state = {
			feedMode: modesFeed.Category,
			searchQuery: "",
			activeFeed: {feed_id: 0, name: ""},
			feedToChangeTo: "",
			availableFeeds: [],
			highestPostId: 0,
			lowestPostId: 0,
			newItems: [],
			displayedItemsData: [],
			feedDropdownIsOpen: false,
			grabPreviousLastTime: Date.now(),
			checkNewPostsAvailableInterval: null,
			fetchAvailablePostInterval: null,
			newPostAvailability: {isAvailable: true, availableCount: 1},
		};

		this.dbgLog();

		this.refFeedContentContainer = React.createRef();

		// We use this to target the dropdown container
		this.refFeedOptionDropdown = React.createRef();
		this.refFeedOptionSearch = React.createRef();
		// this.addLatestItems = this.addLatestItems.bind(this);
		this.searchFeedByContent = this.searchFeedByContent.bind(this);
		this.grabLatestItems = this.grabLatestItems.bind(this);
		this.changeFeedOfficial = this.changeFeedOfficial.bind(this);
		this.openFeedDropdowns = this.openFeedDropdowns.bind(this);
		this.handleClickOutsideDropdown = this.handleClickOutsideDropdown.bind(this);
		this.onFeedScroll = this.onFeedScroll.bind(this);
		this.renderRefreshPrompt = this.renderRefreshPrompt.bind(this);
	}
	refFeedContentContainer: React.RefObject<HTMLDivElement>;
	refFeedOptionDropdown: React.RefObject<HTMLDivElement>;
	refFeedOptionSearch: React.RefObject<HTMLInputElement>;



	/**
	 * This checks if this component should be "debugged" via the process.env variables. If it's true,
	 * we bind console.log so that logging works normally, otherwise we make it not do anything.
	 *
	 * @param {*} [mode]
	 * @param {...any[]} optionalParams
	 * @memberof Feed
	 */
	dbgLog(
		// eslint-disable-next-line
		mode?: any,...optionalParams: any[]
	): void {
		if (globals.DEBUGGER_DBG_LOG === "true" && globals.DEBUGGER_DBG_Feed === "true") {
			this.dbgLog = console.log.bind(window.close);
		}
		else {
			this.dbgLog = (): void => {
				return;
			};
		}
	}



	/**
	 * See https://stackoverflow.com/a/42234988 and ItemLightbox.tsx
	 *
	 * @memberof FeedEditor
	 */
	componentDidMount(): void {
		this.dbgLog("Feed: componentDidMount");
		document.addEventListener("mousedown", this.handleClickOutsideDropdown);

		const sti1 = setInterval(() => {
			this.checkNewPostsAvailable();
		}, 1000);

		const sti2 = setInterval(() => {
			this.fetchAvailableContentIfWithinPostLimit();
		}, 1000);


		// Add an interval to the state. This'll trigger checkNewPostsAvailable every second
		this.setState({
			checkNewPostsAvailableInterval: sti1,
			fetchAvailablePostInterval: sti2,
		});

		// The call to getAvailableFeeds will return a boolean based on whether
		// or not we actually did get and feeds. If we did, aka true, then we also
		// call the method to load sources for that feed
		// ---------------------------------------------------------------------
		// TODO: Add if statmenet
		// ---------------------------------------------------------------------.
		this.getAvailableFeeds().then(() => {
			// This does the same thing as this.changeFeedOfficial, but instead of relying
			// on the input form's value, we use that first feed in the availableFeeds
			this.setState({
				activeFeed: this.state.availableFeeds[0],
				// Wipe the state from previous shenanigans
				highestPostId: 0,
				lowestPostId: 0,
				newItems: [],
				displayedItemsData: [],
			}, () => {
				// We then grab the latest items.
				// This is done in the callback so that grabLatestItems only runs once the state is updated
				this.grabLatestItems(true);
			});
		});
	}



	/**
	 * See https://stackoverflow.com/a/42234988 and ItemLightbox.tsx
	 *
	 * @memberof Feed
	 */
	componentWillUnmount(): void {
		clearInterval(this.state.checkNewPostsAvailableInterval as ReturnType<typeof setInterval>);
		clearInterval(this.state.fetchAvailablePostInterval as ReturnType<typeof setInterval>);
		this.dbgLog("Feed: componentWillUnmount");
		document.removeEventListener("mousedown", this.handleClickOutsideDropdown);
	}



	/**
	 * This adds <Item/>s to the state of rendered items.
	 *
	 * This current implementation basically just "lets go" of the <Item/>s' states in the sense
	 * that they are rendered but we do not hold onto them.
	 *
	 * This should be changed so that addItems adds "stuff" to an array in the state, and then the render
	 * method uses that array of "stuff" to render the actual <Item/>s, which would allow us to still have
	 * access to the things we give to each <Item/>.
	 *
	 * Alternatively, I think something could be done with refs to target the <Item/>s after they have been spawned.
	 *
	 * But basically, because this is implemented imperfectly, one <Item/> cannot really "tell another <Item/> to update",
	 * and I think there were some other things that couldn't really be implemented because of this.
	 *
	 * @param {postData} data
	 * @param {addItemsWhere} location
	 * @memberof Feed
	 */
	addItems(data: postData, location: addItemsWhere): void {
		this.dbgLog("Feed: addItems");
		// const newItemArray: React.ReactElement<Item>[] = [];
		const newItemArray: postItemDetails[] = [];

		// Each thing in data.posts shall be a postItemDetails
		// which contains all of the data for one post item thingy
		// We then create a newItem based off of the data of this, and add
		// that to the array of new items
		for (let i = 0; i < data.posts.length; i++) {
			// We add the feed details to the post's data so that we can identify which
			// feed the posts were from. This is useful for passing the feed name from
			// Item into ItemLightbox
			data.posts[i].parentFeed = this.state.activeFeed;

			// Generate the element, passing down the necessary info to create it
			newItemArray.push(data.posts[i]);
		}

		// The location can ONLY ever be set to latest or previous
		// this selects which was set
		if (location === addItemsWhere.latest) {
			this.setState((prevState) => ({
				// [0] is used to give us the first data.posts item which should
				// have the highest postitem_id which we will then use to keep track of which posts to get
				highestPostId: data.posts[0].postitem_id,
				// This adds to the bottom of the array, aka top of page
				displayedItemsData: newItemArray.concat(prevState.displayedItemsData),
			}));
		}
		else if (location === addItemsWhere.previous) {
			this.setState((prevState) => ({
				// Same as the above, but instead updates the lowest post IDs and adds the post to
				// the end of the page
				lowestPostId: data.posts[data.posts.length -1].postitem_id,
				displayedItemsData: prevState.displayedItemsData.concat(newItemArray),
				// We also update this to allow new fetches
				grabPreviousLastTime: Date.now(),
			}));
		}
	}



	/**
	 * This checks if any new posts are available if the mode is currently set
	 * to category browsing.
	 *
	 * @return {*}  {(void | Error)}
	 * @memberof Feed
	 */
	checkNewPostsAvailable(): void | Error {
		if (this.state.activeFeed !== undefined) {
			if (this.state.feedMode === modesFeed.Category) {
				this.dbgLog("Feed: checkNewPostsAvailable");
				fetch(process.env.REACT_APP_API_URL + "__api.php", {

					method: "POST",
					credentials: "omit",
					body: JSON.stringify({
						request_type: "check_new_posts_available_by_feed",
						active_feed_id: this.state.activeFeed.feed_id,
						highest_current: this.state.highestPostId,
					}),
				})
					.then((response) => response.json())
					.then((data) => {
						if (data.message === "new_posts_available") {
							this.dbgLog("Feed: checkNewPostsAvailable: New posts available");
							this.setState({
								newPostAvailability: {isAvailable: true, availableCount: data.count},
							});
						}
						else if (data.message === "no_new_posts") {
							// We don't set the state here because we expect grabLatestPosts
							// to simply have ran and set newPostAvailability to false
							this.dbgLog("Feed: checkNewPostsAvailable: No new posts available");
						}
					});
			}
		}
		else {
			return new Error("no_active_feed");
		}
	}



	/**
	 * This fetches the posts if any have been marked available, only if the number
	 * of new posts is within the specified limit.
	 *
	 * If you don't want this functionality, comment this out (well, preferably just disable the interval)
	 * and uncomment the manual fetch new in renderRefreshPrompt
	 *
	 * @memberof Feed
	 */
	fetchAvailableContentIfWithinPostLimit(): void {
		if (this.state.newPostAvailability.availableCount <= parseInt(process.env.REACT_APP_MAX_NEW_POST_FETCH_BEFORE_REFRESH as string)) {
			this.grabLatestItems(false);
		}
	}



	/**
	 * This is the fetch statement which shall return the postData. This is supposed
	 * to be somewhat "abstracted" and give postData regardless of the mode.
	 *
	 * @return {*}  {Promise<postData>}
	 * @memberof Feed
	 */
	async grabLatestItemsFetch(): Promise<postData> {
		this.dbgLog("Feed: grabLatestItemsFetch");
		if (this.state.activeFeed !== undefined) {
			if (this.state.feedMode === modesFeed.SearchInSelectedFeed) {
				this.dbgLog("Feed: grabLatestItemsFetch: SearchInSelectedFeed");

				return fetch(process.env.REACT_APP_API_URL + "__api.php", {
					method: "POST",
					credentials: "omit",
					body: JSON.stringify({
						request_type: "get_latest_posts_in_feed_by_search",
						user_auth_token: this.props.authToken,
						active_feed_id: this.state.activeFeed.feed_id,
						search_query: this.state.searchQuery,
					}),
				})
					.then((response) => response.json());
			}
			else if (this.state.feedMode === modesFeed.Category) {
				this.dbgLog("Feed: grabLatestItemsFetch: Category");

				return fetch(process.env.REACT_APP_API_URL + "__api.php", {
					method: "POST",
					credentials: "omit",
					body: JSON.stringify({
						request_type: "get_latest_posts_by_feed",
						user_auth_token: this.props.authToken,
						active_feed_id: this.state.activeFeed.feed_id,
						highest_current: this.state.highestPostId,
					}),
				})
					.then((response) => response.json());
			}
			else {
				this.dbgLog("Feed: grabLatestItemsFetch: default");

				return fetch(process.env.REACT_APP_API_URL + "__api.php", {
					method: "POST",
					credentials: "omit",
					body: JSON.stringify({
						request_type: "get_latest_posts_by_feed",
						user_auth_token: this.props.authToken,
						active_feed_id: this.state.activeFeed.feed_id,
						highest_current: this.state.highestPostId,
					}),
				})
					.then((response) => response.json());
			}
		}
		else {
			return Promise.reject(new Error("no_active_feed"));
		}
	}



	/**
	 * Try to fetch the latest posts based on the category of this feed, adding any new posts to the top
	 * of the feed. Also, if this is being fetched as the "initial" fetch, this method will also set the
	 * lowest post ID.
	 *
	 * @usage Trigger this either on a timer or on some sort of refresh button
	 *
	 * @param {boolean} isInitialRequest
	 * @memberof Feed
	 */
	grabLatestItems(isInitialRequest: boolean): void {
		// Only make a request if posts are available
		if (this.state.newPostAvailability.isAvailable) {
			this.dbgLog("Feed: grabLatestItems");
			this.grabLatestItemsFetch()
				.then((data) => {
					this.setState({
						newPostAvailability: {isAvailable: false, availableCount: 0},
					});
					if (data.message === "fetch_success") {
						// If we are too far behind, the API still sent us the latest x feeds, but we first need to
						// wipe the current feeds because this will act like a "refresh"
						// This also sets the lowest post ID again
						if (data.arrangement === "too_far_behind") {
							this.setState({
								displayedItemsData: [],
								lowestPostId: data.posts[data.posts.length-1].postitem_id,

							});
						}
						// If there's at least 1 item, we add the items to the feed (top)
						// though this should never return less than 1 because we're already checking
						// for available posts
						if (data.posts.length > 0) {
							// If this is the first request for this specific feed, we also want to
							// set the lowest post ID
							if (isInitialRequest) {
								this.setState({
									lowestPostId: data.posts[data.posts.length-1].postitem_id,
								});
							}
							this.addItems(data, addItemsWhere.latest);
						}
					}
				})
				.catch((error) => {
					if (error === "no_active_feed") {
						this.dbgLog("Error: Couldn't grab items, No Active Feed");
					}
				});
		}
	}



	/**
	 * Similar to grabLatestItemsFetch, this tries to abstract the fetch logic
	 * away from the handling of the data.
	 *
	 * @return {*}  {Promise<postData>}
	 * @memberof Feed
	 */
	async grabPreviousItemsFetch(): Promise<postData> {
		if (this.state.feedMode === modesFeed.SearchInSelectedFeed) {
			this.dbgLog("Feed: grabPreviousItemsFetch: SearchInSelectedFeed");

			return fetch(process.env.REACT_APP_API_URL + "__api.php", {
				method: "POST",
				credentials: "omit",
				body: JSON.stringify({
					request_type: "get_previous_posts_in_feed_by_search",
					user_auth_token: this.props.authToken,
					active_feed_id: this.state.activeFeed.feed_id,
					lowest_current: this.state.lowestPostId,
					search_query: this.state.searchQuery,
				}),
			})
				.then((response) => response.json());
		}
		else if (this.state.feedMode === modesFeed.Category) {
			this.dbgLog("Feed: grabPreviousItemsFetch: Category");

			return fetch(process.env.REACT_APP_API_URL + "__api.php", {
				method: "POST",
				credentials: "omit",
				body: JSON.stringify({
					request_type: "get_previous_posts_by_feed",
					user_auth_token: this.props.authToken,
					active_feed_id: this.state.activeFeed.feed_id,
					lowest_current: this.state.lowestPostId,
				}),
			})
				.then((response) => response.json());
		}
		else {
			this.dbgLog("Feed: grabPreviousItemsFetch: default");

			return fetch(process.env.REACT_APP_API_URL + "__api.php", {
				method: "POST",
				credentials: "omit",
				body: JSON.stringify({
					request_type: "get_previous_posts_by_feed",
					user_auth_token: this.props.authToken,
					active_feed_id: this.state.activeFeed.feed_id,
					lowest_current: this.state.lowestPostId,
				}),
			})
				.then((response) => response.json());
		}
	}



	/**
	 * This gets the previous items in the feed
	 *
	 * @usage Trigger this once the user hits, or is close to hitting, the bottom of the feed
	 *
	 * @memberof Feed
	 */
	grabPreviousItems(): void {
		this.dbgLog("Feed: grabPreviousItems");
		const elapsed = Date.now() - this.state.grabPreviousLastTime;

		// If not enough time has passed, try again
		if (elapsed < parseInt(process.env.REACT_APP_POST_FETCH_DELAY as string)) {
			this.dbgLog("Feed: grabPreviousItems: waiting");
			setTimeout(() => {
				this.grabPreviousItems();
			}, 1);
			return;
		}

		this.dbgLog("Feed: grabPreviousItems: executing");
		this.grabPreviousItemsFetch()
			.then((data: postData) => {
				// We expect to receive data matching postData type
				if (data.message === "fetch_success") {
					// If there's at least 1 item, we add the items to the feed (bottom)
					if (data.posts.length > 0) {
						this.addItems(data, addItemsWhere.previous);
					}
				}
			});
	}



	/**
	 * This changes the feedMode to search posts in the selected feed (on the server)
	 *
	 * It also sets the activeFeed, aka what will be searched, to the search query
	 * that the user typed. This will be used in the fetch.
	 *
	 * The state of the feed is also reset
	 *
	 * @memberof Feed
	 */
	searchFeedByContent(): void {
		this.dbgLog("Feed: searchFeedByContent");
		// We only search the user's query if they've actually typed anything
		// Otherwise, we display the feed as normal
		if (this.state.searchQuery.length > 0) {
			this.setState({
				feedMode: modesFeed.SearchInSelectedFeed,
				newPostAvailability: {isAvailable: true, availableCount: 0},
				// activeFeed: {id: 0, name: this.state.searchQuery},
				// Wipe the state from previous shenanigans
				highestPostId: 0,
				lowestPostId: 0,
				newItems: [],
				displayedItemsData: [],
			}, () => {
				// We then grab the latest items.
				// This is done in the callback so that the state is updated when it runs
				this.grabLatestItems(true);
			});
		}
		else {
			this.setState({
				feedMode: modesFeed.Category,
				newPostAvailability: {isAvailable: true, availableCount: 0},
				highestPostId: 0,
				lowestPostId: 0,
				newItems: [],
				displayedItemsData: [],
			}, () => {
				this.grabLatestItems(true);
			});
		}
	}



	/**
	 * This changes which feed to display, based on "official" feeds. The naming here is
	 * probably a little weird, but basically "official" here denotes feeds that are in the DB.
	 *
	 * This differs to the search mode.
	 *
	 * @param {string} feedToSwitchTo
	 * @memberof Feed
	 */
	changeFeedOfficial(feedToSwitchTo: feedDetails): void {
		this.dbgLog("Feed: changeFeedOfficial");
		this.hideFeedDropdowns();
		this.setState({
			feedMode: modesFeed.Category,
			// Here we wipe the search query because we're switching the actual feed
			searchQuery: "",
			activeFeed: {feed_id: feedToSwitchTo.feed_id, name: feedToSwitchTo.name},
			// Wipe the state from previous shenanigans
			highestPostId: 0,
			lowestPostId: 0,
			newItems: [],
			displayedItemsData: [],
			newPostAvailability: {isAvailable: true, availableCount: 0},
		}, () => {
			// We then grab the latest items.
			// This is done in the callback so that grabLatestItems only runs once the state is updated
			this.grabLatestItems(true);
		});
	}



	/**
	 * This gets all the available feed names and loads them into the state.
	 *
	 * It returns true or false depending on whether or not fetching the feed names
	 * was successful.
	 *
	 * @return {*}  {Promise<boolean>}
	 * @memberof Feed
	 */
	getAvailableFeeds(): Promise<boolean> {
		this.dbgLog("Feed: getAvailableFeeds");
		return fetch(process.env.REACT_APP_API_URL + "__api.php", {
			method: "POST",
			credentials: "omit",
			body: JSON.stringify({
				request_type: "feed_get_available",
				// TODO: Use JWT token and validate
			}),
		})
			.then((response) => response.json())
			.then((data) => {
				this.dbgLog("Feed: getAvailableFeeds response");
				if (data.message === "fetch_success") {
					this.dbgLog("Feed: getAvailableFeeds success");
					// We store the ids and names of all the feeds
					this.setState({
						availableFeeds: data.feeds,
					});
					return true;
				}
				// NOTE: Manage error states
				// This only really needs to be one else;
				// I don't see a reason to care about specifics here
				else {
					return false;
				}
			});
	}



	/**
	 * See https://stackoverflow.com/a/42234988
	 *
	 * This handles the clicks and discerns whether or not the elements we defined with ref
	 * as "inner" elements contains the element which was clicked.
	 *
	 * This way, if the user clicks on something inside of a <div ref={this.refInnerEl1}>,
	 * we can tell that the user clicked inside, hence we will not close.
	 *
	 * This also is neat because it allows a user to click and drag text on an "inner" element,
	 * and then drag their mouse "outside" and release, and it still will not close; a click has to
	 * be explicitly on the outside for us to close.
	 *
	 * @param {MouseEvent} event
	 * @memberof Feed
	 */
	handleClickOutsideDropdown(event: MouseEvent): void {
		this.dbgLog("Feed: handleClickOutsideDropdown");

		/**
		 * This is just a function which checks if the provided ref contains the clicked element.
		 * This is done just to clean up the check with a function instead of copy-pasting the line 5000 times
		 *
		 * @param {React.RefObject<HTMLDivElement>} refTarget
		 * @return {*}  {string}
		 */
		const checkClickEventNotInRefElement = (refTarget: React.RefObject<HTMLDivElement>): boolean => {
			// First check to make sure the target is not null
			if (refTarget.current) {
				return refTarget.current.contains(event.target as Node) ? false : true;
			}
			// NOTE: I don't really know if this will ever fire, because the refTarget should be set before the user
			// clicks anything, but whatever - TypeScript was complaining
			else {
				return false;
			}
		};

		// If the user clicked off of the dropdown, hide it
		if (this.refFeedOptionDropdown.current) {
			if (checkClickEventNotInRefElement(this.refFeedOptionDropdown) &&
				checkClickEventNotInRefElement(this.refFeedOptionSearch)
			) {
				this.hideFeedDropdowns();
			}
		}
	}



	/**
	 * This opens the dropdown if it is not already open. It sets the max height as we use a transition.
	 *
	 * This is done with max height instead of height so that we retain the functionality of "the dropdown
	 * list is smaller if it doesn't need to be larger""
	 *
	 * @param {MouseEvent<HTMLDivElement, MouseEvent>} event
	 * @memberof Feed
	 */
	openFeedDropdowns(): void {
		this.dbgLog("Feed: openFeedDropdowns");
		if (this.refFeedOptionDropdown.current && !this.state.feedDropdownIsOpen) {
			this.refFeedOptionDropdown.current.style.maxHeight = "30vh";
			this.setState({
				feedDropdownIsOpen: true,
			});
		}
	}



	/**
	 * This hides the feed dropdown by setting the max height to 0vh, and it updates the state
	 * so that it can be opened
	 *
	 * @param {MouseEvent<HTMLDivElement, MouseEvent>} event
	 * @memberof Feed
	 */
	hideFeedDropdowns(): void {
		this.dbgLog("Feed: hideFeedDropdowns");
		if (this.refFeedOptionDropdown.current) {
			this.refFeedOptionDropdown.current.style.maxHeight = "0vh";
			this.setState({
				feedDropdownIsOpen: false,
			});
		}
	}



	/**
	 * This is triggered every time a user scrolls, and checks if they are at the bottom of the feed.
	 *
	 * @memberof Feed
	 */
	onFeedScroll(): void {
		// Don't run if undefined/null etc.
		if (this.refFeedContentContainer === null || this.refFeedContentContainer.current === null || this.refFeedContentContainer.current === undefined) {
			return;
		}


		const fTop = this.refFeedContentContainer.current.scrollTop;
		const cHeight = this.refFeedContentContainer.current.clientHeight;
		const sHeight = this.refFeedContentContainer.current.scrollHeight;

		// If the user is at the bottom, we try fetching
		if (Math.ceil(fTop) + cHeight >= sHeight) {
			this.dbgLog("Feed: onFeedScroll: User at bottom");
			this.grabPreviousItems();
		}
	}



	/**
	 * This renders a prompt to the user if new posts are available
	 *
	 * @return {*}  {JSX.Element}
	 * @memberof Feed
	 */
	renderRefreshPrompt(): JSX.Element | undefined {
		if (this.state.newPostAvailability.isAvailable && this.state.feedMode === modesFeed.Category) {
			if (this.state.newPostAvailability.availableCount <= parseInt(process.env.REACT_APP_MAX_NEW_POST_FETCH_BEFORE_REFRESH as string)) {
				// -------------------------------------------------------------
				// SECTION HTML: This is the button that displays to get new posts
				// NOTE: Currently, the fetching-new-content is implemented on an interval
				// and it will only fetch new content if it's within the limit
				// You can uncomment this and disable fetchAvailableContentIfWithinPostLimit();
				// -------------------------------------------------------------
				// return <button className="default-button" onClick={() => this.grabLatestItems(false)}>fetch new</button>;
				// -------------------------------------------------------------
				// !SECTION
				// -------------------------------------------------------------
			}
			else if (this.state.newPostAvailability.availableCount > parseInt(process.env.REACT_APP_MAX_NEW_POST_FETCH_BEFORE_REFRESH as string)) {
				// -------------------------------------------------------------
				// SECTION HTML: This is the button that displays to refresh the feed
				// because the amount of posts on the server that area head is too high
				// -------------------------------------------------------------
				return <button className="default-button" onClick={() => this.grabLatestItems(false)}>refresh</button>;
				// -------------------------------------------------------------
				// !SECTION
				// -------------------------------------------------------------
			}
		}
	}



	/**
	 * This renders the feed with all of its info, dragging functionality, and moving functionality.
	 *
	 * The moving and dragging functionality are handled by the parent.
	 *
	 * @return {*}
	 * @memberof Feed
	 */
	render(): JSX.Element {
		// ---------------------------------------------------------------------
		// SECTION HTML: Individual Feed
		// ---------------------------------------------------------------------
		return (
			// Make the id that of the one passed down through props
			// We do this so that we can target this within the draggable event as we'll be overriding
			// the event
			// We also get a prop to this feed and give it a scroll handler
			<div
				id={this.props.feedNumber.toString() as string}
				className="feed-container feed-make-draggable"
			>
				<div className="grid-top-bar-container">
					<div className="grid-top-bar feed-draggable-part">
						<span className="icon-button grid-move-left" onClick={() =>this.props.moveFeedLeft(this.props.feedNumber)}></span>
						<div className="combosearch-container">

							<input ref={this.refFeedOptionSearch} value={this.state.searchQuery} type="text" className="combosearch-text" placeholder="Filter title/content"
								// This opens the dropdowns whenever they click
								onClick={this.openFeedDropdowns}
								// We save their search query as they type
								onChange={(event) => {
									this.setState({searchQuery: event.target.value});
								}}
								// When user presses enter, we switch to the search method of grabbing
								onKeyUp={(event) => {
									if (event.key === "Enter") {
										this.searchFeedByContent();
									}
								}}

							/>

							<div ref={this.refFeedOptionDropdown} className="combosearch-option-container" >
								<option className="description">Available feeds</option>
								{this.state.availableFeeds.map((option) => (
									<option className="actual" key={option.feed_id} value={option.name}
										// All of these options will have an onClick method which will switch the feed to category mode
										// and fetch
										onClick={() => this.changeFeedOfficial(option)}>{option.name}</option>
								))}
							</div>
						</div>
						<span className="icon-button grid-move-right" onClick={() =>this.props.moveFeedRight(this.props.feedNumber)}></span>
						<span className="icon-button grid-close" onClick={() => this.props.removeThisFeed(this.props.feedNumber)}></span>
					</div>
					{/* <input type="submit" className="combosearch-submit" value="Search"/> */}
				</div>
				{/* This'll render a refresh prompt if new post items are available */}
				{this.renderRefreshPrompt()}
				{/* This renders all the displayed items */}
				<div className="feed-inner-content" ref={this.refFeedContentContainer} onScroll={this.onFeedScroll}>
					{/* This renders the Item elements based on the data we've stored in the state
					it's done like this instead of generating the Items and then storing them in the state so that we can actually
					receive prop updates in the Items */}
					{
						this.state.displayedItemsData.map((itemData) => {
							return <Item
								createNotification={this.props.createNotification}
								key={"feednum" + this.props.feedNumber + "_feedId" + this.state.activeFeed.feed_id + "_postId" + itemData.postitem_id}
								authToken={this.props.authToken}
								postItemDetails={itemData}
								openItemInLightbox={this.props.openItemInLightbox}
								isItemFromBookmarks={false}
								itemPostIdsToRefresh={this.props.itemPostIdsToRefresh}
								setLightboxToForceRefreshOnNextOpen={this.props.setLightboxToForceRefreshOnNextOpen}
							/>;
						})
					}
				</div>
			</div>
		);
		// ---------------------------------------------------------------------
		// !SECTION
		// ---------------------------------------------------------------------
	}
}


export default Feed;
