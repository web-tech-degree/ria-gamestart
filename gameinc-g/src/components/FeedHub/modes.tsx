// -----------------------------------------------------------------------------
// Only allow the following modes for the Feeds
// -----------------------------------------------------------------------------
enum modesFeed {
	SearchInSelectedFeed = "SearchInSelectedFeed",
	Category = "Category",
	SourceFavourites = "SourceFavourites",
}

enum feedTypes {
	Normal = "Normal",
	Bookmarks = "Bookmarks",
	Favourites = "Favourites",
}

enum modeCommentBox {
	AddComment = "AddComment",
	EditComment = "EditComment",
}

export {
	modesFeed,
	feedTypes,
	modeCommentBox,
};
