import {feedTypes} from "./modes";

type postItemDetails = {
	// eslint-disable-next-line
	postitem_id: number,
	hash: string,
	title: string,
	url: string,
	content: string,
	date: string,
	author: string,
	// eslint-disable-next-line
	total_votes: number,
	// eslint-disable-next-line
	user_vote_status: 1 | 0 | -1,
	// eslint-disable-next-line
	user_bookmark_status: 1 | 0,
	// eslint-disable-next-line
	user_fav_source_status: 1 | 0,
	// eslint-disable-next-line
	source_id: number,
	// eslint-disable-next-line
	source_name: string,
	// eslint-disable-next-line
	source_url: string,
	// eslint-disable-next-line
	source_avatar: string,
	parentFeed: feedDetails,
}

type feedDetails = {
	// eslint-disable-next-line
	feed_id: number,
	name: string,
}

type sourceDetails = {
	// eslint-disable-next-line
	source_id: number,
	name: string,
	url: string,
	avatar: string,
}

type postUserComment = {
	username?: string,
	avatar?: string,
	// eslint-disable-next-line
	postcomment_id: number,
	// eslint-disable-next-line
	user_id: number | string,
	text: string,
	// eslint-disable-next-line
	parent_postcomment_id: number,
	replies: postUserComment | null,
	// eslint-disable-next-line
	comment_posted_at: string,
	// eslint-disable-next-line
	comment_updated_at: string,
}

type activeFeedData = {
	feedNumber: number,
	feedType: feedTypes,
	extraId?: number,
}

export type {
	postItemDetails,
	feedDetails,
	sourceDetails,
	postUserComment,
	activeFeedData,
};
