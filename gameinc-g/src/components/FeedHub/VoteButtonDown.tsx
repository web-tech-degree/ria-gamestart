import React, {Component} from "react";
import "./VoteButtons.css";





// -----------------------------------------------------------------------------
// Types
import type {createNotificationType} from "../types";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Modes
import {modesNotifTypes} from "../modes";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Components
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Libraries
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Assets
// -----------------------------------------------------------------------------





type VoteButtonDownProps = {
	voteStatus: number,
	authToken: string,
	// eslint-disable-next-line
	postId: number,
	updateParent: () => void,
	createNotification: createNotificationType,
	// This can be undefined because this is only passed/used here from ItemLightbox
	addItemInFeedToRefreshQueue?: (postitemId: number) => void,
	// This can be undefiend because this is only passed/used here from Item
	setLightboxToForceRefreshOnNextOpen?: () => void,
}





/**
 * This is a button that'll toggle the downvote status of the specified
 * post via the props, if the user is logged in.
 *
 * @class VoteButtonDown
 * @extends {Component}
 */
class VoteButtonDown extends Component<VoteButtonDownProps> {
	/**
	 * Creates an instance of VoteButtonDown.
	 * @param {VoteButtonDownProps} props
	 * @memberof VoteButtonDown
	 */
	constructor(props: VoteButtonDownProps) {
		super(props);

		this.toggleDownvote = this.toggleDownvote.bind(this);
	}



	/**
	 * Here we attempt to toggle the downvote
	 *
	 * @memberof VoteButtonDown
	 */
	toggleDownvote(): void {
		fetch(process.env.REACT_APP_API_URL + "__api.php", {
			method: "POST",
			credentials: "omit",
			body: JSON.stringify({
				request_type: "post_toggle_vote",
				user_auth_token: this.props.authToken,
				post_id: this.props.postId,
				vote_status_current: this.props.voteStatus,
				vote_desired: -1,
			}),
		})
			.then((response) => response.json())
			.then((data) => {
				if (data.message === "token_validation_error") {
					this.props.createNotification(modesNotifTypes.Warning, null, "Please log in to vote on posts", null, null);
				}
				else if (data.message !== "vote_success") {
					this.props.createNotification(modesNotifTypes.Error, null, "Voting failed", null, null);
				}
				this.props.updateParent();

				// If this function isn't undefined, then that means it was passed in from ItemLightbox.
				if (this.props.addItemInFeedToRefreshQueue !== undefined) {
					this.props.addItemInFeedToRefreshQueue(this.props.postId);
				}
				// If this function isn't undefined, then that means it was passed in from Item.
				else if (this.props.setLightboxToForceRefreshOnNextOpen !== undefined) {
					this.props.setLightboxToForceRefreshOnNextOpen();
				}
			});
	}



	/**
	 * This renders the vote down button based on if the user has voted down or not.
	 *
	 * @return {*}  {JSX.Element}
	 * @memberof VoteButtonDown
	 */
	render(): JSX.Element {
		// If the user has downvoted, we render a button to reflect that
		if (this.props.voteStatus === -1) {
			return <span className="votebuttondown downvoted" onClick={this.toggleDownvote}/>;
		}
		// Otherwise we render the default span
		else {
			return <span className="votebuttondown" onClick={this.toggleDownvote}/>;
		}
	}
}

export default VoteButtonDown;
