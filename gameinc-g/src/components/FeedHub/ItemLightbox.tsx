import React, {Fragment} from "react";
import "./ItemLightbox.css";
import {authTokenFormat, copyTextToClipboard, globals, sliceReplyPrefixFromPostUserCommentThread} from "../../logic/globals";

// -----------------------------------------------------------------------------
// Types
import type {postItemDetails, postUserComment} from "./types";
import type {createNotificationType} from "../types";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Modes
import {modesNotifTypes} from "../modes";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Components
import Comment from "./Comment";
import Favouritiser from "./Favouritiser";
import VoteButtonUp from "./VoteButtonUp";
import VoteButtonDown from "./VoteButtonDown";
import Bookmarker from "./Bookmarker";
import PopUp from "../PopUp/PopUp";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Libraries
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Assets
// -----------------------------------------------------------------------------




type ItemLightboxState = {
	placeholder: string,
	postItemDetails: postItemDetails,
	postComments: postUserComment[],
	// This is used to track whether or not an "add comment" section is open
	// under any of the comments. This is done so that only one is open at once
	addCommentIsOpen: boolean,
	rootCommentIsOpen: boolean,
	rootCommentText: string,
	serverIsAvailable: boolean,
	showDeleteCommentPopup: boolean,
	commentIdToDelete: number,
	postLink: string,
	showCopyPopup: boolean,
}

type ItemLightboxProps = {
	authToken: string,
	authTokenDecoded: authTokenFormat | null,
	createNotification: createNotificationType,
	itemLightboxShouldDisplay: boolean,
	itemLightboxPostId: number,
	forceRefreshPostDetails: boolean,
	// We use this to check if we can try to fallback on using the localStorage because bookmarked posts
	// *should* be saved in localStorage
	isItemFromBookmarks: boolean,
	parentFeedName: string,
	addItemInFeedToRefreshQueue?: (postitemId: number) => void,
	closeLightbox: () => void,
}

type getPostDetails = {
	message: string,
	details: postItemDetails,
}





/**
 * This component renders the lightbox for the selected item. The way this works is,
 * we accept props which tell us whether or not we should be displaying the lightbox,
 * and which post ID we want to show the details for.
 *
 * The closeLightbox() prop is used to tell the parent to set its state of
 * itemLightboxShouldDisplay to false, making this component hide.
 *
 * @class ItemLightbox
 * @extends {React.Component}
 */
class ItemLightbox extends React.Component<ItemLightboxProps, ItemLightboxState> {
	/**
	 * Creates an instance of ItemLightbox.
	 * @param {ItemLightboxProps} props
	 * @memberof ItemLightbox
	 */
	constructor(props: ItemLightboxProps) {
		super(props);
		this.state = {
			placeholder: "something cool",
			// This looks quite horrific, but we need to initialise the object... and it has a bunch of
			// properties
			postItemDetails: {
				postitem_id: 0,
				hash: "",
				title: "",
				url: "",
				content: "",
				date: "",
				author: "",
				total_votes: 0,
				user_vote_status: 0,
				user_bookmark_status: 0,
				user_fav_source_status: 0,
				source_id: 0,
				source_name: "",
				source_url: "",
				source_avatar: "",
				parentFeed: {feed_id: 0, name: ""},
			},
			postComments: [],
			addCommentIsOpen: false,
			rootCommentIsOpen: false,
			rootCommentText: "",
			serverIsAvailable: true,
			showDeleteCommentPopup: false,
			commentIdToDelete: 0,
			postLink: "",
			showCopyPopup: false,
		};

		this.dbgLog();

		this.refInnerEl1 = React.createRef();
		this.refInnerEl2 = React.createRef();
		// this.refInnerEl2 = React.createRef();
		this.handleClickOutside = this.handleClickOutside.bind(this);
		this.getPostDetails = this.getPostDetails.bind(this);
		this.getPostComments = this.getPostComments.bind(this);
		this.generateAndCopyLink = this.generateAndCopyLink.bind(this);
		this.addCommentClose = this.addCommentClose.bind(this);
		this.addCommentOpen = this.addCommentOpen.bind(this);
		this.submitComment = this.submitComment.bind(this);
		this.editCommentSubmit = this.editCommentSubmit.bind(this);
		this.deleteCommentHandler = this.deleteCommentHandler.bind(this);
	}
	refInnerEl1: React.RefObject<HTMLDivElement>;
	refInnerEl2: React.RefObject<HTMLDivElement>;
	// refInnerEl2: React.RefObject<HTMLDivElement>;

	// -------------------------------------------------------------------------
	// TODO: *Also* Add upvote/downvote fetches here
	// -------------------------------------------------------------------------



	/**
	 * This checks if this component should be "debugged" via the process.env variables. If it's true,
	 * we bind console.log so that logging works normally, otherwise we make it not do anything.
	 *
	 * @param {*} [mode]
	 * @param {...any[]} optionalParams
	 * @memberof ItemLightbox
	 */
	dbgLog(
		// eslint-disable-next-line
		mode?: any,...optionalParams: any[]
	): void {
		if (globals.DEBUGGER_DBG_LOG === "true" && globals.DEBUGGER_DBG_ItemLightbox === "true") {
			this.dbgLog = console.log.bind(window.close);
		}
		else {
			this.dbgLog = (): void => {
				return;
			};
		}
	}



	/**
	 * See https://stackoverflow.com/a/42234988
	 *
	 * @memberof ItemLightbox
	 */
	componentDidMount(): void {
		document.addEventListener("mousedown", this.handleClickOutside);
	}



	/**
	 * See https://stackoverflow.com/a/42234988
	 *
	 * @memberof ItemLightbox
	 */
	componentWillUnmount(): void {
		document.addEventListener("mousedown", this.handleClickOutside);
	}



	/**
	 * This gets the post details and comments if the post ID has changed, or if the user's auth token has changed.
	 * Also, it closes the root comment box if the sub-comment boxes' state has changed
	 *
	 * @param {ItemLightboxProps} prevProps
	 * @param {ItemLightboxState} prevState
	 * @memberof ItemLightbox
	 */
	componentDidUpdate(prevProps: ItemLightboxProps, prevState: ItemLightboxState): void {
		this.dbgLog("ItemLightbox: componentDidUpdate: Root comment is open " + this.state.rootCommentIsOpen);
		this.dbgLog("ItemLightbox: componentDidUpdate: Add comment is open " + this.state.addCommentIsOpen);

		// Make sure we only fetch details when set to display
		// and don't unnecessarily re-fetch data if the post ID is the same, unless we're told to refresh
		if (this.props.itemLightboxShouldDisplay &&
			(this.props.itemLightboxPostId !== prevProps.itemLightboxPostId || this.props.forceRefreshPostDetails)) {
			// if (this.props.itemLightboxShouldDisplay) {
			this.dbgLog("ItemLightbox: componentDidUpdate: Getting details");

			this.getPostDetails();
			this.getPostComments();
		}
		// Also, if the user happened to log out but clicks back onto the same post, we'll re-fetch
		// because we don't want to display the upvote/downvote status of their account when they're
		// not signed in anymore
		else if (this.props.authToken !== prevProps.authToken) {
			this.dbgLog("ItemLightbox: componentDidUpdate: Getting details");
			this.getPostDetails();
			this.getPostComments();
		}


		// If the non-root comment boxes have been toggled, and the root comment is open, hide
		// the root comment.
		// This ensures that, in the case we want a non-root comment box to be open, that we'll
		// close the root comment box
		if (this.state.addCommentIsOpen !== prevState.addCommentIsOpen) {
			if (this.state.addCommentIsOpen) {
				this.dbgLog("ItemLightbox: componentDidUpdate: Closing root comment");
				this.setState({
					rootCommentIsOpen: false,
				});
			}
		}
	}



	/**
	 * See https://stackoverflow.com/a/42234988
	 *
	 * This handles the clicks and discerns whether or not the elements we defined with ref
	 * as "inner" elements contains the element which was clicked.
	 *
	 * This way, if the user clicks on something inside of a <div ref={this.refInnerEl1}>,
	 * we can tell that the user clicked inside, hence we will not close.
	 *
	 * This also is neat because it allows a user to click and drag text on an "inner" element,
	 * and then drag their mouse "outside" and release, and it still will not close; a click has to
	 * be explicitly on the outside for us to close.
	 *
	 * @param {MouseEvent} event
	 * @memberof ItemLightbox
	 */
	handleClickOutside(event: MouseEvent): void {
		/**
		 * This is just a function which checks if the provided ref contains the clicked element.
		 * This is done just to clean up the check with a function instead of copy-pasting the line 5000 times
		 *
		 * @param {React.RefObject<HTMLDivElement>} refTarget
		 * @return {*}  {string}
		 */
		const checkClickEventNotInRefElement = (refTarget: React.RefObject<HTMLDivElement>): boolean => {
			// First check to make sure the target is not null
			if (refTarget.current) {
				return refTarget.current.contains(event.target as Node) ? false : true;
			}
			// NOTE: I don't really know if this will ever fire, because the refTarget should be set before the user
			// clicks anything, but whatever - TypeScript was complaining
			else {
				return false;
			}
		};

		// Here we check that the user did NOT click in the main inner element
		if (
			checkClickEventNotInRefElement(this.refInnerEl1)
		) {
			// If they did not click in the main inner element AND the comment popup is open,
			// we do NOT close the lightbox
			if (this.state.showDeleteCommentPopup === true) {
				// NOTE: I think this additional check could be useful if the PopUp background
				// is also supposed to be click-off-able or whatever, but I don't know; it's not
				// necessary here, for all I know, so I'm keepign it commented
				// if (checkClickEventNotInRefElement(this.refInnerEl2)) {
				// this.props.closeLightbox();
				// }
			}
			// If the popup is NOT open, we close the lightbox
			else {
				this.props.closeLightbox();
			}
		}
	}



	/**
	 * Here we get the post's details from the server. However, if the item was from a bookmark and the fetching fails,
	 * we'll fall back to localStorage.
	 *
	 * @memberof ItemLightbox
	 */
	getPostDetails(): void {
		this.dbgLog("ItemLightbox: getPostDetails");

		fetch(process.env.REACT_APP_API_URL + "__api.php", {
			method: "POST",
			credentials: "omit",
			body: JSON.stringify({
				request_type: "get_post_item_details",
				user_auth_token: this.props.authToken,
				post_id: this.props.itemLightboxPostId,
			}),
		})
			.then((response) => response.json())
			// We expect to receive data matching postData type
			.then((data: getPostDetails) => {
				if (data.message === "fetch_success") {
					this.setState({
						postItemDetails: data.details,
					});
				}
				// If we're here, there was some sort of error, BUT the server isn't unavailable
				else {
					this.props.createNotification(modesNotifTypes.Error, null, "Fetching post details error", null, null);
				}
			}).catch(() => {
				if (this.props.isItemFromBookmarks) {
					// -----------------------------------------------------
					// SECTION: Try load from localStorage
					// -----------------------------------------------------
					const storagePosts = localStorage.getItem("bookmarkedPosts");

					if (storagePosts !== null && storagePosts !== undefined) {
						if (storagePosts.length > 0) {
							// Try to find the post in the storage
							const parsed = JSON.parse(storagePosts) as postItemDetails[];
							const post = parsed.find((o) => o.postitem_id === this.props.itemLightboxPostId);
							// Render the post
							if (post !== undefined ) {
								this.props.createNotification(modesNotifTypes.Info, null, "We couldn't fetch the latest post details, so we loaded it from local storage", 5000, null);
								this.setState({
									postItemDetails: post,
								});
							}
							// We couldn't find the post
							else {
								this.props.createNotification(modesNotifTypes.Error, null, "We couldn't fetch the latest post details, and we couldn't load it from local storage", 5000, null);
							}
						}
					}
					// ---------------------------------------------------------
					// !SECTION
					// ---------------------------------------------------------
				}
				else {
					this.props.createNotification(modesNotifTypes.Error, null, "There was an error contacting the server", null, null);
				}
				this.setState({
					serverIsAvailable: false,
				});
			});
	}



	/**
	 * Here we get the comment data for the selected post. We also do this even if the item
	 * is from a bookmark.
	 *
	 * @return {void}}
	 * @memberof ItemLightbox
	 */
	getPostComments(): void {
		fetch(process.env.REACT_APP_API_URL + "__api.php", {
			method: "POST",
			credentials: "omit",
			body: JSON.stringify({
				request_type: "get_post_item_comments",
				post_id: this.props.itemLightboxPostId,
			}),
		})
			.then((response) => response.json())
			.then((data) => {
				if (data.message === "fetch_success") {
					const newArray: postUserComment[] = [];

					// See comment in this function and in \Post\PostDetails -> get_post_comments
					sliceReplyPrefixFromPostUserCommentThread(newArray, data.comments);

					this.setState({
						postComments: data.comments,
					});
				}
				else {
					this.props.createNotification(modesNotifTypes.Error, null, "Fetching post comments error", null, null);
				}
			}).catch(() => {
				// If we catch, the server is probably dead
				this.props.createNotification(modesNotifTypes.Error, null, "There was an error contacting the server", null, null);
				this.setState({
					serverIsAvailable: false,
				});
			});
	}



	/**
	 * This tries to generate a link to link to the selected post item, falling back to using a popup.
	 *
	 * @memberof ItemLightbox
	 */
	generateAndCopyLink(): void {
		// We generate the link
		this.setState({
			postLink: window.location.protocol +
				"//" +
				window.location.hostname +
				// If there's a port, we add the port and the ":", otherwise we don't add anything
				(location.port ?
					":" + location.port :
					""
				) +
				"?post_item_id=" +
				this.state.postItemDetails.postitem_id +
				"&feed_name=" +
				this.props.parentFeedName,
		}, () => {
			// On callback, we try to copy to clipboard
			if (copyTextToClipboard(this.state.postLink)) {
				// On success, we inform the user
				this.props.createNotification(modesNotifTypes.Success, "Link Copied", "We copied the link to this post to your clipboard", null, null);
			}
			else {
				// If the copy fails, set state to show the popup
				this.setState({showCopyPopup: true});
			}
		});
	}



	/**
	 * This is used to set the state, which is passed down as a prop to
	 * <Comment/>, to false. The <Comment/> implementaiton checks against
	 * this and toggles which box to display.
	 *
	 * @see [[addComponentOpen]]
	 *
	 * @memberof ItemLightbox
	 */
	addCommentClose(): void {
		this.setState({
			addCommentIsOpen: false,
		});
	}



	/**
	 * This is used to set the state, which is passed down as a prop to
	 * <Comment/>, to true. The <Comment/> implementaiton checks against
	 * this and toggles which box to display.
	 *
	 * @see [[addComponentClose]]
	 *
	 * @memberof ItemLightbox
	 */
	addCommentOpen(): void {
		this.setState({
			addCommentIsOpen: !this.state.addCommentIsOpen,
		});
	}



	/**
	 * This attempts to submit a comment to the specifid comment as a child,
	 * only if the user is logged in.
	 *
	 * @param {(number | string)} clickedCommentId
	 * @param {string} commentText
	 * @return {*}  {Promise<boolean>}
	 * @memberof ItemLightbox
	 */
	async submitCommentFetch(clickedCommentId: number | string, commentText: string): Promise<boolean> {
		return fetch(process.env.REACT_APP_API_URL + "__api.php", {
			method: "POST",
			credentials: "omit",
			body: JSON.stringify({
				request_type: "post_submit_comment",
				user_id: "asd",
				jwt_token: this.props.authToken,
				postitem_id: this.props.itemLightboxPostId,
				comment_text: commentText,
				parent_postcomment_id: clickedCommentId,
			}),
		})
			.then((response) => response.json())
			// We expect to receive data matching postData type
			.then((data) => {
				if (data.message === "comment_submitted") {
					// Since the comment was submitted, we want to refresh
					this.getPostComments();
					return true;
				}
				else if (data.message === "failed_to_add_comment") {
					this.props.createNotification(modesNotifTypes.Error, null, "Failed to add comment", null, null);
					return false;
				}
				else if (data.message === "token_validation_error") {
					this.props.createNotification(modesNotifTypes.Warning, "Are you logged in?",
						"Your comment could not be posted as you appear to be logged out", null, null);
					return false;
				}
				else {
					this.props.createNotification(modesNotifTypes.Error, null, "An arbitrary error occured. Please try again", null, null);
					return false;
				}
			});
	}



	/**
	 *This handles the fetch request, which is made somewhere from one of the
	 * <Comment/> elements.
	 *
	 * Note that the clicked comment id corresponds to the comment_id that the user clicked;
	 * this will be used as the parent. This isn't to be confused with parent_postcomment_id as
	 * that is the parent of the clicked post.
	 *
	 * Also, if clickedCommentId is "root", that means it has no parent and will be a root comment.
	 * The server will handle this
	 *
	 * @param {(number | string)} clickedCommentId
	 * @param {string} commentText
	 * @return {*}  {Promise<boolean>}
	 * @memberof ItemLightbox
	 */
	submitComment(clickedCommentId: number | string, commentText: string): Promise<boolean> {
		return this.submitCommentFetch(clickedCommentId, commentText);
	}



	/**
	 * This attemps to edit the specified comment if the user's auth token's user ID matches
	 * the user ID of the comment poster.
	 *
	 * @param {(number | string)} editCommentId
	 * @param {string} commentText
	 * @return {*}  {Promise<boolean>}
	 * @memberof ItemLightbox
	 */
	editCommentSubmit(editCommentId: number | string, commentText: string): Promise<boolean> {
		// return this.submitCommentFetch(editCommentId, commentText);
		return fetch(process.env.REACT_APP_API_URL + "__api.php", {
			method: "POST",
			credentials: "omit",
			body: JSON.stringify({
				request_type: "post_edit_comment",
				jwt_token: this.props.authToken,
				postcomment_id: editCommentId,
				comment_text: commentText,
			}),
		})
			.then((response) => response.json())
			// We expect to receive data matching postData type
			.then((data) => {
				if (data.message === "comment_editted") {
					// Since the comment was editted, we want to refresh
					this.getPostComments();
					return true;
				}
				else if (data.message === "failed_to_edit_comment") {
					this.props.createNotification(modesNotifTypes.Error, null, "Failed to edit comment", null, null);
					return false;
				}
				else if (data.message === "token_validation_error") {
					this.props.createNotification(modesNotifTypes.Warning, "Are you logged in?",
						"Your comment could not be editted as you appear to be logged out", null, null);
					return false;
				}
				else {
					this.props.createNotification(modesNotifTypes.Error, null, "An arbitrary error occured. Please try again", null, null);
					return false;
				}
			});
	}



	/**
	 * This attemps to delete the specified comment if the user's auth token's user ID matches
	 * the user ID of the comment poster. Note that because we use the callback from the PopUp
	 * to trigger this, we instead use the state to get the target comment ID. This state will
	 * be set by deleteCommentHandler() which is called from <Comment/>
	 *
	 * @return {*}  {Promise<Boolean>}
	 * @memberof ItemLightbox
	 */
	deleteCommentSubmit(): Promise<boolean> {
		return fetch(process.env.REACT_APP_API_URL + "__api.php", {
			method: "POST",
			credentials: "omit",
			body: JSON.stringify({
				request_type: "post_delete_comment",
				jwt_token: this.props.authToken,
				postcomment_id: this.state.commentIdToDelete,
			}),
		})
			.then((response) => response.json())
			// We expect to receive data matching postData type
			.then((data) => {
				if (data.message === "comment_deleted") {
					// Since the comment was deleted, we want to refresh
					this.getPostComments();
					return true;
				}
				else if (data.message === "failed_to_delete_comment") {
					this.props.createNotification(modesNotifTypes.Error, null, "Failed to delete comment", null, null);
					return false;
				}
				else if (data.message === "token_validation_error") {
					this.props.createNotification(modesNotifTypes.Warning, "Are you logged in?",
						"Your comment could not be deleted as you appear to be logged out", null, null);
					return false;
				}
				else {
					this.props.createNotification(modesNotifTypes.Error, null, "An arbitrary error occured. Please try again", null, null);
					return false;
				}
			});
	}




	/**
	 * This is the method called by <Comment/> components. Here, we handle
	 * creating the prompt for deleting the comment. We prepare the comment ID
	 * to be deleted by setting the state, and this will be used by deleteCommentSubmit
	 * if and when the user accepts the PopUp
	 *
	 * @param {number} deleteCommentId
	 * @memberof ItemLightbox
	 */
	deleteCommentHandler(deleteCommentId: number ): void {
		this.setState({
			showDeleteCommentPopup: true,
			commentIdToDelete: deleteCommentId,
		});
	}



	/**
	 * Depending on the state, this shows the root comment submission box
	 *
	 * @return {*}  {JSX.Element}
	 * @memberof ItemLightbox
	 */
	renderSubmitRootComment(): JSX.Element {
		if (this.state.rootCommentIsOpen) {
			return (<form className="bare-login-form" onSubmit={(event) => {
				// Prevent default submitting
				event.preventDefault();
				// Here we try to submit a comment, and if it was successful we close the comment box
				// This is the same implementation as in Comment.tsx
				this.submitComment("root", this.state.rootCommentText).then((addWasSuccessful) => {
					if (addWasSuccessful) {
						this.addCommentOpen();
						this.setState({
							rootCommentText: "",
						});
					}
				});
			}}>
				<label htmlFor="comment-insert"><b>Add Comment</b></label>
				<input type="text" placeholder="Enter comment" name="comment-insert" required onChange={(event) => this.setState({rootCommentText: event.target.value})}/>

				<button className="default-button" type="submit">Post comment</button>
			</form>);
		}
		return <Fragment></Fragment>;
	}



	/**
	 * This sets the state which will cause the root comment box to be opened,
	 * and any sub-comment boxes to be closed.
	 *
	 * @memberof ItemLightbox
	 */
	openSubmitRootComment(): void {
		this.setState({
			// addCommentIsOpen: false,
			addCommentIsOpen: false,
			rootCommentIsOpen: !this.state.rootCommentIsOpen,
		});
	}



	/**
	 * This renders the ItemLightbox if it is set to render, showing
	 * all of the contents of the post.
	 *
	 * @return {*}
	 * @memberof ItemLightbox
	 */
	render(): JSX.Element {
		// This generates an ItemLightbox based on the props passed to it from Grid
		return (
			<Fragment>
				{/* This shows the prompt to delete the comment */}
				{this.state.showDeleteCommentPopup ?
					<PopUp
						// We pass the ref as an innerRef so that it gets assigned as a ref to the HTML div element
						innerRef={this.refInnerEl2}
						message={"Are you sure you want to delete your comment?"}
						options={
							[
							// First option is "Yes", which on click triggers the deletion of the comment
								{
									text: "Yes",
									callback: () => {
										this.deleteCommentSubmit();
										this.setState({
											showDeleteCommentPopup: false,
										});
									},
								},
								// Second option is "Cancel", which on click just cancels
								{
									text: "Cancel",
									callback: () => {
										this.setState({
											showDeleteCommentPopup: false,
											commentIdToDelete: 0,
										});
									},
								},
							]
						}/> :
					""
				}

				{/*
				This is so unbelievably hacky, I know. But basically, if showCopyPopup is set to true, we render a popup with the message
				containing the concatenated string of https + domain + params, which will cause the app to open the lightbox when entered.
				This is also done as a popup in case copying fails, which may or may not actually happen.
				*/}
				{this.state.showCopyPopup ?
					<PopUp
						message=
							{
								<Fragment>
									<p className="text-dark">Here&apos;s the link to share this post</p>
									<code>{this.state.postLink}</code>
									<i className="text-dark">We opened this popup because we couldn&apos;t copy the link to your clipbaord</i>
								</Fragment>
							}
						options={
							[
								{
									text: "Ok",
									callback: () => {
										this.setState({
											showCopyPopup: false,
										});
									},
								},
							]
						}
					/> : ""
				}



				{/* Only display content if itemLightboxShouldDisplay is true */}
				{this.props.itemLightboxShouldDisplay ?
				// -------------------------------------------------------------
				// SECTION HTML: This is the HTML of the entire lightbox
				// NOTE: Ignore the colon at the end of the last closing div (:),
				// it's needed for JavaScript
				// -------------------------------------------------------------
					<div className="post-lightbox-wrapper">
						{/*
							We add a ref which is basically used to check if the clicked element contains this element
							That way, if the clicked element does NOT contain this div, it means we clicked outside.
						*/}
						<div className="post-lightbox-container" ref={this.refInnerEl1}>

							<div className="post-lightbox-banner">
								<h2>{this.props.parentFeedName}</h2>
							</div>

							<div className="post-lightbox-author">
								<div className="author-top-row">
									<p><i>{this.state.postItemDetails.author}</i></p>
									<p><b>{this.state.postItemDetails.date}</b></p>
								</div>
								<h3>{this.state.postItemDetails.title}</h3>
							</div>

							<div className="post-lightbox-main">
								<button className="default-button" onClick={this.props.closeLightbox}>Close</button>

								<div className="source-bar-container">
									<div className="source-bar-left">
										<Favouritiser
											favouriteStatus={this.state.postItemDetails.user_fav_source_status}
											authToken={this.props.authToken}
											sourceId={this.state.postItemDetails.source_id}
											updateParent={this.getPostDetails}
											createNotification={this.props.createNotification}
											isItemFromBookmarks={this.props.isItemFromBookmarks}
										/>
										<h3 className="source-name">{this.state.postItemDetails.source_name}</h3>
									</div>
									<div className="source-bar-right">
										<span className="copy-link" onClick={this.generateAndCopyLink} />
										<Bookmarker
											bookmarkStatus={this.state.postItemDetails.user_bookmark_status}
											authToken={this.props.authToken}
											postId={this.state.postItemDetails.postitem_id}
											updateParent={this.getPostDetails}
											createNotification={this.props.createNotification}
											isItemFromBookmarks={this.props.isItemFromBookmarks}
											addItemInFeedToRefreshQueue={this.props.addItemInFeedToRefreshQueue}
										/>
									</div>
								</div>
								<p className="view-orig-article" onClick={() => window.open(this.state.postItemDetails.url, "_blank")}>View Original Article</p>
								<p>{this.state.postItemDetails.content}</p>


								<div className="bottom-bar">
									{/* We only render the vote options if the item is not from bookmarks */}
									{this.props.isItemFromBookmarks ? "" :

										<Fragment>

											<span className="total-votes">{this.state.postItemDetails.total_votes}</span>

											<VoteButtonUp
												voteStatus={this.state.postItemDetails.user_vote_status}
												authToken={this.props.authToken}
												postId={this.state.postItemDetails.postitem_id}
												updateParent={this.getPostDetails}
												createNotification={this.props.createNotification}
												addItemInFeedToRefreshQueue={this.props.addItemInFeedToRefreshQueue}
											/>
											<VoteButtonDown
												voteStatus={this.state.postItemDetails.user_vote_status}
												authToken={this.props.authToken}
												postId={this.state.postItemDetails.postitem_id}
												updateParent={this.getPostDetails}
												createNotification={this.props.createNotification}
												addItemInFeedToRefreshQueue={this.props.addItemInFeedToRefreshQueue}
											/>
										</Fragment>

									}
								</div>


							</div>
							<div className="post-lightbox-comments">
								{/* If the server is available, we show the comments */}
								{this.state.serverIsAvailable ?
									<Fragment>
										<button className="default-button" onClick={() => this.openSubmitRootComment()}>post root comment</button>
										{this.renderSubmitRootComment()}
										<Comment
											authToken={this.props.authToken}
											authTokenDecoded={this.props.authTokenDecoded}
											rootCommentIsOpen={this.state.rootCommentIsOpen}
											addCommentIsOpen={this.state.addCommentIsOpen}
											addCommentClose={this.addCommentClose}
											addCommentOpen={this.addCommentOpen}
											submitComment={this.submitComment}
											editCommentSubmit={this.editCommentSubmit}
											deleteCommentHandler={this.deleteCommentHandler}
											commentsArray={this.state.postComments}
											isInitial={true}
										/>
									</Fragment> :
									// We show this if the server is unavailable
									<Fragment>
										<p>The server seems to be unavailable.</p>
										<button className="default-button" onClick={() => this.getPostDetails()}>Try again</button>
									</Fragment>
								}
							</div>
						</div>
					</div> :
					// ---------------------------------------------------------
					// !SECTION
					// ---------------------------------------------------------
					""
				}

			</Fragment>
		);
	}
}

export default ItemLightbox;
