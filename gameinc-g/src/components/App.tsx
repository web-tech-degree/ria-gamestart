import React, {Fragment} from "react";
import "./App.css";
import {globals, authTokenFormat, verifyAuthTokenRet} from "../logic/globals";





// -----------------------------------------------------------------------------
// Types
import type {appFeedAdderType} from "./types";
import type {postItemDetails} from "./FeedHub/types";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Modes
import {modesAppInternal, modesAppExternal, modesAppBackground, modesNotifTypes, modesDisplayOrientation} from "./modes";
import {feedTypes} from "./FeedHub/modes";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Components
import Home from "./Externals/Home";
import Help from "./Externals/Help";
import LoginLanding from "./Login/LoginLanding";
import LeftNav from "./AppNav/LeftNav";
import FeedHub from "./FeedHub/FeedHub";
import AdminHub from "./AdminHub/AdminHub";
import Profile from "./Profile/Profile";
import PopUp from "./PopUp/PopUp";
import BotNav from "./AppNav/BotNav";
import DebuggerMenu from "./DebuggerMenu/DebuggerMenu";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Libraries
import jwtDecode from "jwt-decode";
import vantaBirds from "vanta/dist/vanta.birds.min";
import {NotificationContainer, NotificationManager} from "react-notifications";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Assets
import gameIngBg from "../assets/img/game-ing.svg";
// -----------------------------------------------------------------------------





type AppState = {
	// This can ONLY be set to either one of the available internal or external modes
	mode: modesAppInternal | modesAppExternal,
	authToken: string,
	authTokenDecoded: authTokenFormat | null,
	authTokenIsValid: string,
	authTokenDidExpire: boolean,
	doubleTapMPrimed: boolean,
	displayOrientation: modesDisplayOrientation,
	displayOrientationOverride: modesDisplayOrientation | null,
	// This is essentially used as a "notification"
	// This can be set to true from anywhere that has access,
	// and when it is set to true,
	// FeedHub will be informed via the props that it should add a feed
	// which it will handle
	feedAdder: appFeedAdderType,
	// This stores the bookmarks that are available to fetch
	availableBookmarks: number[],
	newBookmarksWereFetched: boolean,
	rechekTokenInterval: ReturnType<typeof setInterval> | null,
	bookmarkAvailabilityCheckerInterval: ReturnType<typeof setInterval> | null,
	bookmarkPostsFetcher: ReturnType<typeof setInterval> | null,
	// This is used to check if there is a high amount of bookmarks
	highBookmarkCountWarning: boolean,
	highBookmarkCountWarningAmount: number[],
	highBookmarkCountWarningAccepted: boolean | null,
	// We use this to discern what we're rending in the background
	backgroundMode: modesAppBackground,
}

type AppProps = {
}





/**
 * Main App class that is rendered through index.tsx
 *
 * This is the only element that gets rendered;
 * everything on screen will go through this file in one way or another,
 * so think of this as a parent for everything.
 *
 * @class App
 * @extends {React.Component<AppProps, AppState>}
 */
class App extends React.Component<AppProps, AppState> {
	/**
	 * Creates an instance of App..
	 * `this.state` initialises the state, and this is needed to make these modifiable.
	 *
	 * The `this.x = this.x.bind(this)` lines basically just bind the context of `this`, meaning this component,
	 * to those methods.
	 *
	 * @param {AppProps} props
	 * @memberof App
	 */
	constructor(props: AppProps) {
		super(props);
		this.state = {
			mode: modesAppExternal.ExtHome,
			authTokenDecoded: null,
			// This tries to read the authToken localStorage element.
			// If there was no storage item set, set the authToken to "logged_out" instead
			// This will also be set to "logged_out" in other places, such as the logoutSubmit method
			// in LoginLanding.tsx and whenever the token expires and a logout is forced
			authToken: localStorage.getItem("authToken") as string || "logged_out" as string,
			authTokenIsValid: "",
			authTokenDidExpire: false,
			doubleTapMPrimed: false,
			displayOrientation: modesDisplayOrientation.Desktop,
			displayOrientationOverride: null,
			feedAdder: {shouldAdd: false, type: feedTypes.Normal},
			availableBookmarks: [],
			newBookmarksWereFetched: false,
			rechekTokenInterval: null,
			bookmarkAvailabilityCheckerInterval: null,
			bookmarkPostsFetcher: null,
			highBookmarkCountWarning: false,
			highBookmarkCountWarningAmount: [],
			// If the saved value is "true", we make the var true.
			highBookmarkCountWarningAccepted: this.getLocalStorageBookmarkWarning(),
			backgroundMode: modesAppBackground.GameIngImg,
		};


		this.dbgLog();

		this.vantaRef = React.createRef();
		this.overlayDbgRef = React.createRef();
		this.gridContainerRef = React.createRef();
		this.gridContainerRefHandler = this.gridContainerRefHandler.bind(this);

		this.handleKeyUp = this.handleKeyUp.bind(this);
		this.handleWindowResize = this.handleWindowResize.bind(this);
		this.checkIsAdmin = this.checkIsAdmin.bind(this);
		this.updateAuthToken = this.updateAuthToken.bind(this);
		this.verifyAuthToken = this.verifyAuthToken.bind(this);
		this.setMainMode = this.setMainMode.bind(this);
		this.setBackgroundMode = this.setBackgroundMode.bind(this);
		this.spawnFeed = this.spawnFeed.bind(this);
		this.feedWasAdded = this.feedWasAdded.bind(this);
		this.newBookmarksWereRendered = this.newBookmarksWereRendered.bind(this);
	}
	vantaRef: React.RefObject<HTMLDivElement>;
	overlayDbgRef: React.RefObject<HTMLDivElement>;
	gridContainerRef: React.MutableRefObject<HTMLDivElement | null>;

	/**
	 * The way this works is, when the ref is assigned, it'll be assigned using this function.
	 * This way, when the ref is assigned, the display orientation changes can be triggered,
	 * because the ref is not guaranteed to be set on the first render.
	 *
	 * @param {React.RefObject<HTMLDivElement>} refTarget
	 * @memberof App
	 */
	gridContainerRefHandler(refTarget: HTMLDivElement): void {
		// this.gridContainerRef = React.useRef(null);

		// Actually set the ref here
		this.gridContainerRef.current = refTarget;

		// Check window sizing and handle it
		this.handleWindowResize();
		this.handleDisplayOrientationChanges();
	}
	// This is used to store the vanta "effect"
	// eslint-disable-next-line
	vantaEffect: any;



	/**
	 * Get the value from localStorage for highBookmarkCountWarningAccepted, or default to null.
	 *
	 * @return {*}  {boolean}
	 * @memberof App
	 */
	getLocalStorageBookmarkWarning(): boolean | null {
		const strg = localStorage.getItem("highBookmarkCountWarningAccepted");
		if (strg !== null && strg !== undefined) {
			return JSON.parse(strg);
		}
		return null;
	}



	/**
	 * This saves the highBookmarkCountWarningAccepted state to the localStorage. It does it using
	 * JSON stringify so that we can get the boolean back with JSON parse
	 *
	 * @memberof App
	 */
	saveLocalStorageBookmarkWarning(): void {
		localStorage.setItem("highBookmarkCountWarningAccepted", JSON.stringify(this.state.highBookmarkCountWarningAccepted));
	}


	/**
	 * This checks if this component should be "debugged" via the process.env variables. If it's true,
	 * we bind console.log so that logging works normally, otherwise we make it not do anything.
	 *
	 * @param {*} [mode]
	 * @param {...any[]} optionalParams
	 * @memberof App
	 */
	dbgLog(
		// eslint-disable-next-line
		mode?: any,...optionalParams: any[]
	): void {
		if (globals.DEBUGGER_DBG_LOG === "true" && globals.DEBUGGER_DBG_App === "true") {
			this.dbgLog = console.log.bind(window.close);
		}
		else {
			this.dbgLog = (): void => {
				return;
			};
		}
	}



	/**
	 * This runs when the component mounts. Basically, all we want to do here is
	 * check if the user is already logged in, and load the feedhub and ItemLightbox
	 * if a URL param was passed to render it. This also makes the token verification
	 * run on an interval to constantly recheck.
	 *
	 * @memberof App
	 */
	componentDidMount(): void {
		document.addEventListener("keyup", this.handleKeyUp);

		// NOTE: Because the user will not necessarily resize the browser, and because
		// this only triggers on resize, the initial triggering is done through gridContainerRefHandler()
		window.addEventListener("resize", this.handleWindowResize);

		// Get URL params
		// NOTE: This is quite a single-use solution; if we wanted proper URL routing
		// we'd use React Router and have the URL reflect the state of the app
		const urlParams = new URLSearchParams(window.location.search);
		const postItemIdParam = urlParams.get("post_item_id");
		const feedNameParam = urlParams.get("feed_name");

		// When this opens the FeedHub mode, that itself also checks the params
		// because I couldn't be bothered passing in more props
		if (postItemIdParam !== null && feedNameParam !== null) {
			this.setMainMode(modesAppInternal.FeedHub);
		}

		// Load back in the overlay-debug-visible state
		if (this.overlayDbgRef.current !== null) {
			const dbgSet = localStorage.getItem("overlayDebugVisible");
			if (dbgSet !== null) {
				if (JSON.parse(dbgSet) === true) {
					this.overlayDbgRef.current.classList.add("overlay-debug-visible");
				}
				else {
					this.overlayDbgRef.current.classList.remove("overlay-debug-visible");
				}
			}
		}


		const stiRecheckToken = setInterval(() => {
			this.dbgLog("App: componentDidMount: rechekTokenInterval");
			this.verifyAuthToken();
		}, globals.TOKEN_REFRESH_RATE);

		const stiFetchBookmarkAvailability = setInterval(() => {
			this.dbgLog("App: componentDidMount: bookmarkAvailabilityCheckerInterval");
			this.bookmarkAvailabilityChecker();
		}, globals.FETCH_BOOKMARK_AVAILABILITY_RATE);

		const stiFetchBookmarks = setInterval(() => {
			this.dbgLog("App: componentDidMount: bookmarkAvailabilityCheckerInterval");
			this.bookmarkPostFetcher();
		}, globals.FETCH_BOOKMARK_AVAILABILITY_RATE);


		// Add the two intervals to the state
		this.setState({
			bookmarkAvailabilityCheckerInterval: stiFetchBookmarkAvailability,
			bookmarkPostsFetcher: stiFetchBookmarks,
			rechekTokenInterval: stiRecheckToken,
		});

		this.verifyAuthToken();
	}



	/**
	 * We clean up after ourselves with vanta and the intervals
	 *
	 * @memberof App
	 */
	componentWillUnmount(): void {
		document.removeEventListener("keyup", this.handleKeyUp);
		window.removeEventListener("resize", this.handleWindowResize);

		// if (this.vantaEffect) this.vantaEffect.destroy();
		clearInterval(this.state.rechekTokenInterval as ReturnType<typeof setInterval>);
		clearInterval(this.state.bookmarkAvailabilityCheckerInterval as ReturnType<typeof setInterval>);
		clearInterval(this.state.bookmarkPostsFetcher as ReturnType<typeof setInterval>);
	}



	/**
	 * Here we create the vanta effect if it's what should be made
	 *
	 * @param {AppProps} prevProps
	 * @param {AppState} prevState
	 * @memberof App
	 */
	componentDidUpdate(prevProps: AppProps, prevState: AppState): void {
		if (prevState.backgroundMode !== this.state.backgroundMode) {
			// If the backgroundMode is set to the mode that relates to this
			// And the vantaRef element is not undefined or null, we add the vanta effect here
			// This is done here, rather than the render method, because we need the ref to the element
			// and that is only available after the component actually gets mounted
			if (this.state.backgroundMode === modesAppBackground.VantaBirds &&
				this.vantaRef.current !== undefined &&
				this.vantaRef.current !== null
			) {
				this.vantaEffect = vantaBirds({
					el: this.vantaRef.current,
				});
			}
		}


		// If the authToken ever changes, we wipe the bookmarked posts
		if (prevState.authToken !== this.state.authToken) {
			// We also clear the bookmark warning and the bookmarked posts
			localStorage.removeItem("highBookmarkCountWarningAccepted");
			localStorage.removeItem("bookmarkedPosts");
			// and we clear the warning in memory to be null, meaning "the user didn't yet select"
			this.setState({
				highBookmarkCountWarning: false,
				highBookmarkCountWarningAccepted: null,
			});
		}

		// If the orientation changed, run the method that changes stuff around
		if (prevState.displayOrientation !== this.state.displayOrientation) {
			this.handleDisplayOrientationChanges();
		}
	}



	/**
	 * Simply switch which mode to display via a change of the state; another method
	 * looks at this mode state and renders the respective login/register etc. fields.
	 *
	 * @param {string} modeToSetTo
	 * @memberof App
	 */
	setMainMode(modeToSetTo: modesAppInternal | modesAppExternal): void {
		this.setState({
			mode: modeToSetTo,
		});
	}



	/**
	 * This handles checking key-up press, checking if the user double-tapped m
	 * which then opens the debug menu.
	 *
	 * @param {KeyboardEvent} event
	 * @memberof App
	 */
	handleKeyUp(event: KeyboardEvent): void {
		if (this.state.doubleTapMPrimed && event.key === "m") {
			if (this.overlayDbgRef.current !== null) {
				// Toggle the class for visibility
				this.overlayDbgRef.current.classList.toggle("overlay-debug-visible");
				// Then, depending on whether the class exists on the element (aka whether it's set to visible or not)
				// Set the localstorage accordingly
				if (this.overlayDbgRef.current.classList.contains("overlay-debug-visible")) {
					localStorage.setItem("overlayDebugVisible", JSON.stringify(true));
				}
				else {
					localStorage.setItem("overlayDebugVisible", JSON.stringify(false));
				}
			}
			// Reset primed status because the event was handled
			this.setState({
				doubleTapMPrimed: false,
			});
		}
		else if (event.key === "m") {
			// If the user pressed the m key, but it wasn't primed,
			// prime it so that the next click before the timeout is primed
			// and fires
			this.setState({
				doubleTapMPrimed: true,
			}), () => {
				setTimeout(() => {
					this.setState({
						doubleTapMPrimed: false,
					});
				}, 200);
			};
		}
		else {
			// If the user clicked any other button, "unprime" the double tab
			this.setState({
				doubleTapMPrimed: false,
			}), () => {
				setTimeout(() => {
					this.setState({
						doubleTapMPrimed: false,
					});
				}, 200);
			};
		}
	}



	/**
	 * Whenever the display orientation changes, this will modify whatever needs to be
	 * modified in terms of CSS and whatever, to adjust the visuals.
	 *
	 * This is done like this instead of re-rendering a *different* element because
	 * we do not want to re-render something like FeedHub, because that will reset
	 * its state and son on. Instead, here we can swap whatever CSS classes or class properties
	 * to adjust to desktop or mobile
	 *
	 * @warning This may be a very finnicky thing to do, and it's probably extremely hacky.
	 *
	 * @note These assignments use ternary operators to check for nullness
	 *
	 * @memberof App
	 */
	handleDisplayOrientationChanges(): void {
		if (this.state.displayOrientation === modesDisplayOrientation.Desktop) {
			this.gridContainerRef.current ? this.gridContainerRef.current.style.gridTemplateColumns = "[nav] 1fr [feed-container] 19fr" : "";
		}
		else {
			this.gridContainerRef.current ? this.gridContainerRef.current.style.gridTemplateColumns = "[feed-container] 19fr" : "";
		}
	}



	/**
	 * This runs whenever the window is resized, checking if the aspect ratio
	 * or width is in such a state as to resemble a mobile. Essentially, if the website,
	 * is "significantly portrait", or if the width is too small, mobile mode will be used.
	 *
	 * @memberof App
	 */
	handleWindowResize(): void {
		// Only check if the user hasn't manually selected a display mode
		if (this.state.displayOrientationOverride === null) {
			// If the aspect ratio is "too tall", or if the width is just "too thin", presume they are on mobile
			if (window.innerHeight / window.innerWidth > 1.3 || window.innerWidth < 700) {
				// -------------------------------------------------------------
				// I think reading the state every time to check if it should be
				// changed is faster than just setting it every time
				// -------------------------------------------------------------
				if (this.state.displayOrientation !== modesDisplayOrientation.Mobile) {
					this.dbgLog("App: handleWindowResize: Setting Mobile");
					this.setState({
						displayOrientation: modesDisplayOrientation.Mobile,
					});
				}
			}
			else if (this.state.displayOrientation !== modesDisplayOrientation.Desktop) {
				this.dbgLog("App: handleWindowResize: Setting Desktop");
				this.setState({
					displayOrientation: modesDisplayOrientation.Desktop,
				});
			}
		}
	}



	/**
	 * Check whether the user's token signifies that they are an admin.
	 *
	 * @return {*}  {boolean}
	 * @memberof App
	 */
	checkIsAdmin(): boolean {
		this.dbgLog("App: checkIsAdmin");

		// First ensure the token has been set as "valid", meaning the API
		// told us it's valid
		// NOTE: Because we are using the state, which is asynchronous,
		// this.state.authTokenIsValid could potentially be set to "token_valid" even
		// when this.state.authToken has been removed
		// Hence, we use a try/catch statement
		if (this.state.authTokenIsValid === "token_valid") {
			try {
				// The decoded token shall be in this format
				const decoded: authTokenFormat = jwtDecode(this.state.authToken);
				if (decoded.data.userLevel === "admin") {
					return true;
				}
			}
			// Try to catch the error. If the token is undefined, aka probably removed, then
			// we can assume nothing went horribly wrong. But if the error is something else,
			// this'll proceed to throw and bubble up the error.
			catch (error) {
				if (error.message = "Invalid token specified: e is undefined") {
					this.dbgLog("App: checkIsAdmin tried verifying but JWT token was invalid");
				}
				else {
					throw error;
				}
			}
		}

		// We just return false here instead of using else statements because it's cleaner
		return false;
	}



	/**
	 * This method gets passed down to some child elements so that the authToken state can be updated.
	 *
	 * This is done so that we can keep a variable with the authToken in memory instead of having to read
	 * localStorage/cookie from disk every time.
	 *
	 * This also writes the localStorage so that the authToken persists after the page is closed.
	 *
	 * @param {string} val
	 * @memberof App
	 */
	updateAuthToken(val: string): void {
		// Set the state, then as the callback, verify the auth token
		// NOTE: This is done because setState is asynchronous
		this.setState({
			authToken: val,
		}, () => {
			this.verifyAuthToken();
		});
		// We can just set localStorage based on the value passed and don't
		// need to wait for setState to finish
		localStorage.setItem("authToken", val);
	}



	/**
	 * This is the fetch API request that gets the JSON data from server
	 * to check if the token is actually vaild. It's async and returns the fetch
	 * so that [[this.verifyAuthToken]] can handle that.
	 *
	 * @return {*}  {boolean}
	 * @memberof App
	 */
	async verifyAuthTokenFetch(): Promise<verifyAuthTokenRet> {
		// This makes the actual fetch call to the server and returns the data.
		// It is done like this so we can actually return a boolean to the caller of
		// verifyAuthToken()
		return fetch(process.env.REACT_APP_API_URL + "__api.php", {
			method: "POST",
			credentials: "omit",
			body: JSON.stringify({
				request_type: "user_authenticate",
				user_auth_token: this.state.authToken,
			}),
		})
			.then((response) => response.json());
	}



	/**
	 * This re-verifies the authentication token and updates the state.
	 * Any child components who have been passed the authTokenIsValid
	 * property will update whenever this changes.
	 *
	 * @memberof App
	 */
	verifyAuthToken(): void {
		// First check if the state is set to logged_out so we can avoid unnecessary server requests
		if (this.state.authToken !== "logged_out") {
			// Make the API request which validates the token
			this.verifyAuthTokenFetch().then((data: verifyAuthTokenRet) => {
				// IF the token is NOT valid and there was a token stored in localStorage
				// remove the token from localStorage and inform the user that the token expired
				if (data.message !== "token_valid" && localStorage.getItem("authToken")) {
					// ---------------------------------------------------------
					// SECTION HTML: Somehow we will have to render a notification bar at the bottom
					// to tell the user they have been logged out
					// NOTE: Render a thingy^ for feedback
					// !SECTION
					// ---------------------------------------------------------
					localStorage.removeItem("authToken");
					this.setState({
						authToken: "logged_out",
						authTokenDidExpire: true,
					});
				}
				// If the token happens to be valid, we'll also try to decode it so we can use the data
				// within it easily without re-decoding it every time
				else if (data.message === "token_valid") {
					try {
						// The decoded token shall be in this format
						const decoded: authTokenFormat = jwtDecode(this.state.authToken);
						this.setState({
							authTokenDecoded: decoded,
						});
					}
					// Try to catch the error. If the token is undefined, aka probably removed, then
					// we can assume nothing went horribly wrong. But if the error is something else,
					// this'll proceed to throw and bubble up the error.
					catch (error) {
						if (error.message = "Invalid token specified: e is undefined") {
							this.dbgLog("App: verifyAuthToken tried decoding JWT but failed");
						}
						else {
							throw error;
						}
					}
				}
				// We want to keep track of the validity of the token regardless
				this.setState({authTokenIsValid: data.message});
			});
		}
		else {
			// Since the authToken has been intentionally set to "logged_out", we do not make a request
			// and instead just set it to "token_invalid"
			this.setState({
				authTokenIsValid: "token_invalid",
			});
		}
	}



	/**
	 * This attempts to synchronise the bookmark post content from the server into localStorage in the background
	 * so that the user can always access their bookmarks.
	 *
	 * @memberof App
	 */
	bookmarkAvailabilityChecker(): void {
		this.dbgLog("App: bookmarkAvailabilityChecker");
		if (this.state.authToken === "logged_out") {
			this.dbgLog("App: bookmarkAvailabilityChecker: logged out");
			return;
		}
		// Here we periodically fetch the bookmarked post IDs and just store them
		fetch(process.env.REACT_APP_API_URL + "__api.php", {
			method: "POST",
			credentials: "omit",
			body: JSON.stringify({
				request_type: "get_user_post_bookmark_ids",
				user_auth_token: this.state.authToken,
			}),
		})
			.then((response) => response.json())
			.then((data) => {
				if (data.message === "fetch_success") {
					// If there is a high amount of bookmarks saved, we prompt the user and only save
					// if they have accepted
					if (data.bookmarked_posts.length > globals.BOOKMARK_HIGH_COUNT_WARNING) {
						if (this.state.highBookmarkCountWarningAccepted) {
							this.setState({
								availableBookmarks: data.bookmarked_posts,
							});
						}
						// If the warning has neither been accepted nor denied yet, we prompt the user
						else if (this.state.highBookmarkCountWarningAccepted === null) {
							this.setState({
								highBookmarkCountWarning: true,
								highBookmarkCountWarningAmount: data.bookmarked_posts.length,
							});
						}
					}
					else {
						this.setState({
							availableBookmarks: data.bookmarked_posts,
						});
					}
				}
			});
	}



	/**
	 * This actually handles fetching the bookmarked post data if any has been marked as available.
	 *
	 * @memberof App
	 */
	bookmarkPostFetcher(): void {
		this.dbgLog("App: bookmarkPostFetcher");
		if (this.state.authToken === "logged_out") {
			this.dbgLog("App: bookmarkPostFetcher: logged out");
			return;
		}
		// ---------------------------------------------------------------------
		// SECTION Filter out post IDs we already fetched
		// Because bookmarkAvailabilityChecker is a "dumb" check for all available bookmarks,
		// here is where we actually filter out posts we already have.
		// We get the posts we already have from localStorage and JSON parse it.
		// We then loop over the parsed items and add every post's ID to our array
		// of existingPostIds
		// We then remove every existingPostId from the availablePostIds so that we only
		// try to fetch items we do NOT have
		// Additionally, Here we "assert" that this'll have a string and not be null,
		// however, since it might be null, we check for that here and keep track of it.
		// This saves us having to re-check it every time because of TypeScript
		// ---------------------------------------------------------------------
		const storagePosts = localStorage.getItem("bookmarkedPosts") as string;
		const existingPostIds: number[] = [];
		if (storagePosts !== null && storagePosts !== undefined) {
			if (storagePosts.length > 0) {
				const parsed = JSON.parse(storagePosts);
				for (const k in parsed as postItemDetails[]) {
					if (parsed.hasOwnProperty(k)) {
						if (parsed[k] !== null) {
							existingPostIds.push(parsed[k].postitem_id);
						}
					}
				}
			}
		}

		const postIds = this.state.availableBookmarks;
		const postIdsToFetch = postIds.filter((el) => {
			return existingPostIds.indexOf( el ) < 0;
		});
		this.dbgLog("App: bookmarkPostFetcher: posdIdsToFetch:" + postIdsToFetch);
		// ---------------------------------------------------------------------
		// !SECTION
		// ---------------------------------------------------------------------



		/**
		 * This purges posts we have in localStorage which are not in postIds (this.state.availableBookmarks).
		 * It just removes the items from the array.
		 *
		 * @note Do not fire this if any of the fetches in bookmarkAvailabilityChecker or bookmarkPostFetcher fail,
		 * because if they do, postIds could potentially be incorrect.
		 *
		 * @param {postItemDetails[]} currentPosts
		 * @return {*}  {postItemDetails[]}
		 */
		const purgeOldPosts = (currentPosts: postItemDetails[]): postItemDetails[] => {
			this.dbgLog("App: bookmarkPostFetcher: purgeOldPosts");
			return currentPosts.filter((item) => {
				if (postIds.indexOf(item.postitem_id) !== -1) return item;
			});
		};



		fetch(process.env.REACT_APP_API_URL + "__api.php", {
			method: "POST",
			credentials: "omit",
			body: JSON.stringify({
				request_type: "fetch_posts_in_user_bookmarks",
				user_auth_token: "logged_out",
				post_id_array: postIdsToFetch,
			}),
		})
			.then((response) => response.json())
			.then((data) => {
				this.dbgLog("App: bookmarkPostFetcher: fetch");

				if (data.message === "fetch_success") {
					// -------------------------------------------------------------
					// SECTION Save posts to localStorage
					// If we already have posts, we concat them in after JSON parsing them
					// -------------------------------------------------------------
					if (storagePosts !== null && storagePosts !== undefined) {
						if (storagePosts.length > 0) {
							const old = JSON.parse(storagePosts) as postItemDetails[];
							// -------------------------------------------------
							// I DON'T UNDERSTAND THIS
							// but for some reason, somehow, arbitrarily, old is null when it's passed into purgeOldPosts
							// sometimes
							// -------------------------------------------------
							if (old !== null) {
								const purged = purgeOldPosts(old);
								localStorage.setItem("bookmarkedPosts", JSON.stringify(purged.concat(data.posts)));
							}
						}
						else {
							localStorage.setItem("bookmarkedPosts", JSON.stringify(data.posts));
						}
					}
					else {
						localStorage.setItem("bookmarkedPosts", JSON.stringify(data.posts));
					}
					this.setState({
						newBookmarksWereFetched: true,
					});
				}
				// SECTION
				else if (data.message === "no_more_posts") {
					const old = JSON.parse(storagePosts) as postItemDetails[];
					// ---------------------------------------------------------
					// NOTE: Potential bug here with logic, but I think this works
					// ---------------------------------------------------------
					if (old !== null) {
						const purged = purgeOldPosts(old);
						localStorage.setItem("bookmarkedPosts", JSON.stringify(purged));
					}
					else {
						localStorage.removeItem("bookmarkedPosts");
					}
				}
				// !SECTION

				// -------------------------------------------------------------
				// !SECTION
				// -------------------------------------------------------------
			});
	}



	/**
	 * This method essentially just tells FeedHub to spawn a new feed. This method will be passed around
	 * through props to places such as the nav, so that a button in the nav can trigger this method using the props.
	 *
	 * @param {feedTypes} typeToAdd
	 * @param {number} extraId
	 * @memberof App
	 */
	spawnFeed(typeToAdd: feedTypes, extraId?: number): void {
		if (this.state.mode === modesAppInternal.FeedHub) {
			this.setState({
				feedAdder: {shouldAdd: true, type: typeToAdd, extraId: extraId},
			});
		}
	}



	/**
	 * This is used to pass into child component FeedHub as a handler, so that the shouldAddFeed can be set to false
	 * because we've handled the request to add it. If this is not done, we'll just continue to infinitely add feeds.
	 *
	 * @memberof App
	 */
	feedWasAdded(): void {
		this.setState({
			feedAdder: {shouldAdd: false, type: feedTypes.Normal},
		});
	}



	/**
	 * This is passed into child components so that FeedBookmark can receive it. The functionality of this is
	 * to update the state which FeedBookmark relies on to check if it should re-render.
	 *
	 * @memberof App
	 */
	newBookmarksWereRendered(): void {
		this.setState({
			newBookmarksWereFetched: false,
		});
	}



	/**
	 * This is used temporarily to trigger the PHP in the backend to parse all feed sources.
	 *
	 * This will be done automatically on the backend in the proper implementation.
	 *
	 * @memberof App
	 */
	debugParseSources(): void {
		fetch(process.env.REACT_APP_API_URL + "__api.php", {
			method: "POST",
			credentials: "omit",
			body: JSON.stringify({
				request_type: "test_rss_parse_all_client_request",
			}),
		});
	}



	/**
	 * This creates a notification using react-notifications.
	 *
	 * The type specified the notification type, the title and/or body can be used, and if
	 * only a body is submitted then it'll render as the body, and vice versa. Basically, you
	 * can show just "titles" or just "body", or both. Additionally, a time can be specified
	 * for how long the notification shall stay open, and you can give this function a callback
	 * to call IF the user clicks on the notification.
	 *
	 *
	 * @param {modesNotifTypes} type
	 * @param {string | null} title
	 * @param {string | null} body
	 * @param {number} time
	 * @param {() | null} callback
	 * @memberof App
	 */
	createNotification(type: modesNotifTypes, title: string | null, body: string | null, time: number | null, callback: (() => void) | null): void {
		// First we check over the optional parameters and assign accordingly
		let actualTime;
		if (time === null) {
			actualTime = 5000;
		}
		else {
			actualTime = time;
		}

		let actualCallback: () => void;
		if (callback === null) {
			actualCallback = (): void => {
				return;
			};
		}
		else {
			actualCallback = callback;
		}

		switch (type) {
		case modesNotifTypes.Success:
			NotificationManager.success(body, title, actualTime, actualCallback);
			break;
		case modesNotifTypes.Info:
			NotificationManager.info(body, title, actualTime, actualCallback);
			break;
		case modesNotifTypes.Warning:
			NotificationManager.warning(body, title, actualTime, actualCallback);
			break;
		case modesNotifTypes.Error:
			NotificationManager.error(body, title, actualTime, actualCallback);
			break;
		}
	}

	/**
	 * This is used to allow children components to change the background
	 *
	 * @param {modesAppBackground} modeToSetTo
	 * @memberof App
	 */
	setBackgroundMode(modeToSetTo: modesAppBackground): void {
		this.setState({
			backgroundMode: modeToSetTo,
		});
	}

	/**
	 * This renders the background based on the specified background mode.
	 *
	 * @return {*}  {JSX.Element}
	 * @memberof App
	 */
	renderBackground(): JSX.Element {
		if (this.state.backgroundMode !== modesAppBackground.VantaBirds) {
			if (this.vantaEffect) {
				this.dbgLog("App: renderBackground: destroying vanta");
				this.vantaEffect.destroy();
			}
		}
		switch (this.state.backgroundMode) {
		case modesAppBackground.GameIngImg:
			return <img className="image-background" src={gameIngBg}/>;
		case modesAppBackground.VantaBirds:
			// return <div ref={this.vantaRef}>asdf</div>;
			// VANTA.BIRDS({
			// 	el: "#your-element-selector",
			// 	mouseControls: true,
			// 	touchControls: true,
			// 	gyroControls: false,
			// 	minHeight: 200.00,
			// 	minWidth: 200.00,
			// 	scale: 1.00,
			// 	scaleMobile: 1.00
			//   })
			// This is the element to which we will attached the vantaEffect.
			return <div ref={this.vantaRef} className="some-other-background"></div>;
		default:
			return <div className="some-other-background"></div>;
		}
	}



	/**
	 * This handles which mode to render - Either an internal mode or an external one, based on the enums set in ./modes.tsx
	 *
	 * @return {JSX.Element} Returns the component which corresponds to the requested mode in the state
	 * @memberof App
	 */
	renderBasedOnMode(): JSX.Element {
		let component: JSX.Element;
		switch (this.state.mode) {
		case modesAppExternal.ExtHome:
			component =
			<div>
				<Home setMainMode={this.setMainMode} setBackgroundMode={this.setBackgroundMode}/>
			</div>;
			break;
		case modesAppExternal.ExtHelp:
			component =
			<div>
				<Help setMainMode={this.setMainMode} setBackgroundMode={this.setBackgroundMode}/>
			</div>;
			break;
		case modesAppInternal.Login:
			component =
			<Fragment>
				<div className="main-app">
					<LoginLanding
						createNotification={this.createNotification}
						setMainMode={this.setMainMode}
						authTokenIsValid={this.state.authTokenIsValid}
						updateAuthToken={this.updateAuthToken}
						verifyAuthToken={this.verifyAuthToken}
					/>
				</div>
			</Fragment>;
			break;
		case modesAppInternal.Profile:
			component =
			<Fragment>
				<div className="main-app">
					<Profile
						createNotification={this.createNotification}
						setMainMode={this.setMainMode}
						authToken={this.state.authToken}
						updateAuthToken={this.updateAuthToken}
					/>
				</div>
			</Fragment>;
			break;
		case modesAppInternal.FeedHub:
			component =
				<Fragment>
					<div className="main-app">
						{/* This renders the mobile nav here if the mode is in mobile */}
						{this.state.displayOrientation === modesDisplayOrientation.Mobile ?
							<BotNav
								createNotification={this.createNotification}
								setMainMode={this.setMainMode}
								mainMode={this.state.mode}
								authToken={this.state.authToken}
								checkIsAdmin={this.checkIsAdmin}
								spawnFeed={this.spawnFeed}
								debugParseSources={this.debugParseSources}
								updateAuthToken={this.updateAuthToken}
							/> : ""}
						<div className="grid-container" ref={this.gridContainerRefHandler}>
							{/* This renders the desktop nav here if the mode is in desktop */}
							{this.state.displayOrientation === modesDisplayOrientation.Desktop ?
								<LeftNav
									createNotification={this.createNotification}
									setMainMode={this.setMainMode}
									mainMode={this.state.mode}
									authToken={this.state.authToken}
									checkIsAdmin={this.checkIsAdmin}
									spawnFeed={this.spawnFeed}
									debugParseSources={this.debugParseSources}
									updateAuthToken={this.updateAuthToken}
								/> : ""}
							<FeedHub
								createNotification={this.createNotification}
								setBackgroundMode={this.setBackgroundMode}
								feedAdder={this.state.feedAdder}
								feedWasAdded={this.feedWasAdded}
								authToken={this.state.authToken}
								authTokenDecoded={this.state.authTokenDecoded}
								newBookmarksWereFetched={this.state.newBookmarksWereFetched}
								newBookmarksWereRendered={this.newBookmarksWereRendered}
							/>
						</div>

					</div>
				</Fragment>;

			break;
		case modesAppInternal.AdminHub:
			component =
			<Fragment>
				<div className="main-app">
					<div className="grid-container">
						<LeftNav
							createNotification={this.createNotification}
							setMainMode={this.setMainMode}
							mainMode={this.state.mode}
							authToken={this.state.authToken}
							checkIsAdmin={this.checkIsAdmin}
							spawnFeed={this.spawnFeed}
							debugParseSources={this.debugParseSources}
							updateAuthToken={this.updateAuthToken}
						/>
						<AdminHub
							createNotification={this.createNotification}
							setMainMode={this.setMainMode} checkIsAdmin={this.checkIsAdmin}
							authToken={this.state.authToken}
						/>
					</div>
				</div>
			</Fragment>;
			break;
		case modesAppInternal.DebuggerMenu:
			component = <DebuggerMenu setMainMode={this.setMainMode}/>;
			break;
		default:
			component =
			<div>
				<Home setMainMode={this.setMainMode} setBackgroundMode={this.setBackgroundMode}/>
			</div>;
		}
		return component;
	}



	/**
	 * Renders a button to toggle the elements using [[toggleSomething]] and renders the specified component
	 * @return {JSX.Element}
	 * @memberof App
	 */
	render(): JSX.Element {
		return (
			<Fragment>
				{this.state.highBookmarkCountWarning === true && this.state.highBookmarkCountWarningAccepted === null ?
					<PopUp message={
						<Fragment>
							<p>You have more than {globals.BOOKMARK_HIGH_COUNT_WARNING} bookmarks saved, would you like to proceed with synchronisation?</p>
							<code>The total number of bookmarks you appear to have saved is {this.state.highBookmarkCountWarningAmount}</code>
						</Fragment>
					}
					options={
						[
							{
								text: "Accept Sync",
								callback: () => {
									this.setState({
										highBookmarkCountWarningAccepted: true,
									}, () => {
										this.saveLocalStorageBookmarkWarning();
									});
								},
							},
							{
								text: "Decline Sync",
								callback: () => {
									this.setState({
										highBookmarkCountWarningAccepted: false,
									}, () => {
										this.saveLocalStorageBookmarkWarning();
									});
								},
							},
						]
					}/> : ""
				}
				{/* This is using https://www.npmjs.com/package/react-notifications. We create this globally. */}
				<NotificationContainer/>
				{/* This renders whatever part of the app we have selected */}
				{this.renderBasedOnMode()}
				{/* This renders the background, which would either be set via the mode or via a debug/admin menu */}
				{this.renderBackground()}

				<div ref={this.overlayDbgRef} className="overlay-debug">
					<p>main mode: {this.state.mode}</p>
					<p>API: {process.env.REACT_APP_API_URL}</p>
					{/*
					This is a ternary operator... because we can use those inside here.
					Basically all it does is checks if this.state.authTokenIsValid is equal to "token_valid"
					- If it is equal to "token_valid", it renders (returns) "logged in" in the <p> tag
					- If it is not equal to "token_valid", it renders (returns) "not logged in" in the <p> tag
				*/}
					<p>{this.state.authTokenIsValid === "token_valid" ? "logged in: true" : "logged in: false"}</p>
					{/* This is the same as above, but just checks if the return of the function is a true or false */}
					<p>{this.checkIsAdmin() ? "user: admin" : "user: normal"}</p>

					{/* Show debugger menu access if user is admin */}
					{this.checkIsAdmin() ? <button className="admin-button" onClick={() => this.setMainMode(modesAppInternal.DebuggerMenu)}>Debugger Menu</button> : ""}

					<button className="admin-button" onClick={() => this.setMainMode(modesAppExternal.ExtHome)}>GOTO: About</button>
					<button className="admin-button" onClick={() => this.setMainMode(modesAppInternal.FeedHub)}>GOTO: Hub</button>
					<button className="admin-button" onClick={() => this.setMainMode(modesAppInternal.Login)}>GOTO: Login</button>
					<button className="admin-button" onClick={() => {
						if (this.state.backgroundMode === modesAppBackground.GameIngImg) {
							this.setState({
								backgroundMode: modesAppBackground.VantaBirds,
							});
						}
						else if (this.state.backgroundMode === modesAppBackground.VantaBirds) {
							this.setState({
								backgroundMode: modesAppBackground.GrayColor,
							});
						}
						else if (this.state.backgroundMode === modesAppBackground.GrayColor) {
							this.setState({
								backgroundMode: modesAppBackground.GameIngImg,
							});
						}
					}}>Switch Background</button>
				</div>
			</Fragment>
		);
	}
}

export default App;
