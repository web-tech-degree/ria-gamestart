import React, {Fragment} from "react";





// -----------------------------------------------------------------------------
// Types
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Modes
import {modesLoginLanding} from "./modes";
import {modesAppExternal, modesAppInternal} from "../modes";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Components
import Nav from "../Externals/Nav";
import Footer from "../Externals/Footer";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Libraries
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Assets
// -----------------------------------------------------------------------------





type AlreadyLoggedInProps = {
	setMainMode: (modeToSetTo: modesAppInternal | modesAppExternal) => void,
	setMode: (modeToSetTo: modesLoginLanding) => void,
	updateUiFeedback: (msg: string) => void,
	authTokenIsValid: string,
	updateAuthToken: (val: string) => void,
	verifyAuthToken: () => void,
}





/**
 *
 *
 * @class AlreadyLoggedIn
 * @extends {React.Component}
 */
class AlreadyLoggedIn extends React.Component<AlreadyLoggedInProps> {
	/**
	 * Creates an instance of AlreadyLoggedIn.
	 * @param {AlreadyLoggedInProps} props
	 * @memberof AlreadyLoggedIn
	 */
	constructor(props: AlreadyLoggedInProps) {
		super(props);
	}



	/**
	 * Render this to inform the user that they're already logged in
	 *
	 * @return {*}
	 * @memberof AlreadyLoggedIn
	 */
	render(): JSX.Element {
		return (
			<Fragment>
				<Nav setMainMode={this.props.setMainMode}></Nav>
				<div className="d-md-flex h-md-100 align-items-center">

					{/* <!-- First Half --> */}

					<div className="banner col-md-4 p-0 h-md-100">
						<div className="text-white d-md-flex h-100 p-5 text-center justify-content-center">
							<div className="logoarea pt-5 pb-5">
							</div>
						</div>
					</div>

					<div className="already-logged-in-simple-center">
						<h3>You are already logged in</h3>
						<button className="default-button" onClick={() => this.props.setMainMode(modesAppInternal.FeedHub)}>Take me to the FeedsHub</button>
						<button className="default-button" onClick={() => this.props.updateAuthToken("logged_out")}>Log out?</button>
					</div>

				</div>
				<Footer setMainMode={this.props.setMainMode}></Footer>
			</Fragment>
		);
	}
}

export default AlreadyLoggedIn;
