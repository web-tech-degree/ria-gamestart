import React, {Fragment} from "react";
import "./Login.css";
import {globals} from "../../logic/globals";





// -----------------------------------------------------------------------------
// Types
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Modes
import {modesLoginLanding, modesLoginStage} from "./modes";
import {modesAppExternal, modesAppInternal} from "../modes";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Components
import Nav from "../Externals/Nav";
import Footer from "../Externals/Footer";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Libraries
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Assets
// -----------------------------------------------------------------------------





type LoginState = {
	// Login/Register form stuff
	loginStage: modesLoginStage,
	loginEmail: string,
	loginPassword: string,
	login2FAClientToken: string,
	login2FAEmailToken: string,
}

type LoginProps = {
	setMainMode: (modeToSetTo: modesAppInternal | modesAppExternal) => void,
	setMode: (modeToSetTo: modesLoginLanding) => void,
	updateUiFeedback: (msg: string) => void,
	authTokenIsValid: string,
	updateAuthToken: (val: string) => void,
	verifyAuthToken: () => void,
}





/**
 * This class handles the login and updates the parent based on the events.
 *
 * @class Login
 * @extends {React.Component}
 */
class Login extends React.Component<LoginProps, LoginState> {
	/**
	 * Creates an instance of Login.
	 * @param {LoginProps} props
	 * @memberof Login
	 */
	constructor(props: LoginProps) {
		super(props);
		this.state = {
			loginStage: modesLoginStage.Login,
			loginEmail: "",
			loginPassword: "",
			login2FAClientToken: "",
			login2FAEmailToken: "",
		};

		this.dbgLog();

		this.loginSubmit = this.loginSubmit.bind(this);
		this.loginSubmit2FA = this.loginSubmit2FA.bind(this);
	}



	/**
	 * This checks if this component should be "debugged" via the process.env variables. If it's true,
	 * we bind console.log so that logging works normally, otherwise we make it not do anything.
	 *
	 * @param {*} [mode]
	 * @param {...any[]} optionalParams
	 * @memberof Login
	 */
	dbgLog(
		// eslint-disable-next-line
		mode?: any,...optionalParams: any[]
	): void {
		if (globals.DEBUGGER_DBG_LOG === "true" && globals.DEBUGGER_DBG_Login === "true") {
			this.dbgLog = console.log.bind(window.close);
		}
		else {
			this.dbgLog = (): void => {
				return;
			};
		}
	}



	/**
	 * Send a login request to the backend with the data set in the state and handle the JSON response.
	 *
	 * @param {Event} event
	 * @memberof Login
	 */
	loginSubmit(event: React.FormEvent<HTMLFormElement>): void {
		this.dbgLog("Login: loginSubmit");
		event.preventDefault();
		fetch(process.env.REACT_APP_API_URL + "__api.php", {
			method: "POST",
			credentials: "omit",
			body: JSON.stringify({
				request_type: "user_login",
				user_email: this.state.loginEmail,
				user_password: this.state.loginPassword,
			}),
		})
			.then((response) => response.json())
			.then((data) => {
				this.dbgLog("Login: loginSubmit response");
				if (data.message === "login_success") {
					// Update the authToken state of App component using the updateAuthToken that was passed down
					this.props.updateAuthToken(data.jwt);
					this.dbgLog("Login: loginSubmit success");
				}
				// 2FA request successful, so store the client token and move on to the 2FA verification stage
				else if (data.message === "login_2fa_request_success") {
					this.setState({
						loginStage: modesLoginStage.loginVerify2FA,
						login2FAClientToken: data.client_2fa_token,
					});
				}
				this.props.updateUiFeedback(data.message);
			});
	}



	/**
	 * This is used if 2FA is used (aka email is verified). This re-submits all of the data,
	 * alongside the client-stored token and email token the user should've received
	 *
	 * @param {Event} event
	 * @memberof Login
	 */
	loginSubmit2FA(event: React.FormEvent<HTMLFormElement>): void {
		this.dbgLog("Login: loginSubmit2FA");
		event.preventDefault();
		fetch(process.env.REACT_APP_API_URL + "__api.php", {
			method: "POST",
			credentials: "omit",
			body: JSON.stringify({
				request_type: "user_login_2fa_conf",
				user_email: this.state.loginEmail,
				user_password: this.state.loginPassword,
				client_token: this.state.login2FAClientToken,
				email_token: this.state.login2FAEmailToken,
			}),
		})
			.then((response) => response.json())
			.then((data) => {
				this.dbgLog("Login: loginSubmit2FA response");
				if (data.message === "login_2fa_conf_success") {
					// Update the authToken state of App component using the updateAuthToken that was passed down
					this.props.updateAuthToken(data.jwt);
					this.dbgLog("Login: loginSubmit2FA success");
				}
				this.props.updateUiFeedback(data.message);
			});
	}


	/**
	 *
	 *
	 * @return {*}  {JSX.Element}
	 * @memberof Login
	 */
	renderLoginFormBasedOnStage(): JSX.Element {
		// ---------------------------------------------------------------------
		// Render the 2FA verification form. This'll be the second form; I only
		// put it here with an if/else because I'm lazy and TypeScript complains
		// about undefined return even though loginStage can only ever be one of two
		// things.
		// Anyhow, in this stage the user's details are re-submitted, but they do not
		// have to re-enter them; instead, they have to enter the 2FA token they received
		// to their email, and the JS will also submit the client-stored token alongside it
		// ---------------------------------------------------------------------
		if (this.state.loginStage === modesLoginStage.loginVerify2FA) {
			return <form onSubmit={this.loginSubmit2FA}>
				<h5>2FA Login Confirmation</h5>
				<div className="form-group">
					<label htmlFor="token-email">2FA Email Token</label>
					{/* Value is set to nothing here because for some reason the email was being put here */}
					<input type="text" className="form-control" id="token-email" aria-describedby="2faHelp"placeholder="Enter 2FA token"
						required onChange={(event) => this.setState({login2FAEmailToken: event.target.value})}
						value={this.state.login2FAEmailToken}
					/>
					<small id="2faHelp" className="form-text text-muted">You should have received a verification token in your email.</small>
				</div>
				<div className="form-button">
					<button type="submit" className="btn">Submit</button>
				</div>
				<div className="account">Changed your mind?
					<div className="row">
						<a className="text cursor-pointer" onClick={() => this.props.setMode(modesLoginLanding.Login)}>Sign in</a>
					</div>
					<div className="row">
						<a className="text cursor-pointer" onClick={() => this.props.setMode(modesLoginLanding.Register)}>Sign Up</a>
					</div>
				</div>
			</form>;
		}
		// ---------------------------------------------------------------------
		// Render initial login form. If the user didn't verify their email,
		// this is the only stage that will be used
		// ---------------------------------------------------------------------
		else {
			return <form onSubmit={this.loginSubmit}>
				<h5>Sign In or Register</h5>
				<div className="form-group">
					<label htmlFor="email-input">Email</label>
					<input type="email" className="form-control" id="email-input" aria-describedby="emailHelp" placeholder="Enter email"
						required onChange={(event) => this.setState({loginEmail: event.target.value})}
					/>
					<small id="emailHelp" className="form-text text-muted">We&apos;ll never share your email with anyone else.</small>
				</div>
				<div className="form-group">
					<label htmlFor="password-input">Password</label>
					<input type="password" className="form-control" id="password-input" placeholder="Password"
						required onChange={(event) => this.setState({loginPassword: event.target.value})}
					/>
				</div>
				{/* <div className="form-check">
					<input type="checkbox" className="form-check-input" id="exampleCheck1"/>
					<label className="form-check-label" htmlFor="exampleCheck1">Remember Me</label>
				</div> */}
				<div className="form-button">
					<button type="submit" className="btn">Submit</button>
				</div>
				<div className="account">Don&apos;t have an account?
					<a className="text cursor-pointer" onClick={() => this.props.setMode(modesLoginLanding.Register)}> Sign Up</a>
					<div className="row">
						<a className="text cursor-pointer" onClick={() => this.props.setMode(modesLoginLanding.ForgotPassword)}>Forgot your password?</a>
					</div>
					<div className="row">
						<a className="text cursor-pointer" onClick={() => this.props.setMode(modesLoginLanding.ReconfirmEmail)}>Want to resend your email verification?</a>
					</div>
				</div>
			</form>;
		}
	}

	/**
	 *
	 *
	 * @return {*}
	 * @memberof Login
	 */
	render(): JSX.Element {
		// ---------------------------------------------------------------------
		// SECTION HTML: Login
		// ---------------------------------------------------------------------
		return (
			<Fragment>
				<Nav setMainMode={this.props.setMainMode}></Nav>
				<div className="d-md-flex h-md-100 align-items-center">

					{/* First Half */}

					{/* We have to do the source inline because we need access to process.env and CSS doesn't have that */}
					<div className="banner col-md-4 p-0 h-md-100 mh-100">
						<div className="text-white d-md-flex h-100 p-0 text-center justify-content-center">
							<div className="logoarea pt-5 pb-5">
							</div>
						</div>
					</div>

					{/* Second Half */}

					<div className="col-md-8 p-0 bg-white mh-100 loginarea">
						<div className="d-md-flex align-items-center mh-100 p-5 justify-content-center">
							{this.renderLoginFormBasedOnStage()}
						</div>
					</div>


				</div>
				<Footer setMainMode={this.props.setMainMode}/>
			</Fragment>



		);
		// ---------------------------------------------------------------------
		// !SECTION
		// ---------------------------------------------------------------------
	}
}


export default Login;
