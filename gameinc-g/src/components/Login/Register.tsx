import React, {Fragment} from "react";
import "./Register.css";





// -----------------------------------------------------------------------------
// Types
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Modes
import {modesLoginLanding} from "./modes";
import {modesAppExternal, modesAppInternal} from "../modes";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Components
import Nav from "../Externals/Nav";
import Footer from "../Externals/Footer";
import {globals} from "../../logic/globals";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Libraries
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Assets
// -----------------------------------------------------------------------------





type RegisterState = {
	registerUsername: string,
	registerEmail: string,
	registerPassword: string,
	registerPasswordConf: string,
}

type RegisterProps = {
	setMainMode: (modeToSetTo: modesAppInternal | modesAppExternal) => void,
	setMode: (modeToSetTo: modesLoginLanding) => void,
	updateUiFeedback: (msg: string) => void,
	authTokenIsValid: string,
	updateAuthToken: (val: string) => void,
	verifyAuthToken: () => void,
}





/**
 * This class is used for the register page
 *
 * @class Register
 * @extends {React.Component}
 */
class Register extends React.Component<RegisterProps, RegisterState> {
	/**
	 * Creates an instance of Register.
	 * @param {RegisterProps} props
	 * @memberof Register
	 */
	constructor(props: RegisterProps) {
		super(props);
		this.state = {
			registerUsername: "",
			registerEmail: "",
			registerPassword: "",
			registerPasswordConf: "",
		};

		this.dbgLog();

		this.registerSubmit = this.registerSubmit.bind(this);
	}



	/**
	 * This checks if this component should be "debugged" via the process.env variables. If it's true,
	 * we bind console.log so that logging works normally, otherwise we make it not do anything.
	 *
	 * @param {*} [mode]
	 * @param {...any[]} optionalParams
	 * @memberof Register
	 */
	dbgLog(
		// eslint-disable-next-line
		mode?: any,...optionalParams: any[]
	): void {
		if (globals.DEBUGGER_DBG_LOG === "true" && globals.DEBUGGER_DBG_Register === "true") {
			this.dbgLog = console.log.bind(window.close);
		}
		else {
			this.dbgLog = (): void => {
				return;
			};
		}
	}



	/**
	 * Send a register request to the backend with the data set in the state and handle the JSON response.
	 *
	 * @param {FormEvent<HTMLFormElement>} event
	 * @memberof Register
	 */
	registerSubmit(event: React.FormEvent<HTMLFormElement>): void {
		this.dbgLog("Register: registerSubmit");
		event.preventDefault();
		fetch(process.env.REACT_APP_API_URL + "__api.php", {
			method: "POST",
			credentials: "omit",
			body: JSON.stringify({
				request_type: "user_register",
				user_name: this.state.registerUsername,
				user_email: this.state.registerEmail,
				user_password: this.state.registerPassword,
				user_password_conf: this.state.registerPasswordConf,
			}),
		})
			.then((response) => response.json())
			.then((data) => {
				this.dbgLog("Register: registerSubmit response");
				// Check what the response message is
				if (data.message === "loginAfterRegister_success") {
					this.dbgLog("Register: registerSubmit success");
					this.props.updateAuthToken(data.jwt);
				}
				this.props.updateUiFeedback(data.message);
			});
	}

	// <div className="login-stuff-container">
	// <h2>Register</h2>
	// <form className="bare-login-form" onSubmit={this.registerSubmit}>
	// <label htmlhtmlFor="username"><b>Username (You can change this later)</b></label>
	// <input type="text" placeholder="Enter Username" name="username" required/>
	//
	// <label htmlhtmlFor="email"><b>Email</b></label>
	// <input type="text" placeholder="Enter Email" name="email" required />
	//
	//			<label htmlhtmlFor="password"><b>Password</b></label>
	//			<input type="password" placeholder="Enter Password" name="password">
	//
	//			<label htmlhtmlFor="password"><b>Confirm Password</b></label>
	//			<input type="password" placeholder="Enter Password" name="password" required onChange={(event) => this.setState({registerPasswordConf: event.target.value})}/>
	//
	//			<button type="submit">Register</button>
	//		</form>
	//		<span className="temp-space-top">Already have an account? <a onClick={() => this.props.setMode(modesLoginLanding.Login)} href="#">sign in</a></span>
	//  </div>



	/**
	 * This renders the register page.
	 *
	 * @return {*}
	 * @memberof Register
	 */
	render(): JSX.Element {
		// ---------------------------------------------------------------------
		// SECTION  HTML: Register page
		// ---------------------------------------------------------------------
		return (
			<Fragment>

				<Nav setMainMode={this.props.setMainMode}></Nav>
				<div className="d-md-flex h-md-100 align-items-center">

					{/* <!-- First Half --> */}

					<div className="banner col-md-4 p-0 h-md-100">
						<div className="text-white d-md-flex h-100 p-5 text-center justify-content-center">
							<div className="logoarea pt-5 pb-5">
							</div>
						</div>
					</div>

					{/* <!-- Second Half --> */}

					<div className="col-md-8 p-0 bg-white h-md-100 loginarea">
						<div className="d-md-flex align-items-center h-md-100 p-5 justify-content-center">
							<form onSubmit={this.registerSubmit}>
								<h5>Register New Account</h5>
								<div className="form-group">
									<label htmlFor="username">Username</label>
									<input type="text" className="form-control" id="username" placeholder="Type Here..." required
										onChange={(event) => this.setState({registerUsername: event.target.value})}/>
								</div>
								<div className="form-group">
									<label htmlFor="email">Email</label>
									<input type="email" className="form-control" id="email" aria-describedby="emailHelp" placeholder="test@mail.com" required
										onChange={(event) => this.setState({registerEmail: event.target.value})}/>
								</div>
								<div className="form-group">
									<label htmlFor="password">Password</label>
									<input type="password" className="form-control" id="password" placeholder="*******" required
										onChange={(event) => this.setState({registerPassword: event.target.value})}/>
								</div>
								<div className="form-group">
									<label htmlFor="password-conf">Repeat Password</label>
									<input type="password" className="form-control" id="password-conf" placeholder="*******" required
										onChange={(event) => this.setState({registerPasswordConf: event.target.value})}/>
								</div>
								<div className="form-button just-content-center">
									<button type="submit" className="btn">Submit</button>
								</div>
								<div className="account">Already have an account
									<a className="text cursor-pointer" onClick={() => this.props.setMode(modesLoginLanding.Login)}> Sign In</a>
									<div className="row">
										<a className="text cursor-pointer" onClick={() => this.props.setMode(modesLoginLanding.ForgotPassword)}>Forgot your password?</a>
									</div>
									<div className="row">
										<a className="text cursor-pointer" onClick={() => this.props.setMode(modesLoginLanding.ForgotPassword)}>Want to resend your email verification?</a>
									</div>
								</div>
							</form>
						</div>
					</div>

				</div>
				<Footer setMainMode={this.props.setMainMode}></Footer>
			</Fragment>
		);
		// ---------------------------------------------------------------------
		// !SECTION
		// ---------------------------------------------------------------------
	}
}

export default Register;
