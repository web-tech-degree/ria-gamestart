import React, {Fragment} from "react";
import {globals} from "../../logic/globals";





// -----------------------------------------------------------------------------
// Types
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Modes
import {modesLoginLanding, modesForgotPassword} from "./modes";
import {modesAppExternal, modesAppInternal} from "../modes";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Components
import Nav from "../Externals/Nav";
import Footer from "../Externals/Footer";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Libraries
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Assets
// -----------------------------------------------------------------------------





type ForgotPasswordState = {
	mode: string,
	resetPasswordToken: string,
	resetPasswordEmail: string,
	resetPasswordPassword: string,
	resetPasswordPasswordConf: string,

}

type ForgotPasswordProps = {
	setMainMode: (modeToSetTo: modesAppInternal | modesAppExternal) => void,
	setMode: (modeToSetTo: modesLoginLanding) => void,
	updateUiFeedback: (msg: string) => void,
	authTokenIsValid: string,
	updateAuthToken: (val: string) => void,
	verifyAuthToken: () => void,
}





/**
 * This class handles the forgot password stages, being the "request" and "submit" modes,
 * which splits the forgot password stage into two.
 *
 * @class ForgotPassword
 * @extends {React.Component}
 */
class ForgotPassword extends React.Component<ForgotPasswordProps, ForgotPasswordState> {
	/**
	 * Creates an instance of ForgotPassword.
	 * @param {ForgotPasswordProps} props
	 * @memberof ForgotPassword
	 */
	constructor(props: ForgotPasswordProps) {
		super(props);
		this.state = {
			mode: modesForgotPassword.Request,
			resetPasswordToken: "",
			resetPasswordEmail: "",
			resetPasswordPassword: "",
			resetPasswordPasswordConf: "",
		};

		this.dbgLog();

		this.setMode = this.setMode.bind(this);
		this.resetPasswordRequestSubmit = this.resetPasswordRequestSubmit.bind(this);
		this.resetPasswordProperSubmit = this.resetPasswordProperSubmit.bind(this);
	}


	/**
	 * This checks if this component should be "debugged" via the process.env variables. If it's true,
	 * we bind console.log so that logging works normally, otherwise we make it not do anything.
	 *
	 * @param {*} [mode]
	 * @param {...any[]} optionalParams
	 * @memberof ForgotPassword
	 */
	dbgLog(
		// eslint-disable-next-line
		mode?: any,...optionalParams: any[]
	): void {
		if (globals.DEBUGGER_DBG_LOG === "true" && globals.DEBUGGER_DBG_ForgotPassword === "true") {
			this.dbgLog = console.log.bind(window.close);
		}
		else {
			this.dbgLog = (): void => {
				return;
			};
		}
	}



	/**
	 * This is essentially just an alias/macro to be more explicit about the whole "mode" thing
	 *
	 * @param {availableForgotPasswordModes} modeToSetTo
	 * @memberof ForgotPassword
	 */
	setMode(modeToSetTo: modesForgotPassword): void {
		this.setState(() => ( {
			mode: modeToSetTo,
		}));
	}



	/**
	 * This sends a reset password token to the email and, if the email has supposedly been sent, takes the user to the verification
	 * page.
	 *
	 * @param {FormEvent<HTMLFormElement>} event
	 * @memberof ForgotPassword
	 */
	resetPasswordRequestSubmit(event: React.FormEvent<HTMLFormElement>): void {
		this.dbgLog("ForgotPassword: resetPasswordRequestSubmit");
		event.preventDefault();
		fetch(process.env.REACT_APP_API_URL + "__api.php", {
			method: "POST",
			credentials: "omit",
			body: JSON.stringify({
				request_type: "user_email_password_reset_request",
				user_email: this.state.resetPasswordEmail,
			}),
		})
			.then((response) => response.json())
			.then((data) => {
				this.dbgLog("ForgotPassword: resetPasswordRequestSubmit response");
				// If the password reset link has, supposedly, been sent, display the verification stage
				if (data.message === "sendpasswordreset_email_sent_if_exist") {
					this.dbgLog("ForgotPassword: resetPasswordRequestSubmit success");
					this.setMode(modesForgotPassword.Submit);
				}
				this.props.updateUiFeedback(data.message);
			});
	}



	/**
 	* This is the token verification and password update stage that the user is given if they have requested a reset.
 	* This submits the provided token and new passwords at once.
 	*
 	* @param {FormEvent<HTMLFormElement>} event
 	* @memberof ForgotPassword
 	*/
	resetPasswordProperSubmit(event: React.FormEvent<HTMLFormElement>): void {
		this.dbgLog("ForgotPassword: resetPasswordProperSubmit");
		event.preventDefault();
		fetch(process.env.REACT_APP_API_URL + "__api.php", {
			method: "POST",
			credentials: "omit",
			body: JSON.stringify({
				request_type: "password_reset_proper",
				user_token: this.state.resetPasswordToken,
				user_email: this.state.resetPasswordEmail,
				user_password: this.state.resetPasswordPassword,
				user_password_conf: this.state.resetPasswordPasswordConf,
			}),
		})
			.then((response) => response.json())
			.then((data) => {
				this.dbgLog("ForgotPassword: resetPasswordProperSubmit response");
				if (data.message === "loginAfterResetPassword_success") {
					this.dbgLog("ForgotPassword: resetPasswordProperSubmit success");
					// Update the authToken state of App component using the updateAuthToken that was passed down
					this.props.updateAuthToken(data.jwt);
				}
				this.props.updateUiFeedback(data.message);
			});
	}



	/**
	 * Render either the forgot password email stage, or the token verification + enter new password stage.
	 * The switch/case is done here because this isn't very complex
	 *
	 * @return {*}
	 * @memberof ForgotPassword
	 */
	render(): JSX.Element {
		switch (this.state.mode) {
		case modesForgotPassword.Request:
			this.dbgLog("ForgotPassword: rendering request mode");
			// -----------------------------------------------------------------
			// SECTION HTML: Forgot password request
			// -----------------------------------------------------------------
			return (
				<Fragment>
					<Nav setMainMode={this.props.setMainMode}></Nav>
					<div className="d-md-flex h-md-100 align-items-center">

						{/* <!-- First Half --> */}

						<div className="banner col-md-4 p-0 h-md-100">
							<div className="text-white d-md-flex h-100 p-5 text-center justify-content-center">
								<div className="logoarea pt-5 pb-5">
								</div>
							</div>
						</div>

						{/* <!-- Second Half --> */}

						<div className="col-md-8 p-0 bg-white h-md-100 loginarea">
							<div className="d-md-flex align-items-center h-md-100 p-5 justify-content-center">
								<form onSubmit={this.resetPasswordRequestSubmit}>
									<h5>Request Password Reset</h5>
									<div className="form-group">
										<label htmlFor="token">Email</label>
										<input type="text" className="form-control" id="token" placeholder="Type Here..." required
											onChange={(event) => this.setState({resetPasswordEmail: event.target.value})}
										/>
										<small className="form-text text-muted">You should receive an email with a verification token</small>
									</div>

									<div className="form-button just-content-center">
										<button type="submit" className="btn">Submit</button>
									</div>
									<div className="account">Already have an account
										<a className="text cursor-pointer" onClick={() => this.props.setMode(modesLoginLanding.Login)}> Sign In</a>
										<div className="row">
											<a className="text cursor-pointer" onClick={() => this.props.setMode(modesLoginLanding.ForgotPassword)}>Forgot your password?</a>
										</div>
										<div className="row">
											<a className="text cursor-pointer" onClick={() => this.props.setMode(modesLoginLanding.ReconfirmEmail)}>Want to resend your email verification?</a>
										</div>
									</div>
								</form>
							</div>
						</div>

					</div>
					<Footer setMainMode={this.props.setMainMode}></Footer>
				</Fragment>
			);
			// -----------------------------------------------------------------
			// !SECTION
			// -----------------------------------------------------------------
		case modesForgotPassword.Submit:
			this.dbgLog("ForgotPassword: rendering submit mode");
			// -----------------------------------------------------------------
			// SECTION HTML: Forgot password verification
			// -----------------------------------------------------------------
			return (
				<Fragment>
					<Nav setMainMode={this.props.setMainMode}></Nav>
					<div className="d-md-flex h-md-100 align-items-center">

						{/* <!-- First Half --> */}

						<div className="banner col-md-4 p-0 h-md-100">
							<div className="text-white d-md-flex h-100 p-5 text-center justify-content-center">
								<div className="logoarea pt-5 pb-5">
								</div>
							</div>
						</div>

						{/* <!-- Second Half --> */}

						<div className="col-md-8 p-0 bg-white h-md-100 loginarea">
							<div className="d-md-flex align-items-center h-md-100 p-5 justify-content-center">
								<form onSubmit={this.resetPasswordProperSubmit}>
									<h5>Verify Token</h5>
									<div className="form-group">
										<label htmlFor="token">Token</label>
										<input type="text" className="form-control" id="token" placeholder="Paste here..." required
											onChange={(event) => this.setState({resetPasswordToken: event.target.value})}
											// Value is set here because for some reason the email was being put here
											value={this.state.resetPasswordToken}
										/>
										<small className="form-text text-muted">You should have received an email</small>
									</div>
									<div className="form-group">
										<label htmlFor="email">Email</label>
										{/* The value here is read for UX reasons; there's no point for the user to re-enter their email manually */}
										<input type="text" className="form-control" id="email" placeholder="Type Here..." required
											onChange={(event) => this.setState({resetPasswordEmail: event.target.value})}
											value={this.state.resetPasswordEmail}
										/>
									</div>
									<div className="form-group">
										<label htmlFor="password">New Password</label>
										<input type="password" className="form-control" id="password" placeholder="Type Here..." required
											onChange={(event) => this.setState({resetPasswordPassword: event.target.value})}
										/>
									</div>
									<div className="form-group">
										<label htmlFor="password-conf">Confirm New Password</label>
										<input type="password" className="form-control" id="password-conf" placeholder="Type Here..." required
											onChange={(event) => this.setState({resetPasswordPasswordConf: event.target.value})}
										/>
									</div>
									<div className="form-button just-content-center">
										<button type="submit" className="btn">Submit</button>
									</div>
									<div className="account">Already have an account
										<a className="text cursor-pointer" onClick={() => this.props.setMode(modesLoginLanding.Login)}> Sign In</a>
										<div className="row">
											<a className="text cursor-pointer" onClick={() => this.props.setMode(modesLoginLanding.ForgotPassword)}>Forgot your password?</a>
										</div>
										<div className="row">
											<a className="text cursor-pointer" onClick={() => this.props.setMode(modesLoginLanding.ReconfirmEmail)}>Want to resend your email verification?</a>
										</div>
									</div>
								</form>
							</div>
						</div>

					</div>
					{/* <Footer setMainMode={this.props.setMainMode}></Footer> */}
					<div className="footer-container fixed-bottom">
						<div className="text-center p-3">
					© 2021 Copyright:
							<a className="text" href="#">Game.inc.com</a>
						</div>
					</div>
				</Fragment>


			);
			// -----------------------------------------------------------------
			// !SECTION
			// -----------------------------------------------------------------
		default:
			// This should never be displayed
			this.dbgLog("ForgotPassword: rendering somehow didn't match this.state.mode");
			// -----------------------------------------------------------------
			// SECTION HTML: Error which should never occur
			// -----------------------------------------------------------------
			return (
				<div>Error: ForgotPassword this.state.mode mismatch</div>
			);
			// -----------------------------------------------------------------
			// !SECTION
			// -----------------------------------------------------------------
		}
	}
}

export default ForgotPassword;
