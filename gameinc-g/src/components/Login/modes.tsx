// -----------------------------------------------------------------------------
// Only allow the following modes for the LoginLanding component
// -----------------------------------------------------------------------------
enum modesLoginLanding {
	Login = "Login",
	Register = "Register",
	ForgotPassword = "ForgotPassword",
	ReconfirmEmail = "ReconfirmEmail",
}

// -----------------------------------------------------------------------------
// Only allow the following modes for the ForgotPassword component
// -----------------------------------------------------------------------------
enum modesForgotPassword {
	Request = "Request",
	Submit = "Submit",
}

enum modesLoginStage {
	Login = "Login",
	loginVerify2FA = "loginVerify2FA",
}

export {
	modesLoginLanding,
	modesForgotPassword,
	modesLoginStage,
};
