import React from "react";
import "./LoginLanding.css";
import {globals} from "../../logic/globals";





// -----------------------------------------------------------------------------
// Types
import type {createNotificationType} from "../types";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Modes
import {modesAppInternal, modesAppExternal, modesNotifTypes} from "../modes";
import {modesLoginLanding} from "./modes";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Components
import Login from "./Login";
import Register from "./Register";
import ForgotPassword from "./ForgotPassword";
import ReconfirmEmail from "./ReconfirmEmail";
import AlreadyLoggedIn from "./AlreadyLoggedIn";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Libraries
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Assets
// -----------------------------------------------------------------------------





type LoginLandingState = {
	// Allow only explicitly created modes
	mode: modesLoginLanding,

	// Error message from requests to give user feedback about
	uiFeedbackMessage: string,
}

type LoginLandingProps = {
	createNotification: createNotificationType,
	// Accepts only explicitly created modes
	setMainMode: (modeToSetTo: modesAppInternal | modesAppExternal) => void,
	authTokenIsValid: string,
	updateAuthToken: (val: string) => void,
	verifyAuthToken: () => void,
}





/**
 * This class handles the rendering of the login page which will be accessible from the About section
 * and from within the app itself.
 *
 * @return {JSX.Element}
 */
class LoginLanding extends React.Component<LoginLandingProps, LoginLandingState> {
	/**
	 * Creates an instance of LoginLanding, defaulting to the login mode and ensuring
	 * no uiFeedbackMessage is displayed by default.
	 *
	 * @param {LoginLandingProps} props
	 * @memberof LoginLanding
	 */
	constructor(props: LoginLandingProps) {
		super(props);
		this.state = {
			mode: modesLoginLanding.Login,
			uiFeedbackMessage: "",
		};

		this.dbgLog();

		this.setMode = this.setMode.bind(this);
		this.logoutSubmit = this.logoutSubmit.bind(this);
		this.uiFeedbackDisplay = this.uiFeedbackDisplay.bind(this);
		this.updateUiFeedback = this.updateUiFeedback.bind(this);
	}


	/**
	 * This checks if this component should be "debugged" via the process.env variables. If it's true,
	 * we bind console.log so that logging works normally, otherwise we make it not do anything.
	 *
	 * @param {*} [mode]
	 * @param {...any[]} optionalParams
	 * @memberof LoginLanding
	 */
	dbgLog(
		// eslint-disable-next-line
		mode?: any,...optionalParams: any[]
	): void {
		if (globals.DEBUGGER_DBG_LOG === "true" && globals.DEBUGGER_DBG_LoginLanding === "true") {
			this.dbgLog = console.log.bind(window.close);
		}
		else {
			this.dbgLog = (): void => {
				return;
			};
		}
	}

	/**
	 * All we're doing here is telling the parent, App, to re-verify the token
	 * We do this so the validity of the token is actually accurate so we don't display
	 * the login page if user is already logged in.
	 *
	 * @memberof LoginLanding
	 */
	componentDidMount(): void {
		this.props.verifyAuthToken();
	}



	/**
	 * Because we're managing all the response messages in here from the child components,
	 * we just trigger the method to display the createNotification if a message has been set.
	 *
	 * @memberof LoginLanding
	 */
	componentDidUpdate(): void {
		if (this.state.uiFeedbackMessage.length > 1) {
			this.uiFeedbackDisplay();
		}
	}



	/**
	 * Simply switch which mode to display via a change of the state; another method
	 * looks at this mode state and renders the respective login/register etc. fields.
	 *
	 * @param {string} modeToSetTo
	 * @memberof LoginLanding
	 */
	setMode(modeToSetTo: modesLoginLanding): void {
		this.dbgLog("LoginLanding: setMode");
		this.setState(() => ( {
			mode: modeToSetTo,
		}));
	}



	/**
	 * Give the user UI feedback based on the error. This renders the notification in the App
	 *
	 * @memberof LoginLanding
	 */
	uiFeedbackDisplay(): void {
		this.dbgLog("LoginLanding: uiFeedbackDisplay");
		switch (this.state.uiFeedbackMessage) {
		// ---------------------------------------------------------------------
		// These are individual per error
		// TODO: Style these and customise the text within them to provide useful feedback to the user
		// based on what just happened.
		// I imagine these will remain as one liners and will all simply use one base class and some additional class
		// to add different colours (i.e. green on success, orange on user-error, red on server/arbitrary error)
		// ---------------------------------------------------------------------
		case "json_data_not_received":
			this.props.createNotification(modesNotifTypes.Error, null, "Server did not receive data", null, null);
			break;


		// Generic
		case "empty_field":
			this.props.createNotification(modesNotifTypes.Warning, null, "Empty field", null, null);
			break;


		// Register
		case "register_email_taken":
			this.props.createNotification(modesNotifTypes.Warning, null, "Email already taken", null, null);
			break;

		case "register_password_no_match":
			this.props.createNotification(modesNotifTypes.Warning, null, "Passwords do not match", null, null);
			break;

		case "register_username_taken":
			this.props.createNotification(modesNotifTypes.Warning, null, "Username already taken", null, null);
			break;

		case "register_issue":
			this.props.createNotification(modesNotifTypes.Error, null, "There was an arbitrary issue with registering", null, null);
			break;


		// Register -> login
		case "loginAfterRegister_success":
			this.props.createNotification(modesNotifTypes.Success, null, "Your account has been created and you have been logged in", null, null);
			break;

		case "loginAfterRegister_no_match":
			this.props.createNotification(modesNotifTypes.Error, null, "For some reason, we couldn't log you in after registering", null, null);
			break;

		case "loginAfterRegister_issue":
			this.props.createNotification(modesNotifTypes.Error, null, "For some reason, we couldn't log you in after registering", null, null);
			break;
		case "loginAfterRegister_account_disabled":
			this.props.createNotification(modesNotifTypes.Error, null, "For some reason, your account has been disabled after registering", null, null);
			break;


		// Login
		case "login_success":
			this.props.createNotification(modesNotifTypes.Success, null, "Login success", null, null);
			break;
		case "login_no_match":
			this.props.createNotification(modesNotifTypes.Warning, null, "Your login details do not match", null, null);
			break;
		case "login_issue":
			this.props.createNotification(modesNotifTypes.Error, null, "There was an arbitrary issue with logging in", null, null);
			break;
		case "login_account_disabled":
			this.props.createNotification(modesNotifTypes.Warning, null, "Your account seems to be disabled", null, null);
			break;


		// Login 2FA
		case "login_2fa_request_success":
			this.props.createNotification(modesNotifTypes.Success, "2FA Email Sent", "Please check your email for the 2FA token required to login", null, null);
			break;

		case "login_2fa_request_error":
			this.props.createNotification(modesNotifTypes.Error, "2FA Email Error", "There was an error with sending the 2FA token to your email", null, null);
			break;


		// Login 2FA Confirmation
		case "login_2fa_conf_success":
			this.props.createNotification(modesNotifTypes.Success, null, "Login Success", null, null);
			break;

		case "login_2fa_conf_expired_or_no_match":
			this.props.createNotification(modesNotifTypes.Warning, "2FA Verification Error", "You may have entered the wrong token, or your tokens have expired; please try logging in again", null, null);
			break;


		// Email reconfirmation
		case "emailconf_email_sent_if_exist":
			this.props.createNotification(modesNotifTypes.Success, null, "If your details were valid, a confirmation email has been sent to your account", null, null);
			break;
		case "emailconf_cant_create_token":
			this.props.createNotification(modesNotifTypes.Error, null, "We couldn't create a confirmation token for you", null, null);
			break;
		case "emailconf_cant_send_email":
			this.props.createNotification(modesNotifTypes.Error, null, "We couldn't send the email to you", null, null);
			break;


		// Password reset
		case "sendpasswordreset_email_sent_if_exist":
			this.props.createNotification(modesNotifTypes.Success, null, "If your details were valid, a password reset email has been sent to your account", null, null);
			break;
		case "sendpasswordreset_email_not_verified":
			this.props.createNotification(modesNotifTypes.Warning, null, "We tried to reset your password, but your email is not verified. Please verify your email first", null, null);
			break;
		case "sendpasswordreset_cant_create_token":
			this.props.createNotification(modesNotifTypes.Error, null, "We couldn't create a reset token for you", null, null);
			break;
		case "sendpasswordreset_cant_send_email":
			this.props.createNotification(modesNotifTypes.Error, null, "We couldn't send the email to you", null, null);
			break;


		// Reset password verification stage
		case "properpasswordreset_password_no_match":
			this.props.createNotification(modesNotifTypes.Warning, null, "Your passwords are not identical", null, null);
			break;
		case "properpasswordreset_expired_or_no_match":
			this.props.createNotification(modesNotifTypes.Warning, null, "Your reset token has expired", null, null);
			break;


		// Reset password verification stage -> login
		case "loginAfterResetPassword_success":
			this.props.createNotification(modesNotifTypes.Success, null, "Your password has been reset and you have been logged in", null, null);
			break;
		case "loginAfterResetPassword_no_match":
			this.props.createNotification(modesNotifTypes.Error, null, "For some reason, we couldn't log you in", null, null);
			break;
		case "loginAfterResetPassword_issue":
			this.props.createNotification(modesNotifTypes.Error, null, "For some reason, we couldn't log you in", null, null);
			break;
		case "loginAfterResetPassword_account_disabled":
			this.props.createNotification(modesNotifTypes.Warning, null, "Your account seems to be disabled", null, null);
			break;

		default:
		}
		this.setState({
			uiFeedbackMessage: "",
		});
	}



	/**
	 * Set the uiFeedBack message which will be rendered by uiFeedbackDisplay. This is mainly done to pass
	 * down to children so that they can all control this one thing instead of all having their own individual
	 * elements.
	 *
	 * @param {string} msg
	 * @memberof LoginLanding
	 */
	updateUiFeedback(msg: string): void {
		this.dbgLog("LoginLanding: updateUiFeedback");
		this.setState({
			uiFeedbackMessage: msg,
		});
	}



	/**
	 * This wipes the uiFeedbackMessage state. Trigger this, for example, on an X button on the error message, or
	 * upon re-submission of the form
	 *
	 * @memberof LoginLanding
	 */
	resetUiError(): void {
		this.dbgLog("LoginLanding: resetUiError");
		this.setState({
			uiFeedbackMessage: "",
		});
	}



	/**
	 * Tell the parent App to update the auth token to "logged_out", which will never be a valid JWT token,
	 * thus causing a state change and making any component that relies on the auth token to update.
	 *
	 * @note this is a temporary implementation
	 *
	 * @memberof LoginLanding
	 */
	logoutSubmit(): void {
		this.dbgLog("LoginLanding: logoutSubmit");
		this.setMode(modesLoginLanding.Login);
		this.props.updateAuthToken("logged_out");
	}



	/**
	 * Render the component based on whether or not
	 *
	 * @return {*}  {JSX.Element}
	 * @memberof LoginLanding
	 */
	componentBasedOnMode(): JSX.Element {
		this.dbgLog("LoginLanding: componentBasedOnMode");
		this.dbgLog("LoginLanding: componentBasedOnMode: " + this.props.authTokenIsValid);
		let component;

		// if the token is NOT valid, display the normal login stuff
		// NOTE: We rely on the props so that every component uses this one state set up in App
		// Whenever the authTokenIsValid is updated anywhere, if this component is mounted, this will
		// update
		if (this.props.authTokenIsValid !== "token_valid") {
			switch (this.state.mode) {
			case modesLoginLanding.Login:
				this.dbgLog("LoginLanding: componentBasedOnMode: rendering Login");
				component = <Login
					setMainMode={this.props.setMainMode}
					setMode={this.setMode}
					updateUiFeedback={this.updateUiFeedback}
					authTokenIsValid={this.props.authTokenIsValid}
					updateAuthToken={this.props.updateAuthToken}
					verifyAuthToken={this.props.verifyAuthToken}
				/>;
				break;
			case modesLoginLanding.Register:
				this.dbgLog("LoginLanding: componentBasedOnMode: rendering Register");
				component = <Register
					setMainMode={this.props.setMainMode}
					setMode={this.setMode}
					updateUiFeedback={this.updateUiFeedback}
					authTokenIsValid={this.props.authTokenIsValid}
					updateAuthToken={this.props.updateAuthToken}
					verifyAuthToken={this.props.verifyAuthToken}
				/>;
				break;
			case modesLoginLanding.ForgotPassword:
				this.dbgLog("LoginLanding: componentBasedOnMode: rendering ForgotPassword");
				component = <ForgotPassword
					setMainMode={this.props.setMainMode}
					setMode={this.setMode}
					updateUiFeedback={this.updateUiFeedback}
					authTokenIsValid={this.props.authTokenIsValid}
					updateAuthToken={this.props.updateAuthToken}
					verifyAuthToken={this.props.verifyAuthToken}
				/>;
				break;
			case modesLoginLanding.ReconfirmEmail:
				this.dbgLog("LoginLanding: componentBasedOnMode: rendering ReconfirmEmail");
				component = <ReconfirmEmail
					setMainMode={this.props.setMainMode}
					setMode={this.setMode}
					updateUiFeedback={this.updateUiFeedback}
					authTokenIsValid={this.props.authTokenIsValid}
					updateAuthToken={this.props.updateAuthToken}
					verifyAuthToken={this.props.verifyAuthToken}
				/>;
				break;
			default:
				this.dbgLog("LoginLanding: componentBasedOnMode: rendering somehow didn't match this.state.mode");
				component =
				<div>
					Error: mode state does not correspond to any elements
				</div>;
				break;
			}
		}
		// If the token IS valid, inform user they're already logged in
		else {
			this.dbgLog("LoginLanding: componentBasedOnMode: rendering already-logged-in state");
			component = <AlreadyLoggedIn
				setMainMode={this.props.setMainMode}
				setMode={this.setMode}
				updateUiFeedback={this.updateUiFeedback}
				authTokenIsValid={this.props.authTokenIsValid}
				updateAuthToken={this.props.updateAuthToken}
				verifyAuthToken={this.props.verifyAuthToken}
			/>;
		}
		return component;
	}



	/**
	 * Currently this just renders a login form which has no graphical feedback on login, but the responses when the PHP/SQL is running
	 * are correct
	 * The way onChange={(event) => this.setState({loginEmail: event.target.value})}
	 * works is basically "on any change of this element, set the state of loginEmail to be that of the value of this element".
	 *
	 * Basically, in the this.loginSubmit() function, we can check if the received response is "loginSuccess", and we can also receive
	 * additional data such as the user's username and a token, and save these as cookies. We could then load these cookies on every page
	 * and then we will be able to know if the user is logged in.
	 *
	 * See: https://www.taniarascia.com/full-stack-cookies-localstorage-react-express/
	 *
	 * @return {*}  {JSX.Element}
	 * @memberof LoginLanding
	 */
	render(): JSX.Element {
		// ---------------------------------------------------------------------
		// SECTION HTML: Handle the LoginLanding stuff
		// This essentially acts as a container for the Login/Register/ForgotPassword/ReconfirmEmail components
		// depending on which one is rendered by this.componentBasedOnMode()
		// The component returned by uiFeedBackDisplay
		// ---------------------------------------------------------------------
		return (
			<div>
				{/* This renders based on the "mode" set */}
				{this.componentBasedOnMode()}
			</div>
		);
		// ---------------------------------------------------------------------
		// !SECTION
		// ---------------------------------------------------------------------
	}
}

export default LoginLanding;
