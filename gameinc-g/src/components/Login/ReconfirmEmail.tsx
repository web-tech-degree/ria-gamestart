import React, {Fragment} from "react";
import {globals} from "../../logic/globals";





// -----------------------------------------------------------------------------
// Types
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Modes
import {modesLoginLanding} from "./modes";
import {modesAppExternal, modesAppInternal} from "../modes";
import Footer from "../Externals/Footer";
import Nav from "../Externals/Nav";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Components
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Libraries
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// Assets
// -----------------------------------------------------------------------------





type ReconfirmEmailState = {
	resendEmailConfirmation: string,
}

type ReconfirmEmailProps = {
	setMainMode: (modeToSetTo: modesAppInternal | modesAppExternal) => void,
	setMode: (modeToSetTo: modesLoginLanding) => void,
	updateUiFeedback: (msg: string) => void,
	authTokenIsValid: string,
	updateAuthToken: (val: string) => void,
	verifyAuthToken: () => void,
}





/**
 * This class is used for the simple reconfirm email page and request
 *
 * @class ReconfirmEmail
 * @extends {Component}
 */
class ReconfirmEmail extends React.Component<ReconfirmEmailProps, ReconfirmEmailState> {
	/**
	 * Creates an instance of ReconfirmEmail.
	 * @param {ReconfirmEmailProps} props
	 * @memberof ReconfirmEmail
	 */
	constructor(props: ReconfirmEmailProps) {
		super(props);
		this.state = {
			resendEmailConfirmation: "",

		};

		this.dbgLog();

		this.reconfirmEmailSubmit = this.reconfirmEmailSubmit.bind(this);
	}



	/**
	 * This checks if this component should be "debugged" via the process.env variables. If it's true,
	 * we bind console.log so that logging works normally, otherwise we make it not do anything.
	 *
	 * @param {*} [mode]
	 * @param {...any[]} optionalParams
	 * @memberof ReconfirmEmail
	 */
	dbgLog(
		// eslint-disable-next-line
		mode?: any,...optionalParams: any[]
	): void {
		if (globals.DEBUGGER_DBG_LOG === "true" && globals.DEBUGGER_DBG_ReconfirmEmail === "true") {
			this.dbgLog = console.log.bind(window.close);
		}
		else {
			this.dbgLog = (): void => {
				return;
			};
		}
	}



	/**
	 * This is the API request to resend an email to the user to confirm their email
	 *
	 * @param {FormEvent<HTMLFormElement>} event
	 * @memberof ReconfirmEmail
	 */
	reconfirmEmailSubmit(event: React.FormEvent<HTMLFormElement>): void {
		this.dbgLog("ReconfirmEmail: reconfirmEmailSubmit");
		event.preventDefault();
		fetch(process.env.REACT_APP_API_URL + "__api.php", {
			method: "POST",
			credentials: "omit",
			body: JSON.stringify({
				request_type: "user_resend_email_confirmation",
				user_email: this.state.resendEmailConfirmation,
			}),
		})
			.then((response) => response.json())
			.then((data) => {
				this.dbgLog("ReconfirmEmail: reconfirmEmailSubmit response");
				if (data.message === "emailconf_email_sent_if_exist") {
					// Update the authToken state of App component using the updateAuthToken that was passed down
					this.dbgLog("reconfirmEmailSubmit success");
				}
				this.props.updateUiFeedback(data.message);
			});
	}


	// 	<div>
	// 	<div className="login-stuff-container">
	// 		<h2>Resend Email Confirmation</h2>
	// 		<form className="bare-login-form" onSubmit={this.reconfirmEmailSubmit}>
	// 			<label htmlFor="email"><b>Email</b></label>
	// 			<input type="text" placeholder="Enter Email" name="email" required onChange={(event) => this.setState({resendEmailConfirmation: event.target.value})}/>

	// 			<button type="submit">Confirm</button>
	// 		</form>
	// 		<span className="temp-space-top">Suddenly remembered your <a onClick={() => this.props.setMode(modesLoginLanding.Login)} href="#">login?</a></span>
	// 		<span className="temp-space-top">Or would you rather <a onClick={() => this.props.setMode(modesLoginLanding.Register)} href="#">register?</a></span>
	// 	</div>
	// </div>


	/**
	 * This renders the reconfirm email menu, providing a simple input form.
	 *
	 * @return {*}  {JSX.Element}
	 * @memberof ReconfirmEmail
	 */
	render(): JSX.Element {
		// ---------------------------------------------------------------------
		// SECTION HTML: Resend email confirmation
		// ---------------------------------------------------------------------
		return (
			<Fragment>
				<Nav setMainMode={this.props.setMainMode}></Nav>
				<div className="d-md-flex h-md-100 align-items-center">

					{/* <!-- First Half --> */}

					<div className="banner col-md-4 p-0 h-md-100">
						<div className="text-white d-md-flex h-100 p-5 text-center justify-content-center">
							<div className="logoarea pt-5 pb-5">
							</div>
						</div>
					</div>

					{/* <!-- Second Half --> */}

					<div className="col-md-8 p-0 bg-white h-md-100 loginarea">
						<div className="d-md-flex align-items-center h-md-100 p-5 justify-content-center">
							<form onSubmit={this.reconfirmEmailSubmit}>
								<h5>Resend Email Confirmation</h5>
								<div className="form-group">
									<label htmlFor="email">Email</label>
									<input type="text" className="form-control" id="email" placeholder="Type Here..." required
										onChange={(event) => this.setState({resendEmailConfirmation: event.target.value})}/>
								</div>
								<div className="form-button just-content-center">
									<button type="submit" className="btn">Submit</button>
								</div>
								<div className="account">Already have an account
									<a className="text cursor-pointer" onClick={() => this.props.setMode(modesLoginLanding.Login)}> Sign In</a>
									<div className="row">
										<a className="text cursor-pointer" onClick={() => this.props.setMode(modesLoginLanding.ForgotPassword)}>Forgot your password?</a>
									</div>
									<div className="row">
										<a className="text cursor-pointer" onClick={() => this.props.setMode(modesLoginLanding.ForgotPassword)}>Want to resend your email verification?</a>
									</div>
								</div>
							</form>
						</div>
					</div>

				</div>
				<Footer setMainMode={this.props.setMainMode}></Footer>
			</Fragment>

		);
		// ---------------------------------------------------------------------
		// !SECTION
		// ---------------------------------------------------------------------
	}
}


export default ReconfirmEmail;
