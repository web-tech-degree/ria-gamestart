declare module "vanta/dist/vanta.birds.min" {
	// eslint-disable-next-line
	declare var vantaBirds: any;
	export default vantaBirds;
}
declare module "react-notifications" {
	// eslint-disable-next-line
	declare var NotificationManager: any;
	// eslint-disable-next-line
	declare var NotificationContainer: any;
	export {
		NotificationManager,
		NotificationContainer,
	};
}
