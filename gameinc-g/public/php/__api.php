<?php
// -----------------------------------------------------------------------------
// This file is for handling "internal" API requests
// AKA anything from the app
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// DEVELOPMENT
// header('Access-Control-Allow-Origin: *');
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// SECTION Dependencies
// -----------------------------------------------------------------------------
// DEVELOPMENT:
// require __DIR__ . "../../../vendor/autoload.php";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// PRODUCTION
require __DIR__ . "/vendor/autoload.php";
// -----------------------------------------------------------------------------

// PDO Database
require_once("_dbconnect.php");

// From namespace: \Misc
require_once("misc_extrafunc.php");
require_once("misc_imagecrop.php");

// From namespace: \Conf
require_once("conf_general.php");
require_once("conf_jwt.php");

// From namespace: \User
require_once("user_access.php");
require_once("user_email_change.php");
require_once("user_details.php");
require_once("user_tokenauth.php");
require_once("user_mailer.php");
require_once("user_source_fav.php");

// From namespace: \Feed
require_once("feed_parser.php");
require_once("feed_fetcher.php");
require_once("feed_manager.php");

// From namespace: \Post
require_once("post_details.php");
require_once("post_votes.php");
require_once("post_bookmarks.php");
// !SECTION
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// SECTION External Libraries
// Here we instantiate external classes/libraries

$lib_phpmailer = new \PHPMailer\PHPMailer\PHPMailer(true);
$lib_jwt = new \Firebase\JWT\JWT;
$lib_picofeed = new \PicoFeed\Reader\Reader;
// !SECTION
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// SECTION Internal Classes
// Here we instantiate internal classes

// From namespace:			\Misc
$c_ExtraFunc		= new	\Misc\ExtraFunc();
$c_ImageCrop		= new	\Misc\ImageCrop();

// From namespace:			\Conf
$c_JwtParams		= new	\Conf\JwtParams();
$c_GeneralConf		= new	\Conf\General();

// From namespace:			\User
$c_UserTokenAuth	= new	\User\UserTokenAuth($lib_jwt, $c_JwtParams);
$c_UserMailer		= new	\User\UserMailer($lib_phpmailer, $c_GeneralConf);
$c_UserAccess		= new	\User\UserAccess($dbconn, $c_UserTokenAuth, $c_UserMailer, $c_GeneralConf, $c_ExtraFunc);
$c_UserEmailChange	= new	\User\UserEmailChange($dbconn, $c_UserAccess, $c_GeneralConf);
$c_UserDetails		= new	\User\UserDetails($dbconn, $c_UserAccess, $c_UserEmailChange, $c_ImageCrop);
$c_UserSourceFav	= new	\User\SourceFav($dbconn, $c_UserTokenAuth);

// From namespace:			\Post
$c_PostVotes		= new	\Post\PostVotes($dbconn);
$c_PostBookmarks	= new	\Post\PostBookmarks($dbconn, $c_UserTokenAuth, $c_PostVotes, $c_ExtraFunc);
$c_PostDetails		= new	\Post\PostDetails($dbconn, $c_UserTokenAuth, $c_PostVotes, $c_PostBookmarks, $c_UserSourceFav, $c_ExtraFunc);

// From namespace:			\Feed
$c_FeedParser		= new	\Feed\FeedParser($dbconn, $lib_picofeed, $c_ExtraFunc);
$c_FeedManager		= new	\Feed\FeedManager($dbconn, $c_UserTokenAuth, $c_ExtraFunc);
$c_FeedFetcher		= new	\Feed\FeedFetcher($dbconn, $c_UserTokenAuth, $c_PostVotes, $c_PostBookmarks, $c_UserSourceFav, $lib_picofeed, $c_ExtraFunc);
// !SECTION
// -----------------------------------------------------------------------------





// By default, we'll be sending fetch requests with JSON.stringify
$in_data = json_decode(file_get_contents('php://input'));



// and here we check we actually received JSON data
if (!$in_data) {
	// However, we may also receive POST data by using
	// - const data = new FormData()
	// - data.append("json", someData);
	// in JS, so here we check for that
	if (isset($_POST["json"])) {
		$in_data = json_decode($_POST["json"]);
	} else {
		echo json_encode(
			array(
				"message" => "json_data_not_received",
			)
		);
		die();
	}
}


$req_res;


// -----------------------------------------------------------------------------
// SECTION UserAccess
// -----------------------------------------------------------------------------





// SECTION: user_login
if ($in_data->request_type === "user_login") {
	// Verify neither the user_email nor user_password are empty. Makes a simple check
	// as we do not care which field is empty, just if any is
	$in_data_not_empty = $c_ExtraFunc->verifyNotEmptySimple($in_data->user_email, $in_data->user_password);
	if ($in_data_not_empty === false) {
		echo json_encode(
			array(
				"message" => "empty_field"
			)
		);
		die();
	}

	$req_res = $c_UserAccess->login($in_data->user_email, $in_data->user_password, "login");
}
// !SECTION user_login

// SECTION: user_login_2fa_conf
if ($in_data->request_type === "user_login_2fa_conf") {
	// Verify neither the user_email nor user_password are empty. Makes a simple check
	// as we do not care which field is empty, just if any is
	$in_data_not_empty = $c_ExtraFunc->verifyNotEmptySimple(
		$in_data->user_email,
		$in_data->user_password,
		$in_data->client_token,
		$in_data->email_token
	);
	if ($in_data_not_empty === false) {
		echo json_encode(
			array(
				"message" => "empty_field"
			)
		);
		die();
	}

	$req_res = $c_UserAccess->login_2fa_confirm(
		$in_data->user_email,
		$in_data->user_password,
		$in_data->client_token,
		$in_data->email_token
	);
}
// !SECTION user_login_2fa_conf

// SECTION user_register
if ($in_data->request_type === "user_register") {
	// Verify none of the expected/required fields are empty. Makes a simple check
	// as we do not care which field is empty, just if any is
	$in_data_not_empty = $c_ExtraFunc->verifyNotEmptySimple(
		$in_data->user_name,
		$in_data->user_password,
		$in_data->user_password_conf,
		$in_data->user_email
	);
	if ($in_data_not_empty === false) {
		echo json_encode(
			array(
				"message" => "empty_field"
			)
		);
		die();
	}

	// Ensure both password fiels are the same
	if ($in_data->user_password !== $in_data->user_password_conf) {
		echo json_encode(
			array(
				"message" => "register_password_no_match"
			)
		);
		die();
	}
	$req_res = $c_UserAccess->register(
		$in_data->user_name,
		$in_data->user_email,
		$in_data->user_password,
		0
	);
}
// !SECTION user_register

// SECTION user_email_password_reset_request
if ($in_data->request_type === "user_email_password_reset_request") {
	$in_data_not_empty = $c_ExtraFunc->verifyNotEmptySimple($in_data->user_email);
	if ($in_data_not_empty === false) {
		echo json_encode(
			array(
				"message" => "empty_field"
			)
		);
		die();
	}

	$req_res = $c_UserAccess->send_password_reset($in_data->user_email);
}
// !SECTION user_email_password_reset_request

// SECTION password_reset_proper
if ($in_data->request_type === "password_reset_proper") {
	$in_data_not_empty = $c_ExtraFunc->verifyNotEmptySimple(
		$in_data->user_token,
		$in_data->user_email,
		$in_data->user_password,
		$in_data->user_password_conf
	);

	if ($in_data_not_empty === false) {
		echo json_encode(
			array(
				"message" => "empty_field"
			)
		);
		die();
	}

	// Ensure both password fiels are the same
	if ($in_data->user_password !== $in_data->user_password_conf) {
		echo json_encode(
			array(
				"message" => "properpasswordreset_password_no_match",
			)
		);
		die();
	}

	$req_res = $c_UserAccess->reset_password(
		$in_data->user_token,
		$in_data->user_email,
		$in_data->user_password
	);
}
// !SECTION password_reset_proper

// SECTION user_resend_email_confirmation
if ($in_data->request_type === "user_resend_email_confirmation") {
	$in_data_not_empty = $c_ExtraFunc->verifyNotEmptySimple($in_data->user_email);
	if ($in_data_not_empty === false) {
		echo json_encode(
			array(
				"message" => "email_empty"
			)
		);
		die();
	}

	$req_res = $c_UserAccess->send_email_verification($in_data->user_email);
}
// !SECTION user_resend_email_confirmation

// SECTION user_normal_change_password
if ($in_data->request_type === "user_normal_change_password") {
	$in_data_not_empty = $c_ExtraFunc->verifyNotEmptySimple(
		$in_data->user_auth_token,
		$in_data->current_password,
		$in_data->desired_password,
		$in_data->desired_password_conf,
	);
	if ($in_data_not_empty === false) {
		echo json_encode(
			array(
				"message" => "email_empty"
			)
		);
		die();
	}

	$req_res = $c_UserAccess->user_normal_change_password(
		$in_data->user_auth_token,
		$in_data->current_password,
		$in_data->desired_password,
		$in_data->desired_password_conf,
	);
}
// !SECTION user_normal_change_password





// -----------------------------------------------------------------------------
// !SECTION UserAccess
// -----------------------------------------------------------------------------





// -----------------------------------------------------------------------------
// SECTION UserDetails
// -----------------------------------------------------------------------------





// SECTION fetch_user_pfp_only
if ($in_data->request_type === "fetch_user_pfp_only") {
	$in_data_not_empty = $c_ExtraFunc->verifyNotEmptySimple($in_data->user_auth_token);
	if ($in_data_not_empty === false) {
		echo json_encode(
			array(
				"message" => "token_empty"
			)
		);
		die();
	}

	// This is probably a mess, but basically the email changer needs c_user_access, and UserDetails
	// also needs c_user_access
	
	$req_res = $c_UserDetails->get_user_pfp($in_data->user_auth_token);
}
// !SECTION fetch_user_pfp_only



// SECTION profile_fetch_user_details
if ($in_data->request_type === "profile_fetch_user_details") {
	$in_data_not_empty = $c_ExtraFunc->verifyNotEmptySimple($in_data->user_auth_token);
	if ($in_data_not_empty === false) {
		echo json_encode(
			array(
				"message" => "token_empty"
			)
		);
		die();
	}

	$req_res = $c_UserDetails->get_user_profile_details($in_data->user_auth_token);
}
// !SECTION profile_fetch_user_details

// SECTION profile_update_user_details
if ($in_data->request_type === "profile_update_user_details") {
	$in_data_not_empty = $c_ExtraFunc->verifyNotEmptySimple(
		$in_data->user_auth_token,
		$in_data->username,
		$in_data->email
	);
	if ($in_data_not_empty === false) {
		echo json_encode(
			array(
				"message" => "missing_fields"
			)
		);
		die();
	}

	$req_res = $c_UserDetails->profile_update_user_details(
		$in_data->user_auth_token,
		$in_data->username,
		$in_data->email
	);
}
// !SECTION profile_update_user_details

// SECTION profile_update_user_details_and_pfp
if ($in_data->request_type === "profile_update_user_details_and_pfp") {
	$in_data_not_empty = $c_ExtraFunc->verifyNotEmptySimple(
		$in_data->user_auth_token,
		$in_data->username,
		$in_data->email,
	);
	if ($in_data_not_empty === false) {
		echo json_encode(
			array(
				"message" => "missing_fields"
			)
		);
		die();
	}
	// Here we check for the uploaded image
	if (!isset($_FILES["pfp_image"]["tmp_name"]) || !isset($_FILES["pfp_image"]["name"])) {
		echo json_encode(
			array(
				"message" => "missing_image_field"
			)
		);
		die();
	}

	$req_res = $c_UserDetails->profile_update_user_details(
		$in_data->user_auth_token,
		$in_data->username,
		$in_data->email,
		$_FILES["pfp_image"]
	);
}
// !SECTION profile_update_user_details_and_pfp





// -----------------------------------------------------------------------------
// !SECTION UserDetails
// -----------------------------------------------------------------------------





// -----------------------------------------------------------------------------
// SECTION UserTokenAuth
// -----------------------------------------------------------------------------

// SECTION user_authenticate
if ($in_data->request_type === "user_authenticate") {
	$in_data_not_empty = $c_ExtraFunc->verifyNotEmptySimple($in_data->user_auth_token);
	if ($in_data_not_empty === false) {
		echo json_encode(
			array(
				"message" => "token_empty"
			)
		);
		die();
	}

	$req_res = $c_UserTokenAuth->check_user_token($in_data->user_auth_token);
}
// !SECTION user_authenticate

// -----------------------------------------------------------------------------
// !SECTION UserTokenAuth
// -----------------------------------------------------------------------------





// -----------------------------------------------------------------------------
// SECTION UserSourceFav
// -----------------------------------------------------------------------------

// SECTION source_toggle_favourite
if ($in_data->request_type === "source_toggle_favourite") {
	$in_data_not_empty = $c_ExtraFunc->verifyNotEmptySimple(
		$in_data->user_auth_token,
		$in_data->source_id,
		$in_data->favourite_status_desired,
	);
	if ($in_data_not_empty === false) {
		echo json_encode(
			array(
				"message" => "missing_fields",
			)
		);
		die();
	}

	$req_res = $c_UserSourceFav->source_toggle_favourite(
		$in_data->user_auth_token,
		$in_data->source_id,
		$in_data->favourite_status_desired
	);
}
// !SECTION source_toggle_favourite

// -----------------------------------------------------------------------------
// !SECTION UserSourceFav
// -----------------------------------------------------------------------------





// -----------------------------------------------------------------------------
// SECTION FeedFetcher
// -----------------------------------------------------------------------------

// SECTION feed_get_available
if ($in_data->request_type === "feed_get_available") {
	$req_res = $c_FeedFetcher->feed_get_available();
}
// !SECTION feed_get_available

// SECTION fetch_user_favourite_sources
if ($in_data->request_type === "fetch_user_favourite_sources") {
	$in_data_not_empty = $c_ExtraFunc->verifyNotEmptySimple($in_data->user_auth_token);
	if ($in_data_not_empty === false) {
		echo json_encode(
			array(
				"message" => "fetch_posts_missing_field"
			)
		);
		die();
	}

	$req_res = $c_FeedFetcher->fetch_user_favourite_sources($in_data->user_auth_token);
}
// !SECTION fetch_user_favourite_sources

// SECTION feed_get_source_info
if ($in_data->request_type === "feed_get_source_info") {
	$in_data_not_empty = $c_ExtraFunc->verifyNotEmptySimple($in_data->active_source_id);
	if ($in_data_not_empty === false) {
		echo json_encode(
			array(
				"message" => "fetch_missing_fields"
			)
		);
		die();
	}

	$req_res = $c_FeedFetcher->feed_get_source_info($in_data->active_source_id);
}
// !SECTION feed_get_source_info

// SECTION check_new_posts_available_by_feed
if ($in_data->request_type === "check_new_posts_available_by_feed") {
	$in_data_not_empty = $c_ExtraFunc->verifyNotEmptySimple($in_data->active_feed_id, $in_data->highest_current);
	if ($in_data_not_empty === false) {
		echo json_encode(
			array(
				"message" => "fetch_posts_missing_field"
			)
		);
		die();
	}

	$req_res = $c_FeedFetcher->check_new_posts_available_by_feed($in_data->active_feed_id, $in_data->highest_current);
}
// !SECTION check_new_posts_available_by_feed

// SECTION check_new_posts_available_by_source
if ($in_data->request_type === "check_new_posts_available_by_source") {
	$in_data_not_empty = $c_ExtraFunc->verifyNotEmptySimple($in_data->active_source_id, $in_data->highest_current);
	if ($in_data_not_empty === false) {
		echo json_encode(
			array(
				"message" => "fetch_posts_missing_field"
			)
		);
		die();
	}

	$req_res = $c_FeedFetcher->check_new_posts_available_by_source($in_data->active_source_id, $in_data->highest_current);
}
// !SECTION check_new_posts_available_by_source

// SECTION get_latest_posts_by_feed
if ($in_data->request_type === "get_latest_posts_by_feed") {
	$in_data_not_empty = $c_ExtraFunc->verifyNotEmptySimple($in_data->user_auth_token, $in_data->active_feed_id, $in_data->highest_current);
	if ($in_data_not_empty === false) {
		echo json_encode(
			array(
				"message" => "fetch_posts_missing_field"
			)
		);
		die();
	}

	$req_res = $c_FeedFetcher->get_latest_posts_by_feed($in_data->user_auth_token, $in_data->active_feed_id, $in_data->highest_current);
}
// !SECTION get_latest_posts_by_feed

// SECTION get_latest_posts_by_source
if ($in_data->request_type === "get_latest_posts_by_source") {
	$in_data_not_empty = $c_ExtraFunc->verifyNotEmptySimple($in_data->user_auth_token, $in_data->active_source_id, $in_data->highest_current);
	if ($in_data_not_empty === false) {
		echo json_encode(
			array(
				"message" => "fetch_posts_missing_field"
			)
		);
		die();
	}

	$req_res = $c_FeedFetcher->get_latest_posts_by_source($in_data->user_auth_token, $in_data->active_source_id, $in_data->highest_current);
}
// !SECTION get_latest_posts_by_feed

// SECTION get_latest_posts_in_feed_by_search
if ($in_data->request_type === "get_latest_posts_in_feed_by_search") {
	$in_data_not_empty = $c_ExtraFunc->verifyNotEmptySimple(
		$in_data->user_auth_token,
		$in_data->active_feed_id,
		$in_data->search_query
	);
	if ($in_data_not_empty === false) {
		echo json_encode(
			array(
				"message" => "fetch_posts_missing_field"
			)
		);
		die();
	}

	$req_res = $c_FeedFetcher->get_latest_posts_in_feed_by_search(
		$in_data->user_auth_token,
		$in_data->active_feed_id,
		$in_data->search_query
	);
}
// !SECTION get_latest_posts_in_feed_by_search

// SECTION get_previous_posts_by_feed
if ($in_data->request_type === "get_previous_posts_by_feed") {
	$in_data_not_empty = $c_ExtraFunc->verifyNotEmptySimple($in_data->user_auth_token, $in_data->active_feed_id, $in_data->lowest_current);
	if ($in_data_not_empty === false) {
		echo json_encode(
			array(
				"message" => "fetch_posts_missing_field"
			)
		);
		die();
	}

	$req_res = $c_FeedFetcher->get_previous_posts_by_feed($in_data->user_auth_token, $in_data->active_feed_id, $in_data->lowest_current);
}
// !SECTION get_previous_posts_by_feed

// SECTION get_previous_posts_by_source
if ($in_data->request_type === "get_previous_posts_by_source") {
	$in_data_not_empty = $c_ExtraFunc->verifyNotEmptySimple($in_data->user_auth_token, $in_data->active_source_id, $in_data->lowest_current);
	if ($in_data_not_empty === false) {
		echo json_encode(
			array(
				"message" => "fetch_posts_missing_field"
			)
		);
		die();
	}

	$req_res = $c_FeedFetcher->get_previous_posts_by_source($in_data->user_auth_token, $in_data->active_source_id, $in_data->lowest_current);
}
// !SECTION get_previous_posts_by_source

// SECTION get_previous_posts_in_feed_by_search
if ($in_data->request_type === "get_previous_posts_in_feed_by_search") {
	$in_data_not_empty = $c_ExtraFunc->verifyNotEmptySimple(
		$in_data->user_auth_token,
		$in_data->active_feed_id,
		$in_data->search_query,
		$in_data->lowest_current
	);
	if ($in_data_not_empty === false) {
		echo json_encode(
			array(
				"message" => "fetch_posts_missing_field"
			)
		);
		die();
	}

	$req_res = $c_FeedFetcher->get_previous_posts_in_feed_by_search(
		$in_data->user_auth_token,
		$in_data->active_feed_id,
		$in_data->search_query,
		$in_data->lowest_current
	);
}
// !SECTION get_previous_posts_in_feed_by_search

// SECTION refetch_post_within_feed
if ($in_data->request_type === "refetch_post_within_feed") {
	$in_data_not_empty = $c_ExtraFunc->verifyNotEmptySimple($in_data->user_auth_token, $in_data->post_id);
	if ($in_data_not_empty === false) {
		echo json_encode(
			array(
				"message" => "fetch_posts_missing_field"
			)
		);
		die();
	}

	$req_res = $c_FeedFetcher->refetch_post_within_feed($in_data->user_auth_token, $in_data->post_id);
}
// !SECTION refetch_post_within_feed



// -----------------------------------------------------------------------------
// !SECTION FeedFetcher
// -----------------------------------------------------------------------------





// -----------------------------------------------------------------------------
// SECTION PostDetails
// -----------------------------------------------------------------------------





// SECTION get_post_item_details
if ($in_data->request_type === "get_post_item_details") {
	$in_data_not_empty = $c_ExtraFunc->verifyNotEmptySimple($in_data->user_auth_token, $in_data->post_id);
	if ($in_data_not_empty === false) {
		echo json_encode(
			array(
				"message" => "fetch_posts_missing_field"
			)
		);
		die();
	}

	$req_res = $c_PostDetails->get_post_details($in_data->user_auth_token, $in_data->post_id);
}
// !SECTION get_post_item_details

// SECTION get_post_item_comments
if ($in_data->request_type === "get_post_item_comments") {
	$in_data_not_empty = $c_ExtraFunc->verifyNotEmptySimple($in_data->post_id);
	if ($in_data_not_empty === false) {
		echo json_encode(
			array(
				"message" => "fetch_posts_missing_field"
			)
		);
		die();
	}

	$req_res = $c_PostDetails->get_post_comments($in_data->post_id);
}
// !SECTION get_post_item_comments

// SECTION post_submit_comment
if ($in_data->request_type === "post_submit_comment") {
	$in_data_not_empty = $c_ExtraFunc->verifyNotEmptySimple(
		$in_data->jwt_token,
		$in_data->postitem_id,
		$in_data->comment_text,
		$in_data->parent_postcomment_id
	);
	if ($in_data_not_empty === false) {
		echo json_encode(
			array(
				"message" => "fetch_posts_missing_field"
			)
		);
		die();
	}

	$req_res = $c_PostDetails->submit_comment(
		$in_data->jwt_token,
		$in_data->postitem_id,
		$in_data->comment_text,
		$in_data->parent_postcomment_id
	);
}
// !SECTION post_submit_comment

// SECTION post_edit_comment
if ($in_data->request_type === "post_edit_comment") {
	$in_data_not_empty = $c_ExtraFunc->verifyNotEmptySimple(
		$in_data->jwt_token,
		$in_data->postcomment_id,
		$in_data->comment_text
	);
	if ($in_data_not_empty === false) {
		echo json_encode(
			array(
				"message" => "fetch_posts_missing_field"
			)
		);
		die();
	}

	$req_res = $c_PostDetails->edit_comment(
		$in_data->jwt_token,
		$in_data->postcomment_id,
		$in_data->comment_text,
	);
}
// !SECTION post_edit_comment

// SECTION post_edit_comment
if ($in_data->request_type === "post_delete_comment") {
	$in_data_not_empty = $c_ExtraFunc->verifyNotEmptySimple(
		$in_data->jwt_token,
		$in_data->postcomment_id
	);
	if ($in_data_not_empty === false) {
		echo json_encode(
			array(
				"message" => "fetch_posts_missing_field"
			)
		);
		die();
	}

	$req_res = $c_PostDetails->delete_comment(
		$in_data->jwt_token,
		$in_data->postcomment_id
	);
}
// !SECTION post_delete_comment

// SECTION post_toggle_vote
if ($in_data->request_type === "post_toggle_vote") {
	$in_data_not_empty = $c_ExtraFunc->verifyNotEmptySimple(
		$in_data->user_auth_token,
		$in_data->post_id,
		$in_data->vote_status_current,
		$in_data->vote_desired,
	);
	if ($in_data_not_empty === false) {
		echo json_encode(
			array(
				"message" => "missing_fields"
			)
		);
		die();
	}

	$req_res = $c_PostDetails->post_toggle_vote(
		$in_data->user_auth_token,
		$in_data->post_id,
		$in_data->vote_status_current,
		$in_data->vote_desired
	);
}
// !SECTION post_toggle_vote





// -----------------------------------------------------------------------------
// !SECTION PostDetails
// -----------------------------------------------------------------------------



// -----------------------------------------------------------------------------
// SECTION PostBookmarks
// -----------------------------------------------------------------------------



// SECTION post_toggle_bookmark
if ($in_data->request_type === "post_toggle_bookmark") {
	$in_data_not_empty = $c_ExtraFunc->verifyNotEmptySimple(
		$in_data->user_auth_token,
		$in_data->post_id,
		$in_data->bookmark_status_desired,
	);
	if ($in_data_not_empty === false) {
		echo json_encode(
			array(
				"message" => "missing_fields",
			)
		);
		die();
	}

	$req_res = $c_PostBookmarks->post_toggle_bookmark(
		$in_data->user_auth_token,
		$in_data->post_id,
		$in_data->bookmark_status_desired
	);
}
// !SECTION post_toggle_bookmark

// SECTION get_user_post_bookmark_ids
if ($in_data->request_type === "get_user_post_bookmark_ids") {
	$in_data_not_empty = $c_ExtraFunc->verifyNotEmptySimple($in_data->user_auth_token);
	if ($in_data_not_empty === false) {
		echo json_encode(
			array(
				"message" => "missing_fields",
			)
		);
		die();
	}

	$req_res = $c_PostBookmarks->get_user_post_bookmark_ids($in_data->user_auth_token);
}
// !SECTION get_user_post_bookmark_ids

// SECTION fetch_posts_in_user_bookmarks
if ($in_data->request_type === "fetch_posts_in_user_bookmarks") {
	$in_data_not_empty = $c_ExtraFunc->verifyNotEmptySimple($in_data->user_auth_token);
	if ($in_data_not_empty === false) {
		echo json_encode(
			array(
				"message" => "missing_fields",
			)
		);
		die();
	}

	// NOTE: We can't check $in_data->post_id_array above because it's an array
	if (!isset($in_data->post_id_array)) {
		echo json_encode(
			array(
				"message" => "missing_fields",
			)
		);
	}

	$req_res = $c_PostBookmarks->fetch_posts_in_user_bookmarks($in_data->user_auth_token, $in_data->post_id_array);
}
// !SECTION fetch_posts_in_user_bookmarks



// -----------------------------------------------------------------------------
// !SECTION PostBookmarks
// -----------------------------------------------------------------------------



// -----------------------------------------------------------------------------
// SECTION FeedManager
// -----------------------------------------------------------------------------





// -----------------------------------------------------------------------------
// SECTION FeedEditor
// -----------------------------------------------------------------------------



// SECTION feed_get_all
if ($in_data->request_type === "feed_get_all") {
	$in_data_not_empty = $c_ExtraFunc->verifyNotEmptySimple($in_data->user_auth_token);
	if ($in_data_not_empty === false) {
		echo json_encode(
			array(
				"message" => "missing_field"
			)
		);
		die();
	}

	$req_res = $c_FeedManager->feed_get_all($in_data->user_auth_token);
}
// !SECTION feed_get_all

// SECTION feed_get_linked_sources
if ($in_data->request_type === "feed_get_linked_sources") {
	$in_data_not_empty = $c_ExtraFunc->verifyNotEmptySimple($in_data->user_auth_token, $in_data->selected_feed_id);
	if ($in_data_not_empty === false) {
		echo json_encode(
			array(
				"message" => "missing_field"
			)
		);
		die();
	}

	$req_res = $c_FeedManager->feed_get_linked_sources($in_data->user_auth_token, $in_data->selected_feed_id);
}
// !SECTION feed_get_linked_sources

// SECTION feed_unlink_source
if ($in_data->request_type === "feed_unlink_source") {
	$in_data_not_empty = $c_ExtraFunc->verifyNotEmptySimple($in_data->user_auth_token, $in_data->feed, $in_data->source_to_unlink);
	if ($in_data_not_empty === false) {
		echo json_encode(
			array(
				"message" => "missing_field"
			)
		);
		die();
	}

	$req_res = $c_FeedManager->feed_unlink_source($in_data->user_auth_token, $in_data->feed, $in_data->source_to_unlink);
}
// !SECTION feed_unlink_source

// SECTION feed_get_linkable_sources
if ($in_data->request_type === "feed_get_linkable_sources") {
	$in_data_not_empty = $c_ExtraFunc->verifyNotEmptySimple($in_data->user_auth_token, $in_data->feed);
	if ($in_data_not_empty === false) {
		echo json_encode(
			array(
				"message" => "missing_field"
			)
		);
		die();
	}

	$req_res = $c_FeedManager->feed_get_linkable_sources($in_data->user_auth_token, $in_data->feed);
}
// !SECTION feed_get_linkable_sources

// SECTION feed_link_source
if ($in_data->request_type === "feed_link_source") {
	$in_data_not_empty = $c_ExtraFunc->verifyNotEmptySimple($in_data->user_auth_token, $in_data->feed, $in_data->source_to_link);
	if ($in_data_not_empty === false) {
		echo json_encode(
			array(
				"message" => "missing_field"
			)
		);
		die();
	}

	$req_res = $c_FeedManager->feed_link_source($in_data->user_auth_token, $in_data->feed, $in_data->source_to_link);
}
// !SECTION feed_link_source

// SECTION feed_create_new
if ($in_data->request_type === "feed_create_new") {
	$in_data_not_empty = $c_ExtraFunc->verifyNotEmptySimple($in_data->user_auth_token, $in_data->feed);
	if ($in_data_not_empty === false) {
		echo json_encode(
			array(
				"message" => "missing_field"
			)
		);
		die();
	}

	$req_res = $c_FeedManager->feed_create_new($in_data->user_auth_token, $in_data->feed);
}
// !SECTION feed_create_new

// SECTION feed_unlink_all_and_remove_feed
if ($in_data->request_type === "feed_unlink_all_and_remove_feed") {
	$in_data_not_empty = $c_ExtraFunc->verifyNotEmptySimple($in_data->user_auth_token, $in_data->feed);
	if ($in_data_not_empty === false) {
		echo json_encode(
			array(
				"message" => "missing_field"
			)
		);
		die();
	}

	$req_res = $c_FeedManager->feed_unlink_all_and_remove_feed($in_data->user_auth_token, $in_data->feed);
}
// !SECTION feed_unlink_all_and_remove_feed

// SECTION feed_create_new
if ($in_data->request_type === "feed_update_name") {
	$in_data_not_empty = $c_ExtraFunc->verifyNotEmptySimple($in_data->user_auth_token, $in_data->selected_feed_id, $in_data->new_name);
	if ($in_data_not_empty === false) {
		echo json_encode(
			array(
				"message" => "missing_field"
			)
		);
		die();
	}

	$req_res = $c_FeedManager->feed_update_name($in_data->user_auth_token, $in_data->selected_feed_id, $in_data->new_name);
}
// !SECTION feed_create_new



// -----------------------------------------------------------------------------
// !SECTION FeedEditor
// -----------------------------------------------------------------------------





// -----------------------------------------------------------------------------
// SECTION SourceEditor
// -----------------------------------------------------------------------------





// SECTION source_get_all
if ($in_data->request_type === "source_get_all") {
	$in_data_not_empty = $c_ExtraFunc->verifyNotEmptySimple($in_data->user_auth_token);
	if ($in_data_not_empty === false) {
		echo json_encode(
			array(
				"message" => "missing_field"
			)
		);
		die();
	}

	$req_res = $c_FeedManager->source_get_all($in_data->user_auth_token);
}
// !SECTION source_get_all

// SECTION source_get_details
if ($in_data->request_type === "source_get_details") {
	$in_data_not_empty = $c_ExtraFunc->verifyNotEmptySimple($in_data->user_auth_token, $in_data->source);
	if ($in_data_not_empty === false) {
		echo json_encode(
			array(
				"message" => "missing_field"
			)
		);
		die();
	}

	$req_res = $c_FeedManager->source_get_details($in_data->user_auth_token, $in_data->source);
}
// !SECTION source_get_details

// SECTION source_create_new
if ($in_data->request_type === "source_create_new") {
	$in_data_not_empty = $c_ExtraFunc->verifyNotEmptySimple($in_data->user_auth_token, $in_data->source_url, $in_data->source_name);
	if ($in_data_not_empty === false) {
		echo json_encode(
			array(
				"message" => "missing_field"
			)
		);
		die();
	}

	$req_res = $c_FeedManager->source_create_new($in_data->user_auth_token, $in_data->source_url, $in_data->source_name);
}
// !SECTION source_create_new

// SECTION source_remove_and_purge_posts
if ($in_data->request_type === "source_remove_and_purge_posts") {
	$in_data_not_empty = $c_ExtraFunc->verifyNotEmptySimple($in_data->user_auth_token, $in_data->source);
	if ($in_data_not_empty === false) {
		echo json_encode(
			array(
				"message" => "missing_field"
			)
		);
		die();
	}

	$req_res = $c_FeedManager->source_remove_and_purge_posts($in_data->user_auth_token, $in_data->source);
}
// !SECTION source_remove_and_purge_posts

// SECTION source_create_new
if ($in_data->request_type === "source_update_name") {
	$in_data_not_empty = $c_ExtraFunc->verifyNotEmptySimple($in_data->user_auth_token, $in_data->selected_source_id, $in_data->new_name);
	if ($in_data_not_empty === false) {
		echo json_encode(
			array(
				"message" => "missing_field"
			)
		);
		die();
	}

	$req_res = $c_FeedManager->source_update_name($in_data->user_auth_token, $in_data->selected_source_id, $in_data->new_name);
}
// !SECTION source_create_new



// -----------------------------------------------------------------------------
// !SECTION SourceEditor
// -----------------------------------------------------------------------------





// -----------------------------------------------------------------------------
// !SECTION FeedManager
// -----------------------------------------------------------------------------





// SECTION test_rss_parse_all_client_request
if ($in_data->request_type === "test_rss_parse_all_client_request") {
	$req_res = $c_FeedParser->parse_all_feeds();
	die();
}
// !SECTION test_rss_parse_all_client_request





// -----------------------------------------------------------------------------
// SECTION RETURN JSON
// -----------------------------------------------------------------------------

echo json_encode($req_res, JSON_NUMERIC_CHECK);
die();

// -----------------------------------------------------------------------------
// !SECTION
// -----------------------------------------------------------------------------
