<?php

namespace Post;

/**
 * @brief This is used for fetching post details, getting comments, and submitting comments
 *
 */
class PostDetails
{
	// -------------------------------------------------------------------------
	// SECTION Dependencies
	// -------------------------------------------------------------------------
	/**
	 * dbconn private variable, so that only the class itself can access it, used to store the
	 * connection to the database
	 *
	 * @var mixed
	 */
	// private \PDO $dbconn;
	// private \User\UserTokenAuth $UserTokenAuth;
	// private \Post\PostVotes $PostVotes;
	// private \Post\PostBookmarks $PostBookmarks;
	// private \User\SourceFav $UserSourceFav;
	// private \Misc\ExtraFunc $extrafunc;
	private $dbconn;
	private $UserTokenAuth;
	private $PostVotes;
	private $PostBookmarks;
	private $UserSourceFav;
	private $extrafunc;

	// -------------------------------------------------------------------------
	// !SECTION
	// -------------------------------------------------------------------------


	/**
	 * __construct triggered when constructing, does dependency injection
	 *
	 * These are then bound to the class
	 *
	 * Requirements:
	 * - dbconn:			instance of dbconn from _dbconnect.php
	 * - UserTokenAuth:		instance of \User\UserTokenAuth for authenticating the user's token
	 * - PostVotes:			instance of \Post\PostVotes for getting post vote info and casting votes
	 * - PostBookmarks:		instance of \Post\PostBookmarks for getting post bookmark info etc.
	 * - UserSourceFav:		instance of \User\SourceFav for getting source favourite info etc.
	 * - extrafunc:			instance of \Misc\ExtraFunc from _functions.php
	 *
	 * @param  \PDO $dbconn
	 * @param  \User\UserTokenAuth $UserTokenAuth
	 * @param  \Post\PostVotes $PostVotes
	 * @param  \Post\PostBookmarks $PostBookmarks
	 * @param  \User\SourceFav $UserSourceFav;
	 * @param  \Misc\ExtraFunc $extrafunc
	 * @return void
	 */
	public function __construct(
		\PDO $dbconn,
		\User\UserTokenAuth $UserTokenAuth,
		\Post\PostVotes $PostVotes,
		\Post\PostBookmarks $PostBookmarks,
		\User\SourceFav $UserSourceFav,
		\Misc\ExtraFunc $extrafunc
	) {
		$this->dbconn = $dbconn;
		$this->UserTokenAuth = $UserTokenAuth;
		$this->PostVotes = $PostVotes;
		$this->PostBookmarks = $PostBookmarks;
		$this->UserSourceFav = $UserSourceFav;
		$this->extrafunc = $extrafunc;
	}



	/**
	 * Get all relevant details for the specified post
	 *
	 * @param  string $p_auth_token
	 * @param  int $p_post_id
	 * @return void
	 */
	public function get_post_details($p_auth_token, $p_post_id)
	{
		// Get details of the post
		$details_fetch = $this->dbconn->prepare('SELECT
			postitem.postitem_id,
			postitem.hash,
			postitem.title,
			postitem.url,
			postitem.content,
			postitem.date,
			postitem.author,
			source.source_id,
			source.url as source_url,
			source.name as source_name,
			source.avatar as source_avatar
		FROM postitem
			INNER JOIN source_postitem
			ON postitem.postitem_id = source_postitem.postitem_id
			INNER JOIN feed_source
			ON source_postitem.source_id = feed_source.source_id
			INNER JOIN source
			ON source_postitem.source_id = source.source_id
		WHERE postitem.postitem_id = ?');
		$details_fetch->execute([$p_post_id]);
		$details_fetch_result = $details_fetch->fetch(\PDO::FETCH_ASSOC);

		// If we didn't get any result, tell client
		if (!$details_fetch_result) {
			return json_encode(
				array(
					"message" => "fetch_error",
				)
			);
		}

		// Here we get tet the total number of votes for the individual post
		$details_fetch_result["total_votes"] = $this->PostVotes->get_vote_totals_for_post($details_fetch_result["postitem_id"]);

		// We set the default status here in case the user is not signed in
		$details_fetch_result["user_vote_status"] = 0;
		$details_fetch_result["user_bookmark_status"] = 0;

		// We try to get the user token, so we'll know if they're signed in.
		$token_res = $this->UserTokenAuth->check_user_token($p_auth_token);
		
		// If the user is signed in, we also get their vote/bookmark status
		if ($token_res["message"] === "token_valid") {
			$details_fetch_result["user_vote_status"] = $this->PostVotes->get_user_post_vote_status($details_fetch_result["postitem_id"], $token_res["data"]->id);
			$details_fetch_result["user_bookmark_status"] = $this->PostBookmarks->get_user_post_bookmark_status($details_fetch_result["postitem_id"], $token_res["data"]->id);
			$details_fetch_result["user_fav_source_status"] = $this->UserSourceFav->get_user_source_fav_status($details_fetch_result["source_id"], $token_res["data"]->id);
		}

		return array(
			"message" => "fetch_success",
			"details" => $details_fetch_result,
		);
	}



	/**
	 * This gets all comments in a certain post, linking all of the parents together
	 * based on the parent_postcomment_id. If parent_postcomment_id is null, it is considered
	 * a root comment.
	 *
	 * Code mostly from https://gist.github.com/rmasters/1714891
	 *
	 * @param  int $p_post_id
	 * @return void
	 */
	public function get_post_comments($p_post_id)
	{
		// Here we fetch all the comments on the specified post, and we attempt to
		// get user avatar info for each.
		// Note that the use of LEFT JOIN here allows us to receive comments
		// even if they do not have a corresponding user_id in user
		// Note: LTRIM is used here so that we don't number-encode text when returning
		$comment_fetch = $this->dbconn->prepare("SELECT
			postcomment.postcomment_id,
			postcomment.user_id,
			postcomment.postitem_id,
			postcomment.text,
			postcomment.parent_postcomment_id,
			postcomment.comment_posted_at,
			postcomment.comment_updated_at,
			user.username,
			user.avatar
		FROM postcomment
			LEFT JOIN user
			ON postcomment.user_id = user.user_id
		WHERE postitem_id = ?");
		// $comment_fetch->execute([$p_post_id]);
		$comment_fetch->execute([$p_post_id]);
		$comment_fetch_result = $comment_fetch->fetchAll(\PDO::FETCH_ASSOC);
		$comment_fetch->closeCursor();
	
		foreach ($comment_fetch_result as $key => $field) {
			$x = $comment_fetch_result[$key]["text"];
			// -------------------------------------------------------------------------
			// NOTE: Because JSON and PHP are dumb as ****, I couldn't get numbers to be encoded
			// as strings... because, you know, sometimes a user might just comment with numbers
			// -------------------------------------------------------------------------
			$comment_fetch_result[$key]["text"] = "ihatejson_$x";
		}
		// Convert flat posts array to threaded dictionary
	
		// Recursively find a parent in the thread, return a reference to it
		function &find_parent(&$comment_fetch_result, $id)
		{
			$ref = null;
	
			foreach ($comment_fetch_result as $i => $p) {
				if ($i == $id) {
					$ref =& $comment_fetch_result[$i];
					break;
				}
	
				if (isset($p["replies"])) {
					$in_replies =& find_parent($comment_fetch_result[$i]["replies"], $id);
					if ($in_replies) {
						$ref =& $in_replies;
						break;
					}
				}
			}
	
			return $ref;
		}
	
		$thread = array();
		// Run until all posts have been threaded (maybe infinite if there's a bug :)
		while (count($comment_fetch_result) > 0) {
			// Foreach post
			foreach ($comment_fetch_result as $i => $p) {
				// If the post has a parent, find a reference to it in the thread
				if (!is_null($p["parent_postcomment_id"])) {
					$parent =& find_parent($thread, $p["parent_postcomment_id"]);
					if ($parent) {
						// Create the replies array if it doesn't exist
						if (!isset($parent["replies"])) {
							$parent["replies"] = array();
						}
						// Append it to replies
						$parent["replies"][$p["postcomment_id"]] = $p;
						// Remove from the unsorted comment_fetch_result list
						unset($comment_fetch_result[$i]);
					}
					// Otherwise, wait until it shows up in the thread
				} else {
					// Top-level comment_fetch_result can be threaded immediately
					$thread[$p["postcomment_id"]] = $p;
					// Remove from the unsorted comment_fetch_result list
					unset($comment_fetch_result[$i]);
				}
			}
		}
		return array(
			"message" => "fetch_success",
			"comments" => $thread,
		);
	}



	/**
	 * This checks if the user is logged in, and if they are, it adds their comment to the postcomments field.
	 *
	 * @param  string $p_auth_token
	 * @param  int $p_postitem_id
	 * @param  string $p_comment_text
	 * @param  int $p_parent_postcomment_id
	 * @return void
	 */
	public function submit_comment($p_auth_token, $p_postitem_id, $p_comment_text, $p_parent_postcomment_id)
	{
		// decode jwt, with this we'll verify the user and we'll get their details
		$decoded = $this->UserTokenAuth->check_user_token($p_auth_token);

		// Only continue if the token is valid
		if ($decoded["message"] !== "token_valid") {
			return array(
				"message" => "token_validation_error"
			);
		}
	
		// postitem_id is autoincremented, hence NULL
		$comment_add = $this->dbconn->prepare('INSERT into postcomment (
			postcomment_id,
			user_id,
			postitem_id,
			text,
			parent_postcomment_id,
			comment_posted_at,
			comment_updated_at
		)
		VALUES (NULL, ?, ?, ?, ?, NOW(), NOW())');

		// If the parent is "root", aka no parent, we handle that and change the insert to be null
		$parent_insert = null;
		if ($p_parent_postcomment_id === "root") {
			$parent_insert = null;
		} else {
			$parent_insert = $p_parent_postcomment_id;
		}

		$comment_add->execute([$decoded["data"]->id, $p_postitem_id, $p_comment_text, $parent_insert]);
	
		if (!$comment_add) {
			return array(
				"message" => "failed_to_add_comment"
			);
		}

		return array(
			"message" => "comment_submitted",
		);
	}



	/**
	 * This checks if the user is logged in, and if they are, it edits their comment if it
	 * belongs to them.
	 *
	 * @param  string $p_auth_token
	 * @param  int $postcomment_id
	 * @param  string $p_comment_text
	 * @return void
	 */
	public function edit_comment($p_auth_token, $postcomment_id, $p_comment_text)
	{
		// decode jwt, with this we'll verify the user and we'll get their details
		$decoded = $this->UserTokenAuth->check_user_token($p_auth_token);

		// Only continue if the token is valid
		if ($decoded["message"] !== "token_valid") {
			return array(
				"message" => "token_validation_error"
			);
		}
	
		// postitem_id is autoincremented, hence NULL
		$comment_add = $this->dbconn->prepare('UPDATE postcomment SET text = ?, comment_updated_at = NOW()
		WHERE postcomment_id = ? AND user_id = ?');



		$comment_add->execute([$p_comment_text, $postcomment_id, $decoded["data"]->id,]);
	
		if (!$comment_add) {
			return array(
				"message" => "failed_to_edit_comment"
			);
		}

		return array(
			"message" => "comment_editted",
		);
	}



	/**
	 * This attempts to "delete" the user's comment if their user ID matches the user ID of the
	 * author of the comment. Note that this does not actually delete the comment but, instead,
	 * makes the comment be "[DeletedComment]" and nulls the user ID so it's no longer editable.
	 *
	 * @param  string $p_auth_token
	 * @param  int $postcomment_id
	 * @return void
	 */
	public function delete_comment($p_auth_token, $postcomment_id)
	{
		// decode jwt, with this we'll verify the user and we'll get their details
		$decoded = $this->UserTokenAuth->check_user_token($p_auth_token);

		// Only continue if the token is valid
		if ($decoded["message"] !== "token_valid") {
			return array(
				"message" => "token_validation_error"
			);
		}
	
		// We null the user ID and text of the comment, rendering it uneditable. The client
		// will handle the "[Deleted]" display so we don't need to bother with storing that data
		// here
		$comment_add = $this->dbconn->prepare('UPDATE postcomment
		SET text = ?,
			user_id = NULL,
			comment_updated_at = NOW()
		WHERE postcomment_id = ?
			AND user_id = ?');

		$comment_add->execute(["[DeletedComment]", $postcomment_id, $decoded["data"]->id,]);
	
		if (!$comment_add) {
			return array(
				"message" => "failed_to_delete_comment"
			);
		}

		return array(
			"message" => "comment_deleted",
		);
	}



	/**
	 * This attempts to toggle upvote/downvote the post. If the desired vote is the same as the current vote,
	 * the vote status will be reset to default. Otherwise, the vote status will be set to the desired one.
	 *
	 * @param  string $p_auth_token
	 * @param  int $p_postitem_id
	 * @param  int $p_vote_status
	 * @param  int $p_vote_desired
	 * @return void
	 */
	public function post_toggle_vote($p_auth_token, $p_postitem_id, $p_vote_status_current, $p_vote_desired)
	{
		$decoded = $this->UserTokenAuth->check_user_token($p_auth_token);
		
		if ($decoded["message"] !== "token_valid") {
			return array(
				"message" => "token_validation_error"
			);
		}
		
		$set_vote_to = 0;

		// Here we set the vote to be default if the desired vote is the same as the current vote
		if ($p_vote_status_current === $p_vote_desired) {
			$set_vote_to = 0;
		} else {
			$set_vote_to = $p_vote_desired;
		}

		// NOTE: This either adds the vote, or replaces the existing row where the PRIMARY and UNIQUE keys match
		// For this, the user_id and need to be set as a unique ***TOGETHER*** in the postitem_vote table
		// i.e: ALTER TABLE `postitem_vote` ADD UNIQUE( `user_id`, `postitem_id`);
		// See: https://dev.mysql.com/doc/refman/8.0/en/replace.html
		$toggle = $this->dbconn->prepare('REPLACE INTO postitem_vote (user_id, postitem_id, vote_status) VALUES (?, ?, ?)');
		$toggle_result = $toggle->execute([$decoded["data"]->id, $p_postitem_id, $set_vote_to]);

		if (!$toggle_result) {
			return array(
				"message" => "vote_failed"
			);
		}

		return array(
			"message" => "vote_success"
		);
	}
}
