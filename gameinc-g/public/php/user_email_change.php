<?php

namespace User;

/**
 * @brief This the two-step email change process for existing users.
 */
class UserEmailChange
{
	// -------------------------------------------------------------------------
	// SECTION Dependencies
	// -------------------------------------------------------------------------
	// private \PDO $dbconn;
	// private \User\UserAccess $UserAccess;
	// private \Conf\GeneralConf $GeneralConf;
	private $dbconn;
	private $GeneralConf;
	private $UserAccess;
	// -------------------------------------------------------------------------
	// !SECTION
	// -------------------------------------------------------------------------



	/**
	 * __construct triggered when constructing, does dependency injection
	 *
	 * These are then bound to the class
	 *
	 * Requirements:
	 * - dbconn:		instance of dbconn from _dbconnect.php
	 * - UserTokenAuth:	instance of \User\UserTokenAuth for authenticating the user's token
	 * - GeneralConf:	instance of \Conf\GeneralConf for SMTP username and password
	 *
	 * @param  \PDO $dbconn
	 * @param  \User\UserAccess $UserAccess
	 * @param  \Conf\General $GeneralConf
	 * @return void
	 */
	public function __construct(\PDO $dbconn, \User\UserAccess $UserAccess, \Conf\General $GeneralConf)
	{
		$this->dbconn = $dbconn;
		$this->UserAccess = $UserAccess;
		$this->GeneralConf = $GeneralConf;
	}



	/**
	 * This attempts to verify that the user actually attempted to update their email.
	 *
	 * I'm claiming that this is for security reasons.
	 *
	 * @param  string $p_user_email
	 * @param  string $p_user_email_new
	 * @param  string $p_user_token
	 * @return array
	 */
	public function verify_new_email_step1($p_user_email, $p_user_email_new, $p_user_token)
	{
		// Check email matches given email
		$sql_existing_user_result = $this->UserAccess->db_get_user_details_by_email($p_user_email);

		if (!$sql_existing_user_result) {
			return array(
				"message" => "verifyemail_expired_or_no_matchAA"
			);
			die();
		}

		// Get the emailverify token and request date of the user according to the user we previously selected
		$emailverif_check = $this->dbconn->prepare('SELECT token_hash, requested_at FROM emailverify WHERE user_id = ?');
		$emailverif_check->execute([$sql_existing_user_result["user_id"]]);
		$emailverif_check_result = $emailverif_check->fetch(\PDO::FETCH_ASSOC);

		// If no result, tell user no match
		if (!$emailverif_check_result) {
			return array(
				"message" => "verifyemail_expired_or_no_matchCCC"
			);
			die();
		}

		// We check if the specified token actually matches the user's verify token
		// We use password_verify because the token in the database is stored as a hash
		// and we need to check if the hashes
		// This is done so that if an attacker gets access to the table of tokens, they don't know the actual tokens... only their hashes
		if (!password_verify($p_user_token, $emailverif_check_result["token_hash"])) {
			return array(
				"message" => "verifyemail_expired_or_no_matchDDD",
			);
			die();
		}

		// Compare the time difference from now and the time at which the email verification token was created
		$time_now = time();
		$time_requested = strtotime($emailverif_check_result["requested_at"]);
		// This is in seconds
		$time_diff = $time_now - $time_requested;
		// 48 hours: 48 * 60 * 60 = 172800
		// Check if token is expired (time diff from time requested)
		if ($time_diff > 60*60) {
			// As the verification token has expired, remove it
			$this->UserAccess->db_remove_existing_emailverify_tokens($sql_existing_user_result["user_id"]);
			return array(
				"message" => "verifyemail_expired_or_no_matchEEE",
			);
			die();
		}

		// Remove token
		$this->UserAccess->db_remove_existing_emailverify_tokens($sql_existing_user_result["user_id"]);

		// Set the user's new email as "allowable"
		$verify_user = $this->dbconn->prepare('UPDATE user SET is_email_new_allowed = ? WHERE user_id = ?');
		$verify_user_result = $verify_user->execute([true, $sql_existing_user_result["user_id"]]);

		// Now we make the request to verify their new email
		$this->send_email_change_step2($p_user_email, $p_user_email_new);

		if ($verify_user_result) {
			return array(
				"message" => "email_change_allowed"
			);
			die();
		}
		die();
	}



	/**
	 * This attempts to verify that the user actually has access to their new email.
	 *
	 * I'm claiming that this is for UX and user errors
	 *
	 * @param  string $p_user_email
	 * @param  string $p_user_email_new
	 * @param  string $p_user_token
	 * @return array
	 */
	public function verify_new_email_step2($p_user_email, $p_user_email_new, $p_user_token)
	{
		// Check email matches given email
		$sql_existing_user_result = $this->UserAccess->db_get_user_details_by_email($p_user_email);

		if (!$sql_existing_user_result) {
			return array(
				"message" => "verifyemail_expired_or_no_matchAA"
			);
			die();
		}

		// Here we check they've allowed the email change
		if ($sql_existing_user_result["is_email_new_allowed"] === false) {
			return array(
				"message" => "verifyemail_expired_or_no_matchBB"
			);
			die();
		}

		// Get the emailverify token and request date of the user according to the user we previously selected
		$emailverif_check = $this->dbconn->prepare('SELECT token_hash, requested_at FROM emailverify WHERE user_id = ?');
		$emailverif_check->execute([$sql_existing_user_result["user_id"]]);
		$emailverif_check_result = $emailverif_check->fetch(\PDO::FETCH_ASSOC);

		// If no result, tell user no match
		if (!$emailverif_check_result) {
			return array(
				"message" => "verifyemail_expired_or_no_matchCCC"
			);
			die();
		}

		// We check if the specified token actually matches the user's verify token
		// We use password_verify because the token in the database is stored as a hash
		// and we need to check if the hashes
		// This is done so that if an attacker gets access to the table of tokens, they don't know the actual tokens... only their hashes
		if (!password_verify($p_user_token, $emailverif_check_result["token_hash"])) {
			return array(
				"message" => "verifyemail_expired_or_no_matchDDD",
			);
			die();
		}

		// Compare the time difference from now and the time at which the email verification token was created
		$time_now = time();
		$time_requested = strtotime($emailverif_check_result["requested_at"]);
		// This is in seconds
		$time_diff = $time_now - $time_requested;
		// 48 hours: 48 * 60 * 60 = 172800
		// Check if token is expired (time diff from time requested)
		if ($time_diff > 60*60) {
			// As the verification token has expired, remove it
			$this->UserAccess->db_remove_existing_emailverify_tokens($sql_existing_user_result["user_id"]);
			return array(
				"message" => "verifyemail_expired_or_no_matchEEE",
			);
			die();
		}

		// Remove token
		$this->UserAccess->db_remove_existing_emailverify_tokens($sql_existing_user_result["user_id"]);

		// Here we finally update the user's email
		$verify_user = $this->dbconn->prepare('UPDATE user
		SET
			email = ?,
			email_new = NULL,
			is_emailverified = true,
			is_email_new_allowed = false
		WHERE user_id = ?');
		$verify_user_result = $verify_user->execute([$p_user_email_new, $sql_existing_user_result["user_id"]]);

		if ($verify_user_result) {
			return array(
				"message" => "email_changed"
			);
			die();
		}
		die();
	}

	/**
	 * This sends an email to the user's current email, asking them whether they are permitting a change of their
	 * email address.
	 *
	 * @param  int $p_user_id
	 * @param  string $p_current_email
	 * @param  string $p_new_email
	 * @return void
	 */
	public function send_email_change_step1($p_user_id, $p_current_email, $p_new_email)
	{
		$update_details = $this->dbconn->prepare('UPDATE user SET email_new = ?, is_email_new_allowed = ? WHERE user_id = ?');

		// Here we insert the new email request, and we make the new email "not allowed", aka the user hasn't yet verified on their
		// original email
		$update_details->execute([$p_new_email, false, $p_user_id]);

		$email_token_plain = $this->UserAccess->create_new_token();

		// This makes sure there is only one active token
		$this->UserAccess->db_remove_existing_emailverify_tokens($p_user_id);

		// Here we create hash and insert it into the database
		if (!$this->UserAccess->db_hash_and_insert_new_emailverify_token($p_user_id, $email_token_plain)) {
			return array(
				"message" => "emailchange_cant_create_token"
			);
			// return;
			die();
		}

		// Concat the URL link with which the user will allow their email to be changed
		$email_conf_link = "{$this->GeneralConf->server_api_url}/__api_ext.php?request_type=email_change_step1&email={$p_current_email}&new_email={$p_new_email}&token={$email_token_plain}";

		$mailSubject = "gameinc Account Email Change Confirmation";
		$mailBody = "Hi, you requested an email change.\r\n
			To permit this change, please visit {$email_conf_link}";

		$mailBodyNoHtml = "Hi, you requested an email change.\r\n
			To permit this change, please visit {$email_conf_link}";

		// Here we actually send the email
		return $this->UserAccess->UserMailer->send_email(
			$p_current_email,
			$mailSubject,
			$mailBody,
			$mailBodyNoHtml
		);
	}


	/**
	 * This sends an email to the user's new email, making sure that the email is actually accessible
	 * before updating the user's active email.
	 *
	 * @param  string $p_current_email
	 * @param  string $p_new_email
	 * @return void
	 */
	public function send_email_change_step2($p_current_email, $p_new_email)
	{
		$sql_user = $this->dbconn->prepare('
			SELECT *
			FROM user
			WHERE email = ? AND email_new = ?');

		$sql_user->execute([$p_current_email, $p_new_email]);
		$sql_user_result = $sql_user->fetch(\PDO::FETCH_ASSOC);

		$email_token_plain = $this->UserAccess->create_new_token();

		// This makes sure there is only one active token
		$this->UserAccess->db_remove_existing_emailverify_tokens($sql_user_result["user_id"]);

		// Here we create hash and insert it into the database
		if (!$this->UserAccess->db_hash_and_insert_new_emailverify_token($sql_user_result["user_id"], $email_token_plain)) {
			return array(
				"message" => "emailchange_cant_create_token"
			);
			// return;
			die();
		}

		// Concat the URL link with which the user will allow their email to be changed
		$email_conf_link = "{$this->GeneralConf->server_api_url}/__api_ext.php?request_type=email_change_step2&email={$p_current_email}&new_email={$p_new_email}&token={$email_token_plain}";

		$mailSubject = "gameinc Account Email Change Confirmation";
		$mailBody = "Hi, you requested an email change.\r\n
			Now you just need to verify here, and your account's email will change to the one you're on now {$email_conf_link}";

		$mailBodyNoHtml = "Hi, you requested an email change.\r\n
			Now you just need to verify here, and your account's email will change to the one you're on now {$email_conf_link}";

		// Here we actually send the email
		// NOTE: This is sent to the new email
		$email_req = $this->UserAccess->UserMailer->send_email(
			$p_new_email,
			$mailSubject,
			$mailBody,
			$mailBodyNoHtml
		);

		if ($email_req) {
			return array(
				"message" => "email_change_success",
				"from_email" => $p_current_email,
				"to_email" => $p_new_email,
			);
		}
	}
}
