<?php

namespace Feed;

/**
 * @brief This is used for managing the feeds as an admin.
 *
 * This contains the requests for both the FeedEditor and SourceEditor
 */
class FeedManager
{
	// -------------------------------------------------------------------------
	// SECTION Dependencies
	// -------------------------------------------------------------------------
	/**
	 * dbconn private variable, so that only the class itself can access it, used to store the
	 * connection to the database
	 *
	 * @var \PDO
	 */
	// private \PDO $dbconn;
	// private \User\UserTokenAuth $UserTokenAuth;
	private $dbconn;
	private $UserTokenAuth;
	// -------------------------------------------------------------------------
	// !SECTION
	// -------------------------------------------------------------------------



	/**
	 * __construct triggered when constructing, does dependency injection
	 *
	 * These are then bound to the class
	 *
	 * Requirements:
	 * - dbconn:		instance of dbconn from _dbconnect.php
	 * * - UserTokenAuth:		instance of \User\UserTokenAuth for authenticating the user's token
	 * - extrafunc:		instance of \Misc\ExtraFunc from _functions.php
	 *
	 * @param  \PDO $dbconn
	 * @param  \User\UserTokenAuth $UserTokenAuth
	 * @param  \Misc\ExtraFunc $extrafunc
	 * @return void
	 */
	public function __construct(\PDO $dbconn, \User\UserTokenAuth $UserTokenAuth, \Misc\ExtraFunc $extrafunc)
	{
		$this->dbconn = $dbconn;
		$this->UserTokenAuth = $UserTokenAuth;
		$this->extrafunc = $extrafunc;
	}



	// -------------------------------------------------------------------------
	// SECTION FeedEditor (UI)
	// -------------------------------------------------------------------------
	/**
	 * This gets all feeds in the feed table, returning their names and IDss
	 *
	 * @param  string $p_auth_token
	 * @return array
	 */
	public function feed_get_all($p_auth_token)
	{
		// decode jwt, with this we'll verify the user and we'll get their details
		$decoded = $this->UserTokenAuth->check_user_token($p_auth_token);

		// Only continue if the token is valid
		if ($decoded["message"] !== "token_valid") {
			return array(
				"message" => "token_validation_error"
			);
		}

		if ($decoded["data"]->userLevel !== "admin") {
			return array(
				"message" => "user_not_admin"
			);
		}
		$feeds = $this->dbconn->prepare('SELECT feed_id, name FROM feed');
		$feeds->execute();
		$feeds_result = $feeds->fetchAll(\PDO::FETCH_ASSOC);

		return array(
			"message" => "fetch_success",
			"feeds" => $feeds_result,
		);
	}



	/**
	 * This gets all the sources that are linked to the specified feed
	 * in the linking table.
	 *
	 * @param  string $p_auth_token
	 * @param  int $p_selected_feed_id
	 * @return array
	 */
	public function feed_get_linked_sources($p_auth_token, $p_selected_feed_id)
	{
		// decode jwt, with this we'll verify the user and we'll get their details
		$decoded = $this->UserTokenAuth->check_user_token($p_auth_token);

		// Only continue if the token is valid
		if ($decoded["message"] !== "token_valid") {
			return array(
				"message" => "token_validation_error"
			);
		}

		if ($decoded["data"]->userLevel !== "admin") {
			return array(
				"message" => "user_not_admin"
			);
		}
		// Get all source_id and url data of every source element
		// We use the feed_source linking table
		// Basically
		// - get the source_id and url of every source
		// - that is linked to feed_source
		// - that is linked to feed
		// - where the feed name matches the requested feed name
		// where the source_id is joined to the specified feed id
		$feed_sources = $this->dbconn->prepare('SELECT source.source_id, source.url, source.name
		FROM source
			INNER JOIN feed_source ON source.source_id = feed_source.source_id
			INNER JOIN feed ON feed.feed_id = feed_source.feed_id
		WHERE feed.feed_id = ?
		');
		$feed_sources->execute([$p_selected_feed_id]);
		$feed_sources_result = $feed_sources->fetchAll(\PDO::FETCH_ASSOC);

		return array(
			"message" => "fetch_success",
			"sources" => $feed_sources_result,
		);
	}



	/**
	 * This unlinks the specified source from the specified feed.
	 *
	 * @param  string $p_auth_token
	 * @param  int $p_feed
	 * @param  int $p_source_to_unlink
	 * @return array
	 */
	public function feed_unlink_source($p_auth_token, $p_feed, $p_source_to_unlink)
	{
		// decode jwt, with this we'll verify the user and we'll get their details
		$decoded = $this->UserTokenAuth->check_user_token($p_auth_token);

		// Only continue if the token is valid
		if ($decoded["message"] !== "token_valid") {
			return array(
				"message" => "token_validation_error"
			);
		}

		if ($decoded["data"]->userLevel !== "admin") {
			return array(
				"message" => "user_not_admin"
			);
		}
		// Perform a simple delete in the linking table which links feeds to sources
		$stmt = $this->dbconn->prepare('
		DELETE FROM feed_source
		WHERE feed_id = ? AND source_id = ?
		');
		$stmt->execute([$p_feed, $p_source_to_unlink]);

		return array(
			"message" => "unlink_success"
		);
	}



	/**
	 * This gets all sources that are not-yet-linked, hence are linkable, to the
	 * specified feed.
	 *
	 * @param  string $p_auth_token
	 * @param  int $p_feed
	 * @return array
	 */
	public function feed_get_linkable_sources($p_auth_token, $p_feed)
	{
		// decode jwt, with this we'll verify the user and we'll get their details
		$decoded = $this->UserTokenAuth->check_user_token($p_auth_token);

		// Only continue if the token is valid
		if ($decoded["message"] !== "token_valid") {
			return array(
				"message" => "token_validation_error"
			);
		}

		if ($decoded["data"]->userLevel !== "admin") {
			return array(
				"message" => "user_not_admin"
			);
		}
		// This is the inverse *functionality-wise* from feed_get_linked_sources
		// However, the query is... more complicated
		// Essentially (backwards)
		// - We select feeds only where feed.feed_id matches our given feed
		// - We try to link the source_id in the feed_source and the source table
		// - And we select the feed_id in the feed_source based on our given feed
		// We essentially get an output where:
		// - If the feed exists
		// - Get ALL source fields that are NOT attached to our given feed
		$sources = $this->dbconn->prepare('SELECT source.source_id, source.url, source.name
		FROM source
		WHERE NOT EXISTS (
				SELECT NULL
				FROM feed_source
				WHERE (
						feed_source.source_id = source.source_id
						AND feed_source.feed_id = ?
					)
			)
			AND (
				SELECT feed.feed_id
				FROM feed
				WHERE feed.feed_id = ?
		)');
		// Provide the feed twice cause we use it twice
		$sources->execute([$p_feed, $p_feed]);
		$sources_result = $sources->fetchAll(\PDO::FETCH_ASSOC);

		return array(
			"message" => "fetch_success",
			"sources" => $sources_result,
		);
	}



	/**
	 * This links the speicifed source to the specified feed
	 *
	 * @param  string $p_auth_token
	 * @param  int $p_feed
	 * @param  int $p_source_to_link
	 * @return array
	 */
	public function feed_link_source($p_auth_token, $p_feed, $p_source_to_link)
	{
		// decode jwt, with this we'll verify the user and we'll get their details
		$decoded = $this->UserTokenAuth->check_user_token($p_auth_token);

		// Only continue if the token is valid
		if ($decoded["message"] !== "token_valid") {
			return array(
				"message" => "token_validation_error"
			);
		}

		if ($decoded["data"]->userLevel !== "admin") {
			return array(
				"message" => "user_not_admin"
			);
		}
		// Perform a simple delete in the linking table which links feeds to sources
		$stmt = $this->dbconn->prepare('INSERT into feed_source (feed_source_id, feed_id, source_id)
		VALUES (NULL, ?, ?)');
		try {
			$stmt->execute([$p_feed, $p_source_to_link]);
		} catch (\PDOException $e) {
			// If the error returned is 1062, meaning that it's a duplicate entry
			// tell the user that
			if ($e->errorInfo[1] == 1062) {
				return array(
					"message" => "feed_link_already_exists"
				);
			}
		}

		return array(
			"message" => "link_success"
		);
	}



	/**
	 * This creates a new feed via the specified name if it does not already exist
	 *
	 * @param  string $p_auth_token
	 * @param  string $p_feed
	 * @return array
	 */
	public function feed_create_new($p_auth_token, $p_feed)
	{
		// decode jwt, with this we'll verify the user and we'll get their details
		$decoded = $this->UserTokenAuth->check_user_token($p_auth_token);

		// Only continue if the token is valid
		if ($decoded["message"] !== "token_valid") {
			return array(
				"message" => "token_validation_error"
			);
		}

		if ($decoded["data"]->userLevel !== "admin") {
			return array(
				"message" => "user_not_admin"
			);
		}
		// See: https://stackoverflow.com/questions/3407857/only-inserting-a-row-if-its-not-already-there/3408196#3408196
		$stmt = $this->dbconn->prepare('INSERT into feed (feed.feed_id, feed.name) VALUES (NULL, ?)');

		try {
			$stmt->execute([$p_feed]);
		} catch (\PDOException $e) {
			// If the error returned is 1062, meaning that it's a duplicate entry
			// tell the user that
			if ($e->errorInfo[1] == 1062) {
				return array(
					"message" => "feed_already_exists"
				);
			}
		}

		return array(
			"message" => "create_success"
		);
	}



	/**
	 * This udpates the name of the selected feed
	 *
	 * @param  string $p_auth_token
	 * @param  string $p_selected_feed_id
	 * @param  string $p_new_name
	 * @return array
	 */
	public function feed_update_name($p_auth_token, $p_selected_feed_id, $p_new_name)
	{
		// decode jwt, with this we'll verify the user and we'll get their details
		$decoded = $this->UserTokenAuth->check_user_token($p_auth_token);

		// Only continue if the token is valid
		if ($decoded["message"] !== "token_valid") {
			return array(
				"message" => "token_validation_error"
			);
		}

		if ($decoded["data"]->userLevel !== "admin") {
			return array(
				"message" => "user_not_admin"
			);
		}
		$stmt = $this->dbconn->prepare('UPDATE feed SET name = ? WHERE feed_id = ?');

		$stmt->execute([$p_new_name, $p_selected_feed_id]);

		if (!$stmt) {
			return array(
				"message" => "update_error"
			);
		}

		return array(
			"message" => "update_success"
		);
	}




	/**
	 * This unlinks all sources attached to the specified feed, then it proceeds to
	 * remove the feed itself from the db.
	 *
	 * @param  string $p_auth_token
	 * @param  int $p_feed
	 * @return array
	 */
	public function feed_unlink_all_and_remove_feed($p_auth_token, $p_feed)
	{
		// decode jwt, with this we'll verify the user and we'll get their details
		$decoded = $this->UserTokenAuth->check_user_token($p_auth_token);

		// Only continue if the token is valid
		if ($decoded["message"] !== "token_valid") {
			return array(
				"message" => "token_validation_error"
			);
		}

		if ($decoded["data"]->userLevel !== "admin") {
			return array(
				"message" => "user_not_admin"
			);
		}
		// Perform a simple delete in the linking table which links feeds to sources
		// Here we delete every item corresponding to the selected feed
		$stmt = $this->dbconn->prepare('DELETE FROM feed_source WHERE feed_id = ?');
		$stmt_result = $stmt->execute([$p_feed]);


		// If for some reason this fails, tell user
		if (!$stmt_result) {
			return array(
				"message" => "sources_unlink_error"
			);
		}

		// Otherwise we continue and delete the feed itself

		// Now we remove the actual feed from the feed table
		$stmt = $this->dbconn->prepare('DELETE FROM feed WHERE feed_id = ?');
		$stmt_result = $stmt->execute([$p_feed]);


		// Same as above, but different message
		if (!$stmt_result) {
			return array(
				"message" => "sources_unlinked_but_remove_did_error"
			);
		}

		return array(
			"message" => "unlink_and_remove_success"
		);
	}
	// -------------------------------------------------------------------------
	// !SECTION FeedEditor (UI)
	// -------------------------------------------------------------------------



	// -------------------------------------------------------------------------
	// SECTION SourceEditor (UI)
	// -------------------------------------------------------------------------
	/**
	 * This gets all existing sources
	 *
	 * @param  string $p_auth_token
	 * @return array
	 */
	public function source_get_all($p_auth_token)
	{
		// decode jwt, with this we'll verify the user and we'll get their details
		$decoded = $this->UserTokenAuth->check_user_token($p_auth_token);

		// Only continue if the token is valid
		if ($decoded["message"] !== "token_valid") {
			return array(
				"message" => "token_validation_error"
			);
		}

		if ($decoded["data"]->userLevel !== "admin") {
			return array(
				"message" => "user_not_admin"
			);
		}
		$sources = $this->dbconn->prepare('SELECT source_id, url, name FROM source');
		$sources->execute();
		$sources_result = $sources->fetchAll(\PDO::FETCH_ASSOC);

		return array(
			"message" => "fetch_success",
			"sources" => $sources_result,
		);
	}



	/**
	 * This gets the details of the specified source.
	 *
	 * @param  string $p_auth_token
	 * @param  int $p_source_id
	 * @return array
	 */
	public function source_get_details($p_auth_token, $p_source_id)
	{
		// decode jwt, with this we'll verify the user and we'll get their details
		$decoded = $this->UserTokenAuth->check_user_token($p_auth_token);

		// Only continue if the token is valid
		if ($decoded["message"] !== "token_valid") {
			return array(
				"message" => "token_validation_error"
			);
		}

		if ($decoded["data"]->userLevel !== "admin") {
			return array(
				"message" => "user_not_admin"
			);
		}
		// Just get the basic details
		$details = $this->dbconn->prepare('SELECT source_id, url, name FROM source WHERE source_id = ?');
		$details->execute([$p_source_id]);
		$details_result = $details->fetch(\PDO::FETCH_ASSOC);

		// Select 1 (output 1) for every postitem which matches the specified source
		$postcount = $this->dbconn->prepare('SELECT 1
		FROM postitem
			INNER JOIN source_postitem ON postitem.postitem_id = source_postitem.postitem_id
		WHERE source_postitem.source_id = ?');

		$postcount->execute([$p_source_id]);
		$postcount->fetchAll(\PDO::FETCH_ASSOC);

		$postcount_count = $postcount->rowCount();

		// ---------------------------------------------------------------------
		// TODO: get num of posts as details_result.num_of_posts
		// ---------------------------------------------------------------------

		return array(
			"message" => "fetch_success",
			"details" => $details_result,
			"num_of_posts" => $postcount_count
		);
	}



	/**
	 * This attempts to create a new source if it does not already exist.
	 *
	 * @param  string $p_auth_token
	 * @param  string $p_source_url
	 * @param  string $p_source_name
	 * @return array
	 */
	public function source_create_new($p_auth_token, $p_source_url, $p_source_name)
	{
		// decode jwt, with this we'll verify the user and we'll get their details
		$decoded = $this->UserTokenAuth->check_user_token($p_auth_token);

		// Only continue if the token is valid
		if ($decoded["message"] !== "token_valid") {
			return array(
				"message" => "token_validation_error"
			);
		}

		if ($decoded["data"]->userLevel !== "admin") {
			return array(
				"message" => "user_not_admin"
			);
		}
		$stmt = $this->dbconn->prepare('INSERT into source (source_id, url, name) VALUES (NULL, ?, ?)');

		try {
			$stmt->execute([$p_source_url, $p_source_name]);
		} catch (\PDOException $e) {
			// If the error returned is 1062, meaning that it's a duplicate entry
			// tell the user that
			if ($e->errorInfo[1] == 1062) {
				return array(
					"message" => "source_already_exists"
				);
			}
		}

		return array(
			"message" => "create_success"
		);
	}



	/**
	 * This udpates the name of the selected source
	 *
	 * @param  string $p_auth_token
	 * @param  string $p_selected_source_id
	 * @param  string $p_new_name
	 * @return array
	 */
	public function source_update_name($p_auth_token, $p_selected_source_id, $p_new_name)
	{
		// decode jwt, with this we'll verify the user and we'll get their details
		$decoded = $this->UserTokenAuth->check_user_token($p_auth_token);

		// Only continue if the token is valid
		if ($decoded["message"] !== "token_valid") {
			return array(
				"message" => "token_validation_error"
			);
		}

		if ($decoded["data"]->userLevel !== "admin") {
			return array(
				"message" => "user_not_admin"
			);
		}
		// See: https://stackoverflow.com/questions/3407857/only-inserting-a-row-if-its-not-already-there/3408196#3408196
		$stmt = $this->dbconn->prepare('UPDATE source SET name = ? WHERE source_id = ?');

		$stmt->execute([$p_new_name, $p_selected_source_id]);

		if (!$stmt) {
			return array(
				"message" => "update_error"
			);
		}

		return array(
			"message" => "update_success"
		);
	}



	/**
	 * This removes the specified source and all of the postitems tied to that source that are NOT
	 * linked to any other sources
	 *
	 * @param  string $p_auth_token
	 * @param  int $p_source_id
	 * @return array
	 */
	public function source_remove_and_purge_posts($p_auth_token, $p_source_id)
	{
		// decode jwt, with this we'll verify the user and we'll get their details
		$decoded = $this->UserTokenAuth->check_user_token($p_auth_token);

		// Only continue if the token is valid
		if ($decoded["message"] !== "token_valid") {
			return array(
				"message" => "token_validation_error"
			);
		}

		if ($decoded["data"]->userLevel !== "admin") {
			return array(
				"message" => "user_not_admin"
			);
		}
		// We first remove the feed-source links for the specified source
		$remove_feed_source_link = $this->dbconn->prepare('DELETE FROM feed_source WHERE feed_source.source_id = ?');
		$remove_feed_source_link_result = $remove_feed_source_link->execute([$p_source_id]);

		if (!$remove_feed_source_link_result) {
			return array(
				"message" => "removing_feedsourcelink_error"
			);
		}


		// We then remove the source as the later queries will rely on this source not existing in the source table
		$remove_source = $this->dbconn->prepare('DELETE source FROM source WHERE source.source_id = ?');
		$remove_source_result = $remove_source->execute([$p_source_id]);

		if (!$remove_source_result) {
			return array(
				"message" => "removing_source_error"
			);
		}

		// This query basically deletes ALL post items attached to the specified source
		// ONLY IF they are not present in any other source. This is to ensure that if,
		// for some reason, we are parsing two different sources that provided us with the
		// same post, that we don't wipe it entirely
		$remove_postitems = $this->dbconn->prepare('DELETE FROM postitem
		WHERE postitem.postitem_id IN (
				SELECT postitem.postitem_id
				FROM postitem
					INNER JOIN source_postitem ON postitem.postitem_id = source_postitem.postitem_id
				WHERE source_postitem.source_id = ?
					AND postitem.title NOT IN (
						SELECT postitem.postitem_id
						FROM postitem
							INNER JOIN source_postitem ON postitem.postitem_id = source_postitem.postitem_id
						WHERE source_postitem.source_id != ?
					)
			)
		');
		$remove_postitems_result = $remove_postitems->execute([$p_source_id, $p_source_id]);

		if (!$remove_postitems_result) {
			return array(
				"message" => "removing_postitems_error"
			);
		}

		// This deletes any source_postitem links where the source id is the one we deleted
		// ONLY when the source_postitem link is no longer referenced in ANY postitem
		$remove_postlinks = $this->dbconn->prepare('DELETE FROM source_postitem
		WHERE NOT EXISTS (
				SELECT NULL
				FROM postitem
				WHERE (
						postitem.postitem_id = source_postitem.postitem_id
					)
			)
			AND source_postitem.source_id = ?
		');
		$remove_postlinks_result = $remove_postlinks->execute([$p_source_id]);

		if (!$remove_postlinks_result) {
			return array(
				"message" => "removing_postlinks_error"
			);
		}

		return array(
			"message" => "remove_success"
		);
	}
}
