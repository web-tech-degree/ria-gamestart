<?php

namespace Misc;

/**
 * @brief This is used for cropping images
 *
 */
class ImageCrop
{
	/**
	 * This takes an image from the source image file path, crops and resizes it, then
	 * outputs it into the destination path.
	 *
	 * https://polyetilen.lt/en/resize-and-crop-image-from-center-with-php
	 *
	 * @param  mixed $p_width
	 * @param  mixed $p_height
	 * @param  mixed $p_img_source
	 * @param  mixed $p_img_destination
	 * @param  mixed $quality
	 * @return void
	 */
	public function crop_image_file($p_width, $p_height, $p_img_source, $p_img_destination, $quality = 60)
	{
		try {
			$imgsize = getimagesize($p_img_source);
			$width = $imgsize[0];
			$height = $imgsize[1];
			$mime = $imgsize["mime"];
			
			switch ($mime) {
				case "image/gif":
					$image_create = "imagecreatefromgif";
					$image = "imagegif";
					break;
				
				case "image/png":
					$image_create = "imagecreatefrompng";
					$image = "imagepng";
					$quality = 7;
					break;
				
				case "image/jpeg":
					$image_create = "imagecreatefromjpeg";
					$image = "imagejpeg";
					$quality = 80;
					break;
				
				default:
					// return false;
					break;
			}

			$dst_img = imagecreatetruecolor($p_width, $p_height);
			$src_img = $image_create($p_img_source);

			$width_new = $height * $p_width / $p_height;
			$height_new = $width * $p_height / $p_width;
			//if the new width is greater than the actual width of the image, then the height is too large and the rest cut off, or vice versa
			if ($width_new > $width) {
				//cut point by height
				$h_point = (($height - $height_new) / 2);
				//copy image
				imagecopyresampled($dst_img, $src_img, 0, 0, 0, $h_point, $p_width, $p_height, $width, $height_new);
			} else {
				//cut point by width
				$w_point = (($width - $width_new) / 2);
				imagecopyresampled($dst_img, $src_img, 0, 0, $w_point, 0, $p_width, $p_height, $width_new, $height);
			}

			$image($dst_img, $p_img_destination, $quality);

			// We catch the return values into variables so that we don't return them
			if ($dst_img) {
				$aa = imagedestroy($dst_img);
			}
			if ($src_img) {
				$bb = imagedestroy($src_img);
			}
			return true;
		} catch (\Exception $e) {
			// file_put_contents("___TEST.txt", $e);
			return false;
		}
	}
}
