<?php
// -----------------------------------------------------------------------------
// This file is for handling "external" API requests
// AKA anything not from the app
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// DEVELOPMENT
// header('Access-Control-Allow-Origin: *');
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// SECTION Dependencies
// -----------------------------------------------------------------------------
// DEVELOPMENT:
// require __DIR__ . "../../../vendor/autoload.php";
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// PRODUCTION
require __DIR__ . "/vendor/autoload.php";
// -----------------------------------------------------------------------------

// PDO Database
require_once("_dbconnect.php");

// From namespace: \Misc
require_once("misc_extrafunc.php");
require_once("misc_imagecrop.php");

// From namespace: \Conf
require_once("conf_general.php");
require_once("conf_jwt.php");

// From namespace: \User
require_once("user_access.php");
require_once("user_email_change.php");
require_once("user_details.php");
require_once("user_tokenauth.php");
require_once("user_mailer.php");
require_once("user_source_fav.php");

// From namespace: \Feed
require_once("feed_parser.php");
require_once("feed_fetcher.php");
require_once("feed_manager.php");

// From namespace: \Post
require_once("post_details.php");
require_once("post_votes.php");
require_once("post_bookmarks.php");
// !SECTION
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// SECTION External Libraries
// Here we instantiate external classes/libraries

$lib_phpmailer = new \PHPMailer\PHPMailer\PHPMailer(true);
$lib_jwt = new \Firebase\JWT\JWT;
$lib_picofeed = new \PicoFeed\Reader\Reader;
// !SECTION
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
// SECTION Internal Classes
// Here we instantiate internal classes

// From namespace:			\Misc
$c_ExtraFunc		= new	\Misc\ExtraFunc();
$c_ImageCrop		= new	\Misc\ImageCrop();

// From namespace:			\Conf
$c_JwtParams		= new	\Conf\JwtParams();
$c_GeneralConf		= new	\Conf\General();

// From namespace:			\User
$c_UserTokenAuth	= new	\User\UserTokenAuth($lib_jwt, $c_JwtParams);
$c_UserMailer		= new	\User\UserMailer($lib_phpmailer, $c_GeneralConf);
$c_UserAccess		= new	\User\UserAccess($dbconn, $c_UserTokenAuth, $c_UserMailer, $c_GeneralConf, $c_ExtraFunc);
$c_UserEmailChange	= new	\User\UserEmailChange($dbconn, $c_UserAccess, $c_GeneralConf);
$c_UserDetails		= new	\User\UserDetails($dbconn, $c_UserAccess, $c_UserEmailChange, $c_ImageCrop);
$c_UserSourceFav	= new	\User\SourceFav($dbconn, $c_UserTokenAuth);

// From namespace:			\Post
$c_PostVotes		= new	\Post\PostVotes($dbconn);
$c_PostBookmarks	= new	\Post\PostBookmarks($dbconn, $c_UserTokenAuth, $c_PostVotes, $c_ExtraFunc);
$c_PostDetails		= new	\Post\PostDetails($dbconn, $c_UserTokenAuth, $c_PostVotes, $c_PostBookmarks, $c_UserSourceFav, $c_ExtraFunc);

// From namespace:			\Feed
$c_FeedParser		= new	\Feed\FeedParser($dbconn, $lib_picofeed, $c_ExtraFunc);
$c_FeedManager		= new	\Feed\FeedManager($dbconn, $c_UserTokenAuth, $c_ExtraFunc);
$c_FeedFetcher		= new	\Feed\FeedFetcher($dbconn, $c_UserTokenAuth, $c_PostVotes, $c_PostBookmarks, $c_UserSourceFav, $lib_picofeed, $c_ExtraFunc);
// !SECTION
// -----------------------------------------------------------------------------





// Using GET because it's URL formatted
if (!isset($_GET["request_type"])) {
	$html = $c_ExtraFunc->prettyHtmlError("Request type not specified");
	$html .= "<p>何してるの？</p>";
	echo $html;
	die();
}





// -----------------------------------------------------------------------------
// SECTION UserAuth
// -----------------------------------------------------------------------------



// -----------------------------------------------------------------------------
// SECTION verify_email
// -----------------------------------------------------------------------------
if ($_GET["request_type"] === "verify_email") {
	$valid = true;
	$error_list = [];

	// Check if valid
	if (!isset($_GET["email"])) {
		$valid = false;
		array_push($error_list, "Email not specified\r\n");
	}

	if (!isset($_GET["token"])) {
		$valid = false;
		array_push($error_list, "Token not specified\r\n");
	}

	// If any were invalid, display all errors
	if (!$valid) {
		$html = $c_ExtraFunc->prettyHtmlFeedback("Error");
		// Concattenate each error into the html
		foreach ($error_list as $err) {
			$html .= "<p>{$err}</p>";
		}
		$html .= "<p>何してるの？</p>";
		echo $html;
		die();
	}

	$req_res = $c_UserAccess->verify_email($_GET["email"], $_GET["token"]);
	$html = $c_ExtraFunc->prettyHtmlFeedback("Email Verification");
	echo $html.$req_res["message"];
	die();
}
// -----------------------------------------------------------------------------
// !SECTION verify_email
// -----------------------------------------------------------------------------



// -----------------------------------------------------------------------------
// SECTION email_change_step1
// -----------------------------------------------------------------------------
if ($_GET["request_type"] === "email_change_step1") {
	$valid = true;
	$error_list = [];

	// Check if valid
	if (!isset($_GET["email"])) {
		$valid = false;
		array_push($error_list, "Email not specified\r\n");
	}

	if (!isset($_GET["new_email"])) {
		$valid = false;
		array_push($error_list, "New email not specified\r\n");
	}
	
	if (!isset($_GET["token"])) {
		$valid = false;
		array_push($error_list, "Token not specified\r\n");
	}

	// If any were invalid, display all errors
	if (!$valid) {
		$html = $c_ExtraFunc->prettyHtmlFeedback("Error");
		// Concattenate each error into the html
		foreach ($error_list as $err) {
			$html .= "<p>{$err}</p>";
		}
		$html .= "<p>何してるの？</p>";
		echo $html;
		die();
	}

	$req_res = $c_UserEmailChange->verify_new_email_step1($_GET["email"], $_GET["new_email"], $_GET["token"]);
	$html = $c_ExtraFunc->prettyHtmlFeedback("Email Change Verification Step 1");
	echo json_encode($req_res);
	echo $html;
	die();
}
// -----------------------------------------------------------------------------
// !SECTION email_change_step1
// -----------------------------------------------------------------------------



// -----------------------------------------------------------------------------
// SECTION email_change_step2
// -----------------------------------------------------------------------------
if ($_GET["request_type"] === "email_change_step2") {
	$valid = true;
	$error_list = [];

	// Check if valid
	if (!isset($_GET["email"])) {
		$valid = false;
		array_push($error_list, "Email not specified\r\n");
	}

	if (!isset($_GET["new_email"])) {
		$valid = false;
		array_push($error_list, "New email not specified\r\n");
	}
	
	if (!isset($_GET["token"])) {
		$valid = false;
		array_push($error_list, "Token not specified\r\n");
	}

	// If any were invalid, display all errors
	if (!$valid) {
		$html = $c_ExtraFunc->prettyHtmlFeedback("Error");
		// Concattenate each error into the html
		foreach ($error_list as $err) {
			$html .= "<p>{$err}</p>";
		}
		$html .= "<p>何してるの？</p>";
		echo $html;
		die();
	}

	$req_res = $c_UserEmailChange->verify_new_email_step2($_GET["email"], $_GET["new_email"], $_GET["token"]);
	$html = $c_ExtraFunc->prettyHtmlFeedback("Email Change Verification Step 2");
	echo json_encode($req_res);
	echo $html;
	die();
}
// -----------------------------------------------------------------------------
// !SECTION email_change_step2
// -----------------------------------------------------------------------------



// -----------------------------------------------------------------------------
// SECTION email_password_reset
// -----------------------------------------------------------------------------
// Using GET because it's URL formatted
if ($_GET["request_type"] === "email_password_reset") {
	$valid = true;
	$error_list = [];

	// Check if valid
	if (!isset($_GET["email"])) {
		$valid = false;
		array_push($error_list, "Email not specified\r\n");
	}

	if (!isset($_GET["token"])) {
		$valid = false;
		array_push($error_list, "Token not specified\r\n");
	}

	// If any were invalid, display all errors
	if (!$valid) {
		$html = $c_ExtraFunc->prettyHtmlFeedback("Error");
		// Concattenate each error into the html
		foreach ($error_list as $err) {
			$html .= "<p>{$err}</p>";
		}
		$html .= "<p>何してるの？</p>";
		echo $html;
		die();
	}
	// -------------------------------------------------------------------------
	// TODO: Actually code this
	// -------------------------------------------------------------------------
	// $reset_password = new UserAuth($dbconn);
	// $verify_email_result = $verify_email->verify_email($_GET["email"], $_GET["token"]);
	// $html = prettyHtmlFeedback("Email Verification");
	// echo $verify_email_result."asdf";
	echo $c_ExtraFunc->prettyHtmlFeedback("Page under construction!", "Please use the token within the App UI for now");
	die();
}
// -----------------------------------------------------------------------------
// !SECTION email_password_reset
// -----------------------------------------------------------------------------



// -----------------------------------------------------------------------------
// !SECTION UserAuth
// -----------------------------------------------------------------------------




// -----------------------------------------------------------------------------
// SECTION FeedParser
// -----------------------------------------------------------------------------



// -----------------------------------------------------------------------------
// SECTION test_rss_parse_all
// -----------------------------------------------------------------------------
if ($_GET["request_type"] === "test_rss_parse_all") {
	$req_res = $c_FeedParser->parse_all_feeds();

	echo $req_res;
	die();
}
// -----------------------------------------------------------------------------
// !SECTION test_rss_parse_all
// -----------------------------------------------------------------------------



// -----------------------------------------------------------------------------
// !SECTION FeedParser
// -----------------------------------------------------------------------------
$html = $c_ExtraFunc->prettyHtmlFeedback("Error");
$html .= "<p>Request type specified does not match available request types</p>";
$html .= "<p>何してるの？</p>";
echo $html;
