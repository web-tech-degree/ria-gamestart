<?php

namespace User;

/**
 * @brief This handles fetching and changing user details.
 */
class UserDetails
{
	// -------------------------------------------------------------------------
	// SECTION Dependencies
	// -------------------------------------------------------------------------
	// private \PDO $dbconn;
	// private \User\UserAccess $UserAccess;
	// private \User\UserEmailChange $UserEmailChange;
	// private \Misc\ImageCrop $ImageCrop;
	private $pfp_dir = "./pfp_images/";
	private $dbconn;
	private $UserAccess;
	private $UserEmailChange;
	private $ImageCrop;
	// -------------------------------------------------------------------------
	// !SECTION
	// -------------------------------------------------------------------------



	/**
	 * __construct triggered when constructing, does dependency injection
	 *
	 * These are then bound to the class
	 *
	 * Requirements:
	 * - dbconn:			instance of dbconn from _dbconnect.php
	 * - UserAccess:		instance of \User\UserAccess for some basic functions
	 * - UserEmailChange:	instance of \User\UserEmailChange for changing the user's email
	 * - ImageCrop:			instance of \Misc\ImageCrop for cropping images.
	 *
	 * @param  \PDO $dbconn
	 * @param  \User\UserAccess $UserAccess
	 * @return void
	 */
	public function __construct(\PDO $dbconn, \User\UserAccess $UserAccess, \User\UserEmailChange $UserEmailChange, \Misc\ImageCrop $ImageCrop)
	{
		$this->dbconn = $dbconn;
		$this->UserAccess = $UserAccess;
		$this->UserEmailChange = $UserEmailChange;
		$this->ImageCrop = $ImageCrop;
	}



	// -------------------------------------------------------------------------
	// SECTION API
	// -------------------------------------------------------------------------
	/**
	 * This simply tries to get the user's avatar, returning either their avatar or
	 * a default image if they do not have one or are not signed in.
	 *
	 * @param  mixed $p_auth_token
	 * @return void
	 */
	public function get_user_pfp($p_auth_token)
	{
		$check_token = $this->UserAccess->UserTokenAuth->check_user_token($p_auth_token);
		// If we couldn't verify the token, we give back default pfp image
		if ($check_token["message"] !== "token_valid") {
			return array(
				"message" => "signed_out_default",
				"pfp_location" => $this->pfp_dir . "default.png",
			);
		}

		// Otherwise, we try to get the user's avatar
		$user_details = $this->UserAccess->db_get_user_details_by_email($check_token["data"]->email);

		// If the user does not have an avatar, we set the image to the default
		if ($user_details["avatar"] === null) {
			return array(
				"message" => "signed_in_default",
				"pfp_location" => $this->pfp_dir . "default.png",
			);
		}

		// If we're here, it means the user does have an avatar
		return array(
			"message" => "signed_in_actual",
			"pfp_location" => $this->pfp_dir . $user_details["avatar"],
		);
	}



	/**
	 * This gets the logged in user's profile details if their JWT token is valid.
	 *
	 * @param  string $p_auth_token
	 * @return array
	 */
	public function get_user_profile_details($p_auth_token)
	{
		$check_token = $this->UserAccess->UserTokenAuth->check_user_token($p_auth_token);

		// If we couldn't verify the token, we don't continue
		if ($check_token["message"] !== "token_valid") {
			return array(
				"message" => "token_validation_error",
			);
		}

		// We get the user's details via their email from the JWT
		$user_details = $this->UserAccess->db_get_user_details_by_email($check_token["data"]->email);

		// If the user does not have an avatar, we set the image to the default
		if ($user_details["avatar"] === null) {
			$user_details["avatar"] = "default.png";
		}

		// Here we actually format the data we need in the UI
		// TODO: Get favourited feed info
		$ret_data = array(
			"username" => $user_details["username"],
			"email" => $user_details["email"],
			"pfp_location" => $this->pfp_dir . $user_details["avatar"],
			"fav_feeds" => "TO_BE_DONE",
		);

		return array(
			"message" => "fetch_success",
			"details" => $ret_data,
			// "wtf" => $check_token
		);
	}



	/**
	 * This updates the user's profile details with the ones provided if their JWT token is valid.
	 * This also checks that no existing user already has the selected username.
	 *
	 * @param  string $p_auth_token
	 * @param  string $p_username
	 * @param  string $p_email
	 * @param  file $p_pfp_image_data (Optional, default is null)
	 * @return array
	 */
	public function profile_update_user_details($p_auth_token, $p_new_username, $p_new_email, $p_new_pfp_image_data = null)
	{
		$check_token = $this->UserAccess->UserTokenAuth->check_user_token($p_auth_token);

		// If we couldn't verify the token, we don't continue
		if ($check_token["message"] !== "token_valid") {
			return array(
				"message" => "token_validation_error",
			);
		}

		$current_details = $this->UserAccess->db_get_user_details_by_email($check_token["data"]->email);

		// First we try to set their username if it's changed
		if ($current_details["username"] !== $p_new_username) {
			$update_details = $this->dbconn->prepare('UPDATE user SET username = ? WHERE user_id = ?');
			try {
				$update_details->execute([$p_new_username, $check_token["data"]->id]);
			} catch (\PDOException $e) {
				// If the error returned is 1062, meaning that it's a duplicate entry
				// tell the user that
				if ($e->errorInfo[1] == 1062) {
					return array(
						"message" => "username_taken"
					);
				}
			}
		}

		// Then, if the user changed their email and that email is not already in use, we'll make a request to change it
		if ($current_details["email"] !== $p_new_email) {
			$email_used = $this->UserAccess->db_get_user_details_by_email($p_new_email);
			if (!$email_used) {
				$x = $this->UserEmailChange->send_email_change_step1(
					$current_details["user_id"],
					$current_details["email"],
					$p_new_email,
				);
				// If email failed
				if (!$x) {
					return array(
						"message" => "email_change_failed",
					);
				}
			}
		}

		// Then, if image data isn't null, aka we got an image, we update their pfp
		if ($p_new_pfp_image_data !== null) {
			// Get the file extension
			$ext = pathinfo($_FILES["pfp_image"]["name"], PATHINFO_EXTENSION);

			// Make sure we only get PNG/JPGs
			if ($ext !== "png" &&
				$ext !== "PNG" &&
				$ext !== "jpg" &&
				$ext !== "JPG" &&
				$ext !== "jpeg" &&
				$ext !== "JPEG"
			) {
				return array(
					"message" => "image_failed",
					"ext_is" => $ext
				);
			}

			// We just create a random 32 char length string to suffix to the image name
			// so the result will be user_id + rand_img_suffix
			// This is so that you can't simply scrape profile images via user IDs
			// as each user ID will have a random "hash" next to it
			$rand_img_suffix = $this->UserAccess->create_new_token();

			// We concat the location, the user's ID, and the random string (int).
			$new_path_no_dir = "user" . $check_token["data"]->id . "_" . $rand_img_suffix . "." . $ext;
			$new_path_full = $this->pfp_dir . $new_path_no_dir;
			// We crop the image, copying it from the temporary folder into the new full path
			$imgc = $this->ImageCrop->crop_image_file(200, 200, $_FILES["pfp_image"]["tmp_name"], $new_path_full, 60);

			// And we check cropping was successful
			if (!$imgc) {
				return array(
					"message" => "image_failed"
				);
			}

			// Then we update the user's avatar field to point to the new file
			$update_details = $this->dbconn->prepare('UPDATE user SET avatar = ? WHERE user_id = ?');
			// We insert into the DB without the dir so that it's easier to change the dir for everyone
			// My thought is that if we stored the prefix file dir for every user, we'd have to potentially
			// update a million fields if we wanted to change the directory
			$update_details->execute([$new_path_no_dir, $check_token["data"]->id]);

			// If updating the avatar location fails, we delete the avatar image we just moved
			if (!$update_details) {
				if (file_exists($new_path_full)) {
					unlink($new_path_full);
				}
				return array(
					"message" => "image_failed"
				);
			}

			// After success, we delete the old image only if they actually had one
			if ($current_details["avatar"] !== null) {
				$old_avatar_dir = $this->pfp_dir . $current_details["avatar"];
				
				if (file_exists($old_avatar_dir)) {
					unlink($old_avatar_dir);
				}
			}

			return array(
				"message" => "update_success"
			);
		} else {
			// We also return success here as the user didn't change their pfp
			// TODO: We could differentiate here so that, when re-fetching the details
			// in the client, we avoid fetching the pfp because the client already has it.
			return array(
				"message" => "update_success"
			);
		}
	}
	// -------------------------------------------------------------------------
	// !SECTION API
	// -------------------------------------------------------------------------
}
