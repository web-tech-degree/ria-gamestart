<?php

namespace User;

/**
 * @brief This handles JWT authentication for the user
 */
class UserTokenAuth
{
	// -------------------------------------------------------------------------
	// SECTION Dependencies
	// -------------------------------------------------------------------------
	/**
	 * dbconn private variable, so that only the class itself can access it, used to store the
	 * connection to the database
	 *
	 * @var mixed
	 */
	// private \Firebase\JWT\JWT $jwt;
	// private \Conf\JwtParams $jwt_params;
	private $jwt;
	private $jwt_params;
	// -------------------------------------------------------------------------
	// !SECTION
	// -------------------------------------------------------------------------



	/**
	 * __construct triggered when constructing, does dependency injection
	 *
	 * These are then bound to the class
	 *
	 * Requirements:
	 * - jwt:			instance of \Firebase\JWT\JWT from library
	 * - jwt_params:	instance of \Conf\JwtParams from library
	 *
	 * @param  \Firebase\JWT\JWT $jwt
	 * @param  \Conf\JwtParams $jwt_params
	 * @return void
	 */
	public function __construct(\Firebase\JWT\JWT $jwt, \Conf\JwtParams $jwt_params)
	{
		$this->jwt = $jwt;
		$this->jwt_params = $jwt_params;
	}



	/**
	 * This checks a user's JWT authentication token. It is supposed to be referenced by other classes,
	 * such as in the submit_comment() method in \Post\PostDetails. This is done to keep the implementation
	 * consistent and avoid writing too much redundant code.
	 *
	 * @param  string $p_auth_token
	 * @return array
	 */
	public function check_user_token($p_auth_token)
	{
		// if decode succeed, show user details
		try {
			// decode jwt
			$decoded = $this->jwt::decode($p_auth_token, $this->jwt_params->secret_key, array('HS256'));
	 
			// set response code
			http_response_code(200);
	 
			// show user details
			return array(
				"message" => "token_valid",
				"data" => $decoded->data
			);
		} catch (\Exception $e) {
			return array(
				"message" => "token_validation_error",
			);
		}
	}



	/**
	 * This creates a JWT authentication token for the user. This is meant to be referenced in other
	 * classes that require it, such as \User\UserAccess, but it is separated here into this file
	 * specifically so that the implementation is consistent everywhere.
	 *
	 * @return void
	 */
	public function create_user_token(
		$p_user_id,
		$p_user_username,
		$p_user_email,
		$p_user_is_emailverified,
		$p_user_level
	) {
		
		$auth_token_data = array(
			"alg" => $this->jwt_params->alg,
			"iss" => $this->jwt_params->issuer,
			"aud" => $this->jwt_params->audience,
			"iat" => $this->jwt_params->issue_date,
			"nbf" => $this->jwt_params->not_before,
			"exp" => $this->jwt_params->expire_date,
			"data" => array(
				"id" => $p_user_id,
				"username" => $p_user_username,
				"email" => $p_user_email,
				"isEmailVerified" => $p_user_is_emailverified,
				"userLevel" => $p_user_level
			)
		);

		// Encode the auth token using the specified data
		$jwt_auth_token = $this->jwt::encode($auth_token_data, $this->jwt_params->secret_key);

		return $jwt_auth_token;
	}
}
