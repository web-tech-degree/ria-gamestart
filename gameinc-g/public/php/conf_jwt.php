<?php

namespace Conf;

/**
 * @brief This is used to set parameters for JWT
 */
class JwtParams
{
	public $alg = "YOUR_SECRET_KEY";
	// Details for the JWT token
	public $secret_key = "YOUR_SECRET_KEY";
	public $issuer = "gameinc-g";
	public $audience = "THE_AUDIENCE";
	// issued at
	public $issue_date;
	//not before in seconds
	// $not_before = $issue_date + 10;
	public $not_before;
	// expire time in seconds
	public $expire_date;
	
	/**
	 * @brief This constructs this class. This needs to be done because we can't assign
	 * the values using other values within the main body.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->issue_date = time();
		$this->not_before = $this->issue_date - 5;
		$this->expire_date = $this->issue_date + 6000;
	}
}
