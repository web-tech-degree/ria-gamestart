<?php

namespace Conf;

/**
 * @brief This is used to set parameters for SMTP connections, and some other stuff.
 * Note that this file is blank in the repository, so you will have to create it yourself.
 */
class General
{
	public $smtp_username = "email@domain.com";
	public $smtp_password = "password";
	public $server_url = "https://gamestart.aterlux.com";
	public $server_api_url = "https://gamestart.aterlux.com/php";
}