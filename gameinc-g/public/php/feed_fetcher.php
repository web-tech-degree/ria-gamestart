<?php

namespace Feed;

use User\UserTokenAuth;

/**
 * @brief This is used for fetching content in feeds
 */
class FeedFetcher
{
	// -------------------------------------------------------------------------
	// SECTION Dependencies
	// -------------------------------------------------------------------------
	/**
	 * dbconn private variable, so that only the class itself can access it, used to store the
	 * connection to the database
	 *
	 * @var mixed
	 */
	// private \PDO $dbconn;
	// public \User\UserTokenAuth $UserTokenAuth;
	// private \Post\PostVotes $PostVotes;
	// private \Post\PostBookmarks $PostBookmarks;
	// private \User\SourceFav $UserSourceFav;
	// private \PicoFeed\Reader\Reader $picofeed;
	// private \Misc\ExtraFunc $extrafunc;
	private $dbconn;
	public $UserTokenAuth;
	private $PostVotes;
	private $PostBookmarks;
	private $UserSourceFav;
	private $picofeed;
	private $extrafunc;
	private $post_return_limit;
	private $post_return_limit_search;
	// -------------------------------------------------------------------------
	// !SECTION
	// -------------------------------------------------------------------------



	/**
	 * __construct triggered when constructing, does dependency injection
	 *
	 * These are then bound to the class
	 *
	 * Requirements:
	 * - dbconn:		instance of dbconn from _dbconnect.php
	 * - UserTokenAuth:	instance of \User\UserTokenAuth for authenticating the user's token
	 * - PostVotes:		instance of \Post\PostVotes for getting post vote info and casting votes
	 * - PostBookmarks:	instance of \Post\PostBookmarks for getting post bookmark info etc.
	 * - UserSourceFav:	instance of \User\SourceFav for getting source favourite info etc.
	 * - picofeed:		instance of \PicoFeed\Reader\Reader from library
	 * - extrafunc:		instance of \Misc\ExtraFunc from _functions.php
	 *
	 * @param  \PDO $dbconn
	 * @param  \User\UserTokenAuth $UserTokenAuth
	 * @param  \Post\PostVotes $PostVotes
	 * @param  \Post\PostBookmarks $PostBookmarks
	 * @param  \User\SourceFav $UserSourceFav;
	 * @param  \PicoFeed\Reader\Reader $picofeed
	 * @param  \Misc\ExtraFunc $extrafunc
	 * @return void
	 */
	public function __construct(
		\PDO $dbconn,
		\User\UserTokenAuth $UserTokenAuth,
		\Post\PostVotes $PostVotes,
		\Post\PostBookmarks $PostBookmarks,
		\User\SourceFav $UserSourceFav,
		\PicoFeed\Reader\Reader $picofeed,
		\Misc\ExtraFunc $extrafunc
	) {
		$this->dbconn = $dbconn;
		$this->UserTokenAuth = $UserTokenAuth;
		$this->PostVotes = $PostVotes;
		$this->PostBookmarks = $PostBookmarks;
		$this->UserSourceFav = $UserSourceFav;
		$this->picofeed = $picofeed;
		$this->extrafunc = $extrafunc;
		$this->post_return_limit = 5;
		// We allow for more posts to be returned on search
		$this->post_return_limit_search = 10;
	}



	/**
	 * This gets the details of all the feeds that are available, aka feeds that have at least
	 * one source attached to them.
	 *
	 * @return array
	 */
	public function feed_get_available()
	{
		// This gets all feeds which actually have at least one source attached to them
		$feeds = $this->dbconn->prepare('SELECT feed.feed_id, feed.name
		FROM feed
		WHERE EXISTS
		(
			SELECT 1
			FROM
			feed_source
			WHERE
			(
				feed_source.feed_id = feed.feed_id
			)
		)');
		$feeds->execute();
		$feeds_result = $feeds->fetchAll(\PDO::FETCH_ASSOC);

		return array(
			"message" => "fetch_success",
			"feeds" => $feeds_result,
		);
	}



	/**
	 * This gets all of the user's favourited sources
	 *
	 * @param  string $p_user_token
	 * @return array
	 */
	public function fetch_user_favourite_sources($p_user_token)
	{
		// We try to get the user token, so we'll know if they're signed in.
		$token_res = $this->UserTokenAuth->check_user_token($p_user_token);
		
		if ($token_res["message"] !== "token_valid") {
			return array(
				"message" => "token_validation_error",
			);
		}

		$sources = $this->dbconn->prepare('SELECT source.source_id, source.url, source.name, source.avatar
		FROM source
		INNER JOIN user_favsource
			ON source.source_id = user_favsource.source_id
		WHERE user_favsource.user_id = ?
		');
		$sources->execute([$token_res["data"]->id]);
		$sources_result = $sources->fetchAll(\PDO::FETCH_ASSOC);

		return array(
			"message" => "fetch_success",
			"sources" => $sources_result,
		);
	}



	/**
	 * This is used to get all of the info for the specified source because when a FeedFavourites is loaded,
	 * it does not receive any of the name details.
	 *
	 * @param  string $p_source_id
	 * @return array
	 */
	public function feed_get_source_info($p_source_id)
	{
		$info = $this->dbconn->prepare('SELECT source.source_id, source.url, source.name, source.avatar
		FROM source
		WHERE source.source_id = ?
		');
		$info->execute([$p_source_id]);
		$info_result = $info->fetch(\PDO::FETCH_ASSOC);

		return array(
			"message" => "fetch_success",
			"info" => $info_result,
		);
	}



	/**
	 * This simply checks if there are any new posts available in the selected feed
	 *
	 * @param  int $p_feed_id
	 * @param  int $p_client_highest_id
	 * @return array
	 */
	public function check_new_posts_available_by_feed($p_feed_id, $p_client_highest_id)
	{
		// Get all posts via their linked feed name
		// as long as they are larger than the highest ID
		// We also order descending by highest post id
		$post_fetch = $this->dbconn->prepare('SELECT
			1
		FROM postitem
			INNER JOIN source_postitem
			ON postitem.postitem_id = source_postitem.postitem_id
			INNER JOIN feed_source
			ON source_postitem.source_id = feed_source.source_id
			INNER JOIN feed
			ON feed_source.feed_id = feed.feed_id
		WHERE feed.feed_id = ? AND postitem.postitem_id > ?');
		$post_fetch->execute([$p_feed_id, $p_client_highest_id]);

		// This tells the user if no more posts were found
		if ($post_fetch->rowCount() < 1) {
			return array(
				"message" => "no_new_posts",
			);
			die();
		}

		return array(
			"message" => "new_posts_available",
			"count" => $post_fetch->rowCount(),
		);
	}



	/**
	 * This simply checks if there are any new posts available in the selected source
	 *
	 * @param  int $p_source_id
	 * @param  int $p_client_highest_id
	 * @return array
	 */
	public function check_new_posts_available_by_source($p_source_id, $p_client_highest_id)
	{
		// Get all posts via their linked feed name
		// as long as they are larger than the highest ID
		// We also order descending by highest post id
		$post_fetch = $this->dbconn->prepare('SELECT
			1
		FROM postitem
			INNER JOIN source_postitem
			ON postitem.postitem_id = source_postitem.postitem_id
			INNER JOIN feed_source
			ON source_postitem.source_id = feed_source.source_id
			INNER JOIN source
			ON source_postitem.source_id = source.source_id
		WHERE source.source_id = ? AND postitem.postitem_id > ?');
		$post_fetch->execute([$p_source_id, $p_client_highest_id]);

		// This tells the user if no more posts were found
		if ($post_fetch->rowCount() < 1) {
			return array(
				"message" => "no_new_posts",
			);
			die();
		}

		return array(
			"message" => "new_posts_available",
			"count" => $post_fetch->rowCount(),
		);
	}



	/**
	 * This gets the latest posts within the specified feed, unless the client's latest post
	 * is more than a specified number of posts behind. This is done so that we don't send a client
	 * a ridiculous amount of posts, and instead prompt them to refresh the feed.
	 *
	 * @param  string $p_user_token
	 * @param  int $p_feed_id
	 * @param  int $p_client_highest_id
	 * @return array
	 */
	public function get_latest_posts_by_feed($p_user_token, $p_feed_id, $p_client_highest_id)
	{
		// Get all posts via their linked feed name
		// as long as they are larger than the highest ID
		// We also order descending by highest post id
		$post_fetch = $this->dbconn->prepare('SELECT
			postitem.postitem_id,
			postitem.hash,
			postitem.title,
			postitem.url,
			postitem.content,
			postitem.date,
			postitem.author,
			source.source_id,
			source.url as source_url,
			source.name as source_name,
			source.avatar as source_avatar
		FROM postitem
			INNER JOIN source_postitem
			ON postitem.postitem_id = source_postitem.postitem_id
			INNER JOIN feed_source
			ON source_postitem.source_id = feed_source.source_id
			INNER JOIN source
			ON source_postitem.source_id = source.source_id
			INNER JOIN feed
			ON feed_source.feed_id = feed.feed_id
		WHERE feed.feed_id = ? AND postitem.postitem_id > ?
		ORDER BY postitem.postitem_id DESC');

		$post_fetch->execute([$p_feed_id, $p_client_highest_id]);
		// This tells the user if no more posts were found
		if ($post_fetch->rowCount() < 1) {
			return array(
				"message" => "no_more_posts"
			);
			die();
		}

		$proper_result = $this->fetch_to_count_limit_and_truncate_and_grab_vote_status($p_user_token, $post_fetch, $this->post_return_limit);

		return array(
			"message" => "fetch_success",
			"posts" => $proper_result["posts"],
			"arrangement" => $proper_result["arrangement"],
		);
	}



	/**
	 * This gets the latest posts within the specified source, unless the client's latest post
	 * is more than a specified number of posts behind. This is done so that we don't send a client
	 * a ridiculous amount of posts, and instead prompt them to refresh the feed.
	 *
	 * @param  string $p_user_token
	 * @param  int $p_source_id
	 * @param  int $p_client_highest_id
	 * @return array
	 */
	public function get_latest_posts_by_source($p_user_token, $p_source_id, $p_client_highest_id)
	{
		// Get all posts via their linked feed name
		// as long as they are larger than the highest ID
		// We also order descending by highest post id
		$post_fetch = $this->dbconn->prepare('SELECT
			postitem.postitem_id,
			postitem.hash,
			postitem.title,
			postitem.url,
			postitem.content,
			postitem.date,
			postitem.author,
			source.source_id,
			source.url as source_url,
			source.name as source_name,
			source.avatar as source_avatar
		FROM postitem
			INNER JOIN source_postitem
			ON postitem.postitem_id = source_postitem.postitem_id
			INNER JOIN feed_source
			ON source_postitem.source_id = feed_source.source_id
			INNER JOIN source
			ON source_postitem.source_id = source.source_id
		WHERE source.source_id = ? AND postitem.postitem_id > ?
		ORDER BY postitem.postitem_id DESC');

		$post_fetch->execute([$p_source_id, $p_client_highest_id]);

		// This tells the user if no more posts were found
		if ($post_fetch->rowCount() < 1) {
			return array(
				"message" => "no_more_posts"
			);
			die();
		}

		$proper_result = $this->fetch_to_count_limit_and_truncate_and_grab_vote_status($p_user_token, $post_fetch, $this->post_return_limit);

		return array(
			"message" => "fetch_success",
			"posts" => $proper_result["posts"],
			"arrangement" => $proper_result["arrangement"],
		);
	}



	/**
	 * This gets a specified number of posts from the specified feed. This gets posts *older*
	 * than the specified post, which shall be the lowest (oldest) post the client has.
	 *
	 * @param  string $p_user_token
	 * @param  int $p_feed_id
	 * @param  int $p_client_lowest_id
	 * @return array
	 */
	public function get_previous_posts_by_feed($p_user_token, $p_feed_id, $p_client_lowest_id)
	{
		// Get any posts whose ID is lower than the client's lowest, and where either the title or content
		// includes whatevr the $p_category is
		// We also order descending by highest post id
		$post_fetch = $this->dbconn->prepare('SELECT
			postitem.postitem_id,
			postitem.hash,
			postitem.title,
			postitem.url,
			postitem.content,
			postitem.date,
			postitem.author,
			source.source_id,
			source.url as source_url,
			source.name as source_name,
			source.avatar as source_avatar
		FROM postitem
			INNER JOIN source_postitem
			ON postitem.postitem_id = source_postitem.postitem_id
			INNER JOIN feed_source
			ON source_postitem.source_id = feed_source.source_id
			INNER JOIN source
			ON source_postitem.source_id = source.source_id
			INNER JOIN feed
			ON feed_source.feed_id = feed.feed_id
		WHERE feed.feed_id = ? AND postitem.postitem_id < ?
		ORDER BY postitem.postitem_id DESC');

		$post_fetch->execute([$p_feed_id, $p_client_lowest_id]);

		// This tells the user if no more posts were found
		if ($post_fetch->rowCount() < 1) {
			return array(
				"message" => "no_more_posts"
			);
			die();
		}

		$proper_result = $this->fetch_to_count_limit_and_truncate_and_grab_vote_status($p_user_token, $post_fetch, $this->post_return_limit);

		return array(
			"message" => "fetch_success",
			"posts" => $proper_result["posts"],
		);
	}



	/**
	 * This gets a specified number of posts from the specified feed. This gets posts *older*
	 * than the specified post, which shall be the lowest (oldest) post the client has.
	 *
	 * @param  string $p_user_token
	 * @param  int $p_source_id
	 * @param  int $p_client_lowest_id
	 * @return array
	 */
	public function get_previous_posts_by_source($p_user_token, $p_source_id, $p_client_lowest_id)
	{
		// Get any posts whose ID is lower than the client's lowest, and where either the title or content
		// includes whatevr the $p_category is
		// We also order descending by highest post id
		$post_fetch = $this->dbconn->prepare('SELECT
			postitem.postitem_id,
			postitem.hash,
			postitem.title,
			postitem.url,
			postitem.content,
			postitem.date,
			postitem.author,
			source.source_id,
			source.url as source_url,
			source.name as source_name,
			source.avatar as source_avatar
		FROM postitem
			INNER JOIN source_postitem
			ON postitem.postitem_id = source_postitem.postitem_id
			INNER JOIN feed_source
			ON source_postitem.source_id = feed_source.source_id
			INNER JOIN source
			ON source_postitem.source_id = source.source_id
		WHERE source.source_id = ? AND postitem.postitem_id < ?
		ORDER BY postitem.postitem_id DESC');

		$post_fetch->execute([$p_source_id, $p_client_lowest_id]);

		// This tells the user if no more posts were found
		if ($post_fetch->rowCount() < 1) {
			return array(
				"message" => "no_more_posts"
			);
			die();
		}

		$proper_result = $this->fetch_to_count_limit_and_truncate_and_grab_vote_status($p_user_token, $post_fetch, $this->post_return_limit);

		return array(
			"message" => "fetch_success",
			"posts" => $proper_result["posts"],
		);
	}



	/**
	 * This gets any posts, within the context of a feed, that match the provided
	 * search query either in their title or content.
	 *
	 * @param  string $p_user_token
	 * @param  int $p_feed_id
	 * @param  string $p_search_query
	 * @return array
	 */
	public function get_latest_posts_in_feed_by_search($p_user_token, $p_feed_id, $p_search_query)
	{
		// This gets any posts in the user's selected feed where their search query matches either content or title
		$post_fetch = $this->dbconn->prepare('SELECT
			postitem.postitem_id,
			postitem.hash,
			postitem.title,
			postitem.url,
			postitem.content,
			postitem.date,
			postitem.author,
			source.source_id,
			source.url as source_url,
			source.name as source_name,
			source.avatar as source_avatar
		FROM postitem
			INNER JOIN source_postitem
			ON postitem.postitem_id = source_postitem.postitem_id
			INNER JOIN feed_source
			ON source_postitem.source_id = feed_source.source_id
			INNER JOIN source
			ON source_postitem.source_id = source.source_id
			INNER JOIN feed
			ON feed_source.feed_id = feed.feed_id
		WHERE feed.feed_id = ? AND (
				postitem.title LIKE ?
				OR postitem.content LIKE ?
			)
		ORDER BY postitem.postitem_id DESC');

		$post_fetch->execute([$p_feed_id, "%$p_search_query%", "%$p_search_query%"]);

		// This tells the user if no more posts were found
		if ($post_fetch->rowCount() < 1) {
			return array(
				"message" => "no_more_posts"
			);
			die();
		}

		$proper_result = $this->fetch_to_count_limit_and_truncate_and_grab_vote_status($p_user_token, $post_fetch, $this->post_return_limit_search);

		return array(
			"message" => "fetch_success",
			"posts" => $proper_result["posts"],
			"arrangement" => $proper_result["arrangement"],
		);
	}



	/**
	 * This gets any posts, within the context of a feed, that match the provided
	 * search query either in their title or content and are older than the client's oldest/lowest id.
	 *
	 * @param  string $p_user_token
	 * @param  int $p_feed_id
	 * @param  string $p_search_query
	 * @param  int $p_client_lowest_id
	 * @return array
	 */
	public function get_previous_posts_in_feed_by_search($p_user_token, $p_feed_id, $p_search_query, $p_client_lowest_id)
	{
		// This gets any posts in the user's selected feed where their search query matches either content or title
		// and where the post ID is lower than their lowest
		$post_fetch = $this->dbconn->prepare('SELECT
			postitem.postitem_id,
			postitem.hash,
			postitem.title,
			postitem.url,
			postitem.content,
			postitem.date,
			postitem.author,
			source.url as source_url,
			source.name as source_name,
			source.avatar as source_avatar
		FROM postitem
			INNER JOIN source_postitem
			ON postitem.postitem_id = source_postitem.postitem_id
			INNER JOIN feed_source
			ON source_postitem.source_id = feed_source.source_id
			INNER JOIN source
			ON source_postitem.source_id = source.source_id
			INNER JOIN feed
			ON feed_source.feed_id = feed.feed_id
		WHERE feed.feed_id = ? AND postitem.postitem_id < ? AND
			(
				postitem.title LIKE ?
				OR postitem.content LIKE ?
			)
		ORDER BY postitem.postitem_id DESC');

		$post_fetch->execute([$p_feed_id, $p_client_lowest_id, "%$p_search_query%", "%$p_search_query%"]);

		// If no rows
		if ($post_fetch->rowCount() < 1) {
			return array(
				"message" => "no_more_posts"
			);
		}

		$proper_result = $this->fetch_to_count_limit_and_truncate_and_grab_vote_status($p_user_token, $post_fetch, $this->post_return_limit_search);

		// This tells the user if no more posts were found
		if ($post_fetch->rowCount() < 1) {
			return array(
				"message" => "no_more_posts"
			);
			die();
		}
		return array(
			"message" => "fetch_success",
			"posts" => $proper_result["posts"],
		);
	}



	/**
	 * This mimmicks the functionaltiy of the normal fetches within the feed context, but it only
	 * fetches a singular item. This is implemented as a re-fetch of details, used to synchronise
	 * the status of posts when they are interacted with in the Feed.
	 *
	 * We also skip the massive fetch_to_count_limit_and_truncate_and_grab_vote_status function
	 * and do the stuff here manually.
	 *
	 * @param  string $p_user_token
	 * @param  int $p_post_id
	 * @return array
	 */
	public function refetch_post_within_feed($p_user_token, $p_post_id)
	{
		// Get the one post's details
		$post_fetch = $this->dbconn->prepare('SELECT
			postitem.postitem_id,
			postitem.hash,
			postitem.title,
			postitem.url,
			postitem.content,
			postitem.date,
			postitem.author,
			source.source_id,
			source.url as source_url,
			source.name as source_name,
			source.avatar as source_avatar
		FROM postitem
		INNER JOIN source_postitem
			ON postitem.postitem_id = source_postitem.postitem_id
		INNER JOIN source
			ON source_postitem.source_id = source.source_id
		WHERE postitem.postitem_id = ?');

		$post_fetch->execute([$p_post_id]);

		// This shouldn't happen but there's the potential case that
		// a post was deleted while a user was upvoting etc.
		if ($post_fetch->rowCount() < 1) {
			return array(
				"message" => "refetch_fail"
			);
			die();
		}

		$proper_result = $post_fetch->fetch(\PDO::FETCH_ASSOC);

		$proper_result = $this->truncate_title_and_content_single($proper_result);

		$proper_result["total_votes"] = $this->PostVotes->get_vote_totals_for_post($p_post_id);

		// We try to get the user token, so we'll know if they're signed in.
		$token_res = $this->UserTokenAuth->check_user_token($p_user_token);

		// If the user is signed in, we also get their vote/bookmark/fav status
		if ($token_res["message"] === "token_valid") {
			$proper_result["user_vote_status"] = $this->PostVotes->get_user_post_vote_status($p_post_id, $token_res["data"]->id);
			$proper_result["user_bookmark_status"] = $this->PostBookmarks->get_user_post_bookmark_status($p_post_id, $token_res["data"]->id);
			$proper_result["user_fav_source_status"] = $this->UserSourceFav->get_user_source_fav_status($proper_result["source_id"], $token_res["data"]->id);
		}

		return array(
			"message" => "fetch_success",
			"postDetails" => $proper_result,
		);
	}



	/**
	 * This is the function which arbitrarily completes all of the fetching of posts to a limited count,
	 * checking if the number of new posts is reasonable to "add" them (synchronisable) instead of
	 * refreshing (too_far_behind). This also handles truncating the title and content, attributing the total
	 * vote count, and attributing the user's vote status if they happen to be logged in.
	 *
	 * @return void
	 */
	private function fetch_to_count_limit_and_truncate_and_grab_vote_status(
		string $p_user_token,
		\PDOStatement $p_fetch_statement,
		int $p_return_limit
	) {
		$real_ret_count = $p_return_limit;

		$arrangement = "synchronisable";

		// Check that the number of post fetched are not higher than the return limit
		if ($p_fetch_statement->rowCount() > $p_return_limit) {
			$arrangement = "too_far_behind";
		}

		// If there are less matching rows than the amount of rows we actually intended to get,
		// we adjust the limit so we don't try to fetch inexistent data
		if ($p_fetch_statement->rowCount() < $real_ret_count) {
			$real_ret_count = $p_fetch_statement->rowCount();
		}

		// Limit the number of rows will be given to the client per fetch
		$return_result = array();
		
		for ($i = 0; $i < $real_ret_count; $i++) {
			// We push the return_result of the next row based on however many times this loops
			$res = $p_fetch_statement->fetch(\PDO::FETCH_ASSOC);
			// Here we get the user's vote status
			// $res["user_vote_status"] = $this->UserVotes->get_user_post_vote_status($res["postitem_id"]);
			array_push($return_result, $res);
		}

		$trunc_result = $this->truncate_title_and_content_array($return_result);

		$proper_result = $this->PostVotes->concat_vote_totals_for_array_of_posts($trunc_result);

		// We try to get the user token, so we'll know if they're signed in.
		$token_res = $this->UserTokenAuth->check_user_token($p_user_token);

		// If the user is signed in, we also get their bookmark/vote/fav status
		if ($token_res["message"] === "token_valid") {
			$proper_result = $this->PostVotes->concat_user_vote_status_for_array_of_posts($proper_result, $token_res["data"]->id);
			$proper_result = $this->PostBookmarks->concat_user_bookmark_status_for_array_of_posts($proper_result, $token_res["data"]->id);
			$proper_result = $this->UserSourceFav->concat_user_fav_status_for_array_of_posts($proper_result, $token_res["data"]->id);
		}

		return array(
			"posts" => $proper_result,
			"arrangement" => $arrangement,
		);
	}



	/**
	 * This truncates the title and content of the provided posts array to a specified length,
	 * affixing "..." if it's too long.
	 *
	 * @param  mixed $posts
	 * @return void
	 */
	private function truncate_title_and_content_array($posts)
	{
		$post_arr = $posts;
		// This loops over the array... the `=>` is some sort of pass-by-reference
		// wherewith basically we can modify the original array value
		// We do this to overwrite any title or content that's too long
		foreach ($post_arr as $key => $field) {
			$post_arr[$key] = $this->truncate_title_and_content_single($field);
		}
		return $post_arr;
	}



	private function truncate_title_and_content_single($post)
	{
		// See: https://stackoverflow.com/a/11434149
		// This modifies the title of the post within the original array using a ternary operate
		// Basically, if the length of the title is longer than whatever we want, we cut the
		// string at the specified location and affix "..." to the end
		$post["title"] = strlen($post["title"]) > 50 ?
			substr($post["title"], 0, 50) . "..." :
			$post["title"];
		
		// We do the same as above, but for the content
		$post["content"] = strlen($post["content"]) > 200 ?
			substr($post["content"], 0, 200) . "..." :
			$post["content"];

		return $post;
	}
}
