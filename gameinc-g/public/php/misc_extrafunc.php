<?php

namespace Misc;

/**
 * @brief This is just some random additional functions
 */
class ExtraFunc
{
	/**
	 * This is just a function that'll return less ugly text in a HTML.
	 * It'll append every argument given to it as a new paragraph.
	 * NOTE: DO not use this for anything within the app; this is just a
	 * placeholder error because I was bored.
	 */
	public function prettyHtmlError()
	{
		$returnHtml = "<html><title>gameinc-g</title><style> * { font-family: 'arial', sans-serif; } </style><h1>Error</h1>";
		// For every argument passed into this function
		for ($i = 0; $i < func_num_args(); $i++) {
			$returnHtml .= "<p>" . func_get_arg($i) . "</p>";
		}
		
		return $returnHtml;
	}
	public function prettyHtmlFeedback($p_header_text)
	{
		$returnHtml = "<html><title>gameinc-g</title><style> * { font-family: 'arial', sans-serif; } </style><h1>{$p_header_text}</h1>";
		// For all arguments after the first one
		for ($i = 1; $i < func_num_args(); $i++) {
			$returnHtml .= "<p>" . func_get_arg($i) . "</p>";
		}
		return $returnHtml;
	}

	// -------------------------------------------------------------------------
	// TODO: Currently, verifyNotEmptySimple gives PHP errors when a value was not set
	// so it actually needs a rewrite.
	// I think verifyNotEmpty also needs a rewrite.
	// -------------------------------------------------------------------------

	/**
	 * This checks which fields are empty and gives back an array with either true or
	 * false for each argument. If an argument was empty, the respective field in the array will be empty
	 */
	public function verifyNotEmpty()
	{
		// Array to store the result of whether or not the arguments passed into this function
		// are empty. The index of the array will match the order of the functions passed
		$verifiedInputFields = [];
	
		// For every argument passed into this function
		for ($i = 0; $i < func_num_args(); $i++) {
			// If the length is larger than 0, set it as "verified" thus not empty
			if (strlen(func_get_arg($i)) > 0) {
				$verifiedInputFields[$i] = true;
			} else {
				// Otherwise we assume the length is not larger than 0, hence false
				$verifiedInputFields[$i] = false;
			}
		}
	
		return $verifiedInputFields;
	}
	
	/**
	 * This function simply checks if the provided arguments are empty and returns true if none are empty.
	 */
	public function verifyNotEmptySimple()
	{
		$noneAreEmpty = true;
	
		// For every argument passed into this function
		for ($i = 0; $i < func_num_args(); $i++) {
			// If any argument is smaller than 1, set noneAreEmpty to false
			if (strlen(func_get_arg($i)) < 1) {
				$noneAreEmpty = false;
			}
		}
	
		return $noneAreEmpty;
	}




	// /**
	//  * Here we get rid of the upvotes and downvotes from the result, and instead give back
	//  * a number of total votes.
	//  *
	//  * @param  \PDO::FETCH_ASSOC $p_post_item_single_result_assoc
	//  * @return void
	//  */
	// public function convert_votes_to_total_votes($p_post_item_single_result_assoc)
	// {
	// 	$ret = $p_post_item_single_result_assoc;
	// 	$ret["total_votes"] = $p_post_item_single_result_assoc["upvotes"] - $p_post_item_single_result_assoc["downvotes"];
	// 	unset($ret["upvotes"]);
	// 	unset($ret["downvotes"]);
	// 	return $ret;
	// }



	// /**
	//  * Here we get rid of the upvotes and downvotes from the array of results, and instead give back
	//  * a number of total votes.
	//  *
	//  * @param  \PDO::FETCH_ASSOC $p_post_item_array_result_assoc
	//  * @return void
	//  */
	// public function convert_votes_to_total_votes_in_array($p_post_item_array_result_assoc)
	// {
	// 	$post_arr = $p_post_item_array_result_assoc;
	// 	foreach ($post_arr as $key => $field) {
	// 		$post_arr[$key] = $this->convert_votes_to_total_votes($field);
	// 	}
	// 	return $post_arr;
	// }
}
