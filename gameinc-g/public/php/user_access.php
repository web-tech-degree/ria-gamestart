<?php

namespace User;

/**
 * @brief This handles basic account access functionality.
 */
class UserAccess
{
	// -------------------------------------------------------------------------
	// SECTION Dependencies
	// -------------------------------------------------------------------------
	// private \PDO $dbconn;
	// public \User\UserTokenAuth $UserTokenAuth;
	// public \User\UserMailer $UserMailer;
	// private \Conf\GeneralConf $GeneralConf;
	// private \Misc\ExtraFunc $extrafunc;
	private $dbconn;
	public $UserTokenAuth;
	public $UserMailer;
	private $GeneralConf;
	private $extrafunc;
	// -------------------------------------------------------------------------
	// !SECTION
	// -------------------------------------------------------------------------



	/**
	 * __construct triggered when constructing, does dependency injection
	 *
	 * These are then bound to the class
	 *
	 * Requirements:
	 * - dbconn:		instance of dbconn from _dbconnect.php
	 * - UserTokenAuth:	instance of \User\UserTokenAuth for authenticating the user's token
	 * - UserMailer:	instance of \User\UserMailer for sending email to the user
	 * - GeneralConf:	instance of \Conf\GeneralConf for SMTP username and password
	 * - extrafunc:		instance of \Misc\ExtraFunc from _functions.php
	 *
	 * @param  \PDO $dbconn
	 * @param  \User\UserTokenAuth $UserTokenAuth
	 * @param  \User\UserMailer $UserMailer
	 * @param  \Conf\General $GeneralConf
	 * @param  \Misc\ExtraFunc $extrafunc
	 * @return void
	 */
	public function __construct(
		\PDO $dbconn,
		\User\UserTokenAuth $UserTokenAuth,
		\User\UserMailer $UserMailer,
		\Conf\General $GeneralConf,
		\Misc\ExtraFunc $extrafunc
	) {
		$this->dbconn = $dbconn;
		$this->UserTokenAuth = $UserTokenAuth;
		$this->UserMailer = $UserMailer;
		$this->GeneralConf = $GeneralConf;
		$this->extrafunc = $extrafunc;
	}



	// -------------------------------------------------------------------------
	// SECTION API
	// -------------------------------------------------------------------------
	/**
	 * register method which attempts to register a user account if an account with the same name does
	 * not already exist. It also sets the password and account creation dates to the current time/date.
	 *
	 * It also attempts to login the user after registering, which runs the login method in typical operation
	 * but it concatenates the return message with "loginAfterRegister" so that the messages can be
	 * differentiated on the frontend.

	 *
	 * @param  string $p_user_name
	 * @param  string $p_user_email
	 * @param  string $p_user_password
	 * @param  string $p_user_level
	 * @return array
	 */
	public function register($p_user_name, $p_user_email, $p_user_password, $p_user_level)
	{
		// Try to get user field that matches the email
		$sql_existing_user_result = $this->db_get_user_password_by_email($p_user_email);

		// If a field with the email does already exist
		if ($sql_existing_user_result) {
			return array(
				"message"=>"register_email_taken"
			);
		}
		
		// Try to get user field that matches the username
		$sql_existing_user_result = $this->db_get_user_password_by_username($p_user_name);

		// If a field with the username does already exist
		if ($sql_existing_user_result) {
			return array(
				"message"=>"register_username_taken"
			);
		}

		$hashed_password = password_hash($p_user_password, PASSWORD_ARGON2ID, ['memory_cost' => 2048, 'time_cost' => 4, 'threads' => 3]);

		$sql_add_user = $this->dbconn->prepare('
		INSERT into user (
			username,
			avatar,
			email,
			password, 
			password_updated_at,
			user_level, 
			is_emailverified,
			is_disabled,
			account_created_at
		  ) 
		  VALUES 
			(
				?,
				NULL,
				?,
				?,
				now(),
				?,
				?,
				?,
				now()
			)
		  ');

		$sql_add_user->execute([$p_user_name, $p_user_email, $hashed_password, $p_user_level, 0, 0]);

		// Check register was successful
		if (!$sql_add_user) {
			return array(
				"message"=>"register_issue"
			);
		}

		$this->send_email_verification($p_user_email);
		
		// Attempt to login, returning the result so that the caller of the register
		// gets the proper error or data response
		return $this->login($p_user_email, $p_user_password, "loginAfterRegister");
	}


	
	/**
	 * This is used to log in the user if they have verified their email, taking a generated token from
	 * the client and an input token from the user's email, and then it checks against their hashes in the DB.
	 *
	 * @param  string $p_user_email
	 * @param  string $p_user_password
	 * @param  string $p_conf_client_token
	 * @param  string $p_conf_email_token
	 * @return void
	 */
	public function login_2fa_confirm($p_user_email, $p_user_password, $p_conf_client_token, $p_conf_email_token)
	{
		// Get user field that matches the email
		$sql_user_pw = $this->db_get_user_password_by_email($p_user_email);

		// Check field exists
		if (!$sql_user_pw) {
			return array(
				"message"=>"login_2fa_conf_no_match"
			);
		}

		// Verify password hash
		// note that password_verify automatically deducts which hash to use based on what is stored in the DB
		if (!password_verify($p_user_password, $sql_user_pw["password"])) {
			return array(
				"message"=>"login_2fa_conf_no_match"
			);
		}

		// Get the user details where the username is the provided username, and where the password
		// matches the hashed password
		$sql_login_result = $this->db_get_user_details_by_matching_credentials($p_user_email, $sql_user_pw["password"]);

		// Check there is a matching row. This should be redundant I think, but whatever
		if (!$sql_login_result) {
			return array(
				"message"=>"login_2fa_conf_issue"
			);
		}

		if ($sql_login_result["is_disabled"]) {
			return array(
				"message"=>"login_2fa_conf_account_disabled"
			);
		}



		// Get the 2fa tokens and request date of the user according to the user we previously selected
		$login_2fa_check = $this->dbconn->prepare('SELECT client_token_hash, email_token_hash, requested_at FROM user2fa WHERE user_id = ?');
		$login_2fa_check->execute([$sql_login_result["user_id"]]);
		$login_2fa_check_result = $login_2fa_check->fetch(\PDO::FETCH_ASSOC);

		// If no result, tell user no match
		if (!$login_2fa_check_result) {
			return array(
				"message" => "login_2fa_conf_expired_or_no_match"
			);
			die();
		}

		// Verify client token->hash
		if (!password_verify($p_conf_client_token, $login_2fa_check_result["client_token_hash"])) {
			return array(
				"message" => "login_2fa_conf_expired_or_no_match",
			);
			die();
		}
		
		// Verify email token->hash
		if (!password_verify($p_conf_email_token, $login_2fa_check_result["email_token_hash"])) {
			return array(
				"message" => "login_2fa_conf_expired_or_no_match",
			);
			die();
		}

		// Compare the time difference from now and the time at which the email verification token was created
		$time_now = time();
		$time_requested = strtotime($login_2fa_check_result["requested_at"]);
		// This is in seconds
		$time_diff = $time_now - $time_requested;
		// 48 hours: 48 * 60 * 60 = 172800
		// Check if token is expired (time diff from time requested)
		if ($time_diff > 60*60) {
			// As the verification token has expired, remove it
			$x = $this->db_remove_existing_2fa_tokens($sql_login_result["user_id"]);
			return array(
				"message" => "login_2fa_conf_expired_or_no_match",
			);
			die();
		}

		// Remove token
		$x = $this->db_remove_existing_2fa_tokens($sql_login_result["user_id"]);

		// Create a user token with the specified data
		$jwt_auth_token = $this->UserTokenAuth->create_user_token(
			$sql_login_result["user_id"],
			$sql_login_result["username"],
			$sql_login_result["email"],
			$sql_login_result["is_emailverified"],
			$sql_login_result["user_level"]
		);
		
		return array(
			"message"=>"login_2fa_conf_success",
			"jwt"=>$jwt_auth_token
		);
	}


	/**
	 * login method which checks if a user with the specified username and password exist.
	 *
	 * @todo the use of db_get_user_password_by_email and db_get_user_details_by_matching_credentials is redundant as a get_user_details_with_credentials_and_password method
	 * could be used to get the user's details and their password so that all of the details are captured in one SQL
	 * query.
	 *
	 * If the username/password match, this also generates a JWT token which is encoded into an array with the required
	 * data fields
	 *
	 *
	 * @param  string $p_user_email
	 * @param  string $p_user_password
	 * @param  string $mode
	 * @return array
	 */
	public function login($p_user_email, $p_user_password, $mode)
	{
		// Get user field that matches the email
		$sql_existing_user_result = $this->db_get_user_password_by_email($p_user_email);

		// Check field exists
		if (!$sql_existing_user_result) {
			return array(
				"message"=>$mode."_no_match"
			);
		}

		// Verify password hash
		// note that password_verify automatically deducts which hash to use based on what is stored in the DB
		if (!password_verify($p_user_password, $sql_existing_user_result["password"])) {
			return array(
				"message"=>$mode."_no_match"
			);
		}

		// Get the user details where the username is the provided username, and where the password
		// matches the hashed password
		$sql_login_result = $this->db_get_user_details_by_matching_credentials($p_user_email, $sql_existing_user_result["password"]);

		// Check there is a matching row. This should be redundant I think, but whatever
		if (!$sql_login_result) {
			return array(
				"message"=>$mode."_issue"
			);
		}

		if ($sql_login_result["is_disabled"]) {
			return array(
				"message"=>$mode."_account_disabled"
			);
		}

		// If the user's email is verified, we do not process the login here.
		// Instead. we create two tokens, one that is sent to the client, and one that is sent to the user's email.
		// This token pair will be stored in the database and there will be a timeout duration for how long it's valid
		// We also ignore loginAfterRegister requests here
		if ($sql_login_result["is_emailverified"] && $mode === "login") {
			$conf_token_client_plain = $this->create_new_token();
			$conf_token_email_plain = $this->create_new_token();
			
			$x = $this->db_remove_existing_2fa_tokens($sql_login_result["user_id"]);

			if (!$this->db_hash_and_insert_new_2fa_token($sql_login_result["user_id"], $conf_token_client_plain, $conf_token_email_plain)) {
				return array(
					"message" => "login_2fa_cant_create_token"
				);
				die();
			}

			$mailSubject = "gameinc 2FA Confirmation";
			$mailBody = "Hi, you just attempted to login.\r\n
				And since your account has 2FA enabled, you'll need to paste this token into the app: {$conf_token_email_plain}";
			
			$mailBodyNoHtml = "Hi, you just attempted to login.\r\n
			And since your account has 2FA enabled, you'll need to paste this token into the app: {$conf_token_email_plain}";

			// Here we actually send the email
			$email_attempt = $this->UserMailer->send_email(
				$sql_login_result["email"],
				$mailSubject,
				$mailBody,
				$mailBodyNoHtml
			);

			if (!$email_attempt) {
				return array(
					"message" => "login_2fa_request_error",
				);
			}

			return array(
				"message" => "login_2fa_request_success",
				"client_2fa_token" => $conf_token_client_plain
			);
		}

		// Create a user token with the specified data
		$jwt_auth_token = $this->UserTokenAuth->create_user_token(
			$sql_login_result["user_id"],
			$sql_login_result["username"],
			$sql_login_result["email"],
			$sql_login_result["is_emailverified"],
			$sql_login_result["user_level"]
		);
		
		return array(
			"message"=>$mode."_success",
			"jwt"=>$jwt_auth_token
		);
	}




	/**
	 * This attempts to verify the user's email
	 *
	 * @param  string $p_user_email
	 * @param  string $p_user_token
	 * @return array
	 */
	public function verify_email($p_user_email, $p_user_token)
	{
		// Check email matches given email
		$sql_existing_user_result = $this->db_get_user_details_by_email($p_user_email);

		if (!$sql_existing_user_result) {
			return array(
				"message" => "verifyemail_expired_or_no_matchAA"
			);
			die();
		}

		// If the user is already verified
		// NOTE: Do not tell the user that the email is already verified so that a malicious user
		// cannot arbitrarily check what emails are verified on our app
		if ($this->check_email_verified($sql_existing_user_result)) {
			return array(
				"message" => "verifyemail_expired_or_no_matchBBB"
			);
			die();
		}

		// Get the emailverify token and request date of the user according to the user we previously selected
		$emailverif_check = $this->dbconn->prepare('SELECT token_hash, requested_at FROM emailverify WHERE user_id = ?');
		$emailverif_check->execute([$sql_existing_user_result["user_id"]]);
		$emailverif_check_result = $emailverif_check->fetch(\PDO::FETCH_ASSOC);

		// If no result, tell user no match
		if (!$emailverif_check_result) {
			return array(
				"message" => "verifyemail_expired_or_no_matchCCC"
			);
			die();
		}

		// We check if the specified token actually matches the user's verify token
		// We use password_verify because the token in the database is stored as a hash
		// and we need to check if the hashes
		// This is done so that if an attacker gets access to the table of tokens, they don't know the actual tokens... only their hashes
		if (!password_verify($p_user_token, $emailverif_check_result["token_hash"])) {
			return array(
				"message" => "verifyemail_expired_or_no_matchDDD",
			);
			die();
		}

		// Compare the time difference from now and the time at which the email verification token was created
		$time_now = time();
		$time_requested = strtotime($emailverif_check_result["requested_at"]);
		// This is in seconds
		$time_diff = $time_now - $time_requested;
		// 48 hours: 48 * 60 * 60 = 172800
		// Check if token is expired (time diff from time requested)
		if ($time_diff > 60*60) {
			// As the verification token has expired, remove it
			$this->db_remove_existing_emailverify_tokens($sql_existing_user_result["user_id"]);
			return array(
				"message" => "verifyemail_expired_or_no_matchEEE",
			);
			die();
		}

		// Remove token
		$this->db_remove_existing_emailverify_tokens($sql_existing_user_result["user_id"]);

		// Set user's email as verified
		$verify_user = $this->dbconn->prepare('UPDATE user SET is_emailverified = ? WHERE user_id = ?');
		$verify_user_result = $verify_user->execute([true, $sql_existing_user_result["user_id"]]);

		if ($verify_user_result) {
			return array(
				"message" => "verifyemail_email_verified"
			);
			die();
		}
		die();
	}
	



	/**
	 * This handles the next "stage" of password resetting within the app.
	 *
	 * Basically, once the user has (hopefully) received a password token from send_password_reset,
	 * this method is called from the UI.
	 *
	 * What we do here (Assuming all are successful):
	 * - Get the details of the user according to their email
	 * - Check the the user's submitted token matches their account's password reset token
	 * - Check the password reset token has not expired, aka wasn't made too long ago (we can specify how long)
	 * - Remove the password reset token from their account
	 * - Generate a new password hash based on their provided password and set that to their password
	 * - Attempt to log them in
	 *
	 * @param  string $p_user_token
	 * @param  string $p_user_email
	 * @param  string $p_user_password
	 * @return array
	 */
	public function reset_password($p_user_token, $p_user_email, $p_user_password)
	{
		// Check email matches given email
		$sql_existing_user_result = $this->db_get_user_details_by_email($p_user_email);

		if (!$sql_existing_user_result) {
			return array(
				"message" => "properpasswordreset_expired_or_no_match"
			);
			die();
		}

		// Get the passwordreset token and request date of the user according to the user we previously selected
		$passwordreset_check = $this->dbconn->prepare('SELECT token_hash, requested_at FROM passwordreset WHERE user_id = ?');
		$passwordreset_check->execute([$sql_existing_user_result["user_id"]]);
		$passwordreset_check_result = $passwordreset_check->fetch(\PDO::FETCH_ASSOC);

		// If no result, tell user no match
		if (!$passwordreset_check_result) {
			return array(
				"message" => "properpasswordreset_expired_or_no_match"
			);
			die();
		}

		// We check if the specified token actually matches the user's verify token
		// We use password_verify because the token in the database is stored as a hash
		// and we need to check if the hashes
		// This is done so that if an attacker gets access to the table of tokens, they don't know the actual tokens... only their hashes
		if (!password_verify($p_user_token, $passwordreset_check_result["token_hash"])) {
			return array(
				"message" => "properpasswordreset_expired_or_no_match",
			);
			die();
		}

		// Compare the time difference from now and the time at which the password reset token was created
		$time_now = time();
		$time_requested = strtotime($passwordreset_check_result["requested_at"]);
		// This is in seconds
		$time_diff = $time_now - $time_requested;
		// 30 mins: 30 * 60
		// Check if token is expired (time diff from time requested)
		if ($time_diff > 60*60) {
			// As the reset token has expired, remove it
			$this->db_remove_existing_passwordreset_tokens($sql_existing_user_result["user_id"]);
			return array(
				"message" => "properpasswordreset_expired_or_no_match",
			);
			die();
		}

		// Remove token
		$this->db_remove_existing_passwordreset_tokens($sql_existing_user_result["user_id"]);

		$hashed_password = password_hash($p_user_password, PASSWORD_ARGON2ID, ['memory_cost' => 2048, 'time_cost' => 4, 'threads' => 3]);

		$update_password_result = $this->db_update_user_password($sql_existing_user_result["user_id"], $hashed_password);

		// Since updating password was successful, we can now try to login
		if ($update_password_result) {
			$login_after_reset = $this->login($p_user_email, $p_user_password, "loginAfterResetPassword");
			return $login_after_reset;
			die();
		}

		// return json_encode(
		// 	array(
		// 		"message" => "properpasswordreset_error"
		// 	)
		// );
		die();
	}



	public function reset_password_via_email($p_user_email, $p_user_token)
	{
		// TODO This^
	}



	/**
	 * This creates a new email verification token, invalidating any previous tokens, for the specified user.
	 * This only creates the token if the user is not already verified.
	 * It then sends the user an email with their token.
	 *
	 * @param  string $p_user_email
	 * @return array
	 */
	public function send_email_verification($p_user_email)
	{
		// Get user field that matches the email
		$sql_existing_user_result = $this->db_get_user_details_by_email($p_user_email);

		// Check field exists; if it does not, tell the user that an email was sent if one exists
		// NOTE: This could perhaps be a very fast response, so someone observant might be able to
		// benchmark the average response time and realise that very fast responses mean that email
		// has not been used. Could be a bruteforce concern.
		if (!$sql_existing_user_result) {
			return array(
				"message" => "emailconf_email_sent_if_exist"
			);
			die();
		}

		// NOTE: If the email is already verified, currently this'll just inform the user that it's
		// sent if exists. This is to prevent malicious users from checking if someone else has
		// already verified an account with us
		if ($this->check_email_verified($sql_existing_user_result)) {
			return array(
				"message" => "emailconf_email_sent_if_exist"
			);
			die();
		}

		// ---------------------------------------------------------------------
		// The way the token works is we generate a token, which is what the user will use,
		// but we only store a HASH of that token in the datbase. Then, when we are verifying,
		// we use password_verify on the user's provided token against the hash
		// ---------------------------------------------------------------------
		$email_token_plain = $this->create_new_token();

		// This makes sure there is only one active token
		$this->db_remove_existing_emailverify_tokens($sql_existing_user_result["user_id"]);

		// Here we create hash and insert it into the database
		if (!$this->db_hash_and_insert_new_emailverify_token($sql_existing_user_result["user_id"], $email_token_plain)) {
			return array(
				"message" => "emailconf_cant_create_token"
			);
			die();
		}
		
		// Concat the URL link with which the user will verify their email
		$email_conf_link = "{$this->GeneralConf->server_api_url}/__api_ext.php?request_type=verify_email&email={$p_user_email}&token={$email_token_plain}";

		$mailSubject = "gameinc Account Email Confirmation";
		$mailBody = "Hi, you created an account with us.\r\n
			To verify your account, please visit {$email_conf_link}";
		
		$mailBodyNoHtml = "Hi, you created an account with us.\r\n
			To verify your account, please visit {$email_conf_link}";

		// Here we actually send the email
		$email_attempt = $this->UserMailer->send_email(
			$sql_existing_user_result["email"],
			$mailSubject,
			$mailBody,
			$mailBodyNoHtml
		);
		
		// We're not doing anything else here, just returning a msg based on whether the email
		// was seemingly successful
		if ($email_attempt) {
			return array(
				"message" => "emailconf_email_sent_if_exist"
			);
		} else {
			return array(
				"message" => "emailconf_cant_send_email",
			);
		}
	}




	/**
	 * This creates a new password reset token, invalidating any previous tokens, for the specified user.
	 * It then sends the user an email with their token.
	 *
	 * @param  string $p_user_email
	 * @return array
	 */
	public function send_password_reset($p_user_email)
	{
		// Get user field that matches the email
		$sql_existing_user_result = $this->db_get_user_details_by_email($p_user_email);

		// Check field exists; if it does not, tell the user that an email was sent if one exists
		// NOTE: This could perhaps be a very fast response, so someone observant might be able to
		// benchmark the average response time and realise that very fast responses mean that email
		// has not been used. Could be a bruteforce concern.
		if (!$sql_existing_user_result) {
			return array(
				"message" => "sendpasswordreset_email_sent_if_exist"
			);
			die();
		}

		// Check email verified first; if the email is not verified
		// ask them to verify first
		$sql_email_verified_result = $this->check_email_verified($sql_existing_user_result);

		if (!$sql_email_verified_result) {
			return array(
				"message" => "sendpasswordreset_email_not_verified"
			);
			die();
		}

		// ---------------------------------------------------------------------
		// The way the token works is we generate a token, which is what the user will use,
		// but we only store a HASH of that token in the datbase. Then, when we are verifying,
		// we use password_verify on the user's provided token against the hash
		// ---------------------------------------------------------------------
		$password_token_plain = $this->create_new_token();

		// This makes sure there is only one active token
		$this->db_remove_existing_passwordreset_tokens($sql_existing_user_result["user_id"]);

		// Here we create hash and insert it into the database
		if (!$this->db_hash_and_insert_new_password_token($sql_existing_user_result["user_id"], $password_token_plain)) {
			return array(
				"message" => "sendpasswordreset_cant_create_token"
			);
			die();
		}

		// Concat the URL link with which the user will reset their password
		$password_reset_link = "{$this->GeneralConf->server_api_url}/__api_ext.php?request_type=email_password_reset&email={$p_user_email}&token={$password_token_plain}";

		$mailSubject = "gameinc Password Reset Request";
		$mailBody = "<p>Hi, it seems you have requested a password reset.</p>" .
		"<p>If this action was <b>not</b> made by you, please change your password immediately.</p>".
		"<p>This is the token to reset your password: <b style=color:#1C0E4D>{$password_token_plain}</b></p>".
		"<br></br>".
		"<p>You can also use the following link to reset your password: {$password_reset_link}\r\n";

		$mailBodyNoHtml = "Hi, it seems you have requested a password reset.\r\n" .
		"If this action was not made by you, please change your password immediately.\r\n".
		"This is the token to reset your password: {$password_token_plain}".
		"\r\n".
		"You can also use the following link to reset your password: {$password_reset_link}";

		// Here we actually send the email
		$email_attempt = $this->UserMailer->send_email(
			$sql_existing_user_result["email"],
			$mailSubject,
			$mailBody,
			$mailBodyNoHtml
		);
		
		// We're not doing anything else here, just returning a msg based on whether the email
		// was seemingly successful
		if ($email_attempt) {
			return array(
				"message" => "sendpasswordreset_email_sent_if_exist"
			);
		} else {
			return array(
				"message" => "sendpasswordreset_cant_send_email",
			);
		}
	}

		
	/**
	 * This changes the user's password if their auth token is valid, they re-typed the password correctly,
	 * and their new passwords match.
	 *
	 * @param  string $p_user_auth_token
	 * @param  string $p_current_password
	 * @param  string $p_desired_password
	 * @param  string $p_desired_password_conf
	 * @return void
	 */
	public function user_normal_change_password($p_user_auth_token, $p_current_password, $p_desired_password, $p_desired_password_conf)
	{
		// Double check the passwords match, even though client already does
		if ($p_desired_password !== $p_desired_password_conf) {
			return array(
				"message" => "mismatch_new_passwords"
			);
		}

		// decode jwt, with this we'll verify the user and we'll get their details
		$decoded = $this->UserTokenAuth->check_user_token($p_user_auth_token);

		// Only continue if the token is valid
		if ($decoded["message"] !== "token_valid") {
			return array(
				"message" => "token_validation_error"
			);
		}

		// Get the user's details via their email
		$sql_existing_user_result = $this->db_get_user_details_by_email($decoded["data"]->email);

		// Verify password hash
		// note that password_verify automatically deducts which hash to use based on what is stored in the DB
		if (!password_verify($p_current_password, $sql_existing_user_result["password"])) {
			return array(
				"message"=>"incorrect_password"
			);
		}

		$hashed_password = password_hash($p_desired_password, PASSWORD_ARGON2ID, ['memory_cost' => 2048, 'time_cost' => 4, 'threads' => 3]);

		$update_password_result = $this->db_update_user_password($sql_existing_user_result["user_id"], $hashed_password);

		if (!$update_password_result) {
			return array(
				"message"=>"failed_to_update"
			);
		}

		// Get the user's up to date details
		$sql_login_result = $this->db_get_user_details_by_matching_credentials($sql_existing_user_result["email"], $hashed_password);
		
		// Create a user token with the new data
		$jwt_auth_token = $this->UserTokenAuth->create_user_token(
			$sql_login_result["user_id"],
			$sql_login_result["username"],
			$sql_login_result["email"],
			$sql_login_result["is_emailverified"],
			$sql_login_result["user_level"]
		);
		
		return array(
			"message"=>"password_change_success",
			"jwt"=>$jwt_auth_token
		);
	}
	// -------------------------------------------------------------------------
	// !SECTION API
	// -------------------------------------------------------------------------



	// -------------------------------------------------------------------------
	// SECTION Internal Stuff
	// -------------------------------------------------------------------------
	/**
	 * This checks if the email is verified in the specified user's details
	 *
	 * @param  mixed $p_user_details - this should be a result from db_get_user_details_by_email
	 * @return boolean
	 */
	private function check_email_verified($p_user_details)
	{
		if ($p_user_details["is_emailverified"]) {
			return true;
		} else {
			return false;
		}
	}



	/**
	 * This just creates a new token.
	 *
	 * @return string
	 */
	public function create_new_token()
	{
		$length = 32;
		return bin2hex(random_bytes($length));
	}
	// -------------------------------------------------------------------------
	// !SECTION Internal Stuff
	// -------------------------------------------------------------------------



	// -------------------------------------------------------------------------
	// SECTION Database Queries
	// -------------------------------------------------------------------------
	/**
	 * This updates the user's password via their ID.
	 *
	 * @param  int $p_user_id
	 * @param  string $p_user_password_hash
	 * @return boolean
	 */
	private function db_update_user_password($p_user_id, $p_user_password_hash)
	{
		// Set user's password as verified
		$update_password = $this->dbconn->prepare('UPDATE user SET password = ?, password_updated_at = now() WHERE user_id = ?');
		return $update_password->execute([$p_user_password_hash, $p_user_id]);
	}



	/**
	 * This generates a hash for the token and inserts it into the specified user's
	 * email verification field.
	 *
	 * @param  int $p_user_id
	 * @param  string $p_token_plain
	 * @return boolean
	 */
	public function db_hash_and_insert_new_emailverify_token($p_user_id, $p_token_plain)
	{
		// We hash the email token as we do not want to store the token in plaintext
		$email_token_hash = password_hash($p_token_plain, PASSWORD_ARGON2ID, ['memory_cost' => 2048, 'time_cost' => 4, 'threads' => 3]);

		$sql_add = $this->dbconn->prepare('
		INSERT into emailverify (
			emailverify_id, user_id, token_hash, requested_at
		  ) 
		  VALUES 
			(NULL, ?, ?, now())
		  ');

		return $sql_add->execute([$p_user_id, $email_token_hash]);
	}



	/**
	 * This generates a hash for the token and inserts it into the specified user's
	 * password reset field.
	 *
	 * @param  int $p_user_id
	 * @param  string $p_token_plain
	 * @return boolean
	 */
	private function db_hash_and_insert_new_password_token($p_user_id, $p_token_plain)
	{
		// We hash the email token as we do not want to store the token in plaintext
		$password_token_hash = password_hash($p_token_plain, PASSWORD_ARGON2ID, ['memory_cost' => 2048, 'time_cost' => 4, 'threads' => 3]);

		$sql_add = $this->dbconn->prepare('
		INSERT into passwordreset (
			passwordreset_id, user_id, token_hash, requested_at
		) 
		VALUES 
			(NULL, ?, ?, now())
		');

		return $sql_add->execute([$p_user_id, $password_token_hash]);
	}



	/**
	 * This generates a hash for both the client and email tokens and inserts them into the specified user's
	 * 2fa field.
	 *
	 * @param  int $p_user_id
	 * @param  string $p_token_client_plain
	 * @param  string $p_token_email_plain
	 * @return boolean
	 */
	public function db_hash_and_insert_new_2fa_token($p_user_id, $p_token_client_plain, $p_token_email_plain)
	{
		// We hash the 2fa token as we do not want to store the token in plaintext
		$client_token_hash = password_hash($p_token_client_plain, PASSWORD_ARGON2ID, ['memory_cost' => 2048, 'time_cost' => 4, 'threads' => 3]);
		$email_token_hash = password_hash($p_token_email_plain, PASSWORD_ARGON2ID, ['memory_cost' => 2048, 'time_cost' => 4, 'threads' => 3]);

		$sql_add = $this->dbconn->prepare('
		INSERT into user2fa (
			user2fa_id, user_id, client_token_hash, email_token_hash, requested_at
		  ) 
		  VALUES 
			(NULL, ?, ?, ?, now())
		  ');

		return $sql_add->execute([$p_user_id, $client_token_hash, $email_token_hash]);
	}



	/**
	 * Remove all existing email verification tokens for the specified user;
	 * we should only ever have ONE or ZERO email verification tokens per user.
	 *
	 * @param  int $p_user_id
	 * @return boolean
	 */
	public function db_remove_existing_emailverify_tokens($p_user_id)
	{
		$sql_delete = $this->dbconn->prepare('DELETE FROM emailverify WHERE user_id = ?');
		return $sql_delete->execute([$p_user_id]);
	}



	/**
	 * Remove all existing password verification tokens for the specified user;
	 * we should only ever have ONE or ZERO password verification tokens per user.
	 *
	 * @param  int $p_user_id
	 * @return boolean
	 */
	private function db_remove_existing_passwordreset_tokens($p_user_id)
	{
		$sql_delete = $this->dbconn->prepare('DELETE FROM passwordreset WHERE user_id = ?');
		return $sql_delete->execute([$p_user_id]);
	}



	/**
	 * Remove all existing 2fa token pairs for the specified user;
	 * we should only ever have ONE or ZERO 2fa token pairs per user.
	 *
	 * @param  int $p_user_id
	 * @return boolean
	 */
	private function db_remove_existing_2fa_tokens($p_user_id)
	{
		$sql_delete = $this->dbconn->prepare('DELETE FROM user2fa WHERE user_id = ?');
		return $sql_delete->execute([$p_user_id]);
	}



	/**
	 * This gets the user whose username matches that in the parameter
	 * Use prepare statement with placeholders, then fill those placeholders with the
	 * execture function, to prevent SQL injection
	 *
	 * @note PDO::FETCH_ASSOC uses syntax $result["datafield"]
	 *
	 * @param  string $p_user_email
	 * @return string - this returns the password
	 */
	private function db_get_user_password_by_email($p_user_email)
	{
		$sql_user_check = $this->dbconn->prepare('SELECT password FROM user WHERE email = ?');
		$sql_user_check->execute([$p_user_email]);
		$sql_user_check_result = $sql_user_check->fetch(\PDO::FETCH_ASSOC);
		return $sql_user_check_result;
	}



	/**
	 * This gets the specified user's password via their username.
	 *
	 * @param  string $p_user_name
	 * @return string - this returns the password
	 */
	public function db_get_user_password_by_username($p_user_name)
	{
		$sql_user_check = $this->dbconn->prepare('SELECT password FROM user WHERE username = ?');
		$sql_user_check->execute([$p_user_name]);
		$sql_user_check_result = $sql_user_check->fetch(\PDO::FETCH_ASSOC);
		return $sql_user_check_result;
	}



	/**
	 * This gets the user's details simply via their email without requiring password
	 *
	 * @param  string $p_user_email
	 * @return mixed - this returns the user details
	 */
	public function db_get_user_details_by_email($p_user_email)
	{
		$sql_user = $this->dbconn->prepare('
			SELECT user_id, username, avatar, email, password, is_disabled, is_emailverified, is_email_new_allowed
			FROM user
			WHERE email = ?');

		$sql_user->execute([$p_user_email]);
		$sql_user_result = $sql_user->fetch(\PDO::FETCH_ASSOC);
		return $sql_user_result;
	}



	/**
	 * This grabs the user_id, username, avatar, is_disabled,
	 * and user_level values from their fields for the user where the email and password hash match
	 * those in the parameters.
	 *
	 * @note PDO::FETCH_ASSOC uses syntax $result["datafield"]
	 *
	 * @param  string $p_user_email
	 * @param  string $hashed_password
	 * @return mixed - this returns the user's details
	 */
	private function db_get_user_details_by_matching_credentials($p_user_email, $hashed_password)
	{
		$sql_login = $this->dbconn->prepare('
			SELECT user_id, username, email, avatar, user_level, is_disabled, is_emailverified
			FROM user
			WHERE email = ? AND password = ?');

		$sql_login->execute([$p_user_email, $hashed_password]);
		$sql_login_result = $sql_login->fetch(\PDO::FETCH_ASSOC);
		return $sql_login_result;
	}
	// -------------------------------------------------------------------------
	// !SECTION Database Queries
	// -------------------------------------------------------------------------
}
