<?php

// This file is used to connect to the database for easy modification

date_default_timezone_set("Europe/London");

$serverName = "localhost";
// $username = "root";
// $password = "";
$username = "ria-dev";
$password = "JeNeSaisPas@@@4\"";
$databaseName = "gameinc";


try {
	$dbconn = new PDO("mysql:host=$serverName;dbname=$databaseName", $username, $password);

	// Set the PDO error mode to exception
	$dbconn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
	die();
}
