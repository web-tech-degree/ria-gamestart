<?php

namespace User;

/**
 * @brief This handles getting source favourited info and toggling it
 */
class SourceFav
{
	// -------------------------------------------------------------------------
	// SECTION Dependencies
	// -------------------------------------------------------------------------
	// private \PDO $dbconn;
	// private \User\UserTokenAuth $UserTokenAuth;
	private $dbconn;
	private $UserTokenAuth;
	// -------------------------------------------------------------------------
	// !SECTION
	// -------------------------------------------------------------------------



	/**
	 * __construct triggered when constructing, does dependency injection
	 *
	 * These are then bound to the class
	 *
	 * Requirements:
	 * - dbconn:		instance of dbconn from _dbconnect.php
	 * - UserTokenAuth:instance of \User\UserTokenAuth for authenticating the user's token
	 *
	 * @param  \PDO $dbconn
	 * @param  \User\UserTokenAuth $UserTokenAuth
	 * @return void
	 */
	public function __construct(\PDO $dbconn, \User\UserTokenAuth $UserTokenAuth)
	{
		$this->dbconn = $dbconn;
		$this->UserTokenAuth = $UserTokenAuth;
	}


	
	/**
	 * This adds a field "user_vote_stats" to the passed assoc array. This'll store
	 * a value based on whether or not the current user upvoted that post.
	 *
	 * @param  \PDO::FETCH_ASSOC $p_post_item_array_result_assoc
	 * @param  mixed $p_user_id
	 * @return \PDO::FETCH_ASSOC
	 */
	public function concat_user_fav_status_for_array_of_posts($p_post_item_array_result_assoc, $p_user_id)
	{
		$post_arr = $p_post_item_array_result_assoc;
		foreach ($post_arr as $key => $field) {
			$post_arr[$key]["user_fav_source_status"] = $this->get_user_source_fav_status($field["source_id"], $p_user_id);
		}
		return $post_arr;
	}



	/**
	 * This gets the favourite status of the specified user on the specified post
	 *
	 * @param  mixed $p_source_id
	 * @param  mixed $p_user_id
	 * @return int
	 */
	public function get_user_source_fav_status($p_source_id, $p_user_id)
	{

		$post_fetch = $this->dbconn->prepare('SELECT
			1
		FROM user_favsource
		WHERE user_favsource.source_id = ? AND user_favsource.user_id = ?');

		$post_fetch->execute([$p_source_id, $p_user_id]);
		$post_fetch_result = $post_fetch->fetch(\PDO::FETCH_COLUMN);

		// The user will not necessarily have favourited/unfavourited every post, and they may be signed out,
		// so we default to 0
		$ret = 0;
		if ($post_fetch_result) {
			$ret = $post_fetch_result;
		}

		return $ret;
	}



	/**
	 * This adds the selected source as a favourite for the logged in user.
	 *
	 * @param  string $p_auth_token
	 * @param  int $p_source_id
	 * @param  int $p_fav_desired
	 * @return void
	 */
	public function source_toggle_favourite($p_auth_token, $p_source_id, $p_fav_desired)
	{
		$decoded = $this->UserTokenAuth->check_user_token($p_auth_token);
		
		if ($decoded["message"] !== "token_valid") {
			return array(
				"message" => "token_validation_error"
			);
		}

		// Here we simply delete the existing favourite combo, regardless.
		$remove = $this->dbconn->prepare('DELETE FROM user_favsource WHERE user_id = ? AND source_id = ?');
		$remove_result = $remove->execute([$decoded["data"]->id, $p_source_id]);

		if (!$remove_result) {
			return array(
				"message" => "favourite_failed"
			);
		}

		// Now, if the user wanted to *add* the bookmark, we add it.
		if ($p_fav_desired === 1) {
			$add = $this->dbconn->prepare('INSERT INTO user_favsource (user_id, source_id) VALUES (?, ?)');
			$add_result = $add->execute([$decoded["data"]->id, $p_source_id]);

			if (!$add_result) {
				return array(
					"message" => "favourite_failed"
				);
			}
		}

		return array(
			"message" => "favourite_success"
		);
	}
}
