<?php

namespace Post;

/**
 * @brief This is used for adding, removing, and synchronising bookmarks
 *
 */
class PostBookmarks
{
	// -------------------------------------------------------------------------
	// SECTION Dependencies
	// -------------------------------------------------------------------------
	/**
	 * dbconn private variable, so that only the class itself can access it, used to store the
	 * connection to the database
	 *
	 * @var mixed
	 */
	// private \PDO $dbconn;
	// private \User\UserTokenAuth $UserTokenAuth;
	// private \Post\PostVotes $PostVotes;
	// private \Misc\ExtraFunc $ExtraFunc;
	// private int $max_fetch_post_count;
	private $dbconn;
	private $UserTokenAuth;
	private $PostVotes;
	private $ExtraFunc;
	private $max_fetch_post_count;
	
	// -------------------------------------------------------------------------
	// !SECTION
	// -------------------------------------------------------------------------



	/**
	 * __construct triggered when constructing, does dependency injection
	 *
	 * These are then bound to the class
	 *
	 * Requirements:
	 * - dbconn:		instance of dbconn from _dbconnect.php
	 * - UserTokenAuth:	instance of \User\UserTokenAuth for authenticating the user's token
	 * * - PostVotes:		instance of \Post\PostVotes for getting post vote info and casting votes
	 * - ExtraFunc:		instance of \Misc\ExtraFunc from _functions.php
	 *
	 * @param  \PDO $dbconn
	 * @param  \User\UserTokenAuth $UserTokenAuth
	 * * @param  \Post\PostVotes $PostVotes
	 * @param  \Misc\ExtraFunc $ExtraFunc
	 * @return void
	 */
	public function __construct(\PDO $dbconn, \User\UserTokenAuth $UserTokenAuth, \Post\PostVotes $PostVotes, \Misc\ExtraFunc $ExtraFunc)
	{
		$this->dbconn = $dbconn;
		$this->UserTokenAuth = $UserTokenAuth;
		$this->PostVotes = $PostVotes;
		$this->ExtraFunc = $ExtraFunc;
		$this->max_fetch_post_count = 2;
	}



	
	/**
	 * This adds a field "user_vote_stats" to the passed assoc array. This'll store
	 * a value based on whether or not the current user upvoted that post.
	 *
	 * @param  \PDO::FETCH_ASSOC $p_post_item_array_result_assoc
	 * @param  mixed $p_user_id
	 * @return \PDO::FETCH_ASSOC
	 */
	public function concat_user_bookmark_status_for_array_of_posts($p_post_item_array_result_assoc, $p_user_id)
	{
		$post_arr = $p_post_item_array_result_assoc;
		foreach ($post_arr as $key => $field) {
			$post_arr[$key]["user_bookmark_status"] = $this->get_user_post_bookmark_status($field["postitem_id"], $p_user_id);
		}
		return $post_arr;
	}



	/**
	 * This gets the bookmark status of the specified user on the specified post
	 *
	 * @param  mixed $p_postitem_id
	 * @param  mixed $p_user_id
	 * @return int
	 */
	public function get_user_post_bookmark_status($p_postitem_id, $p_user_id)
	{
		$post_fetch = $this->dbconn->prepare('SELECT
			1
		FROM postitem_bookmark
		WHERE postitem_bookmark.postitem_id = ? AND postitem_bookmark.user_id = ?');

		$post_fetch->execute([$p_postitem_id, $p_user_id]);
		$post_fetch_result = $post_fetch->fetch(\PDO::FETCH_COLUMN);

		// The user will not necessarily have bookmarked any/every post, and they may be signed out,
		// so we default to false
		$ret = 0;
		if ($post_fetch_result) {
			$ret = 1;
		}

		return $ret;
	}



	/**
	 * This delete's the user-post bookmark regardless of what the user wanted, and if the user wanted to add
	 * the bookmark, then it adds it. This is probably a really meh way to do it, but I didn't want to look
	 * into merges.
	 *
	 * @param  string $p_user_token
	 * @param  int $p_post_id
	 * @param  int $p_bookmark_status_desired
	 * @return void
	 */
	public function post_toggle_bookmark($p_user_token, $p_post_id, $p_bookmark_status_desired)
	{
		$decoded = $this->UserTokenAuth->check_user_token($p_user_token);
		
		if ($decoded["message"] !== "token_valid") {
			return array(
				"message" => "token_validation_error"
			);
		}

		// Here we simply delete the existing bookmark, regardless.
		$remove = $this->dbconn->prepare('DELETE FROM postitem_bookmark WHERE user_id = ? AND postitem_id = ?');
		$remove_result = $remove->execute([$decoded["data"]->id, $p_post_id]);

		if (!$remove_result) {
			return array(
				"message" => "bookmark_failed"
			);
		}

		// Now, if the user wanted to *add* the bookmark, we add it.
		if ($p_bookmark_status_desired === 1) {
			$add = $this->dbconn->prepare('INSERT INTO postitem_bookmark (user_id, postitem_id) VALUES (?, ?)');
			$add_result = $add->execute([$decoded["data"]->id, $p_post_id]);

			if (!$add_result) {
				return array(
					"message" => "bookmark_failed"
				);
			}
		}

		return array(
			"message" => "bookmark_success"
		);
	}

	/**
	 * This gets the post IDs that the user has bookmarked.
	 *
	 * @param  string $p_user_token
	 * @return void
	 */
	public function get_user_post_bookmark_ids($p_user_token)
	{
		$decoded = $this->UserTokenAuth->check_user_token($p_user_token);

		if ($decoded["message"] !== "token_valid") {
			return array(
				"message" => "token_validation_error"
			);
		}

		$post_fetch = $this->dbconn->prepare('SELECT postitem_id
		FROM postitem_bookmark
		WHERE postitem_bookmark.user_id = ?');

		$post_fetch->execute([$decoded["data"]->id]);
		// We fetch it as a column because we literally just want the array of numbers; we don't
		// care about objects with data
		$post_fetch_result = $post_fetch->fetchAll(\PDO::FETCH_COLUMN);

		return array(
			"message" => "fetch_success",
			"bookmarked_posts" => $post_fetch_result,
		);
	}



	/**
	 * This fetches a set number of posts which match those given in the array, and returns
	 * them, making the UI update its state of "what still needs to be fetched" appropriately.
	 *
	 * @param  mixed $p_user_token
	 * @param  mixed $p_post_id_array
	 * @return void
	 */
	public function fetch_posts_in_user_bookmarks($p_user_token, $p_post_id_array)
	{
		$token_res = $this->UserTokenAuth->check_user_token($p_user_token);

		// We only want to fetch the first n number of posts
		$posts_to_fetch = array_splice($p_post_id_array, 0, $this->max_fetch_post_count);

		// We need to implode the array to use WHERE x IN clause
		$imploded_posts_to_fetch = implode("','", $posts_to_fetch);

		$post_fetch = $this->dbconn->prepare("SELECT
			postitem.postitem_id,
			postitem.hash,
			postitem.title,
			postitem.url,
			postitem.content,
			postitem.date,
			postitem.author
		FROM postitem
		WHERE postitem.postitem_id IN ('$imploded_posts_to_fetch')");

		$post_fetch->execute();
		
		// This tells the user if no more posts were found
		if ($post_fetch->rowCount() < 1) {
			return array(
				"message" => "no_more_posts"
			);
			die();
		}

		$post_fetch_result = $post_fetch->fetchAll(\PDO::FETCH_ASSOC);

		return array(
			"message" => "fetch_success",
			"posts" => $post_fetch_result,
		);
	}
}
