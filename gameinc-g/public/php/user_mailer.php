<?php

namespace User;

/**
 * @brief This handles some simple SMTP stuff for UserAccess. It's separated
 * into its own class just so it's easier to configure stuff.
 *
 */
class UserMailer
{
	// -------------------------------------------------------------------------
	// SECTION Dependencies
	// -------------------------------------------------------------------------
	/**
	 * dbconn private variable, so that only the class itself can access it, used to store the
	 * connection to the database
	 *
	 * @var mixed
	 */
	// private \PHPMailer\PHPMailer\PHPMailer $phpmailer;
	// private \Conf\GeneralConf $GeneralConf;
	private $phpmailer;
	private $GeneralConf;
	// -------------------------------------------------------------------------
	// !SECTION
	// -------------------------------------------------------------------------



	/**
	 * __construct triggered when constructing, does dependency injection
	 *
	 * These are then bound to the class
	 *
	 * Requirements:
	 * - phpmailer:		instance of \PicoFeed\Reader\Reader from library
	 * - GeneralConf:		instance of \Conf\GeneralConf for SMTP username and password
	 *
	 * @param  \PHPMailer\PHPMailer\PHPMailer $phpmailer
	 * @param  \Conf\General $GeneralConf
	 * @return void
	 */
	public function __construct(\PHPMailer\PHPMailer\PHPMailer $phpmailer, \Conf\General $GeneralConf)
	{
		$this->phpmailer = $phpmailer;
		$this->GeneralConf = $GeneralConf;
	}



	/**
	 * This sends an email to the specified user with the specified subject and body.
	 *
	 * Note that you have to configure conf_general.php file for yourself for this to work.
	 *
	 * @param  mixed $p_email_to
	 * @param  mixed $p_subject
	 * @param  mixed $p_body
	 * @param  mixed $p_body_nohtml
	 * @return void
	 */
	public function send_email($p_email_to, $p_subject, $p_body, $p_body_nohtml)
	{
		try {
			$this->phpmailer->SMTPDebug = \PHPMailer\PHPMailer\SMTP::DEBUG_OFF;
			// $this->phpmailer->SMTPDebug = $this->phpmailer->SMTP::DEBUG_OFF;
			// Send using SMTP
			$this->phpmailer->isSMTP();
			$this->phpmailer->Host = "smtp.gmail.com";
			$this->phpmailer->SMTPAuth = true;
			// NOTE: Configure these for yourself in _smtpconf.php
			$this->phpmailer->Username = $this->GeneralConf->smtp_username;
			$this->phpmailer->Password = $this->GeneralConf->smtp_password;
			// Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
			// $this->phpmailer->SMTPSecure = \PHPMailer\PHPMailer\PHPMailer::ENCRYPTION_STARTTLS;
			$this->phpmailer->SMTPSecure = $this->phpmailer::ENCRYPTION_STARTTLS;
			// TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
			$this->phpmailer->Port = 587;

			$this->phpmailer->setFrom("from@example.com", "gameinc-g");
			$this->phpmailer->addAddress($p_email_to);
			$this->phpmailer->addReplyTo("info@example.com", "Information");

			// Content
			// Set email format to HTML
			$this->phpmailer->isHTML(true);
			$this->phpmailer->Subject = $p_subject;
			$this->phpmailer->Body    = $p_body;
			$this->phpmailer->AltBody = $p_body_nohtml;
			$this->phpmailer->send();
		} catch (\PHPMailer\PHPMailer\Exception $e) {
			// If the email fails, whoever called this function will have to deal with it.
			return false;
		}
		// If we get here, it means we didn't hit an error; hence, the email was *probably* successful
		return true;
	}
}
