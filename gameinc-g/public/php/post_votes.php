<?php

namespace Post;

/**
 * @brief This handles getting vote info and casting votes on posts.
 */
class PostVotes
{
	// -------------------------------------------------------------------------
	// SECTION Dependencies
	// -------------------------------------------------------------------------
	// private \PDO $dbconn;
	private $dbconn;
	// -------------------------------------------------------------------------
	// !SECTION
	// -------------------------------------------------------------------------



	/**
	 * __construct triggered when constructing, does dependency injection
	 *
	 * These are then bound to the class
	 *
	 * Requirements:
	 * - dbconn:		instance of dbconn from _dbconnect.php
	 *
	 * @param  \PDO $dbconn
	 * @return void
	 */
	public function __construct(\PDO $dbconn)
	{
		$this->dbconn = $dbconn;
	}



	/**
	 * This adds a field "user_vote_stats" to the passed assoc array. This'll store
	 * a value based on whether or not the current user upvoted that post.
	 *
	 * @param  \PDO::FETCH_ASSOC $p_post_item_array_result_assoc
	 * @param  mixed $p_user_id
	 * @return \PDO::FETCH_ASSOC
	 */
	public function concat_user_vote_status_for_array_of_posts($p_post_item_array_result_assoc, $p_user_id)
	{
		$post_arr = $p_post_item_array_result_assoc;
		foreach ($post_arr as $key => $field) {
			$post_arr[$key]["user_vote_status"] = $this->get_user_post_vote_status($field["postitem_id"], $p_user_id);
		}
		return $post_arr;
	}


	
	/**
	 * This gets the total number of votes for every post in the passed array.
	 *
	 * @param  mixed $p_post_item_array_result_assoc
	 * @return \PDO::FETCH_ASSOC
	 */
	public function concat_vote_totals_for_array_of_posts($p_post_item_array_result_assoc)
	{
		$post_arr = $p_post_item_array_result_assoc;
		foreach ($post_arr as $key => $field) {
			$post_arr[$key]["total_votes"] = $this->get_vote_totals_for_post($field["postitem_id"]);
		}
		return $post_arr;
	}



	/**
	 * This gets the vote status of the specified user on the specified post
	 *
	 * @param  mixed $p_postitem_id
	 * @param  mixed $p_user_id
	 * @return int
	 */
	public function get_user_post_vote_status($p_postitem_id, $p_user_id)
	{
		$post_fetch = $this->dbconn->prepare('SELECT
			postitem_vote.vote_status
		FROM postitem_vote
		WHERE postitem_vote.postitem_id = ? AND postitem_vote.user_id = ?');

		$post_fetch->execute([$p_postitem_id, $p_user_id]);
		$post_fetch_result = $post_fetch->fetch(\PDO::FETCH_COLUMN);

		// The user will not necessarily have voted on every post, and they may be signed out,
		// so we default to 0
		$ret = 0;
		if ($post_fetch_result) {
			$ret = $post_fetch_result;
		}

		return $ret;
	}



	/**
	 * This gets the total number of votes for all of the specified posts.
	 *
	 * Note that this automatically negates downvotes from upvotes via the sum,
	 * as downvotes will be stored as -1 and upvotes as 1.
	 *
	 * @param  mixed $p_postitem_id
	 * @return int
	 */
	public function get_vote_totals_for_post($p_postitem_id)
	{
		$post_fetch = $this->dbconn->prepare('SELECT SUM(postitem_vote.vote_status)
		FROM postitem_vote
		WHERE postitem_vote.postitem_id = ?');
		$post_fetch->execute([$p_postitem_id]);
		$post_fetch_result = $post_fetch->fetch(\PDO::FETCH_COLUMN);

		// Not all posts will have user votes, so if we happen to not get anything we just return 0
		$ret = 0;
		if ($post_fetch_result !== null) {
			$ret = $post_fetch_result;
		}

		return $ret;
	}
}
