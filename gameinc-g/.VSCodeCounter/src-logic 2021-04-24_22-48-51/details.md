# Details

Date : 2021-04-24 22:48:51

Directory d:\Productivity NVMe\__Git\web-tech-degree\ria-gamestart\gameinc-g\src\logic

Total : 2 files,  147 codes, 99 comments, 52 blanks, all 298 lines

[summary](results.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [src/logic/globals.tsx](/src/logic/globals.tsx) | TypeScript React | 135 | 93 | 48 | 276 |
| [src/logic/test.js](/src/logic/test.js) | JavaScript | 12 | 6 | 4 | 22 |

[summary](results.md)