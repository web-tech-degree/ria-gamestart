# Summary

Date : 2021-04-24 22:48:51

Directory d:\Productivity NVMe\__Git\web-tech-degree\ria-gamestart\gameinc-g\src\logic

Total : 2 files,  147 codes, 99 comments, 52 blanks, all 298 lines

[details](details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| TypeScript React | 1 | 135 | 93 | 48 | 276 |
| JavaScript | 1 | 12 | 6 | 4 | 22 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 2 | 147 | 99 | 52 | 298 |

[details](details.md)