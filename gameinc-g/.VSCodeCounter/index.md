# Lines of Code
This is mostly just-for-fun, because lines of code barely mean anything.

Also please note that when viewing the details, the links to the files will not work because they are local; should you wish to view the files, use the GitLab repository.

## public/php
[results](https://gamestart.aterlux.com/loc/public-php/results.html)
[details](https://gamestart.aterlux.com/loc/public-php/details.html)

## src/components
[results](https://gamestart.aterlux.com/loc/src-components/results.html)
[details](https://gamestart.aterlux.com/loc/src-components/details.html)

## src/logic
[results](https://gamestart.aterlux.com/loc/src-logic/results.html)
[details](https://gamestart.aterlux.com/loc/src-logic/details.html)