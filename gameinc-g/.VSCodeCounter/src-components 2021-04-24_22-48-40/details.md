# Details

Date : 2021-04-24 22:48:40

Directory d:\Productivity NVMe\__Git\web-tech-degree\ria-gamestart\gameinc-g\src\components

Total : 63 files,  7742 codes, 4167 comments, 2126 blanks, all 14035 lines

[summary](results.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [src/components/AdminHub/AdminHub.css](/src/components/AdminHub/AdminHub.css) | CSS | 51 | 12 | 7 | 70 |
| [src/components/AdminHub/AdminHub.tsx](/src/components/AdminHub/AdminHub.tsx) | TypeScript React | 89 | 67 | 39 | 195 |
| [src/components/AdminHub/FeedEditor.tsx](/src/components/AdminHub/FeedEditor.tsx) | TypeScript React | 410 | 320 | 90 | 820 |
| [src/components/AdminHub/SourceEditor.tsx](/src/components/AdminHub/SourceEditor.tsx) | TypeScript React | 301 | 168 | 61 | 530 |
| [src/components/AdminHub/modes.tsx](/src/components/AdminHub/modes.tsx) | TypeScript React | 32 | 9 | 9 | 50 |
| [src/components/AdminHub/types.tsx](/src/components/AdminHub/types.tsx) | TypeScript React | 23 | 8 | 5 | 36 |
| [src/components/App.css](/src/components/App.css) | CSS | 45 | 29 | 8 | 82 |
| [src/components/App.test.tsx](/src/components/App.test.tsx) | TypeScript React | 8 | 0 | 2 | 10 |
| [src/components/App.tsx](/src/components/App.tsx) | TypeScript React | 714 | 382 | 132 | 1,228 |
| [src/components/AppNav/BotNav.css](/src/components/AppNav/BotNav.css) | CSS | 67 | 4 | 17 | 88 |
| [src/components/AppNav/BotNav.tsx](/src/components/AppNav/BotNav.tsx) | TypeScript React | 136 | 102 | 62 | 300 |
| [src/components/AppNav/FavouritesSelector.css](/src/components/AppNav/FavouritesSelector.css) | CSS | 72 | 9 | 10 | 91 |
| [src/components/AppNav/FavouritesSelector.tsx](/src/components/AppNav/FavouritesSelector.tsx) | TypeScript React | 92 | 83 | 40 | 215 |
| [src/components/AppNav/LeftNav.css](/src/components/AppNav/LeftNav.css) | CSS | 73 | 2 | 17 | 92 |
| [src/components/AppNav/LeftNav.tsx](/src/components/AppNav/LeftNav.tsx) | TypeScript React | 140 | 100 | 63 | 303 |
| [src/components/DebuggerMenu/DebuggerMenu.css](/src/components/DebuggerMenu/DebuggerMenu.css) | CSS | 9 | 0 | 4 | 13 |
| [src/components/DebuggerMenu/DebuggerMenu.tsx](/src/components/DebuggerMenu/DebuggerMenu.tsx) | TypeScript React | 60 | 34 | 62 | 156 |
| [src/components/Externals/Footer.css](/src/components/Externals/Footer.css) | CSS | 10 | 1 | 2 | 13 |
| [src/components/Externals/Footer.tsx](/src/components/Externals/Footer.tsx) | TypeScript React | 30 | 41 | 25 | 96 |
| [src/components/Externals/Help.css](/src/components/Externals/Help.css) | CSS | 43 | 12 | 15 | 70 |
| [src/components/Externals/Help.tsx](/src/components/Externals/Help.tsx) | TypeScript React | 136 | 110 | 32 | 278 |
| [src/components/Externals/Home.css](/src/components/Externals/Home.css) | CSS | 136 | 0 | 30 | 166 |
| [src/components/Externals/Home.tsx](/src/components/Externals/Home.tsx) | TypeScript React | 131 | 45 | 36 | 212 |
| [src/components/Externals/Nav.css](/src/components/Externals/Nav.css) | CSS | 11 | 0 | 5 | 16 |
| [src/components/Externals/Nav.tsx](/src/components/Externals/Nav.tsx) | TypeScript React | 46 | 40 | 25 | 111 |
| [src/components/FeedHub/Bookmarker.css](/src/components/FeedHub/Bookmarker.css) | CSS | 19 | 0 | 3 | 22 |
| [src/components/FeedHub/Bookmarker.tsx](/src/components/FeedHub/Bookmarker.tsx) | TypeScript React | 57 | 46 | 29 | 132 |
| [src/components/FeedHub/Comment.css](/src/components/FeedHub/Comment.css) | CSS | 54 | 15 | 13 | 82 |
| [src/components/FeedHub/Comment.tsx](/src/components/FeedHub/Comment.tsx) | TypeScript React | 336 | 182 | 78 | 596 |
| [src/components/FeedHub/Favouritiser.css](/src/components/FeedHub/Favouritiser.css) | CSS | 17 | 0 | 3 | 20 |
| [src/components/FeedHub/Favouritiser.tsx](/src/components/FeedHub/Favouritiser.tsx) | TypeScript React | 51 | 42 | 28 | 121 |
| [src/components/FeedHub/Feed.css](/src/components/FeedHub/Feed.css) | CSS | 91 | 38 | 19 | 148 |
| [src/components/FeedHub/Feed.tsx](/src/components/FeedHub/Feed.tsx) | TypeScript React | 509 | 315 | 111 | 935 |
| [src/components/FeedHub/FeedBookmarks.tsx](/src/components/FeedHub/FeedBookmarks.tsx) | TypeScript React | 133 | 101 | 53 | 287 |
| [src/components/FeedHub/FeedFavourites.tsx](/src/components/FeedHub/FeedFavourites.tsx) | TypeScript React | 307 | 198 | 82 | 587 |
| [src/components/FeedHub/FeedHub.css](/src/components/FeedHub/FeedHub.css) | CSS | 12 | 10 | 1 | 23 |
| [src/components/FeedHub/FeedHub.tsx](/src/components/FeedHub/FeedHub.tsx) | TypeScript React | 353 | 283 | 93 | 729 |
| [src/components/FeedHub/Item.css](/src/components/FeedHub/Item.css) | CSS | 60 | 18 | 11 | 89 |
| [src/components/FeedHub/Item.tsx](/src/components/FeedHub/Item.tsx) | TypeScript React | 233 | 96 | 63 | 392 |
| [src/components/FeedHub/ItemLightbox.css](/src/components/FeedHub/ItemLightbox.css) | CSS | 107 | 48 | 32 | 187 |
| [src/components/FeedHub/ItemLightbox.tsx](/src/components/FeedHub/ItemLightbox.tsx) | TypeScript React | 552 | 269 | 103 | 924 |
| [src/components/FeedHub/VoteButtonDown.tsx](/src/components/FeedHub/VoteButtonDown.tsx) | TypeScript React | 57 | 45 | 29 | 131 |
| [src/components/FeedHub/VoteButtonUp.tsx](/src/components/FeedHub/VoteButtonUp.tsx) | TypeScript React | 57 | 45 | 29 | 131 |
| [src/components/FeedHub/VoteButtons.css](/src/components/FeedHub/VoteButtons.css) | CSS | 30 | 11 | 7 | 48 |
| [src/components/FeedHub/modes.tsx](/src/components/FeedHub/modes.tsx) | TypeScript React | 19 | 3 | 4 | 26 |
| [src/components/FeedHub/types.tsx](/src/components/FeedHub/types.tsx) | TypeScript React | 52 | 16 | 7 | 75 |
| [src/components/Login/AlreadyLoggedIn.tsx](/src/components/Login/AlreadyLoggedIn.tsx) | TypeScript React | 41 | 32 | 28 | 101 |
| [src/components/Login/ForgotPassword.tsx](/src/components/Login/ForgotPassword.tsx) | TypeScript React | 224 | 85 | 52 | 361 |
| [src/components/Login/Login.css](/src/components/Login/Login.css) | CSS | 48 | 10 | 15 | 73 |
| [src/components/Login/Login.tsx](/src/components/Login/Login.tsx) | TypeScript React | 185 | 85 | 46 | 316 |
| [src/components/Login/LoginLanding.css](/src/components/Login/LoginLanding.css) | CSS | 23 | 0 | 7 | 30 |
| [src/components/Login/LoginLanding.tsx](/src/components/Login/LoginLanding.tsx) | TypeScript React | 262 | 139 | 81 | 482 |
| [src/components/Login/ReconfirmEmail.tsx](/src/components/Login/ReconfirmEmail.tsx) | TypeScript React | 102 | 66 | 43 | 211 |
| [src/components/Login/Register.css](/src/components/Login/Register.css) | CSS | 37 | 10 | 12 | 59 |
| [src/components/Login/Register.tsx](/src/components/Login/Register.tsx) | TypeScript React | 128 | 73 | 40 | 241 |
| [src/components/Login/modes.tsx](/src/components/Login/modes.tsx) | TypeScript React | 19 | 6 | 4 | 29 |
| [src/components/PopUp/PopUp.css](/src/components/PopUp/PopUp.css) | CSS | 32 | 4 | 5 | 41 |
| [src/components/PopUp/PopUp.tsx](/src/components/PopUp/PopUp.tsx) | TypeScript React | 31 | 37 | 26 | 94 |
| [src/components/Profile/Profile.css](/src/components/Profile/Profile.css) | CSS | 156 | 50 | 38 | 244 |
| [src/components/Profile/Profile.tsx](/src/components/Profile/Profile.tsx) | TypeScript React | 430 | 203 | 120 | 753 |
| [src/components/Profile/modes.tsx](/src/components/Profile/modes.tsx) | TypeScript React | 8 | 0 | 2 | 10 |
| [src/components/modes.tsx](/src/components/modes.tsx) | TypeScript React | 33 | 8 | 7 | 48 |
| [src/components/types.tsx](/src/components/types.tsx) | TypeScript React | 12 | 0 | 4 | 16 |

[summary](results.md)