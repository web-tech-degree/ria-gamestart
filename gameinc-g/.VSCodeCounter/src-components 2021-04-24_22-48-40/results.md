# Summary

Date : 2021-04-24 22:48:40

Directory d:\Productivity NVMe\__Git\web-tech-degree\ria-gamestart\gameinc-g\src\components

Total : 63 files,  7742 codes, 4167 comments, 2126 blanks, all 14035 lines

[details](details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| TypeScript React | 40 | 6,539 | 3,884 | 1,845 | 12,268 |
| CSS | 23 | 1,203 | 283 | 281 | 1,767 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 63 | 7,742 | 4,167 | 2,126 | 14,035 |
| AdminHub | 6 | 906 | 584 | 211 | 1,701 |
| AppNav | 6 | 580 | 300 | 209 | 1,089 |
| DebuggerMenu | 2 | 69 | 34 | 66 | 169 |
| Externals | 8 | 543 | 249 | 170 | 962 |
| FeedHub | 21 | 3,106 | 1,781 | 798 | 5,685 |
| Login | 10 | 1,069 | 506 | 328 | 1,903 |
| PopUp | 2 | 63 | 41 | 31 | 135 |
| Profile | 3 | 594 | 253 | 160 | 1,007 |

[details](details.md)