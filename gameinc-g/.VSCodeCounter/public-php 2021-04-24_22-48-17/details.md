# Details

Date : 2021-04-24 22:48:17

Directory d:\Productivity NVMe\__Git\web-tech-degree\ria-gamestart\gameinc-g\public\php

Total : 20 files,  3448 codes, 1958 comments, 1035 blanks, all 6441 lines

[summary](results.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [public/php/__api.php](/public/php/__api.php) | PHP | 768 | 220 | 236 | 1,224 |
| [public/php/__api_ext.php](/public/php/__api_ext.php) | PHP | 161 | 103 | 71 | 335 |
| [public/php/_dbconnect.php](/public/php/_dbconnect.php) | PHP | 12 | 4 | 7 | 23 |
| [public/php/conf_general.php](/public/php/conf_general.php) | PHP | 9 | 4 | 2 | 15 |
| [public/php/conf_general_template.php](/public/php/conf_general_template.php) | PHP | 9 | 4 | 2 | 15 |
| [public/php/conf_jwt.php](/public/php/conf_jwt.php) | PHP | 18 | 14 | 4 | 36 |
| [public/php/feed_fetcher.php](/public/php/feed_fetcher.php) | PHP | 465 | 217 | 104 | 786 |
| [public/php/feed_manager.php](/public/php/feed_manager.php) | PHP | 395 | 214 | 109 | 718 |
| [public/php/feed_parser.php](/public/php/feed_parser.php) | PHP | 92 | 96 | 37 | 225 |
| [public/php/misc_extrafunc.php](/public/php/misc_extrafunc.php) | PHP | 43 | 60 | 18 | 121 |
| [public/php/misc_imagecrop.php](/public/php/misc_imagecrop.php) | PHP | 53 | 24 | 11 | 88 |
| [public/php/post_bookmarks.php](/public/php/post_bookmarks.php) | PHP | 114 | 86 | 41 | 241 |
| [public/php/post_details.php](/public/php/post_details.php) | PHP | 238 | 141 | 60 | 439 |
| [public/php/post_votes.php](/public/php/post_votes.php) | PHP | 53 | 54 | 23 | 130 |
| [public/php/user_access.php](/public/php/user_access.php) | PHP | 545 | 364 | 170 | 1,079 |
| [public/php/user_details.php](/public/php/user_details.php) | PHP | 145 | 92 | 37 | 274 |
| [public/php/user_email_change.php](/public/php/user_email_change.php) | PHP | 178 | 107 | 50 | 335 |
| [public/php/user_mailer.php](/public/php/user_mailer.php) | PHP | 36 | 53 | 11 | 100 |
| [public/php/user_source_fav.php](/public/php/user_source_fav.php) | PHP | 62 | 51 | 25 | 138 |
| [public/php/user_tokenauth.php](/public/php/user_tokenauth.php) | PHP | 52 | 50 | 17 | 119 |

[summary](results.md)