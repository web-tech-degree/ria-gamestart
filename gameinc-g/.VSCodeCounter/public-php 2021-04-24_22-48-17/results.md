# Summary

Date : 2021-04-24 22:48:17

Directory d:\Productivity NVMe\__Git\web-tech-degree\ria-gamestart\gameinc-g\public\php

Total : 20 files,  3448 codes, 1958 comments, 1035 blanks, all 6441 lines

[details](details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| PHP | 20 | 3,448 | 1,958 | 1,035 | 6,441 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 20 | 3,448 | 1,958 | 1,035 | 6,441 |

[details](details.md)