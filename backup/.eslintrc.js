// I know, confusing, right?
// Essentially, I am monkey and don"t know how to code cleanly
// To hell with Paradigms
// To hell with coding standards
// Standards make code
// But here at GameStart,
// Code breaks standards

// This is the linter configuration file for ESLint
// Uses Google standard on top of whatever

module.exports = {

	"parser": "@typescript-eslint/parser",

	// Environment
	// https://kangax.github.io/compat-table/es6/
	// https://kangax.github.io/compat-table/es2016plus/
	// https://kangax.github.io/compat-table/esnext/
	"env": {
		"es6": true,
		"browser": true
	},

	// "eslint.validate": [
	// 	"javascript",
	// 	"javascriptreact",
	// 	"typescript",
	// 	"typescriptreact"
	// ],
	"plugins": [
		"react",
		"@typescript-eslint/eslint-plugin"
	],

	// Default standards
	"extends": [
		"google",
		"eslint:recommended",
		"plugin:@typescript-eslint/recommended",
		"plugin:react/recommended"
	],
	

	// Global variables so that we don't get screamed at by ESLint
	"globals": {
		"Atomics": "readonly",
		// Define any custom variables
	},


	// Something to do with JSX for React
	// Also sets ecma version
	// and sets the typescript config so it can locate the project
	"parserOptions": {
		"ecmaFeatures": {
			"jsx": true
		},
		"ecmaVersion": 2018,
		"sourceType": "module",
		"tsconfigRootDir": __dirname,
		"project": "tsconfig.json"
	},

	"ignorePatterns": ['.eslintrc.js'],

	// Our rules
	"rules": {


		// Allow the use of ts-ignore for one liners
		"@typescript-eslint/ban-ts-ignore": "off",

		// Turn off requirement for jsdoc
		"require-jsdoc": [
			"error", {
				"require": {
					"FunctionDeclaration": false,
					"MethodDefinition": false,
					"ClassDeclaration": false,
					"ArrowFunctionExpression": false,
					"FunctionExpression": false
				}
			}
		],

		// ---------------------------------------------------------------------
		// SECTION Styling
		// ---------------------------------------------------------------------
		
		// SECTION Lines
		// Max line length
		"max-len": [
			"error",
			{
				"code": 256
			}
		],

		// Maximum lines of code within any function
		// This will hopefully limit the spaghetti we write
		// and at least untangle it into several, strands of spaghetti
		// instead of one bowl of spaghetti
		"max-lines-per-function": [
			"warn",
			{
				"max": 200,
				"skipBlankLines": true,
				"skipComments": true
			}
		],
		
		// Maximum number of empty lines
		"no-multiple-empty-lines": [
			"warn",
			{
				"max": 5,
				"maxEOF": 0
			}
		],
		// !SECTION Lines

		// SECTION Comments
		// Require line comments to be above the code, not in-line
		"line-comment-position": [
			"error",
			"above"
		],

		// Use // on multi-line comments
		"multiline-comment-style": [
			"error",
			"separate-lines"
		],

		// "no-warning-comments": [
		// 	"warn",
		// ],
		// !SECTION Comments

		// Brace style for if/else statements etc.
		// This essentially requires else/else if statements to be on a new line
		// I don't see this often, but I don't like else if statements 
		// on the same line as the closing curly brace when the code is relatively complex
		"brace-style": [
			"error",
			"stroustrup"
		],

		// Use double quotes. None of 'these'. If you want to use a ' within a string, escape it
		// like a normal person using \
		"quotes": [
			"error",
			"double"
		],

		// Use camelCase variableNames, anything else gives error
		"camelcase": [
			"error"
		],

		// Require semicolons where they are required
		"semi": [
			"error",
			"always"
		],

		// Use tab indentation
		"indent": [
			"error",
			"tab"
		],

		// No tabs also stops ESLint from screaming about tabs in comments
		"no-tabs": 0,

		// Linebreak using Windows \r\n instead of the Unix \n
		"linebreak-style": [
			"error",
			"windows"
		],

		// ---------------------------------------------------------------------
		// !SECTION Styling
		// ---------------------------------------------------------------------

		// Allow classes to be used above/below each other... JavaScript is hoisted
		// and I'm making the exception for classes, though we'll probably also have exceptions
		// for functions
		"no-use-before-define": [
			0, {
				"classes": false
			}
		],
		"@typescript-eslint/no-use-before-define": [
			0, {
				"classes": false
			}
		],

		// ---------------------------------------------------------------------
		// SECTION Error stuff
		// ---------------------------------------------------------------------

		// No endless for loops; use while loops instead.
		// "for-direction": [
		// 	"error"
		// ],

		// Always require === and !== instead of == and !=
		"eqeqeq": [
			"error",
			"always"
		],
		// ---------------------------------------------------------------------
		// !SECTION Error stuff
		// ---------------------------------------------------------------------


		

	}



};